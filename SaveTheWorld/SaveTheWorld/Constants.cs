﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SaveTheWorld
{
    public static class Constants
    {
        public static class DocumentFileTypes
        {
            public const string FILE = "FILE";
            public const string PHOTO = "PHOTO";

            public const string ORIGINAL_IMAGE = "IMG-O";
            public const string BIG_IMAGE = "IMG-B";
            public const string MIDDLE_IMAGE = "IMG-M";
            public const string SMALL_IMAGE = "IMG-S";

            public const string DEFAULT = FILE;

            /// <summary>
            /// Used only in code. Do not use it with procedures!
            /// </summary>
            public const string DOCUMENT = "DOCUMENT";
        }
        public static class DocumentRelationTypes
        {
            public const string CONTRACT = "C";
            public const string WORKFLOW = "WF";
            public const string CONTRACT_INVENTORY = "CI";
            public const string CONTRACT_INTERVENTION_IMPORT = "CII";
            public const string CONTRACT_INVENTORY_SUPPLEMENTS = "CIS";

            // agent
            public const string AGENT_PHOTO = "AP";
            public const string AGENT_DOCUMENTS = "AD";
            public const string AGENT_DOCUMENTS_WEB = "ADW";

            // store
            public const string STORE_CATEGORY = "SC";
            public const string STORE_PRODUCT = "SP";
            public const string STORE_PRODUCT_VARIANT = "SPV";

            // event
            public const string EVENT_ATTACHMENT = "EA";
            public const string EVENT_FEEDBACK = "EF";

            // web
            public const string WEB_BANNER = "BA";

            // client
            public const string CLIENT_DOCUMENTS = "CD";

            // partners
            public const string PARTNER_DOCUMENTS = "PD";

            // commission statements
            public const string COMMISSION_STATEMENTS = "CS";

            // collection cases
            public const string COLLECTION_CASES = "CC";

            //Complaints
            public const string COMPLAINTS = "CM";

            //AMLS
            public const string AML = "AML";

            public const string FUS = "FUS";

            public const string CH = "CH";

        }
        public static class DocumentTypes
        {
            //used
            public const int CONTRACT = 1;
            public const int FUS = 2;
            public const int COMMISSION_STATEMENTS = 4;
            public const int AGENT_CIMASTER = 5;
            public const int OTHERS = 13;
            public const int DIRECTORY = 14;
            public const int USER_PROFILE_DOCUMENT = 15;
            public const int WORK_FLOW = 16;
            public const int INTERVENTION_IMPORT = 17;
            public const int AGENT_PHOTO = 18;
            public const int GALLERY_PICTURE = 19;
            public const int CIMASTER = 20;
            public const int CIMASTER_NOT_ASSIGNED = 21;
            public const int BANNER = 26;
            public const int FUS_INTERVENTION = 27;
            // who uses missing?
            public const int STORE_PHOTO = 25;

            //not used?
            public const int DOWNLOAD = 11;

            public const int FUS_INTERVENTION_AML = 31;
            public const int FUS_INTERVENTION_SOMETHING_MORE = 32;
            public const int FUS_INTERVENTION_STANDARD = 33;
            public const int FUS_INTERVENTION_SERVICE_FAMILIAR = 34;
            public const int FUS_INTERVENTION_NEW = 40;
            public const int FUS_INTERVENTION_PROTOCOL = 58;

            public const int COLLECTION_CASES = 35;

            public const int FUS_INTERVENTION_TEMPLATE_AML = 36;
            public const int FUS_INTERVENTION_TEMPLATE_SOMETHING_MORE = 37;
            public const int FUS_INTERVENTION_TEMPLATE_STANDARD = 38;
            public const int FUS_INTERVENTION_TEMPLATE_SERVICE_FAMILIAR = 39;
            public const int INTERVENTION_DOCUMENT = 53;

            public const int AML = 44;
            public const int CIMASTER_ATTACHMENT = 49;
            public const int FUS_PROTOCOL = 51;

            public const int Logo240x120 = 50;
            public const int IMPORTEDFUS = 52;
            public const int FUS_ATTACHMENT = 54;

            public const int CLIENT_TRANSFER = 55;

            public const int CLIENT_CHANGE = 1054;

            // Agent Documents
            public const int SOS = 6;
            public const int ZL = 9;
            public const int Dodatek = 60;
            public const int Pruvodni_form = 61;
            public const int CP = 62;
            public const int VRT = 63;
            public const int Zmena_cuc = 64;
            public const int Zmena_ve_strukture = 65;
            public const int Samofakturace = 66;
            public const int Vzdelani = 67;
            public const int Zaloha = 68;
            public const int Akviz_zaloha = 69;
            public const int Participace_nadrizenych = 70;
            public const int Ruceni = 71;
            public const int Zaloha_CNB = 72;
            public const int VIP = 73;
            public const int FIX = 74;
            public const int Banker = 75;
        }

        public static class Session
        {
            //login
            public const string UserProfile = "UserProfile";
            public const string NeedPasswordReset = "NeedPasswordReset";
            public const string SendSmsStartTime = "SmsTokenStartTime";
            public const string SendSmsThread = "SmsTokenThread";
            public const string Instructions = "Instructions";
            public const string UserWithUniPassword = "UserWithUniPassword";

            public const string FinDataServer = "FinDataServer";
            public const string SearchObject = "SearchObject";
            public const string CurrentAgent = "CurrentAgent";
            public const string AgentNetwork = "AgentNetwork";
            public const string AccessibleAgents = "AccessibleAgents";
            public const string DataGrid1 = "DataGrid1";
            public const string DataGrid2 = "DataGrid2";
            public const string DataGrid3 = "DataGrid3";
            public const string ControllerAction = "ControllerAction";
            public const string CIMasterID = "CIMasterID";
            public const string EmailInfo = "EmailInfo";
            public const string EmailInfoReality = "EmailInfoReality";
            public const string Cart = "Cart";
            public const string AllCategories = "AllCategories";
            public const string ReportPrintImage = "ReportPrintIMage";

            public const string PublicDocPwd = "PublicDocPwd";
            public const string FileOrder = "FileOrder";

            public const string SmsResetToken = "SmsResetToken";

            public const string DebugDbCalls = "DebugDbCalls";

            public const int SendSmsTimeoutSecs = 20;
            public const int DisabledAuthExpirationTimeout = 24 * 60; // in minutes

            public const string AgentNotSelected = "AgentNotSelected";
            public const string KeepProfile = "KeepProfile";
        }

        public static class Configuration
        {
            public const string FinDataEndPoint = "NetTcpBinding_IService";
            public const string FinDataResCredentials = "WebResourcesCredentials";
            public const string SupportedCultures = "SupportedCultures";
            public const string CurrentCulture = "CurrentCulture";
            public const string WcfPoolSize = "WcfPoolSize";
            public const string WcfPoolConnWaitTimeout = "WcfPoolConnWaitTimeout";
            public const string WcfPoolConnRefreshInterval = "WcfPoolConnRefreshInterval";
            public const string AutoLoginCredentials = "AutoLoginCredentials";
            public const string DecimalPlaceNumber = "DecimalPlaceNumber";
            public const string ExchangeUrl = "ExchangeUrl";
            public const string MailServerUrl = "MailServerUrl";
            public const string MailServerUrlGoogle = "MailServerUrlGoogle";
            public const string PostOffice = "PostOffice";
            public const string SyncEmail = "SyncEmail";
            public const string AppCacheTimeout = "AppCacheTimeout";
            public const string HomepageWorkflowItems = "HomepageWorkflowItems";
            public const int ApplicationID = 4;
        }

        public static class Resources
        {
            public const string ResourceFormat = "Web.{0}.Text";
            public const string ResourceGlobalFormat = "Web.Com.{0}.Text";
            public const string ResourceComFormat = "Com.{0}";

            public const string ResourceGlobalOneFormat = "Web.Com.{0}.One.Text";
            public const string ResourceGlobalFewFormat = "Web.Com.{0}.Few.Text";
            public const string ResourceGlobalOtherFormat = "Web.Com.{0}.Other.Text";
            public const string ResourceGlobalNothingFormat = "Web.Com.{0}.Nothing.Text";

            public const string ResViewPath = "ResViewPath";
            public const string CultureCookie = "Culture";
        }

        public static class ViewData
        {
            public const string ErrorMsg = "ErrorMsg";
            public const string TokenExpired = "TokenExpired";
            public const string Sort = "Sort";
            public const string PageNr = "PageNr";
            public const string PageSize = "PageSize";
            public const string SuccessMsg = "SuccessMsg";
            public const string FUSError = "FUSError";
        }

        public static class Common
        {
            public const int MaxFailedLoginAttempts = 10;
            public const int RowsPerPage = 30;
            public const int MaxFailedAccountCredentialsAttemps = 10;
            public const string FindataWebUrl = "https://www.fincentrum.cz/stranky/predsmluvni-informace/";
            public const string FindataWebZVUrl = "https://www.fincentrum.cz/stranky/predsmluvni-informace-zamestnanecke-vyhody/";
        }

        public static class StringFormat
        {
            public const string ControllerActionGrid = "{0}_{1}_{2}";
            public const string ControllerAction = "{0}_{1}";
        }

        public static class Report
        {
            public const string ReportsPath = @"~/Reports";
        }

        public static class AccessibleRecords
        {
            public const string Customer_Contracts = "Customer.Contracts";
            public const string Customer_Clients_C = "Customer.Clients.C";
            public const string Customer_Clients_P = "Customer.Clients.P";
            public const string Agent_AgentNetwork = "Agent.AgentStructure";
        }

        public static class CampaignTypes
        {
            public const int ClientsHypoFix = 1;
            public const int ClientsWithoutInvestment = 2;
            public const int SleepingClient = 3;
            public const int ClientsWithLI = 5;
            public const int HypoClientsWithoutLI = 6;
        }

        public static class TicketClasses
        {
            public const string AgentObjection = "AO";
            public const string AcquisitionForms = "AF";
            public const string ContractInventory = "CI";
            public const string CollectionCase = "CC";
            public const string PartnerObjection = "PO";
            public const string PartnerObjectionFUS = "POFUS";
            public const string PartnerObjectionFI = "POFI";
            public const string PartnerObjectionInternalAudit = "POIA";
            public const string ClientNotExists = "RECN";
            public const string ClientExists = "RECE";
            public const string Order = "OR";
            public const string BusinessCard = "BS";
            public const string Reservation = "RE";
            public const string ContractNote = "CN";
            public const string FUS = "EF";
        }

        public static class Functions
        {
            public const string Store_RestrictedToOffice = "Store.RestrictedToOfficeMember";
        }

        public static class HttpRuntimeCache
        {
            public const string AgentQRCodeImage = "AgentQRCodeImage";
            public const string ClientQRCodeImage = "ClientQRCodeImage";
            public static TimeSpan QRCodeImageExpiration = TimeSpan.FromMinutes(10);
        }
        public static class HttpContextCache
        {
            public const string StoreCategories = "StoreCategories";
        }

        /// <summary>
        /// Returns MessageTypeCode for LogWritter.
        /// </summary>
        public enum MessageType
        {
            Information,
            Warning,
            Error,
        }

        public static class ContractTrxAgentTypes
        {
            public const int REALIZATOR = 4;
        }

        public static class TransactionTypes
        {
            public const string ZA = "ZA";
        }

        public static class ContractTypes
        {
            public const string NewContract = "NS";
            public const string ChangeContract = "ZS";
        }

        public static class TransformationCoefficientsContractTypes
        {
            public const int OneTime = 1;
            public const int Regular = 2;
            public const int Undefined = 3;
        }

        public static class Shipping
        {
            public const int Personal = 1;
            public const int PPL = 2;
        }

        public static class ProductCategory
        {
            public const int ITProducts = 247;
            public const int Web = 13501;
        }

        public static class Acquisitions
        {

            public const int Idnes_DS = 6;
            public const int Hypo_DS = 7;

            public const int FinHit = 9;

            public const int TopReality = 13;
            public const int Mesec = 14;
            public const int TnCz = 15;
            public const int Usetreno = 16;
            public const int Denik = 17;
            public const int FinanceCz = 18;
            public const int CallCentrum = 19;
            public const int TotalMoney = 20;

            public const int CSOB = 21;

            public const int HypotekaRoka = 23;

            public const int FincentrumCom = 29;
            public const int FincentrumComAgentWeb = 30;
            public const int RecruimentFincentrumCom = 31;
            public const int RecruimentFincentrumComAgentWeb = 32;

            public const int Newsletter = 34;
            public const int NewsletterAgentWeb = 35;

            public const int UnassignedClients = 37;
        }

        public static class CustomUserSettings
        {
            public static readonly string WebHomeAgentPanelVisible = "Web.Home.AgentPanel.Visible";
        }

        public static class Attributes
        {
            public static readonly string Agent_Agents_AgreedToPublishDocuments = "Agent.Agents.AgreedToPublishDocuments";
            public static string Agent_ForwardAdvancesDate = "Agent.Agents.ForwardAdvancesDate";
            public const string Client_Clients_Nationality = "Client.Clients.Nationality";
            public const string Client_Clients_Profession = "Client.Clients.Profession";

            public const string Client_Clients_MaritalStatus = "Client.Clients.MaritalStatus";
            public const string Client_Clients_ResidentUSA = "Client.Clients.ResidentUSA";
            public const string Client_Clients_DocumentPOCode = "Client.Clients.DocumentPOCode";
            public const string Client_Companies_ResidentUSA = "Client.Companies.ResidentUSA";
            public const string Client_Companies_DocumentPOCode = "Client.Companies.DocumentPOCode";

            public const string Client_Clients_Released = "Client.Clients.Released";
            public const string Client_Clients_DocumentFrom = "Client.Clients.DocumentFrom";
            public const string Client_Clients_DocumentTo = "Client.Clients.DocumentTo";

            public const string Agent_Agents_ShowLiability = "Agent.Agents.ShowLiability";
            public const string Agent_Agents_AcquisitionLeadsNetwork = "Agent.Agents.AcquisitionLeadsNetwork";
            public static string Agent_Platinum = "Agent.Agent.Platinum";
            public static string Agent_Agent_IsAssignLeadEnable = "Agent.Agent.IsAssignLeadEnable";
            public static string Agent_Agents_UserCode = "Agent.Agents.UserCode";
            public static string Agent_Agents_PasswordPhrase = "Agent.Agents.PasswordPhrase";

            public static string Agent_Escalation_Unsubscribe = "Agent.Escalation.Unsubscribe";

            public static int MaritalStatus = 1109;

        }

        public static class AttributeGroups
        {
            public const int Agents = 100;
            public const int Clients_Person = 101;
            public const int Transactions = 102;
            public const int Clients_Company = 103;
            public const int Agents_Details = 104;
            public const int Templates = 105;
            public const int Leads = 106;
            public const int Contracts = 107;
            public const int Agents_Hidden = 108;
        }

        public static class OAuthClients
        {
            public const string Superpoistenie = "SPSK";
            public const string eSanon = "eSanon";
            public const string Generali = "Generali";
            public const string Learn2Grow = "Learn2Grow";
            public const string euroap = "euroap";
            public const string Allianz = "Allianz";
            public const string Nefa = "Nefa";
            public const string SugarCRM = "SugarCRM";
            public const string EuroapFCTRWeb = "EuroapFCTRWeb"; // fincentrum.com
            public const string Fingram = "Fingram";
            public const string FinCalc = "FinCalc";
            public const string Novis = "Novis";
            public const string Blcktg = "Blcktg";
            public const string Andromeda = "Andromeda";
            public const string FinPlanSSO = "FinPlanSSO";
        }

        public static class PartnerEntityType
        {
            public const int PZ = 1;
            public const int ZAPLAC = 2;
            public const int SKODY = 3;

            public const int STAV_ZMLUVY = 4;
            public const int DOVOD_STORNA = 5;
            public const int FREKV_PLATENIA = 6;

            public const int INTERVENCE_METLIFE = 7;

            public const int KOOP_CEKARNA = 8;
            public const int KOOP_CEKSTR = 9;
            public const int KOOP_DLUHY = 10;
            public const int KOOP_NEDOR = 11;
            public const int KOOP_NI = 12;
            public const int KOOP_PSTR = 13;

            public const int ALLIANZCZ_INTERVENCE = 14;
            public const int ALLIANZCZ_DLUH = 15;
            public const int AXA_INTERVENCE = 16;
        }

        // 2015-05-18 - DT - tento seznam je také v BL.FinData.Common.Constants
        public static class FUSStatuses
        {
            public const int InProgress = 1;
            public const int Closed = 2;
            public const int Released = 3;
            public const int ClosedAMLRequired = 4;
            public const int ClosedConverted = 5;
            public const int ClosedUnrealised = 6;
            public const int ClosedConvertedBO = 7;
            public const int ClosedConvertedAmlRequired = 8;
            public const int InProgressWaitForSMS = 9;
        }

        public static class FUSTypes
        {
            public const string STANDARDNI = "STD";
            public const string CSOB = "CSOB";
        }

        public static class FUSCZTypes
        {
            public const string STANDARDNI = "STD";
            public const string ZV = "ZV";
            public const string ADK = "ADK";
            public const string SMS = "SMS";
            public const string SMSZV = "SMSZV";

        }
        public static class FUSPDFTypes
        {
            public const string Classic = "Classic";
            public const string Draft = "Draft";
            public const string Double = "Double";
            public const string FUSWithAML = "FUSWithAML";
            public const string FUSWithAttachment = "FUSWithAttachment";
            public const string AML = "AML";
            public const string SMSFUS = "SMSFUS";
        }
        public static class FUSListOptions
        {
            public const string WASNTSPECIFIED = "WASNTSPECIFIED";

            public const string AMONTHLY = "AMONTHLY";
            public const string AQAERTERLY = "AQAERTERLY";
            public const string AHALFYEAR = "AHALFYEAR";
            public const string AYEAR = "AYEAR";
            public const string AONCE = "AONCE";
            public const string AFINAL = "AFINAL";
            public const string AHYPO = "AHYPO";

            public const string FREE = "FREE";
            public const string MARRIED = "MARRIED";
            public const string DIVORCED = "DIVORCED";
            public const string WIDOW = "WIDOW";
            public const string REGISTERED = "REGISTERED";

            public const string PERSONALLY = "PERSONALLY";
            public const string ATTORNEY = "ATTORNEY";

            public const string FOOP = "FOOP";
            public const string FOPASS = "FOPASS";


            public const string CZCZK = "CZCZK";
            public const string CZEUR = "CZEUR";
            public const string CZUSD = "CZUSD";
            public const string CZJPY = "CZJPY";
            public const string CZGBP = "CZGBP";
            public const string CZHUF = "CZHUF";
        }
        public static class FUSListTypes
        {
            public const int STATEMENT = 1;
            public const int GENDER = 2;
            public const int PVREQUIRMENTS = 3;
            public const int EXPCLIENT = 4;
            public const int NEEDFINANCE = 5;
            public const int PUREQUIRMENTS = 6;
            public const int PURPOSELOAN = 7;
            public const int ENSURING = 8;
            public const int KTREQUIRMENTS = 9;
            public const int EXPINVESTOR = 10;
            public const int INVESTQUEST = 11;
            public const int QEIFINANCIALEDU = 12;
            public const int QEIWORKPOSITION = 13;
            public const int QEIRISKEDUCATION = 14;
            public const int QEIEXPINVESTINST = 15;
            public const int QEIEXPINVEST = 16;
            public const int QEIFREQCLOSINGDEAL = 17;
            public const int FOCUSON = 18;
            public const int DDSEXP = 19;
            public const int SDSEXP = 21;

            public const int YESNONULL = 22;
            public const int STATUTORYREG = 23;
            public const int DOCUMENTFO = 24;
            public const int DOCUMENTPO = 25;

            public const int INCOMEREGULAR = 26;
            public const int INCOMEIRREGULAR = 27;
            public const int SOURCEREGULAR = 28;
            public const int SOURCEIRREGULAR = 29;
            public const int BUYSELL = 30;
            public const int AMOUNTTYPE = 31;
            public const int CURRENCY = 32;
            public const int SERVICEMEET = 33;

            public const int SECTORS = 34;
            public const int FINANCIALTOOLS = 35;
            public const int PERIODAMLOUNT = 36;
            public const int MARITIALSTATUS = 37;

            public const int FUSTYPES = 38;
            //IncomeLevelRegularOptions
        }

        public static class FUSPage
        {
            public const string Agent = "Agent";
            public const string OverallInfo = "OverallInfo";
            public const string Announcement = "Announcement";
            public const string Client = "Client";
            public const string Partner = "Partner";
            public const string Childs = "Childs";
            //public const string GeneralPurpose = "GeneralPurpose";
            public const string FinancialIntermediations = "FinancialIntermediations";
            public const string ProposedSolution = "ProposedSolution";
            public const string FinalStatementSignatures = "FinalStatementSignatures";
            public const string Summary = "Summary";
            public const string Closed = "Closed"; //Not Used
            public const string CSOB = "CSOB"; // #4436
        }

        public static class AgentRegistration
        {
            public const string DDS = "DDS";
            public const string KT = "KT";
            public const string NBS = "NBS";
            public const string PaZ = "PaZ";
            public const string SDS = "SDS";
            public const string Uv = "Uv";
            public const string Vkl = "Vkl";


        }

        public static class AgentRegistrationCZ
        {
            public const string PPZFC = "PPZFC";
            public const string VZ = "VZ";
            public const string SU = "SU";
            public const string DEP = "DEP";
            public const string DOP = "DOP";

        }
        public static class ValidateType
        {
            public const string Error = "Error";

            public const string MustBeTrue = "MustBeTrue";
            public const string MustBeFalse = "MustBeFalse";
            public const string AtLeastOne = "AtleastOne";
            public const string RegExp = "RegExp";
            public const string MaxLength = "MaxLength";
        }
        public static class ContractFUSTYPE
        {
            public const int PVPU = 1;
            public const int KT = 2;
            public const int DDSSDS = 3;
        }
        public static class ContractFUSSectionTYPE
        {
            public const string SECTOR_PV = "SECTOR_PV";
            public const string SECTOR_PU = "SECTOR_PU";
            public const string SECTOR_PZP = "SECTOR_PZP";
            public const string SECTOR_KT = "SECTOR_KT";
            public const string SECTOR_DDS = "SECTOR_DDS";
            public const string SECTOR_SDS = "SECTOR_SDS";

        }
        public static class DocumentFOType
        {
            public const string FOOP = "FOOP";
            public const string FOPASS = "FOPASS";
        }
        public static class AclFunctionGroup
        {
            public static List<string> Account
            {
                get
                {
                    return new List<string>() {
                "Web.Account.Index"
                ,"Web.Account.Decay"
                ,"Web.Account.Decay.Network"
                ,"Web.Account.AnnualSettlement"
                ,"Web.Account.AnnualSettlement.Network"
                ,"Web.Account.Billing"
                ,"Web.Account.Billing.Network"
				//,"Web.Account.Biz"
				//,"Web.Account.Biz.Network"
				//,"Web.Account.Chart"
				//,"Web.Account.Chart.Network"
				,"Web.Account.DecaySelf"
                ,"Web.Account.DecaySelf.Network"
                ,"Web.Account.DirectProvision"
				//,"Web.Account.HomepageProduction"
				,"Web.Account.Index.Network"
                ,"Web.Account.Production"
                ,"Web.Account.ProvisionSchedule"
                ,"Web.Account.Reality"
                ,"Web.Account.SectionBDetail"
                ,"Web.Account.SectionBDetail.Network"
                ,"Web.Account.SectionCDetail"
                ,"Web.Account.SectionCDetail.AdvanceProvision"
                ,"Web.Account.SectionCDetail.AdvanceProvision.Network"
                ,"Web.Account.SectionCDetail.CurrentProvision"
                ,"Web.Account.SectionCDetail.CurrentProvision.Network"
                ,"Web.Account.SectionCDetail.Express"
                ,"Web.Account.SectionCDetail.Express.Network"
                ,"Web.Account.SectionCDetail.Invoices"
                ,"Web.Account.SectionCDetail.Invoices.Network"
                ,"Web.Account.SectionCDetail.OfficeContribution"
                ,"Web.Account.SectionCDetail.OfficeContribution.Network"
                ,"Web.Account.SectionCDetail.PastProvision"
                ,"Web.Account.SectionCDetail.PastProvision.Network"
                ,"Web.Account.SectionCDetail.PayedCancelFund"
                ,"Web.Account.SectionCDetail.PayedCancelFund.Network"
                ,"Web.Account.SectionCDetail.Provision"
                ,"Web.Account.SectionCDetail.Provision.Network"
                ,"Web.Account.SectionCDetail.RecommendRevenue"
                ,"Web.Account.SectionCDetail.RecommendRevenue.Network"
                ,"Web.Account.SectionCDetail.ReturnedAdvanceProvision"
                ,"Web.Account.SectionCDetail.ReturnedAdvanceProvision.Network"
                ,"Web.Account.SectionCDetail.TurnoverProvision"
                ,"Web.Account.SectionCDetail.TurnoverProvision.Network"
                ,"Web.Account.SectionDDetail"
                ,"Web.Account.SectionDDetail.Others"
                ,"Web.Account.SectionDDetail.Others.Network"
                ,"Web.Account.SectionDDetail.Rewards"
                ,"Web.Account.SectionDDetail.Rewards.Network"
                ,"Web.Account.SectionE"
                ,"Web.Account.SectionEDetail"
                ,"Web.Account.SectionSingleContract"
                ,"Web.Account.SectionSingleContract.Network"
                ,"Web.Account.SelectAction"
                ,"Web.Account.Statistics"
                ,"Web.Account.Settings.ExpressPassword"
            };
                }
            }
        }
        public static class StatementFunctionGroup
        {
            public static List<string> Statement
            {
                get
                {
                    return new List<string>() {
                "Web.Statement.Decay"
                ,"Web.Statement.Decay.Network"
                ,"Web.Statement.DecaySelf"
                ,"Web.Statement.DecaySelf.Network"
                ,"Web.Statement.DisallowCommissionContractDetail"
                ,"Web.Statement.HideCommissionFilter"
                ,"Web.Statement.Index"
                ,"Web.Statement.Index.Network"
                ,"Web.Statement.SectionBDetail"
                ,"Web.Statement.SectionBDetail.Network"
                ,"Web.Statement.SectionCDetail"
                ,"Web.Statement.SectionCDetail.AdvanceProvision"
                ,"Web.Statement.SectionCDetail.AdvanceProvision.Network"
                ,"Web.Statement.SectionCDetail.Bonus"
                ,"Web.Statement.SectionCDetail.Bonus.Network"
                ,"Web.Statement.SectionCDetail.CurrentProvision"
                ,"Web.Statement.SectionCDetail.CurrentProvision.Network"
                ,"Web.Statement.SectionCDetail.Express"
                ,"Web.Statement.SectionCDetail.Express.Network"
                ,"Web.Statement.SectionCDetail.Invoices"
                ,"Web.Statement.SectionCDetail.Invoices.Network"
                ,"Web.Statement.SectionCDetail.Network"
                ,"Web.Statement.SectionCDetail.OfficeContribution"
                ,"Web.Statement.SectionCDetail.OfficeContribution.Network"
                ,"Web.Statement.SectionCDetail.PastProvision"
                ,"Web.Statement.SectionCDetail.PastProvision.Network"
                ,"Web.Statement.SectionCDetail.PayedCancelFund"
                ,"Web.Statement.SectionCDetail.PayedCancelFund.Network"
                ,"Web.Statement.SectionCDetail.Provision"
                ,"Web.Statement.SectionCDetail.Provision.Network"
                ,"Web.Statement.SectionCDetail.RecommendRevenue"
                ,"Web.Statement.SectionCDetail.RecommendRevenue.Network"
                ,"Web.Statement.SectionCDetail.ReturnedAdvanceProvision"
                ,"Web.Statement.SectionCDetail.ReturnedAdvanceProvision.Network"
                ,"Web.Statement.SectionCDetail.TiperReward"
                ,"Web.Statement.SectionCDetail.TiperReward.Network"
                ,"Web.Statement.SectionCDetail.TurnoverProvision"
                ,"Web.Statement.SectionCDetail.TurnoverProvision.Network"
                ,"Web.Statement.SectionDDetail"
                ,"Web.Statement.SectionDDetail.Network"
                ,"Web.Statement.SectionDDetail.Others"
                ,"Web.Statement.SectionDDetail.Others.Network"
                ,"Web.Statement.SectionDDetail.Rewards"
                ,"Web.Statement.SectionDDetail.Rewards.Network"
                ,"Web.Statement.Settings.ExpressPassword"
            };
                }
            }
        }

        public static class PersonTypes
        {
            public const string FO = "FO";
            public const string PO = "PO";

        }

        public static class TransferErrorTypes
        {
            public const string SUCESSFULL = "SA";
            public const string FAILED = "FA";
            public const string DIFFERENTCONTRACT = "DC";
            public const string INDIRECTCONTRACT = "IC";
            public const string SUCCESSFULLBUTNOTACTIVE = "NA";
        }

        public static class TransferErrors
        {
            public const int NotInVP = 1;
            public const int MissingDocument = 2;
            public const int Before2011 = 3;
            public const int MissingClientPhoneEmail = 4;
            public const int MissingClientEmail = 5;
            public const int NextProvision = 6;
            public const int Unknow = 7;
            public const int SucessFullContract = 8;
            public const int TransferedBaseOnContract = 9;
            public const int ContractIndirect = 10;

        }

        public static class TransferClientErrors
        {
            public const int NotFO = 1;
            public const int MissingName = 2;
            public const int MissingLastName = 3;
            public const int InvalidRC = 4;
            public const int InvalidEmail = 5;
            public const int InvalidPhone = 6;
            public const int MissingContract = 7;
            public const int SucessFullClient = 8;
            public const int DuplicateEmail = 9;
            public const int MissingPhone = 10;
            public const int DuplicateSanonPhone = 11;
            public const int DuplicateSanonEmail = 12;
            public const int NotActiveClient = 13;
        }

        public static class ObligationTypes
        {
            public const string Debt = "Debt";
            public const string Liability = "Liability";
            public const string CancellationFund = "CancellationFund";
            public const string DebtAfterDueDate = "DebtAfterDueDate";
            public const string DebtBeforeDueDate = "DebtBeforeDueDate";
        }

        public static class CancellationFundType
        {
            public const string ActualCancelFund = "ActualCancelFund";
            public const string CancelFund = "CancelFund";
        }

        public static class AgentPositions
        {
            public const int Zemsky = 2;
            public const int Oblastni = 3;
            public const int SeniorPoradce = 16;
            public const int Regionalni = 18;
            public const int SamostatnyOsobniPoradce = 19;
            public const int TOPPoradce = 20;
            public const int ExkluzivniPoradce = 23;
            public const int Manazer = 24;
        }

        public static class FUSCZListTypes
        {
            public const int DOCUMENTFO = 100;
            public const int MARITIALSTATUS = 101;
            public const int STATEMENT = 102;
            public const int QEInvestGoal = 103;
            public const int QEInvestTools = 104;
            public const int QEInvestHorizont = 105;
            public const int QEInvestExperience = 106;
            public const int QEInvestRisk = 107;
            public const int INVESTQUEST = 108;
            public const int QEIFINANCIALEDU = 109;
            public const int QEIWORKPOSITION = 110;
            public const int QEIRISKEDUCATION = 111;
            public const int QEIEXPINVESTINST = 112;
            public const int QEIEXPINVEST = 113;
            public const int QEIFREQCLOSINGDEAL = 114;
            public const int EXPINVESTOR = 115;
            public const int SECTORS = 116;
            public const int BUYSELL = 117;
            public const int AMOUNTTYPE = 118;
            public const int CURRENCY = 119;
            public const int FINANCIALTOOLS = 120;

            public const int DPPROFIL = 122;

            public const int DPSPROFIL = 123;

            public const int INCOMEREGULAR = 124;
            public const int INCOMEIRREGULAR = 125;
            public const int SOURCEREGULAR = 126;
            public const int SOURCEIRREGULAR = 127;

            public const int FUSTYPES = 128;

            public const int STATUTORYREG = 129;

            public const int PERIODAMLOUNT = 130;

            public const int QEIEDUCATION = 150;
            public const int QEIINVESTJOB = 151;
            public const int QEIINVESTYEARS = 152;
            public const int QEIFREQDEAL = 153;

            public const int PARTNERCOUNT = 155;

        }

        public sealed class FUSCZPages
        {
            public const string ClientBasic = "ClientBasic";
            public const string Agent = "Agent";
            public const string OverallInfo = "OverallInfo";
            public const string Announcement = "Announcement";
            public const string Client = "Client";
            public const string Partner = "Partner";
            public const string Childs = "Childs";
            public const string FinancialIntermediations = "FinancialIntermediations";
            public const string ProposedSolution = "ProposedSolution";
            public const string FinalStatementSignatures = "FinalStatementSignatures";
            public const string Summary = "Summary";
            //public const string Closed = "Closed"; //Not Used
        }

        public static class ContractFUSCZSectionTYPE
        {
            ////public const string SECTOR_SAVING_OTHERS = "SECTOR_SAVING_OTHERS";
            //public const string SECTOR_LOAN = "SECTOR_LOAN";
            ////public const string SECTOR_INSURANCELIVE = "SECTOR_INSURANCELIVE";
            ////public const string SECTOR_INSURANCENONLIVE = "SECTOR_INSURANCENONLIVE";
            public const string SECTOR_INSURANCE = "SECTOR_INSURANCE";
            public const string SECTOR_INVESTMENT = "SECTOR_INVESTMENT";
            public const string SECTOR_OTHER = "SECTOR_OTHER";
            public const string SECTOR_SAVING_DSDPS = "SECTOR_SAVING_DSDPS";
            public const string SECTOR_SAVING_BUILD = "SECTOR_SAVING_BUILD";

            public const string SECTOR_LOAN_CONSUMER = "SECTOR_LOAN_CONSUMER";
            public const string SECTOR_LOAN_OTHER = "SECTOR_LOAN_OTHER";
        }

        public static class AgentCZRegistration
        {
            public const string PPZFC = "PPZFC";
            public const string VZ = "VZ";
            public const string DOP = "DOP";
            public const string DEP = "PaZ";
        }

        public static class InteractiveStatuses
        {
            public const string Reserved = "MR";
            public const string Interest = "MZ";
            public const string Structure = "SR";
        }

        public static class AssignStatus
        {
            public const int CanAssign = 1;
            public const int CannotAssign = 2;
        }

        public static class Mandates_CZ
        {
            public const int INSURANCE_PRODUCTS = 1;
            public const int INVESTMENT_PRODUCTS_INTERMEDIATION = 2;
            public const int INVESTMENT_PRODUCTS_INTERMEDIATION_AND_ADVICE = 3;
            public const int CONSUMER_LOANS = 10;

            public const int CREDIT_PRODUCTS = 4;
            public const int BUILDING_SAVINGS = 5;
            public const int PENSTION_SAVINGS = 14;
        }

        public static class NumberFormat
        {
            public const string IntFormat = "#,###";
            public const string IntZeroFormat = "#,##0";
        }

        public static class ContactTypes
        {
            public const string BIZ = "BIZ";
            public const string GEN = "GEN";
            public const string HOME = "HOME";
            public const string OTHER = "OTHER";
            public const string SANON = "SANON";
        }

        public static class PointsTypes
        {
            public const string BB = "BB";
            public const string ZB = "ZB";
        }

        public static class SelectedSections
        {
            public const string ProdukceBB = "ProdukceBB";
            public const string StrukturaProdukce = "StrukturaProdukce";
            public const string NaboryAProplacenost = "NaboryAProplacenost";
            public const string SpolupracovniciProdukce = "SpolupracovniciProdukce";
            public const string NaborPoradcuDetail = "NaborPoradcuDetail";
            public const string SpolupracovniciStorno = "SpolupracovniciStorno";
            public const string TopPoradci = "TopPoradci";
            public const string PoradciNad400BB = "PoradciNad400BB";
            public const string Zpravy = "Zpravy";
            public const string ProvizeProplacenost = "ProvizeProplacenost";
            public const string Hypo = "Hypo";
            public const string AUM = "AUM";
        }

        public static class OauthUdalostiRole
        {
            public const string User = "USER";
            public const string Manager = "MANAGER";
            public const string Centrala = "CENTRALA";
        }

        public static class OauthGroupCode
        {
            public const string Agent = "AGENT";
            public const string Manager = "MANAGER";
        }

        public static class SMSeFUSStatusCodes
        {
            public const string OK = "OK";
            public const string Error = "ERROR";
            public const string NotValid = "NOTVALID";
            public const string Expired = "EXPIRED";
            public const string Closed = "CLOSED";
            public const string EmailFailed = "EMAIL";
            public const string ManyAttempts = "MANYATTEMPTS";
            public const string FUSWasUpdated = "FUSWasUpdated";
        }

        //public static class Zeteo
        //{
        //    //ZETEO
        //    public static readonly string ProjectName = ConfigurationManager.AppSettings["ProjectName"];

        //    public static readonly string AdminLogin = ConfigurationManager.AppSettings["AdminLogin"];
        //    public static readonly string AdminPassword = ConfigurationManager.AppSettings["AdminPassword"];

        //    public static readonly string AgentLogin = ConfigurationManager.AppSettings["AgentLogin"];
        //    public static readonly string AgentPassword = ConfigurationManager.AppSettings["AgentPassword"];

        //    public static readonly string ZeteoAuthKey = ConfigurationManager.AppSettings["ZeteoAuthKey"];
        //    public static readonly string ZeteoURL = ConfigurationManager.AppSettings["ZeteoURL"];

        //    public const int Borovicka = 400004394;
        //    public const int Kovar = 400000895;
        //}

        public static class OrderStatus
        {
            public const string NEW = "N";
            public const string AUTORIZED = "A";
        }

        public static class ITProducts
        {
            public const string Deskless = "IT0000003";
            public const string Unlimited = "IT0000001";
            public const string UnlimitedUpgrade = "IT0000002";
            public const string Web = "IT0000004";
        }

        public static class OAuthClientTypeCodes
        {
            public const string ALL = "All";
            public const string SSO = "SSO";
        }
    }

    public class SealedConstants
    {
        public sealed class ErrorTypes
        {
            public static readonly ErrorTypes NullableFUS = new ErrorTypes("NullableFUS");
            public static readonly ErrorTypes FUSIsClosed = new ErrorTypes("FUSIsClosed");
            public static readonly ErrorTypes PDFIsNotValid = new ErrorTypes("PDFIsNotValid");
            public static readonly ErrorTypes EmptyFUSType = new ErrorTypes("EmptyFUSType");
            public static readonly ErrorTypes AMLIsClosed = new ErrorTypes("AMLIsClosed");

            private ErrorTypes(string value)
            {
                Value = value;
            }

            public string Value { get; private set; }
        }
        public sealed class SuccessTypes
        {
            public static readonly SuccessTypes Delete = new SuccessTypes("Deleted");
            public static readonly SuccessTypes Saved = new SuccessTypes("Save");

            private SuccessTypes(string value)
            {
                Value = value;
            }

            public string Value { get; private set; }
        }

        public sealed class PersonType
        {
            public static readonly PersonType Individual = new PersonType("FO");
            public static readonly PersonType Corporation = new PersonType("PO");
            public static readonly PersonType IndividualBusinessman = new PersonType("FOP");

            private PersonType(string value)
            {
                Value = value;
            }
            public string Value { get; private set; }
        }

        public sealed class FUSCZType
        {
            public static readonly FUSCZType Standard = new FUSCZType("Standard");
            public static readonly FUSCZType ZV = new FUSCZType("ZV");

            private FUSCZType(string value)
            {
                Value = value;
            }
            public string Value { get; private set; }
        }


    }


}