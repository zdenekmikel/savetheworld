﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SaveTheWorld.Extensions
{
    public class FAChBValue
    {
        public string Name { get; set; }
        public string TrueValue { get; set; }
        public string FUSName { get; set; }

        public FAChBValue(string name, string trueValue, string fUSName = null)
        {
            Name = name;
            TrueValue = trueValue;
            FUSName = fUSName;
        }
    }

    public class FASelectValue
    {
        public string Name { get; set; }
        public Dictionary<string, string> CodeList { get; set; }
        public string FUSName { get; set; }

        public FASelectValue(string name, Dictionary<string, string> codeList, string fUSName = null)
        {
            Name = name;
            CodeList = codeList;
            FUSName = fUSName;
        }
    }


    public class FATextValue
    {
        public string Name { get; set; }
        public string FUSName { get; set; }

        public FATextValue(string name, string fUSName = null)
        {
            Name = name;
            FUSName = fUSName;
        }
    }



    public static class PDFExtensions
    {
        //public static BaseFont getBaseFont()
        //{
        //	FontFactory.RegisterDirectories();
        //	Font fontNormal = new Font(FontFactory.GetFont("Arial", BaseFont.IDENTITY_H, BaseFont.EMBEDDED)); ;
        //	return fontNormal.BaseFont;
        //}
        //Set Fields
        public static void MySetField(this AcroFields AF, string name, string value)
        {
            AF.SetField(name, value);
        }

        public static void MySetField(this AcroFields AF, FAChBValue fav, bool? value)
        {
            if (value.HasValue && value == true)
                AF.SetField(fav.Name, fav.TrueValue);
        }

        /// <summary>2015-04-10 - DT - menší overload</summary>
        /// <param name="AF"></param>
        /// <param name="fav"></param>
        /// <param name="value"></param>
        public static void MySetField(this AcroFields AF, FASelectValue fav, bool? value)
        {
            AF.MySetField(fav, value.HasValue ? value.Value.ToString() : false.ToString());
        }

        public static void MySetField(this AcroFields AF, FASelectValue fav, string value)
        {
            if (value == null)
                return;
            if (fav.CodeList == null || fav.CodeList.Count == 0)
                return;

            string code = fav.CodeList.Where(p => p.Key == value).Select(p => p.Value).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(code))
            {
                AF.SetField(fav.Name, code);
            }
        }

        public static void MySetField(this AcroFields AF, FATextValue fav, string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                //AF.SetFieldProperty(fav.Name, "textfont", getBaseFont(), null);
                AF.SetField(fav.Name, value);
            }
        }

        public static MemoryStream LockAcroFieldPdfForms(byte[] sourceFiles)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                if (sourceFiles != null && sourceFiles.Length > 0)
                {
                    byte[] tempByte = null;
                    var pdfTemplate2 = new PdfReader(sourceFiles);
                    var stamper = new PdfStamper(pdfTemplate2, ms, PdfWriter.VERSION_1_7, true);

                    var names = new string[stamper.AcroFields.Fields.Keys.Count];
                    stamper.AcroFields.Fields.Keys.CopyTo(names, 0);
                    foreach (string name in names)
                    {
                        stamper.AcroFields.SetFieldProperty(name, "setfflags", PdfFormField.FF_READ_ONLY, null);
                    }
                    stamper.Close();
                }
                return ms;
            }
            return null;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region MergePdfFormFiles
        /// <summary>
        /// 2015-05-15 - DT - narazil jsem v této metodě na nekonečnou smyčku, proto jsem ji změnil
        /// <para>while(f < sourceFiles.NullableCount()) ale f-ko nikdy nebylo inkrementováno, protože soubor byl prazdný</para>
        /// </summary>
        /// <param name="sourceFiles"></param>
        /// <returns></returns>
        public static MemoryStream MergePdfFormFiles(List<byte[]> sourceFiles)
        {
            if (sourceFiles == null)
                return null;

            using (var ms = new MemoryStream())
            {
                PdfCopyFields copy = new PdfCopyFields(ms);
                copy.Writer.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                int pageOffset = 0;

                for (int i = 0; i < sourceFiles.Count; ++i)
                {
                    var pole = sourceFiles[i];
                    if (pole == null || pole.Length == 0) continue;

                    byte[] tempByte = null;
                    using (var msTemp = new MemoryStream())
                    {
                        var tmpReader = new PdfReader(pole);

                        using (var stamper = new PdfStamper(tmpReader, msTemp, PdfWriter.VERSION_1_7, true))
                        {
                            var names = new string[stamper.AcroFields.Fields.Keys.Count];
                            stamper.AcroFields.Fields.Keys.CopyTo(names, 0);
                            foreach (string name in names)
                            {
                                stamper.AcroFields.SetFieldProperty(name, "setfflags", PdfFormField.FF_READ_ONLY, null);
                                stamper.AcroFields.RenameField(name, name + "_file" + i.ToString());
                            }
                        }
                        tempByte = msTemp.ToArray();

                        tmpReader.Close();
                    }

                    PdfReader reader = new PdfReader(tempByte);
                    copy.AddDocument(reader);

                    pageOffset += reader.NumberOfPages;
                }

                copy.Close();

                var fileMemoryStrem = new MemoryStream();

                var pdfReader = new PdfReader(ms.ToArray());

                using (var pdfStamper = new PdfStamper(pdfReader, fileMemoryStrem, '4'))
                {
                    pdfStamper.FormFlattening = true;
                }

                return fileMemoryStrem;
            }
        }

        public static byte[] LockAndDisablePDFPrint(byte[] FinalPdfMemory)
        {
            return EncryptPdfAndSetPermissions(FinalPdfMemory, null, 0);
        }

        private static byte[] EncryptPdfAndSetPermissions(byte[] FinalPdfMemory, string password, int permission)
        {
            using (MemoryStream input = new MemoryStream(FinalPdfMemory))
            {
                using (MemoryStream output = new MemoryStream())
                {
                    PdfReader reader = new PdfReader(input);
                    PdfEncryptor.Encrypt(reader, output, true, password, "kT5aJd5M6a1", permission); //ownerPassword is password for unlock PDF in any pdf viewer
                    return output.ToArray();
                }
            }
        }

        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
		public static MemoryStream MergePdfForms(List<byte[]> files)
		{
			files = files.Where(p => p != null && p.Length > 0).ToList();
			
			if (files.Count > 1)
			{
				string[] names;
				PdfStamper stamper;
				MemoryStream msTemp = null;
				PdfReader pdfTemplate = null;
				PdfReader pdfFile;
				Document doc;
				PdfWriter pCopy;
				MemoryStream msOutput = new MemoryStream();

				pdfFile = new PdfReader(files[0]);

				doc = new Document();
				pCopy = new PdfSmartCopy(doc, msOutput);
				pCopy.PdfVersion = PdfWriter.VERSION_1_7;

				doc.Open();

				for (int k = 0; k < files.Count; k++)
				{
					for (int i = 1; i < pdfFile.NumberOfPages + 1; i++)
					{
						msTemp = new MemoryStream();
						pdfTemplate = new PdfReader(files[k]);

						stamper = new PdfStamper(pdfTemplate, msTemp, '\n', false);

						names = new string[stamper.AcroFields.Fields.Keys.Count];
						stamper.AcroFields.Fields.Keys.CopyTo(names, 0);
						foreach (string name in names)
						{
							stamper.AcroFields.SetFieldProperty(name , "setfflags", PdfFormField.FF_READ_ONLY, null);
							stamper.AcroFields.RenameField(name, name + "_file" + k.ToString());
						}

						stamper.Close();
						pdfFile = new PdfReader(msTemp.ToArray());
						((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
						pCopy.FreeReader(pdfFile);
					}
				}

				pdfFile.Close();
				pCopy.Close();
				doc.Close();

				var data = msOutput.ToArray();


				return msOutput;
			}
			else if (files.Count == 1)
			{
				return new MemoryStream(files[0]);
			}

			return null;
		}
		*/
        public static byte[] LockPdf(byte[] FinalPdfMemory, string password)
        {
            using (MemoryStream input = new MemoryStream(FinalPdfMemory))
            {
                using (MemoryStream output = new MemoryStream())
                {
                    PdfReader reader = new PdfReader(input);
                    PdfEncryptor.Encrypt(reader, output, true, password, "secret", PdfWriter.ALLOW_SCREENREADERS | PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_COPY);
                    return output.ToArray();
                }
            }
        }

        //Get Fields
        public static string MyGetField(this AcroFields AF, FATextValue fav)
        {
            string fieldValue = AF.GetField(fav.Name);

            return fieldValue;
        }

        public static string MyGetField(this AcroFields AF, FASelectValue fav, string type = "string")
        {
            var value = AF.GetField(fav.Name);

            if (fav.CodeList == null)
                return value;

            string fieldValue = null;
            if (type == "string")
                fieldValue = fav.CodeList.Where(p => p.Value == value).Select(p => (string)p.Key).FirstOrDefault();

            return fieldValue;
        }

        public static bool MyGetField(this AcroFields AF, FAChBValue fav)
        {
            var value = AF.GetField(fav.Name);

            if (!string.IsNullOrWhiteSpace(value) && value == fav.TrueValue)
            {
                return true;
            }
            return false;
        }

        public static bool ToBoolean(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }
            else if (input.ToLower() == "true")
            {
                return true;
            }
            else if (input.ToLower() == "false")
            {
                return false;
            }
            return false;
        }

        public static bool? ToNullableBoolean(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return null;
            }
            else if (input.ToLower() == "true")
            {
                return true;
            }
            else if (input.ToLower() == "false")
            {
                return false;
            }
            return null;
        }

        public static DateTime? ToDateTime(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return null;

            DateTime date;
            if (DateTime.TryParse(input, out date))
            {
                return date;
            }

            return null;
        }

        public static int? ToNullableInt(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return null;

            int value;
            if (Int32.TryParse(input, out value))
            {
                return value;
            }

            return null;
        }

    }
}