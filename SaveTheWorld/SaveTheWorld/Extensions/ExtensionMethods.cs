﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Web;
using System.Reflection;
using SaveTheWorld.Models;

namespace SaveTheWorld.Extensions
{
    /// <summary>
    /// Extension methods class
    /// </summary>
    public static class ExtensionMethods
    {
        public static string ToShortDateTimeString(this DateTime? time)
        {
            if (!time.HasValue) return "";
            return ToShortDateTimeString(time.Value);
        }

        public static bool IsNumber(this object value)
        {
            if (value is sbyte) return true;
            if (value is byte) return true;
            if (value is short) return true;
            if (value is ushort) return true;
            if (value is int) return true;
            if (value is uint) return true;
            if (value is long) return true;
            if (value is ulong) return true;
            if (value is float) return true;
            if (value is double) return true;
            if (value is decimal) return true;
            return false;
        }

        public static string ToShortDateString(this DateTime? date)
        {
            if (!date.HasValue) return "";
            return date.Value.ToShortDateString();
        }

        public static string ToShortDateStringFormat(this DateTime? date)
        {
            if (!date.HasValue) return "";
            return date.Value.ToString("dd.MM.yyyy");
        }

        public static string ToShortDateTimeString(this DateTime time)
        {
            return time.ToShortDateString() + " " + time.ToShortTimeString();
        }

        public static DateTime? GetDate(this DateTime? date)
        {
            return date == null ? (DateTime?)null : date.Value.Date;
        }

        public static TimeSpan? ToTimespan(this DateTime? date)
        {
            if (!date.HasValue) return null;
            return new TimeSpan(date.Value.Hour, date.Value.Minute, date.Value.Second);
        }

        //public static decimal? RoundToCurrencyDecimalPlaces(this decimal value, string language = null, string currencyCode = null)
        //{
        //    return Math.Round(value,
        //        CultureManager.GetLanguageCurrencyDecimalPlaces(language, currencyCode), MidpointRounding.AwayFromZero);
        //}

        //public static decimal? RoundToCurrencyDecimalPlaces(this decimal? value, string language = null, string currencyCode = null)
        //{
        //    if (value == null)
        //        return null;
        //    else
        //        return value.Value.RoundToCurrencyDecimalPlaces(language, currencyCode);
        //}

        public static string PhoneFormat(this string number)
        {
            if (number == null) return null;
            string origNumber = number;

            number = number.Replace(" ", "").Replace("+", ""); //trim spaces
            if (number.Length == 12)
            {
                string newNumber = "";
                for (int i = 0; i < 12; i++)
                {
                    newNumber += ((i != 0 && i % 3 == 0) ? " " : "") + number[i];
                }
                return "+" + newNumber;
            }

            return origNumber;
        }

        // Returns default value of M when x is null. Returns selected value when x is not null.
        public static M ValueOrDefault<T, M>(this T x, Func<T, M> select)
        {
            return object.Equals(x, default(T)) ? default(M) : select(x);
        }

        // shortcut for ValueOrDefault
        public static M VoD<T, M>(this T x, Func<T, M> select)
        {
            return x.ValueOrDefault(select);
        }

        public static M ValueOrCustomDefault<T, M>(this T x, Func<T, M> select, M def)
        {
            return object.Equals(x, default(T)) ? def : select(x);
        }

        //public static string RequestIP(this HttpContextBase context)
        //{
        //    string ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (string.IsNullOrWhiteSpace(ip))
        //    {
        //        ip = context.Request.ServerVariables["REMOTE_ADDR"];
        //    }
        //    return ip;
        //}

        //public static string RequestIP(this HttpContext context)
        //{
        //    string ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (string.IsNullOrWhiteSpace(ip))
        //    {
        //        ip = context.Request.ServerVariables["REMOTE_ADDR"];
        //    }
        //    return ip;
        //}

        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static int NullableCount<T>(this IEnumerable<T> collection)
        {
            return collection == null ? 0 : collection.Count();
        }

        public static int NullableLength(this string text)
        {
            return text == null ? 0 : text.Length;
        }


        public static decimal? ToNullableDecimal(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return null;

            decimal parsedValue;
            decimal? temp = decimal.TryParse(input, out parsedValue) ? parsedValue : (decimal?)null;

            return temp;
        }

        public static string DecimalToPoinString(this decimal? input)
        {
            if (input == null)
                return "0";

            return input.ToString().Replace(",", ".");
        }

        public static string DecimalToRoundPoinString(this decimal? input)
        {
            if (input == null)
                return "0";

            var value = String.Format("{0:0}", input);
            return value;
        }
        public static string DecimalToRoundPoinFormated(this decimal? input)
        {
            if (input == null)
                return "0";

            input = Math.Round(input.Value);

            var f = new System.Globalization.NumberFormatInfo { NumberGroupSeparator = " " };
            var value = String.Format("{0:# ### ### ### ###}", input);
            return string.IsNullOrWhiteSpace(value) ? "0" : value;
        }

        public static string DecimalToRoundPoinFormatedCulture(this decimal? input)
        {
            if (input == null)
                return "0";

            var f = new System.Globalization.NumberFormatInfo { NumberGroupSeparator = " " };
            string value = string.Empty;

            if (true)
            {
                input = Math.Round(input.Value);
                value = String.Format("{0:# ### ### ### ###}", input);
            }
            else
            {
                input = Math.Round(input.Value, 1);
                value = String.Format("{0:### ### ### ##0.0}", input);
            }

            return string.IsNullOrWhiteSpace(value) ? "0" : value;
        }

        public static string DecimalToRoundPoinFormatedSign(this decimal? input)
        {
            if (input == null)
                return "0";

            input = Math.Round(input.Value);

            var f = new System.Globalization.NumberFormatInfo { NumberGroupSeparator = " " };
            var value = String.Format("{0:# ### ### ### ###}", input);

            if (string.IsNullOrWhiteSpace(value))
                return "0";

            if (input > 0)
                return "+ " + value.Trim();
            else
                value = value.Replace("   ", " ");

            return value;
        }

        public static int Floor(this decimal? number)
        {
            if (!number.HasValue) return 0;
            return number.Value.Floor();
        }
        /// <summary>
        /// 2015-09-02 - they dont want rounding but just interger part of number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int Floor(this decimal number) { return (int)number; }

        public static string DecimalToRoundhigerFormated(this decimal? input, out bool IsCround)
        {
            IsCround = false;

            if (input == null)
                return "0";

            if (Math.Abs(input.Value) < 1000)
            {
                input = input.Value;
                IsCround = true;
            }
            else if (Math.Abs(input.Value) >= 1000 && Math.Abs(input.Value) < 100000)
            {
                input = Math.Round(input.Value / 1000, 1);
            }
            else
            {
                input = Math.Round(input.Value / 1000);
            }

            var f = new System.Globalization.NumberFormatInfo { NumberGroupSeparator = " " };
            string value = "";
            if (IsCround)
                value = String.Format("{0:# ### ### ### ###}", input);
            else
                value = String.Format("{0:# ### ### ### ###.#}", input);

            return string.IsNullOrWhiteSpace(value) ? "0" : value;
        }

        public static string DecimalToRoundhigerFormatedSignPlusMinus(this decimal? input, out bool IsCround)
        {
            IsCround = false;

            if (input == null)
                return "0";

            if (Math.Abs(input.Value) < 1000)
            {
                input = input.Value;
                IsCround = true;
            }
            else if (Math.Abs(input.Value) >= 1000 && Math.Abs(input.Value) < 100000)
            {
                input = Math.Round(input.Value / 1000, 1);
            }
            else
            {
                input = Math.Round(input.Value / 1000);
            }

            var f = new System.Globalization.NumberFormatInfo { NumberGroupSeparator = " " };
            string value = "";
            if (IsCround)
                value = String.Format("{0:# ### ### ### ###}", input);
            else
                value = String.Format("{0:# ### ### ### ###.#}", input);

            if (!string.IsNullOrWhiteSpace(value) && input > 0)
                return "+ " + value.Trim();

            return string.IsNullOrWhiteSpace(value) ? "0" : value;
        }

        public static bool IsAllNull(params string[] strings)
        {
            return strings.All(p => string.IsNullOrWhiteSpace(p));
        }
        public static bool IsAnyNull(params string[] strings)
        {
            return strings.Any(p => string.IsNullOrWhiteSpace(p));
        }

        public static bool IsAllFalseOrNull(params bool?[] items)
        {
            return items.All(p => p == null || p == false);
        }

        public static bool AtleastOnetrue(params bool?[] items)
        {
            return items.Any(p => p != null && p == true);
        }

        public static bool AtleastOnetrue(params StringClientPartner[] items)
        {
            return items.Any(p => p != null && (!string.IsNullOrWhiteSpace(p.Client) || !string.IsNullOrWhiteSpace(p.Partner)));
        }

        public static bool AtleastOnetrue(params BoolClientPartnerChild[] items)
        {
            return items.Any(p => p != null && (p.Client || p.Partner || p.Child));
        }

        public static bool ChangeKey<TKey, TValue>(this IDictionary<TKey, TValue> dict,
                                           TKey oldKey, TKey newKey)
        {
            TValue value;
            if (!dict.TryGetValue(oldKey, out value))
                return false;

            dict.Remove(oldKey);  // do not change order
            dict[newKey] = value;  // or dict.Add(newKey, value) depending on ur comfort
            return true;
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var knownKeys = new HashSet<TKey>();
            return source.Where(element => knownKeys.Add(keySelector(element)));
        }
        public static bool IsAny<T>(this IEnumerable<T> data, Func<T, bool> predicate)
        {
            return data != null && data.Any(predicate);
        }
    }
}