﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SaveTheWorld.Extensions
{
    public static class XmlHelper
    {
        public static T DeSearialize<T>(this string xml)
        {
            if (string.IsNullOrWhiteSpace(xml))
                return (T)Activator.CreateInstance(typeof(T));

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            StringReader reader = new StringReader(xml);
            var fus = (T)serializer.Deserialize(reader);
            reader.Close();

            return fus;
        }

        public static string Serialize<T>(this T fus)
        {
            string xml;
            var serializer = new XmlSerializer(typeof(T));

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, fus);
                xml = writer.ToString();
            }

            return xml;
        }

    }
}