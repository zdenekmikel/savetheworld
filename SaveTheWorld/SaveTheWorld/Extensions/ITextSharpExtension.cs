﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveTheWorld.Extensions
{
    static class iTextSharpExtensions
    {
        public static void StyleFrom(this PdfPCell cell, PdfPCell other)
        {
            cell.PaddingLeft = other.PaddingLeft;
            cell.PaddingRight = other.PaddingRight;
            cell.PaddingTop = other.PaddingTop;
            cell.PaddingBottom = other.PaddingBottom;
            cell.BorderWidth = other.BorderWidth;
            cell.Border = other.Border;
            cell.HorizontalAlignment = other.HorizontalAlignment;
            cell.VerticalAlignment = other.HorizontalAlignment;
            cell.SetLeading(other.Leading, other.MultipliedLeading);
            cell.BackgroundColor = other.BackgroundColor;
        }

        public static Chunk Underline(this Chunk c)
        {
            c.SetUnderline(0.5f, -2);
            return c;
        }
    }

    class Cell
    {
        Chunk chunk;
        PdfPCell cell;
        Phrase phrase;
        PdfPTable table;

        public Cell(Table style)
        {
            phrase = new Phrase();
            cell = new PdfPCell(phrase);
            StyleFrom(style);
        }

        public Cell(Table style, Image img)
        {
            cell = new PdfPCell(img);
            StyleFrom(style);
        }

        public Cell(Table style, Table table)
        {
            this.table = table;
            cell = new PdfPCell(table);
            StyleFrom(style);
        }

        public Cell(Table style, string text, Font font)
            : this(style, new Chunk(text, font))
        {
        }

        public Cell(string text, Font font)
            : this((Cell)null, new Chunk(text, font))
        {
        }

        public Cell(Table style, Chunk chunk)
            : this(style)
        {
            Add(chunk);
        }

        public Cell(Cell style = null)
        {
            phrase = new Phrase();
            cell = new PdfPCell(phrase);
            StyleFrom(style);
        }

        public Cell(Cell style, Table table)
        {
            this.table = table;
            cell = new PdfPCell(table);
            StyleFrom(style);
        }

        public Cell(Cell style, string text, Font font)
            : this(style, new Chunk(text, font))
        {
        }

        public Cell(Cell style, Chunk chunk)
            : this(style)
        {
            Add(chunk);
        }

        public Cell Add(Chunk chunk)
        {
            this.chunk = chunk;
            phrase.Add(chunk);
            return this;
        }

        public PdfPCell C { get { return cell; } }

        public static implicit operator PdfPCell(Cell c)
        {
            return c.cell;
        }

        public Cell Align(int align)
        {
            cell.HorizontalAlignment = align;
            return this;
        }
        public Cell AlignLeft()
        {
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            return this;
        }
        public Cell AlignCenter()
        {
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            return this;
        }
        public Cell AlignRight()
        {
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            return this;
        }

        public Cell Leading(double @fixed, double multiplied)
        {
            cell.SetLeading((float)@fixed, (float)multiplied);
            return this;
        }

        internal Cell BackgroundColor(int p)
        {
            cell.BackgroundColor = new BaseColor(p);
            return this;
        }

        internal Cell BackgroundColor(BaseColor p)
        {
            cell.BackgroundColor = p;
            return this;
        }

        public Cell Padding(double p)
        {
            cell.Padding = (float)p;
            return this;
        }

        public Cell PaddingBottom(double p)
        {
            cell.PaddingBottom = (float)p;
            return this;
        }
        public Cell PaddingLeft(double p)
        {
            cell.PaddingLeft = (float)p;
            return this;
        }
        public Cell PaddingRight(double p)
        {
            cell.PaddingRight = (float)p;
            return this;
        }

        public Cell With(Action<Cell> apply)
        {
            apply(this);
            return this;
        }

        public Cell NoWrap()
        {
            cell.NoWrap = true;
            return this;
        }

        public Cell FixedHeight(int p)
        {
            cell.FixedHeight = p;
            return this;
        }

        public Cell AlignBottom()
        {
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
            return this;
        }

        public Cell AlignJustifyAll()
        {
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL;
            return this;
        }

        public Cell AlignJustify()
        {
            cell.HorizontalAlignment = Element.ALIGN_JUSTIFIED;
            return this;
        }

        public Cell Indent(double p, double? following = null)
        {
            cell.Indent = (float)p;
            cell.FollowingIndent = (float)(following ?? p);
            return this;
        }

        public Cell Colspan(int p)
        {
            cell.Colspan = p;
            return this;
        }

        public Cell Rowspan(int p)
        {
            cell.Rowspan = p;
            return this;
        }

        public Cell AlignMiddle()
        {
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            return this;
        }

        public Cell PaddingTop(double p)
        {
            cell.PaddingTop = (float)p;
            return this;
        }

        public Cell BorderWidth(double p)
        {
            cell.BorderWidth = (float)p;
            return this;
        }

        internal Cell Rotation(int p)
        {
            cell.Rotation = p;
            return this;
        }

        public Cell StyleFrom(Table style)
        {
            cell.BorderWidth = 0;
            cell.Padding = 0;
            if (style != null && style.Style != null)
                cell.StyleFrom(style.Style);

            return this;
        }

        public Cell StyleFrom(Cell style)
        {
            cell.BorderWidth = 0;
            cell.Padding = 0;
            if (style != null)
                cell.StyleFrom(style);
            return this;
        }
    }

    class Table
    {
        PdfPTable table;
        public Cell Style { get; set; }

        public Table(Table style)
        {
            table = new PdfPTable(1);
            StyleFrom(style);
        }

        public Table(Cell style = null)
        {
            table = new PdfPTable(1);
            StyleFrom(style);
        }

        private void StyleFrom(Table style)
        {
            if (style != null)
                StyleFrom(style.Style);
        }

        private void StyleFrom(Cell style)
        {
            T.DefaultCell.Padding = 0;
            table.DefaultCell.BorderWidth = 0;
            Style = new Cell(style);
        }

        public Table(Table style, int columns)
        {
            table = new PdfPTable(columns);
            StyleFrom(style);
        }

        public Table(int columns)
            : this((Table)null, columns)
        {
            table = new PdfPTable(columns);
            StyleFrom((Table)null);
        }
        public Table(params double[] widths)
        {
            table = new PdfPTable(widths.Select(p => (float)p).ToArray());
            StyleFrom((Table)null);
        }

        public Table(Table style, params double[] widths)
        {
            table = new PdfPTable(widths.Select(p => (float)p).ToArray());
            StyleFrom(style);
        }

        public Table(Cell style, int columns)
        {
            table = new PdfPTable(columns);
            StyleFrom(style);
        }

        public Table(Cell style, params double[] widths)
        {
            table = new PdfPTable(widths.Select(p => (float)p).ToArray());
            StyleFrom(style);
        }

        public PdfPTable T { get { return table; } }

        public Table With(Action<Table> apply)
        {
            apply(this);
            return this;
        }

        public void Add(Cell c)
        {
            table.AddCell(c);
        }

        public void Add(Table t)
        {
            table.AddCell(new Cell(this, t));
        }

        public static implicit operator PdfPTable(Table t)
        {
            return t.table;
        }

        public void AddDataOrEmpty(bool? hasAss, Cell c)
        {
            if (hasAss != null && hasAss == true)
                Add(c);
            else
                AddEmptyCell();
        }
        public void AddEmptyCell()
        {
            Add(new Cell(this));
        }
    }
}
