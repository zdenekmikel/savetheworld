﻿using iTextSharp.text.pdf;
using SaveTheWorld.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static SaveTheWorld.Models.FusHelper;

namespace SaveTheWorld.Models
{
    public class ProcCIClientsAutocomplete_Result
    {
        public int? ClientID { get; set; }
    }

    public class PartyRelationshipDetail
    {
        public string RelatedPartyTypeCode { get; set; }
        public int RelatedPartyID { get; set; }
        public string PartyName { get; set; }
    }

    public class Attribute
    {
        public int AttributeID { get; set; }
        public string Description { get; set; }
        public string ResourceCode { get; set; }
        public string DataType { get; set; }
    }
    public class ClientDetail
    {
        public int ClientID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthPlace { get; set; }
        public string DocumentTypeCode { get; set; }
        public string DocumentCode { get; set; }
        public string CompanyName { get; set; }
        public string Prefix { get; set; }
        public int PartyID { get; set; }
    }

    public class PartyAddress
    {
        public string Address1 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public int Active { get; set; }
        public int IsPrimary { get; set; }
        public string AddressTypeCode { get; set; }
    }

    public class PartyContact
    {
        public int Active { get; set; }
        public bool IsPrimary { get; set; }
        public string EmailAddr { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
    }

    public class ClientAttribute
    {
        public int AttributeID { get; set; }
        public int? NumericValue { get; set; }
        public string StringValue { get; set; }
        public DateTime? DateTimeValue { get; set; }
    }
    public class AttributeWorker
    {
        public static string GetStringAttributeValue(List<ClientAttribute> attributes, string attributeName)
        {
            var attrType = DbProvider.Attributes().SingleOrDefault(p => p.ResourceCode == attributeName);
            if (attrType == null)
            {
                //App.HandleException(new Exception("attrType is null in standardFUSModel"));
            }
            var clientAttr = attributes.SingleOrDefault(p => p.AttributeID == attrType.AttributeID);

            if (clientAttr == null)
                return null;

            if (attrType.DataType == "bool")
            {
                return clientAttr.ValueOrDefault(p => p.NumericValue == 1 ? "True" : "False");
            }
            else
            {
                return clientAttr.ValueOrDefault(p => p.StringValue);
            }
        }

        //public static string GetStringAttributeListValue(List<ClientAttribute> attributes, string attributeName)
        //{
        //    Core.Com.Attribute attrType = AppCache.Inst.Attributes.SingleOrDefault(p => p.ResourceCode == attributeName);
        //    var clientAttr = attributes.SingleOrDefault(p => p.AttributeID == attrType.AttributeID);

        //    return clientAttr.ValueOrDefault(p => p.StringValue);
        //}

        public static int? GetIntAttributeListValue(List<ClientAttribute> attributes, string attributeName)
        {
            var attrType = DbProvider.Attributes().SingleOrDefault(p => p.ResourceCode == attributeName);
            var clientAttr = attributes.SingleOrDefault(p => p.AttributeID == attrType.AttributeID);

            var value = clientAttr.ValueOrDefault(p => p.NumericValue);
            int? intValue = value == null ? (int?)null : Convert.ToInt32(value);

            return intValue;
        }

        public static DateTime? GetDateAttributeValue(List<ClientAttribute> attributes, string attributeName)
        {
            var attrType = DbProvider.Attributes().SingleOrDefault(p => p.ResourceCode == attributeName);
            var clientAttr = attributes.SingleOrDefault(p => p.AttributeID == attrType.AttributeID);

            if (clientAttr == null) return null;

            if (attrType.DataType == "date")
            {
                return clientAttr.DateTimeValue;
            }
            return null;
        }
    }
    public class PartyCodeHelper
    {
        public string PartyCode { get; set; }
        public string PartyCodeSlash { get; set; }

        public DateTime? BirthDate { get; set; }
        public string GenderCode { get; set; }
        public bool IsValid { get; set; }

        int y, m, d;

        public PartyCodeHelper(string input)
        {
            if (input == null)
            {
                IsValid = false;
                return;
            }

            input = input.Replace("/", "");
            this.PartyCode = Regex.Match(input, @"\d+").Value;
            this.PartyCodeSlash = PartyCode;

            this.IsValid = true;

            if (string.IsNullOrWhiteSpace(this.PartyCode))
            {
                IsValid = false;
                return;
            }
            else
            {

                if (this.PartyCode.Length > 6)
                    this.PartyCodeSlash = PartyCode.Insert(6, "/");
            }

            if (PartyCode.Length >= 6)
                GetDataFromPartyCode();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        private void GetDataFromPartyCode()
        {
            Int32.TryParse(PartyCode.Substring(0, 2), out y);
            Int32.TryParse(PartyCode.Substring(2, 2), out m);
            Int32.TryParse(PartyCode.Substring(4, 2), out d);

            if (this.PartyCode.Length == 9 && y >= 54)
            {
                this.IsValid = false;
                return;
            }
            else if (this.PartyCode.Length == 9)
                y += 1900;
            else if (this.PartyCode.Length == 10 && y >= 54)
                y += 1900;
            else if (this.PartyCode.Length == 10 && y < 54)
                y += 2000;

            if (m >= 1 && m <= 12 || m >= 21 && m <= 32)
            {
                GenderCode = "M";
                // is male
                if (m >= 21) m -= 20;
            }
            else if (m >= 51 && m <= 62 || m >= 71 && m <= 82)
            {
                GenderCode = "F";
                //is female
                if (m <= 62) m -= 50;
                else if (m >= 71) m -= 70;
            }

            if (d > 50) d -= 50; // cizinci mají ke dni přičteno 50

            if (m >= 1 && m <= 12 && d >= 1 && d <= DateTime.DaysInMonth(y, m))
            {
                DateTime birthDate;
                if (!DateTime.TryParse(d.ToString() + "." + m.ToString() + "." + y.ToString(), out birthDate))
                {
                    IsValid = false;
                    return;
                }
                else
                {
                    this.BirthDate = birthDate;
                }

                // 2015-05-20 - DT - přidána funkčnost pro kontrolu zda je rč validní podle kontrolní číslice
                if (this.PartyCode.Length < 10)
                    return;

                int ctrlBase, ctrlNum;
                bool result = true;

                result &= Int32.TryParse(this.PartyCode.Substring(0, 9), out ctrlBase);
                result &= Int32.TryParse(this.PartyCode.Substring(9, 1), out ctrlNum);

                if (!result)
                {
                    this.IsValid = false;
                    return;
                }

                int modulo = ctrlBase % 11;
                if (modulo != ctrlNum && modulo - 10 != ctrlNum)
                    this.IsValid = false;
            }
        }

        public static bool IsValidPartyCodeDate(DateTime date, string partyCode)
        {
            PartyCodeHelper codeHelper = new PartyCodeHelper(partyCode);
            return codeHelper.IsValid && codeHelper.BirthDate == date;
        }

        public static bool IsValidChildBirthDate(DateTime date)
        {
            if (date.Date <= DateTime.Now.Date.AddYears(-18))
                return false;

            return true;
        }

        public static bool IsValidGenderCode(string genderCode, string p)
        {
            PartyCodeHelper party = new PartyCodeHelper(p);
            return genderCode == party.GenderCode;

        }

        public static bool NeedsPapers(DateTime date)
        {
            return date.AddYears(15) < DateTime.Now.AddDays(-15);
        }
    }
    public static class Extensions
    {
        public static bool ReadBoolValue(this AcroFields ff, FASelectValue radioButton, bool ifnullvalue = true)
        {
            bool? tmp = ff.MyGetField(radioButton).ToNullableBoolean();
            return tmp ?? ifnullvalue;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static bool? ReadBoolValueNullable(this AcroFields ff, FASelectValue radioButton)
        {
            if (radioButton == null)
                return null;

            bool? tmp = ff.MyGetField(radioButton).ToNullableBoolean();
            return tmp;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Form_Fill - child
        public static void Form_Fill(this Child child, AcroFields ff,
            FASelectValue genderF,
            FATextValue lastNameF,
            FATextValue firstNameF,
            FATextValue countryCodeF,
            FATextValue partyCode0F,
            FATextValue partyCode1F,
            FATextValue documentCodeF,
            FATextValue documentFromToReleasedF,
            FATextValue birthPlaceF,
            FATextValue addressF,
            FATextValue postalCodeF,
            FATextValue cityF)
        {
            if (ff == null || child == null) return;

            ff.MySetField(genderF, child.GenderCode);
            ff.MySetField(lastNameF, child.LastName);
            ff.MySetField(firstNameF, child.FirstName);
            ff.MySetField(countryCodeF, child.CountryCode);

            // rodné číslo
            if (!string.IsNullOrEmpty(child.PartyCode))
            {
                string[] subResult = child.PartyCode.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                if (subResult.Length > 0)
                    ff.MySetField(partyCode0F, subResult[0]);

                if (subResult.Length > 1)
                    ff.MySetField(partyCode1F, subResult[1]);
            }

            ff.MySetField(documentCodeF, child.DocumentCode);

            #region doklad od - do / vydal
            StringBuilder doc = new StringBuilder();
            if (child.DocumentDateFrom.HasValue && child.DocumentDateTo.HasValue)
                doc.AppendFormat("{0:dd'/'MM'/'yyyy} - {1:dd'/'MM'/'yyyy}", child.DocumentDateFrom, child.DocumentDateTo);

            if (!string.IsNullOrWhiteSpace(child.DocumentReleased))
            {
                if (doc.Length > 0) doc.Append(" / ");
                doc.Append(child.DocumentReleased);
            }

            ff.MySetField(documentFromToReleasedF, doc.ToString());
            #endregion

            ff.MySetField(birthPlaceF, child.BirthPlace);

            if (child.IsAddressDifferent)
            {
                ff.MySetField(addressF, child.Address);
                ff.MySetField(postalCodeF, child.PostalCode);
                ff.MySetField(cityF, child.City);
            }
        }
        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Form_Read - child
        public static void Form_Read(this Child child, FormFUSCZ fus, AcroFields ff,
            FASelectValue genderF,
            FATextValue lastNameF,
            FATextValue firstNameF,
            FATextValue countryCodeF,
            FATextValue partyCode0F,
            FATextValue partyCode1F,
            FATextValue documentCodeF,
            FATextValue documentFromToReleasedF,
            FATextValue birthPlaceF,
            FATextValue addressF,
            FATextValue postalCodeF,
            FATextValue cityF)
        {
            child.LastName = ff.MyGetField(lastNameF);
            child.FirstName = ff.MyGetField(firstNameF);
            child.CountryCode = ff.MyGetField(countryCodeF);

            child.PartyCode = string.Format("{0}/{1}", ff.MyGetField(partyCode0F), ff.MyGetField(partyCode1F));
            if (child.PartyCode == "/")
                child.PartyCode = null;

            PartyCodeHelper partyCodeHlpr = new PartyCodeHelper(child.PartyCode);
            if (partyCodeHlpr.IsValid)
            {
                child.BirthDate = partyCodeHlpr.BirthDate;
                child.GenderCode = partyCodeHlpr.GenderCode;
            }

            child.DocumentCode = ff.MyGetField(documentCodeF);

            if (!string.IsNullOrEmpty(child.DocumentCode))
                child.DocumentTypeCode = Constants.DocumentFOType.FOOP;

            // 2015-05-28 - DT - added few separators for parsing document from/to/where field
            string fromToReleased = ff.MyGetField(documentFromToReleasedF);

            var result = FUShelper.SplitByChar(fromToReleased, '/');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, '\\');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, ',');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, ';');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, '|');

            if (result != null)
            {
                child.DocumentDateFrom = result.From;
                child.DocumentDateTo = result.To;
                child.DocumentReleased = result.Where;
            }

            child.BirthPlace = ff.MyGetField(birthPlaceF);

            child.Address = ff.MyGetField(addressF);
            child.PostalCode = ff.MyGetField(postalCodeF);
            child.City = ff.MyGetField(cityF);

            if (partyCodeHlpr.IsValid)
            {
                var dbClient = DbProvider.FUSClientsSearchWeb(0, fus.AgentDetail.AgentID.Value, "P", child.PartyCode, null, true).FirstOrDefault();
                if (dbClient != null)
                {
                    child.ClientID = dbClient.ClientID;

                    ClientDetail clientDetail = null;
                    clientDetail = DbProvider.ClientDetail(0, dbClient.VoD(p => p.ClientID.Value));
                    var addresses = DbProvider.PartyAddresses(clientDetail.PartyID, 0);
                    var attributes = DbProvider.ClientAttributes(clientDetail.ClientID);

                    if (string.IsNullOrWhiteSpace(child.LastName))
                        child.LastName = clientDetail.LastName;

                    if (string.IsNullOrWhiteSpace(child.FirstName))
                        child.FirstName = clientDetail.FirstName;

                    if (string.IsNullOrWhiteSpace(child.BirthPlace))
                        child.BirthPlace = clientDetail.BirthPlace;

                    if (string.IsNullOrWhiteSpace(child.DocumentTypeCode))
                        child.DocumentTypeCode = clientDetail.DocumentTypeCode;

                    if (string.IsNullOrWhiteSpace(child.DocumentCode))
                        child.DocumentCode = clientDetail.DocumentCode;

                    var permanentAddress = addresses.Where(p => p.Active == 1 && (p.IsPrimary == 1 || p.AddressTypeCode == "PA")).OrderByDescending(p => p.IsPrimary).Select(p => new { p.Address1, p.City, p.PostalCode }).FirstOrDefault();
                    if (permanentAddress != null)
                    {
                        if (string.IsNullOrWhiteSpace(child.Address))
                            child.Address = permanentAddress.Address1;

                        if (string.IsNullOrWhiteSpace(child.City))
                            child.City = permanentAddress.City;

                        if (string.IsNullOrWhiteSpace(child.PostalCode))
                            child.PostalCode = permanentAddress.PostalCode;
                    }

                    var countryCode = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_Nationality);
                    if (string.IsNullOrWhiteSpace(child.CountryCode))
                        child.CountryCode = countryCode;

                    var Clients_DocumentReleased = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_Released);
                    var Clients_DocumentDateFrom = AttributeWorker.GetDateAttributeValue(attributes, Constants.Attributes.Client_Clients_DocumentFrom).ToShortDateString();
                    var Clients_DocumentDateTo = AttributeWorker.GetDateAttributeValue(attributes, Constants.Attributes.Client_Clients_DocumentTo).ToShortDateString();

                    if (child.DocumentDateFrom == null)
                        child.DocumentDateFrom = Clients_DocumentDateFrom.ToDateTime();

                    if (child.DocumentDateTo == null)
                        child.DocumentDateTo = Clients_DocumentDateTo.ToDateTime();

                    if (string.IsNullOrWhiteSpace(child.DocumentReleased))
                        child.DocumentReleased = Clients_DocumentReleased;
                }
            }

            if (!string.IsNullOrEmpty(child.Address) || !string.IsNullOrEmpty(child.PostalCode) || !string.IsNullOrEmpty(child.City))
                child.IsAddressDifferent = true;
        }
        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Form_Fill - client
        public static void Form_Fill(this Client client, AcroFields ff,
            bool isCompany,
            FASelectValue genderF,
            FASelectValue residentF,
            FATextValue lastNameF,
            FATextValue firstNameF,
            FATextValue prefixF,
            FATextValue partyCode0F,
            FATextValue partyCode1F,
            FATextValue tradeLicenceF,
            FATextValue countryCodeF,
            FATextValue birthPlaceF,
            FATextValue documentCodeF,
            FATextValue documentFromToReleasedF,
            FATextValue permanentAddressF,
            FATextValue permanentPostalCodeF,
            FATextValue permanentCityF,
            FATextValue mailingAddressF,
            FATextValue mailingPostalCodeF,
            FATextValue mailingCityF,
            FATextValue maritalF,
            FATextValue occupationF,
            FATextValue phoneF,
            FATextValue emailF,
            FATextValue employerF,
            string personType)
        {
            if (ff == null || client == null) return;

            ff.MySetField(genderF, client.GenderCode);
            if (client.ResidentUSA.HasValue && residentF != null)
                ff.MySetField(residentF, client.ResidentUSA);

            ff.MySetField(lastNameF, isCompany ? client.BusinessName : client.LastName);
            ff.MySetField(firstNameF, client.FirstName);
            ff.MySetField(prefixF, client.Prefix);

            ff.MySetField(tradeLicenceF, client.TradeLicenceNo);

            // 2015-05-25 - DT - pokud je klient fyzická osoba podnikající a ičo je prázdné
            if (SealedConstants.PersonType.IndividualBusinessman.Value == personType && string.IsNullOrWhiteSpace(client.TradeLicenceNo))
                ff.MySetField(tradeLicenceF, "XXXXXXXX");

            // rodné číslo
            if (!string.IsNullOrEmpty(client.PartyCode))
            {
                string[] subResult = client.PartyCode.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                if (subResult.Length > 0)
                    ff.MySetField(partyCode0F, subResult[0]);

                if (subResult.Length > 1)
                    ff.MySetField(partyCode1F, subResult[1]);
            }

            ff.MySetField(countryCodeF, client.CountryCode);
            ff.MySetField(birthPlaceF, client.BirthPlace);
            ff.MySetField(documentCodeF, client.DocumentCode);

            #region doklad od - do / vydal
            if (!isCompany)
            {
                StringBuilder doc = new StringBuilder();
                if (client.DocumentDateFrom.HasValue && client.DocumentDateTo.HasValue)
                    doc.AppendFormat("{0:dd'/'MM'/'yyyy} - {1:dd'/'MM'/'yyyy}", client.DocumentDateFrom, client.DocumentDateTo);

                if (!string.IsNullOrWhiteSpace(client.DocumentReleased))
                {
                    if (doc.Length > 0) doc.Append(" / ");
                    doc.Append(client.DocumentReleased);
                }

                ff.MySetField(documentFromToReleasedF, doc.ToString());
            }
            #endregion

            ff.MySetField(permanentAddressF, client.PermanentAddress);
            ff.MySetField(permanentPostalCodeF, client.PermanentPostalCode);
            ff.MySetField(permanentCityF, client.PermanentCity);

            if (client.IsMailingDifferent)
            {
                ff.MySetField(mailingAddressF, client.MailingAddress);
                ff.MySetField(mailingPostalCodeF, client.MailingPostalCode);
                ff.MySetField(mailingCityF, client.MailingCity);
            }

            ff.MySetField(maritalF, FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == client.MaritalStatus).Select(p => p.Name).FirstOrDefault());
            ff.MySetField(occupationF, client.Occupation);
            ff.MySetField(phoneF, client.Phone);
            ff.MySetField(emailF, client.EmailAddr);

            if (employerF != null)
                ff.MySetField(employerF, client.Employer);
        }
        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Form_Read - client
        public static void Form_Read(this Client client, FormFUSCZ fus, AcroFields ff,
            FASelectValue genderF,
            FASelectValue residentF,
            FATextValue lastNameF,
            FATextValue firstNameF,
            FATextValue prefixF,
            FATextValue partyCode0F,
            FATextValue partyCode1F,
            FATextValue tradeLicenceF,
            FATextValue countryCodeF,
            FATextValue birthPlaceF,
            FATextValue documentCodeF,
            FATextValue documentFromToReleasedF,
            FATextValue permanentAddressF,
            FATextValue permanentPostalCodeF,
            FATextValue permanentCityF,
            FATextValue mailingAddressF,
            FATextValue mailingPostalCodeF,
            FATextValue mailingCityF,
            FATextValue maritalF,
            FATextValue occupationF,
            FATextValue phoneF,
            FATextValue emailF,
            FATextValue employerF,
            bool isClient)
        {
            if (client == null || fus == null || ff == null) return;

            client.PartyCode = string.Format("{0}/{1}", ff.MyGetField(partyCode0F), ff.MyGetField(partyCode1F));
            if (client.PartyCode == "/")
                client.PartyCode = null;

            PartyCodeHelper partyCodeHlpr = new PartyCodeHelper(client.PartyCode);

            if (partyCodeHlpr.IsValid)
            {
                client.BirthDate = partyCodeHlpr.BirthDate;
                client.GenderCode = partyCodeHlpr.GenderCode;
            }

            client.ResidentUSA = residentF != null ? ff.ReadBoolValue(residentF, false) : false;
            client.LastName = ff.MyGetField(lastNameF);
            client.FirstName = ff.MyGetField(firstNameF);

            client.Prefix = ff.MyGetField(prefixF);
            client.TradeLicenceNo = ff.MyGetField(tradeLicenceF);

            client.DocumentTypeCode = Constants.DocumentFOType.FOOP;

            client.CountryCode = ff.MyGetField(countryCodeF);
            client.BirthPlace = ff.MyGetField(birthPlaceF);
            client.DocumentCode = ff.MyGetField(documentCodeF);

            // 2015-05-28 - DT - added few separators for parsing document from/to/where field
            string fromToReleased = ff.MyGetField(documentFromToReleasedF);

            var result = FUShelper.SplitByChar(fromToReleased, '/');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, '\\');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, ',');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, ';');
            if (result == null)
                result = FUShelper.SplitByChar(fromToReleased, '|');

            if (result != null)
            {
                client.DocumentDateFrom = result.From;
                client.DocumentDateTo = result.To;
                client.DocumentReleased = result.Where;
            }

            client.PermanentAddress = ff.MyGetField(permanentAddressF);
            client.PermanentPostalCode = ff.MyGetField(permanentPostalCodeF);
            client.PermanentCity = ff.MyGetField(permanentCityF);

            client.MailingAddress = ff.MyGetField(mailingAddressF);
            client.MailingPostalCode = ff.MyGetField(mailingPostalCodeF);
            client.MailingCity = ff.MyGetField(mailingCityF);


            string maritalStatus = ff.MyGetField(maritalF);
            if (!string.IsNullOrEmpty(maritalStatus) && maritalStatus.Length > 4)
            {
                maritalStatus = maritalStatus.Substring(0, 5);
                client.MaritalStatus = FUShelper.FUSListOptionsResource.Where(p => p.Name.IndexOf(maritalStatus, StringComparison.CurrentCultureIgnoreCase) >= 0 && p.FUSListTypeID == Constants.FUSCZListTypes.MARITIALSTATUS).Select(p => p.FUSListOptionCode).FirstOrDefault();
            }

            client.Occupation = ff.MyGetField(occupationF);
            client.Phone = ff.MyGetField(phoneF);
            client.EmailAddr = ff.MyGetField(emailF);

            if (string.IsNullOrWhiteSpace(client.PartyCode))
                client.BusinessName = client.LastName;

            if (employerF != null)
                client.Employer = ff.MyGetField(employerF);

            #region load from database


            if (!string.IsNullOrWhiteSpace(client.PartyCode) || !string.IsNullOrWhiteSpace(client.TradeLicenceNo) || !string.IsNullOrWhiteSpace(client.LastName))
            {
                if (isClient)
                    fus.FUSForClient = true;
                else
                    fus.FUSForPartner = true;
            }

            bool isPO = string.IsNullOrWhiteSpace(client.PartyCode) && !string.IsNullOrWhiteSpace(client.TradeLicenceNo);

            if (isPO)
            {
                var dbClient = DbProvider.FUSClientsSearchWeb(0, fus.AgentDetail.AgentID.Value, "C", client.TradeLicenceNo, null, true).FirstOrDefault();
                if (dbClient != null)
                {
                    client.ClientID = dbClient.ClientID;
                    client.AllowEdit = true;

                    ClientDetail clientDetail = null;
                    clientDetail = DbProvider.ClientDetail(0, dbClient.VoD(p => p.ClientID.Value));
                    var addresses = DbProvider.PartyAddresses(clientDetail.PartyID, 0);
                    var attributes = DbProvider.ClientAttributes(clientDetail.ClientID);

                    if (string.IsNullOrWhiteSpace(client.BusinessName))
                        client.BusinessName = clientDetail.CompanyName;

                    var Clients_ResidentUSA = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Companies_ResidentUSA);

                    if (ff.ReadBoolValueNullable(residentF) == null)
                        client.ResidentUSA = Clients_ResidentUSA == "True" ? true : false;

                    var businessAddress = addresses.Where(p => p.Active == 1 && p.AddressTypeCode == "HQ").OrderByDescending(p => p.IsPrimary).ThenBy(p => p.AddressTypeCode == "HQ" ? 1 : 4).Select(p => new { p.Address1, p.City, p.PostalCode }).FirstOrDefault();

                    if (businessAddress != null)
                    {
                        if (string.IsNullOrWhiteSpace(client.PermanentAddress))
                            client.PermanentAddress = businessAddress.Address1;

                        if (string.IsNullOrWhiteSpace(client.PermanentCity))
                            client.PermanentCity = businessAddress.City;

                        if (string.IsNullOrWhiteSpace(client.PermanentPostalCode))
                            client.PermanentPostalCode = businessAddress.PostalCode;
                    }

                    var contact = DbProvider.PartyContacts(clientDetail.PartyID, 0)
                    .Where(p => p.Active == 1)
                    .OrderByDescending(p => p.IsPrimary)
                    .Select(p => new Contact { Email = p.EmailAddr, Phone = p.Mobile ?? p.Phone }).FirstOrDefault();

                    if (string.IsNullOrWhiteSpace(client.EmailAddr))
                        client.EmailAddr = contact.Email;

                    fus.showImportedInfo = true;
                }
            }
            else if (partyCodeHlpr.IsValid)
            {
                var dbClient = DbProvider.FUSClientsSearchWeb(0, fus.AgentDetail.AgentID.Value, "P", partyCodeHlpr.PartyCode, null, true).FirstOrDefault();
                if (dbClient != null)
                {
                    client.ClientID = dbClient.ClientID;
                    client.AllowEdit = true;

                    ClientDetail clientDetail = null;
                    clientDetail = DbProvider.ClientDetail(0, dbClient.VoD(p => p.ClientID.Value));
                    var addresses = DbProvider.PartyAddresses(clientDetail.PartyID, 0);
                    var attributes = DbProvider.ClientAttributes(clientDetail.ClientID);

                    if (string.IsNullOrWhiteSpace(client.LastName))
                        client.LastName = clientDetail.LastName;

                    if (string.IsNullOrWhiteSpace(client.FirstName))
                        client.FirstName = clientDetail.FirstName;

                    if (string.IsNullOrWhiteSpace(client.Prefix))
                        client.Prefix = clientDetail.Prefix;

                    if (string.IsNullOrWhiteSpace(client.BirthPlace))
                        client.BirthPlace = clientDetail.BirthPlace;

                    if (string.IsNullOrWhiteSpace(client.DocumentTypeCode))
                        client.DocumentTypeCode = clientDetail.DocumentTypeCode;

                    if (string.IsNullOrWhiteSpace(client.DocumentCode))
                        client.DocumentCode = clientDetail.DocumentCode;

                    var Clients_ResidentUSA = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_ResidentUSA);
                    var Clients_DocumentPOCode = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_DocumentPOCode);
                    var countryCode = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_Nationality);
                    var occupation = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_Profession);

                    if (ff.ReadBoolValueNullable(residentF) == null)
                        client.ResidentUSA = Clients_ResidentUSA == "True" ? true : false;

                    var mailingAddress = addresses.Where(p => p.Active == 1 && p.AddressTypeCode == "MA").OrderByDescending(p => p.IsPrimary).Select(p => new { p.Address1, p.City, p.PostalCode }).FirstOrDefault();
                    var permanentAddress = addresses.Where(p => p.Active == 1 && (p.IsPrimary == 1 || p.AddressTypeCode == "PA")).OrderByDescending(p => p.IsPrimary).Select(p => new { p.Address1, p.City, p.PostalCode }).FirstOrDefault();

                    if (permanentAddress != null)
                    {
                        if (string.IsNullOrWhiteSpace(client.PermanentAddress))
                            client.PermanentAddress = permanentAddress.Address1;

                        if (string.IsNullOrWhiteSpace(client.PermanentCity))
                            client.PermanentCity = permanentAddress.City;

                        if (string.IsNullOrWhiteSpace(client.PermanentPostalCode))
                            client.PermanentPostalCode = permanentAddress.PostalCode;
                    }

                    if (mailingAddress != null)
                    {
                        if (string.IsNullOrWhiteSpace(client.MailingAddress))
                            client.MailingAddress = mailingAddress.Address1;

                        if (string.IsNullOrWhiteSpace(client.MailingCity))
                            client.MailingCity = mailingAddress.City;

                        if (string.IsNullOrWhiteSpace(client.MailingPostalCode))
                            client.MailingPostalCode = mailingAddress.PostalCode;
                    }

                    if (string.IsNullOrWhiteSpace(client.MaritalStatus))
                    {
                        string Clients_MaritalStatus = "";
                        var maritialID = AttributeWorker.GetIntAttributeListValue(attributes, Constants.Attributes.Client_Clients_MaritalStatus);
                        switch (maritialID)
                        {
                            case 0:
                                Clients_MaritalStatus = Constants.FUSListOptions.FREE;
                                break;
                            case 1:
                                Clients_MaritalStatus = Constants.FUSListOptions.MARRIED;
                                break;
                            case 2:
                                Clients_MaritalStatus = Constants.FUSListOptions.DIVORCED;
                                break;
                            case 3:
                                Clients_MaritalStatus = Constants.FUSListOptions.WIDOW;
                                break;
                            case 4:
                                Clients_MaritalStatus = Constants.FUSListOptions.REGISTERED;
                                break;
                        }
                        client.MaritalStatus = Clients_MaritalStatus;
                    }

                    if (string.IsNullOrWhiteSpace(client.CountryCode))
                        client.CountryCode = countryCode;

                    if (string.IsNullOrWhiteSpace(client.Occupation))
                        client.Occupation = occupation;

                    var contact = DbProvider.PartyContacts(clientDetail.PartyID, 0)
                    .Where(p => p.Active == 1)
                    .OrderByDescending(p => p.IsPrimary)
                    .Select(p => new Contact { Email = p.EmailAddr, Phone = p.Mobile ?? p.Phone }).FirstOrDefault();

                    if (string.IsNullOrWhiteSpace(client.EmailAddr))
                        client.EmailAddr = contact.Email;

                    if (string.IsNullOrWhiteSpace(client.Phone))
                        client.Phone = contact.Phone;

                    var Clients_DocumentReleased = AttributeWorker.GetStringAttributeValue(attributes, Constants.Attributes.Client_Clients_Released);
                    var Clients_DocumentDateFrom = AttributeWorker.GetDateAttributeValue(attributes, Constants.Attributes.Client_Clients_DocumentFrom).ToShortDateString();
                    var Clients_DocumentDateTo = AttributeWorker.GetDateAttributeValue(attributes, Constants.Attributes.Client_Clients_DocumentTo).ToShortDateString();

                    if (client.DocumentDateFrom == null)
                        client.DocumentDateFrom = Clients_DocumentDateFrom.ToDateTime();

                    if (client.DocumentDateTo == null)
                        client.DocumentDateTo = Clients_DocumentDateTo.ToDateTime();

                    if (string.IsNullOrWhiteSpace(client.DocumentReleased))
                        client.DocumentReleased = Clients_DocumentReleased;

                    if (employerF != null)
                    {
                        var partyRelationshipDetail = DbProvider.PartyRelationships(clientDetail.PartyID, 0, null).ToList();
                        var employerItem = partyRelationshipDetail.Where(p => p.RelatedPartyTypeCode == "ER").FirstOrDefault();
                        if (employerItem != null)
                        {
                            var employerID = DbProvider.ClientIDByPartyID(0, employerItem.RelatedPartyID);

                            client.Employer = employerItem.PartyName;
                            client.EmployerID = employerID;
                        }
                    }

                    fus.showImportedInfo = true;
                }
            }
            #endregion

            if (!string.IsNullOrEmpty(client.MailingAddress) || !string.IsNullOrEmpty(client.MailingPostalCode) || !string.IsNullOrEmpty(client.MailingCity))
                client.IsMailingDifferent = true;
        }
        #endregion
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region
        public static void Form_Read(this SectorInvestment sector, FormFUSCZ fus, AcroFields ff,
            FATextValue insitution,
            FATextValue kTFinancialTool,
            FATextValue kTFinancialToolType,
            FATextValue kTBuySell,
            FATextValue amountOne,
            FATextValue amountMulti,
            FATextValue kTCurrency,
            FASelectValue selClient

            )
        {
            sector.SectorCode = Constants.ContractFUSCZSectionTYPE.SECTOR_INVESTMENT;

            sector.KTFinancialTool = ff.MyGetField(kTFinancialTool);

            string toolType = ff.MyGetField(kTFinancialToolType);
            var financial = FUShelper.FUSListOptionsResource.Where(p => p.FUSListTypeID == Constants.FUSCZListTypes.FINANCIALTOOLS);
            if (!string.IsNullOrWhiteSpace(toolType))
            {
                toolType = "CZ" + toolType.ToUpper();
                sector.KTFinancialToolType = FUShelper.FUSListOptionsResource.Where(p => p.FUSListTypeID == Constants.FUSCZListTypes.FINANCIALTOOLS && toolType == p.FUSListOptionCode).Select(p => p.FUSListOptionCode).FirstOrDefault();
            }

            string buysell = ff.MyGetField(kTBuySell);
            if (buysell != null && buysell.ToLower().StartsWith("n"))
                sector.KTBuySell = "CZBUY";
            else if (buysell != null && buysell.ToLower().StartsWith("p"))
                sector.KTBuySell = "CZSELL";

            if (!string.IsNullOrWhiteSpace(ff.MyGetField(amountOne)))
            {
                sector.Amount = ff.MyGetField(amountOne);
                sector.KTInvestAmountTypeCode = "CZONETIME";
            }
            else if (!string.IsNullOrWhiteSpace(ff.MyGetField(amountMulti)))
            {
                sector.Amount = ff.MyGetField(amountMulti);
                sector.KTInvestAmountTypeCode = "CZMULTITIME";
            }

            string currency = ff.MyGetField(kTCurrency);
            if (!string.IsNullOrWhiteSpace(currency))
                sector.KTCurrency = FUShelper.FUSListOptionsResource.Where(p => p.FUSListTypeID == Constants.FUSCZListTypes.CURRENCY && p.FUSListOptionCode.IndexOf(currency, StringComparison.CurrentCultureIgnoreCase) >= 0).Select(p => p.FUSListOptionCode).FirstOrDefault();

            //True == zákazník, False == Partner
            var client = ff.MyGetField(selClient);
            if (!string.IsNullOrWhiteSpace(client))
            {
                if (client == "True")
                    sector.ClientTempID = fus.Client.VoD(v => v.ClientTempID);
                else if (client == "False")
                    sector.ClientTempID = fus.Partner.VoD(v => v.ClientTempID);
            }

            if (fus.FinancialInvestment.VoD(v => v.InvestDate) != null)
                sector.DateSigned = fus.FinancialInvestment.VoD(v => v.InvestDate);
        }

        #endregion
    }
    public static class Files
    {
        public static class FUS
        {
            public static readonly string AML = "FUS_AML_150527.pdf";
            public static readonly string AML_Draft = "FUS_AML_150527_nahled.pdf";

            //public static readonly string Attachment = "FUS_150408_priloha.pdf";
            //public static readonly string AttachmentDraft = "FUS_150408_priloha_nahled.pdf";

            //public static readonly string Raw = "FUS_150527.pdf";
            //public static readonly string Draft = "FUS_150527_nahled.pdf";

            public static readonly string Raw = "FUS_160308.pdf";
            public static readonly string Draft = "FUS_160308_nahled.pdf";

            public static readonly string Attachment = "FUS_160308_priloha.pdf";
            public static readonly string AttachmentDraft = "FUS_160308_priloha_nahled.pdf";

        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static class ZV
        {
            public static readonly string AML = "FUS_ZV_AML_150527.pdf";
            public static readonly string AML_Draft = "FUS_ZV_AML_150527_nahled.pdf";

            //public static readonly string Attachment = "FUS_ZV_150408_priloha.pdf";
            //public static readonly string AttachmentDraft = "FUS_ZV_150408_priloha_nahled.pdf";

            //public static readonly string Raw = "FUS_ZV_150527.pdf";
            //public static readonly string Draft = "FUS_ZV_150527_nahled.pdf";

            public static readonly string Attachment = "FUS_ZV_160308_priloha.pdf";
            public static readonly string AttachmentDraft = "FUS_ZV_160308_priloha_nahled.pdf";

            public static readonly string Raw = "FUS_ZV_160308.pdf";
            public static readonly string Draft = "FUS_ZV_160308_nahled.pdf";
        }

        public static class ADK
        {
            public static readonly string AML = "FUS_ADK_AML_160501.pdf";
            public static readonly string AML_Draft = "FUS_ADK_AML_160501_nahled.pdf";

            public static readonly string Raw = "FUS_ADK_160501.pdf";
            public static readonly string Draft = "FUS_ADK_160501_nahled.pdf";

            public static readonly string Attachment = "FUS_ADK_160501_priloha.pdf";
            public static readonly string AttachmentDraft = "FUS_ADK_160501_priloha_nahled.pdf";
        }
    }

    public static class DynFields
    {
        public static class AML_FUS
        {
            public static class TextFields
            {
                public static FATextValue T1 = new FATextValue("T1");
                public static FATextValue TX1 = new FATextValue("TX1");
                public static FATextValue T2 = new FATextValue("T2");
                public static FATextValue TX2 = new FATextValue("TX2");
                public static FATextValue T3 = new FATextValue("T3");
                public static FATextValue TX31 = new FATextValue("TX31");
                public static FATextValue TX32 = new FATextValue("TX32");
                public static FATextValue T258 = new FATextValue("T258");
                public static FATextValue T259 = new FATextValue("T259");
                public static FATextValue T260 = new FATextValue("T260");
                public static FATextValue T261 = new FATextValue("T261");
                public static FATextValue T262 = new FATextValue("T262");
                public static FATextValue T263 = new FATextValue("T263");
                //public static FATextValue T264 = new FATextValue("T264"); // podpis nepoužijeme
            }

            public static class CheckBoxFields
            {
                public static FAChBValue chb100X = new FAChBValue("chb100X", "Ano");
                public static FAChBValue chb103 = new FAChBValue("chb103", "Ano");
                public static FAChBValue chb103x = new FAChBValue("chb103x", "Ano");
                public static FAChBValue chb104 = new FAChBValue("chb104", "Ano");
                public static FAChBValue chb104x = new FAChBValue("chb104x", "Ano");
                public static FAChBValue chb105 = new FAChBValue("chb105", "Ano");
                public static FAChBValue chb105x = new FAChBValue("chb105x", "Ano");
                public static FAChBValue chb106 = new FAChBValue("chb106", "Ano");
                public static FAChBValue chb106x = new FAChBValue("chb106x", "Ano");
                public static FAChBValue chb107 = new FAChBValue("chb107", "Ano");
                public static FAChBValue chb107x = new FAChBValue("chb107x", "Ano");
                public static FAChBValue chb108x = new FAChBValue("chb108x", "Ano");
                public static FAChBValue chb108 = new FAChBValue("chb108", "Ano");
                public static FAChBValue chb229 = new FAChBValue("chb229", "Ano");
                public static FAChBValue chb230 = new FAChBValue("chb230", "Ano");
                public static FAChBValue chb231 = new FAChBValue("chb231", "Ano");
                public static FAChBValue chb232 = new FAChBValue("chb232", "Ano");
                public static FAChBValue chb233 = new FAChBValue("chb233", "Ano");
                public static FAChBValue chb234 = new FAChBValue("chb234", "Ano");
                public static FAChBValue chb235 = new FAChBValue("chb235", "Ano");
                public static FAChBValue chb236 = new FAChBValue("chb236", "Ano");
                public static FAChBValue chb237 = new FAChBValue("chb237", "Ano");
                public static FAChBValue chb238 = new FAChBValue("chb238", "Ano");
                public static FAChBValue chb239 = new FAChBValue("chb239", "Ano");
                public static FAChBValue chb240 = new FAChBValue("chb240", "Ano");
                public static FAChBValue chb241 = new FAChBValue("chb241", "Ano");
                public static FAChBValue chb242 = new FAChBValue("chb242", "Ano");
                public static FAChBValue chb243 = new FAChBValue("chb243", "Ano");
                public static FAChBValue chb244 = new FAChBValue("chb244", "Ano");
                public static FAChBValue chb245 = new FAChBValue("chb245", "Ano");
                public static FAChBValue chb246 = new FAChBValue("chb246", "Ano");
                public static FAChBValue chb247 = new FAChBValue("chb247", "Ano");
                public static FAChBValue chb248 = new FAChBValue("chb248", "Ano");
                public static FAChBValue chb249 = new FAChBValue("chb249", "Ano");
                public static FAChBValue chb250 = new FAChBValue("chb250", "Ano");
                public static FAChBValue chb251 = new FAChBValue("chb251", "Ano");
                public static FAChBValue chb252 = new FAChBValue("chb252", "Ano");
                public static FAChBValue chb253 = new FAChBValue("chb253", "Ano");
                public static FAChBValue chb254 = new FAChBValue("chb254", "Ano");
                public static FAChBValue chb255 = new FAChBValue("chb255", "Ano");
                public static FAChBValue chb256 = new FAChBValue("chb256", "Ano");
                public static FAChBValue chb100X2 = new FAChBValue("chb100X2", "Ano");
            }

            public static class RadioButtonFields
            {
                public static FASelectValue r258 = new FASelectValue("r258", new Dictionary<string, string>() { { "CZIRLESS500", "1" }, { "CZIRLESS1300", "2" }, { "CZIRMORE1300", "3" } });
                public static FASelectValue r259 = new FASelectValue("r259", new Dictionary<string, string>() { { "CZIRLESS500", "1" }, { "CZIRLESS1300", "2" }, { "CZIRMORE1300", "3" } });
                public static FASelectValue r260 = new FASelectValue("r260", new Dictionary<string, string>() { { "CZIILESS6", "1" }, { "CZIILESS17", "2" }, { "CZIIMORE17", "3" } });
                public static FASelectValue r261 = new FASelectValue("r261", new Dictionary<string, string>() { { "CZIILESS6", "1" }, { "CZIILESS17", "2" }, { "CZIIMORE17", "3" } });
                public static FASelectValue r262 = new FASelectValue("r262", new Dictionary<string, string>() { { "CZSRDEPEND", "1" }, { "CZSRINDEPEND", "2" }, { "CZSRCOMB", "3" } });
                public static FASelectValue r263 = new FASelectValue("r263", new Dictionary<string, string>() { { "CZSRDEPEND", "1" }, { "CZSRINDEPEND", "2" }, { "CZSRCOMB", "3" } });
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static class AML_ZV
        {
            public static class TextFields
            {
                public static FATextValue T1 = new FATextValue("T1");
                public static FATextValue T2 = new FATextValue("T2");
                public static FATextValue T3 = new FATextValue("T3");
                public static FATextValue T4 = new FATextValue("T4");
                public static FATextValue T5 = new FATextValue("T5");
                public static FATextValue T6 = new FATextValue("T6");
                public static FATextValue T7 = new FATextValue("T7");
                public static FATextValue T8 = new FATextValue("T8");
                public static FATextValue T9 = new FATextValue("T9");
                public static FATextValue T10 = new FATextValue("T10");
                public static FATextValue T11 = new FATextValue("T11");
            }

            public static class CheckBoxFields
            {
                public static FAChBValue chb72 = new FAChBValue("chb72", "Ano");
                public static FAChBValue chb73 = new FAChBValue("chb73", "Ano");
                public static FAChBValue chb74 = new FAChBValue("chb74", "Ano");
                public static FAChBValue chb77 = new FAChBValue("chb77", "Ano");
                public static FAChBValue chb78 = new FAChBValue("chb78", "Ano");
                public static FAChBValue chb79 = new FAChBValue("chb79", "Ano");
                public static FAChBValue chb80 = new FAChBValue("chb80", "Ano");
                public static FAChBValue chb81 = new FAChBValue("chb81", "Ano");
                public static FAChBValue chb82 = new FAChBValue("chb82", "Ano");
                public static FAChBValue chb83 = new FAChBValue("chb83", "Ano");
                public static FAChBValue chb84 = new FAChBValue("chb84", "Ano");
                public static FAChBValue chb85 = new FAChBValue("chb85", "Ano");
                public static FAChBValue chb86 = new FAChBValue("chb86", "Ano");
                public static FAChBValue chb87 = new FAChBValue("chb87", "Ano");
                public static FAChBValue chb100X = new FAChBValue("chb100X", "Ano");
                public static FAChBValue chb103 = new FAChBValue("chb103", "Ano");
                public static FAChBValue chb104 = new FAChBValue("chb104", "Ano");
                public static FAChBValue chb105 = new FAChBValue("chb105", "Ano");
                public static FAChBValue chb106 = new FAChBValue("chb106", "Ano");
                public static FAChBValue chb107 = new FAChBValue("chb107", "Ano");
                public static FAChBValue chb107X = new FAChBValue("chb107X", "Ano");
            }

            public static class RadioButtonFields
            {
                public static FASelectValue r258 = new FASelectValue("r258", new Dictionary<string, string>() { { "CZIRLESS500", "1" }, { "CZIRLESS1300", "2" }, { "CZIRMORE1300", "3" } });
                public static FASelectValue r259 = new FASelectValue("r259", new Dictionary<string, string>() { { "CZIILESS6", "1" }, { "CZIILESS17", "2" }, { "CZIIMORE17", "3" } });
                public static FASelectValue r262 = new FASelectValue("r262", new Dictionary<string, string>() { { "CZSRDEPEND", "1" }, { "CZSRINDEPEND", "2" }, { "CZSRCOMB", "3" } });
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static class FUS
        {
            public static class TextFields
            {
                public static FATextValue T1 = new FATextValue("T1");
                public static FATextValue T2 = new FATextValue("T2");
                public static FATextValue T3 = new FATextValue("T3");
                public static FATextValue T4 = new FATextValue("T4");
                public static FATextValue T5 = new FATextValue("T5");
                public static FATextValue T7 = new FATextValue("T7");
                public static FATextValue T8 = new FATextValue("T8");
                public static FATextValue T9 = new FATextValue("T9");
                public static FATextValue T10 = new FATextValue("T10");
                public static FATextValue T11 = new FATextValue("T11");
                public static FATextValue T12 = new FATextValue("T12");
                public static FATextValue T13 = new FATextValue("T13");
                public static FATextValue T14 = new FATextValue("T14");
                public static FATextValue T15 = new FATextValue("T15");
                public static FATextValue T16 = new FATextValue("T16");
                public static FATextValue T17 = new FATextValue("T17");
                public static FATextValue T18 = new FATextValue("T18");
                public static FATextValue T19 = new FATextValue("T19");
                public static FATextValue T20 = new FATextValue("T20");
                public static FATextValue T22 = new FATextValue("T22");
                public static FATextValue T23 = new FATextValue("T23");
                public static FATextValue T24 = new FATextValue("T24");
                public static FATextValue T26 = new FATextValue("T26");
                public static FATextValue T27 = new FATextValue("T27");
                public static FATextValue T28 = new FATextValue("T28");
                public static FATextValue T29 = new FATextValue("T29");
                public static FATextValue T30 = new FATextValue("T30");
                public static FATextValue T31 = new FATextValue("T31");
                public static FATextValue T32 = new FATextValue("T32");
                public static FATextValue T33 = new FATextValue("T33");
                public static FATextValue T34 = new FATextValue("T34");
                public static FATextValue T35 = new FATextValue("T35");
                public static FATextValue T36 = new FATextValue("T36");
                public static FATextValue T37 = new FATextValue("T37");
                public static FATextValue T38 = new FATextValue("T38");
                public static FATextValue T39 = new FATextValue("T39");
                public static FATextValue T40 = new FATextValue("T40");
                public static FATextValue T41 = new FATextValue("T41");
                public static FATextValue T42 = new FATextValue("T42");
                public static FATextValue T44 = new FATextValue("T44");
                public static FATextValue T45 = new FATextValue("T45");
                public static FATextValue T46 = new FATextValue("T46");
                public static FATextValue T48 = new FATextValue("T48");
                public static FATextValue T49 = new FATextValue("T49");
                public static FATextValue T50 = new FATextValue("T50");
                public static FATextValue T51 = new FATextValue("T51");
                public static FATextValue T52 = new FATextValue("T52");
                public static FATextValue T53 = new FATextValue("T53");
                public static FATextValue T54 = new FATextValue("T54");
                public static FATextValue T55 = new FATextValue("T55");
                public static FATextValue T56 = new FATextValue("T56");
                public static FATextValue T57 = new FATextValue("T57");
                public static FATextValue T58 = new FATextValue("T58");
                public static FATextValue T59 = new FATextValue("T59");
                public static FATextValue T60 = new FATextValue("T60");
                public static FATextValue T61 = new FATextValue("T61");
                public static FATextValue T62 = new FATextValue("T62");
                public static FATextValue T63 = new FATextValue("T63");
                public static FATextValue T65 = new FATextValue("T65");
                public static FATextValue T66 = new FATextValue("T66");
                public static FATextValue T67 = new FATextValue("T67");
                public static FATextValue T68 = new FATextValue("T68");
                public static FATextValue T69 = new FATextValue("T69");
                public static FATextValue T70 = new FATextValue("T70");
                public static FATextValue T71 = new FATextValue("T71");
                public static FATextValue T72 = new FATextValue("T72");
                public static FATextValue T73 = new FATextValue("T73");
                public static FATextValue T74 = new FATextValue("T74");
                public static FATextValue T75 = new FATextValue("T75");
                public static FATextValue T77 = new FATextValue("T77");
                public static FATextValue T78 = new FATextValue("T78");
                public static FATextValue T79 = new FATextValue("T79");
                public static FATextValue T80 = new FATextValue("T80");
                public static FATextValue T81 = new FATextValue("T81");
                public static FATextValue T82 = new FATextValue("T82");
                public static FATextValue T83 = new FATextValue("T83");
                public static FATextValue T84 = new FATextValue("T84");
                public static FATextValue T85 = new FATextValue("T85");
                public static FATextValue T86 = new FATextValue("T86");
                public static FATextValue T87 = new FATextValue("T87");
                public static FATextValue T89 = new FATextValue("T89");
                public static FATextValue T92 = new FATextValue("T92");
                public static FATextValue T93 = new FATextValue("T93");
                public static FATextValue T94 = new FATextValue("T94");
                public static FATextValue T95 = new FATextValue("T95");
                public static FATextValue T96 = new FATextValue("T96");
                public static FATextValue T97 = new FATextValue("T97");
                public static FATextValue T98 = new FATextValue("T98");
                public static FATextValue T163 = new FATextValue("T163");
                public static FATextValue T164 = new FATextValue("T164");
                public static FATextValue T165 = new FATextValue("T165");
                public static FATextValue T166 = new FATextValue("T166");
                public static FATextValue T167 = new FATextValue("T167");
                public static FATextValue T172 = new FATextValue("T172");
                public static FATextValue T173 = new FATextValue("T173");
                public static FATextValue T178 = new FATextValue("T178");
                public static FATextValue T179 = new FATextValue("T179");
                public static FATextValue T181 = new FATextValue("T181");
                public static FATextValue T182 = new FATextValue("T182");
                public static FATextValue T183 = new FATextValue("T183");
                public static FATextValue T184 = new FATextValue("T184");
                public static FATextValue T185 = new FATextValue("T185");
                public static FATextValue T186 = new FATextValue("T186");
                public static FATextValue T187 = new FATextValue("T187");
                public static FATextValue T188 = new FATextValue("T188");
                public static FATextValue T189 = new FATextValue("T189");
                public static FATextValue T190 = new FATextValue("T190");
                public static FATextValue T191 = new FATextValue("T191");
                public static FATextValue T192 = new FATextValue("T192");
                public static FATextValue T193 = new FATextValue("T193");
                public static FATextValue T194 = new FATextValue("T194");
                public static FATextValue T195 = new FATextValue("T195");
                public static FATextValue T196 = new FATextValue("T196");
                public static FATextValue T197 = new FATextValue("T197");
                public static FATextValue T198 = new FATextValue("T198");
                public static FATextValue T199 = new FATextValue("T199");
                public static FATextValue T200 = new FATextValue("T200");
                public static FATextValue T201 = new FATextValue("T201");
                public static FATextValue T202 = new FATextValue("T202");
                public static FATextValue T203 = new FATextValue("T203");
                public static FATextValue T204 = new FATextValue("T204");
                public static FATextValue T205 = new FATextValue("T205");
                public static FATextValue T206 = new FATextValue("T206");
                public static FATextValue T207 = new FATextValue("T207");
                public static FATextValue T208 = new FATextValue("T208");
                public static FATextValue T209 = new FATextValue("T209");
                public static FATextValue T210 = new FATextValue("T210");
                public static FATextValue T211 = new FATextValue("T211");
                public static FATextValue T212 = new FATextValue("T212");
                public static FATextValue T213 = new FATextValue("T213");
                public static FATextValue T214 = new FATextValue("T214");
                public static FATextValue T215 = new FATextValue("T215");
                public static FATextValue T216 = new FATextValue("T216");
                public static FATextValue T217 = new FATextValue("T217");
                public static FATextValue T218 = new FATextValue("T218");
                public static FATextValue T219 = new FATextValue("T219");
                public static FATextValue T220 = new FATextValue("T220");
                public static FATextValue T221 = new FATextValue("T221");
                public static FATextValue T222 = new FATextValue("T222");
                public static FATextValue T223 = new FATextValue("T223");
                public static FATextValue T224 = new FATextValue("T224");
                public static FATextValue T225 = new FATextValue("T225");
                public static FATextValue T222_X = new FATextValue("T222_X");
                public static FATextValue T223_X = new FATextValue("T223_X");
            }

            public static class CheckBoxFields
            {
                public static FAChBValue chb91 = new FAChBValue("chb91", "Ano");
                public static FAChBValue chb99 = new FAChBValue("chb99", "Ano");
                public static FAChBValue chb100 = new FAChBValue("chb100", "Ano");
                public static FAChBValue chb101 = new FAChBValue("chb101", "Ano");
                public static FAChBValue chb102 = new FAChBValue("chb102", "Ano");
                public static FAChBValue chb105 = new FAChBValue("chb105", "Ano");
                public static FAChBValue chb106 = new FAChBValue("chb106", "Ano");
                public static FAChBValue chb107 = new FAChBValue("chb107", "Ano");
                public static FAChBValue chb108 = new FAChBValue("chb108", "Ano");
                public static FAChBValue chb109 = new FAChBValue("chb109", "Ano");
                public static FAChBValue chb110 = new FAChBValue("chb110", "Ano");
                public static FAChBValue chb111 = new FAChBValue("chb111", "Ano");
                public static FAChBValue chb112 = new FAChBValue("chb112", "Ano");
                public static FAChBValue chb113 = new FAChBValue("chb113", "Ano");
                public static FAChBValue chb114 = new FAChBValue("chb114", "Ano");
                public static FAChBValue chb115 = new FAChBValue("chb115", "Ano");
                public static FAChBValue chb116 = new FAChBValue("chb116", "Ano");
                public static FAChBValue chb117 = new FAChBValue("chb117", "Ano");
                public static FAChBValue chb118 = new FAChBValue("chb118", "Ano");
                public static FAChBValue chb119 = new FAChBValue("chb119", "Ano");
                public static FAChBValue chb120 = new FAChBValue("chb120", "Ano");
                public static FAChBValue chb121 = new FAChBValue("chb121", "Ano");
                public static FAChBValue chb122 = new FAChBValue("chb122", "Ano");
                public static FAChBValue chb123 = new FAChBValue("chb123", "Ano");
                public static FAChBValue chb124 = new FAChBValue("chb124", "Ano");
                public static FAChBValue chb125 = new FAChBValue("chb125", "Ano");
                public static FAChBValue chb126 = new FAChBValue("chb126", "Ano");
                public static FAChBValue chb127 = new FAChBValue("chb127", "Ano");
                public static FAChBValue chb128 = new FAChBValue("chb128", "Ano");
                public static FAChBValue chb129 = new FAChBValue("chb129", "Ano");
                public static FAChBValue chb130 = new FAChBValue("chb130", "Ano");
                public static FAChBValue chb131 = new FAChBValue("chb131", "Ano");
                public static FAChBValue chb132 = new FAChBValue("chb132", "Ano");
                public static FAChBValue chb133 = new FAChBValue("chb133", "Ano");
                public static FAChBValue chb134 = new FAChBValue("chb134", "Ano");
                public static FAChBValue chb135 = new FAChBValue("chb135", "Ano");
                public static FAChBValue chb136 = new FAChBValue("chb136", "Ano");
                public static FAChBValue chb137 = new FAChBValue("chb137", "Ano");
                public static FAChBValue chb138 = new FAChBValue("chb138", "Ano");
                public static FAChBValue chb139 = new FAChBValue("chb139", "Ano");
                public static FAChBValue chb140 = new FAChBValue("chb140", "Ano");
                public static FAChBValue chb141 = new FAChBValue("chb141", "Ano");
                public static FAChBValue chb142 = new FAChBValue("chb142", "Ano");
                public static FAChBValue chb143 = new FAChBValue("chb143", "Ano");
                public static FAChBValue chb144 = new FAChBValue("chb144", "Ano");
                public static FAChBValue chb145 = new FAChBValue("chb145", "Ano");
                public static FAChBValue chb146 = new FAChBValue("chb146", "Ano");
                public static FAChBValue chb147 = new FAChBValue("chb147", "Ano");
                public static FAChBValue chb148 = new FAChBValue("chb148", "Ano");
                public static FAChBValue chb149 = new FAChBValue("chb149", "Ano");
                public static FAChBValue chb150 = new FAChBValue("chb150", "Ano");
                public static FAChBValue chb151 = new FAChBValue("chb151", "Ano");
                public static FAChBValue chb152 = new FAChBValue("chb152", "Ano");
                public static FAChBValue chb153 = new FAChBValue("chb153", "Ano");
                public static FAChBValue chb154 = new FAChBValue("chb154", "Ano");
                public static FAChBValue chb155 = new FAChBValue("chb155", "Ano");
                public static FAChBValue chb156 = new FAChBValue("chb156", "Ano");
                public static FAChBValue chb157 = new FAChBValue("chb157", "Ano");
                public static FAChBValue chb158 = new FAChBValue("chb158", "Ano");
                public static FAChBValue chb159 = new FAChBValue("chb159", "Ano");
                public static FAChBValue chb160 = new FAChBValue("chb160", "Ano");
                public static FAChBValue chb161 = new FAChBValue("chb161", "Ano");
                public static FAChBValue chb162 = new FAChBValue("chb162", "Ano");
                public static FAChBValue chb168 = new FAChBValue("chb168", "Ano");
                public static FAChBValue chb169 = new FAChBValue("chb169", "Ano");
                public static FAChBValue chb170 = new FAChBValue("chb170", "Ano");
                public static FAChBValue chb171 = new FAChBValue("chb171", "Ano");

                public static FAChBValue r37_1 = new FAChBValue("r37_1", "Ano");
                public static FAChBValue r37_2 = new FAChBValue("r37_2", "Ano");
                public static FAChBValue r37_3 = new FAChBValue("r37_3", "Ano");

                public static FAChBValue r37_1_X = new FAChBValue("r37_1_X", "Ano");
                public static FAChBValue r37_2_X = new FAChBValue("r37_2_X", "Ano");
                public static FAChBValue r37_3_X = new FAChBValue("r37_3_X", "Ano");
                public static FAChBValue r37_4_X = new FAChBValue("r37_4_X", "Ano");
                public static FAChBValue r37_5_X = new FAChBValue("r37_5_X", "Ano");
                public static FAChBValue r37_6_X = new FAChBValue("r37_6_X", "Ano");
                public static FAChBValue r37_7_X = new FAChBValue("r37_7_X", "Ano");
                public static FAChBValue r37_8_X = new FAChBValue("r37_8_X", "Ano");
                public static FAChBValue r37_9_X = new FAChBValue("r37_9_X", "Ano");

                public static FAChBValue r45 = new FAChBValue("r45", "Ano");
                public static FAChBValue r46 = new FAChBValue("r46", "Ano");

                public static FAChBValue r45_2 = new FAChBValue("r45_2", "Ano");
                public static FAChBValue r46_2 = new FAChBValue("r46_2", "Ano");
            }

            public static class RadioButtonFields
            {
                public static FASelectValue r1 = new FASelectValue("r1", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r2 = new FASelectValue("r2", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r3 = new FASelectValue("r3", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r4 = new FASelectValue("r4", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r5 = new FASelectValue("r5", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r6 = new FASelectValue("r6", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r7 = new FASelectValue("r7", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r8 = new FASelectValue("r8", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r9 = new FASelectValue("r9", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r10 = new FASelectValue("r10", new Dictionary<string, string>() { { "PERSONALLY", "1" }, { "ATTORNEY", "2" } });
                public static FASelectValue r11 = new FASelectValue("r11", new Dictionary<string, string>() { { "PERSONALLY", "1" }, { "ATTORNEY", "2" } });
                public static FASelectValue r12 = new FASelectValue("r12", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r14 = new FASelectValue("r14", new Dictionary<string, string>() { { "WASSPECIFIED", "1" }, { "REFUSESET", "2" }, { "WASNTSPECIFIED", "3" } });
                public static FASelectValue r15 = new FASelectValue("r15", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r16 = new FASelectValue("r16", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r17 = new FASelectValue("r17", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r18 = new FASelectValue("r18", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r19 = new FASelectValue("r19", new Dictionary<string, string>() { { "M", "1" }, { "F", "2" } });
                public static FASelectValue r20 = new FASelectValue("r20", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r21 = new FASelectValue("r21", new Dictionary<string, string>() { { "M", "1" }, { "F", "2" } });
                public static FASelectValue r22 = new FASelectValue("r22", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r23 = new FASelectValue("r23", new Dictionary<string, string>() { { "M", "1" }, { "F", "2" } });
                public static FASelectValue r24 = new FASelectValue("r24", new Dictionary<string, string>() { { "M", "1" }, { "F", "2" } });
                public static FASelectValue r25 = new FASelectValue("r25", new Dictionary<string, string>() { { "M", "1" }, { "F", "2" } });
                public static FASelectValue r26 = new FASelectValue("r26", new Dictionary<string, string>() { { "Q1OptionA", "1" }, { "Q1OptionB", "2" }, { "Q1OptionC", "3" } });
                public static FASelectValue r27 = new FASelectValue("r27", new Dictionary<string, string>() { { "Q1OptionA", "1" }, { "Q1OptionB", "2" }, { "Q1OptionC", "3" } });
                public static FASelectValue r28 = new FASelectValue("r28", new Dictionary<string, string>() { { "Q3OptionA", "1" }, { "Q3OptionB", "2" }, { "Q3OptionC", "3" } });
                public static FASelectValue r29 = new FASelectValue("r29", new Dictionary<string, string>() { { "Q3OptionA", "1" }, { "Q3OptionB", "2" }, { "Q3OptionC", "3" } });
                public static FASelectValue r30 = new FASelectValue("r30", new Dictionary<string, string>() { { "Q2OptionA", "1" }, { "Q2OptionB", "2" }, { "Q2OptionC", "3" }, { "Q2OptionD", "4" } });
                public static FASelectValue r31 = new FASelectValue("r31", new Dictionary<string, string>() { { "Q2OptionA", "1" }, { "Q2OptionB", "2" }, { "Q2OptionC", "3" }, { "Q2OptionD", "4" } });
                public static FASelectValue r32 = new FASelectValue("r32", new Dictionary<string, string>() { { "Q4OptionA", "1" }, { "Q4OptionB", "2" }, { "Q4OptionC", "3" }, { "Q4OptionD", "4" } });
                public static FASelectValue r33 = new FASelectValue("r33", new Dictionary<string, string>() { { "Q4OptionA", "1" }, { "Q4OptionB", "2" }, { "Q4OptionC", "3" }, { "Q4OptionD", "4" } });
                public static FASelectValue r34 = new FASelectValue("r34", new Dictionary<string, string>() { { "Q5OptionA", "1" }, { "Q5OptionB", "2" }, { "Q5OptionC", "3" }, { "Q5OptionD", "4" } });
                public static FASelectValue r35 = new FASelectValue("r35", new Dictionary<string, string>() { { "Q5OptionA", "1" }, { "Q5OptionB", "2" }, { "Q5OptionC", "3" }, { "Q5OptionD", "4" } });
                public static FASelectValue r36 = new FASelectValue("r36", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r37 = new FASelectValue("r37", new Dictionary<string, string>() { { "DPS", "1" }, { "TF", "2" } });
                public static FASelectValue x37 = new FASelectValue("x37", new Dictionary<string, string>() { { "DPS", "1" }, { "TF", "2" } });
                //public static FASelectValue r38 = new FASelectValue("r38", new Dictionary<string, string>() { { "CZDSKONZE", "1" }, { "CZDSVYVAZ", "2" }, { "CZDSDYNAM", "3" }, { "CZDSOTHER", "4" } });
                //public static FASelectValue r39 = new FASelectValue("r39", new Dictionary<string, string>() { { "CZDSKONZE", "1" }, { "CZDSVYVAZ", "2" }, { "CZDSDYNAM", "3" }, { "CZDSOTHER", "4" } });
                //public static FASelectValue r40 = new FASelectValue("r40", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                //public static FASelectValue r41 = new FASelectValue("r41", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                //public static FASelectValue r42 = new FASelectValue("r42", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                //public static FASelectValue r43 = new FASelectValue("r43", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });


                public static FASelectValue r38 = new FASelectValue("r38", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r39 = new FASelectValue("r39", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r40 = new FASelectValue("r40", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r41 = new FASelectValue("r41", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r42 = new FASelectValue("r42", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r43 = new FASelectValue("r43", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });

                public static FASelectValue r44 = new FASelectValue("r44", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r47 = new FASelectValue("r47", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r175 = new FASelectValue("r175", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r176 = new FASelectValue("r176", new Dictionary<string, string>() { { "CZQTRULY", "1" }, { "CZQREFUSE", "2" }, { "CZINVESTORINEXPERIENCED", "3" }, { "CZINVESTORLOW", "4" }, { "CZINVESTORMEDIUM", "5" }, { "CZINVESTORHIGH", "6" } });
                public static FASelectValue r177 = new FASelectValue("r177", new Dictionary<string, string>() { { "CZQTRULY", "1" }, { "CZQREFUSE", "2" }, { "CZINVESTORINEXPERIENCED", "3" }, { "CZINVESTORLOW", "4" }, { "CZINVESTORMEDIUM", "5" }, { "CZINVESTORHIGH", "6" } });

                public static FASelectValue r178 = new FASelectValue("r178", new Dictionary<string, string>() { { "CZQ1NONE", "1" }, { "CZQ1ECONOMIC", "2" }, { "CZQ1FINANCIAL", "3" } });
                public static FASelectValue r179 = new FASelectValue("r179", new Dictionary<string, string>() { { "CZQ1NONE", "1" }, { "CZQ1ECONOMIC", "2" }, { "CZQ1FINANCIAL", "3" } });
                public static FASelectValue r180 = new FASelectValue("r180", new Dictionary<string, string>() { { "CZQ2NONE", "1" }, { "CZQ2WORKLESS1Y", "2" }, { "CZQ2WORKING", "3" } });
                public static FASelectValue r181 = new FASelectValue("r181", new Dictionary<string, string>() { { "CZQ2NONE", "1" }, { "CZQ2WORKLESS1Y", "2" }, { "CZQ2WORKING", "3" } });
                public static FASelectValue r182 = new FASelectValue("r182", new Dictionary<string, string>() { { "CZQ3DONTKNOW", "1" }, { "CZQ3UNDERSTAND", "2" }, { "CZQ3UBONDS", "3" }, { "CZQ3UINVESTING", "4" }, { "CZQ3STOCKS", "5" } });
                public static FASelectValue r183 = new FASelectValue("r183", new Dictionary<string, string>() { { "CZQ3DONTKNOW", "1" }, { "CZQ3UNDERSTAND", "2" }, { "CZQ3UBONDS", "3" }, { "CZQ3UINVESTING", "4" }, { "CZQ3STOCKS", "5" } });
                public static FASelectValue r184 = new FASelectValue("r184", new Dictionary<string, string>() { { "CZQ4ACCOUNT", "1" }, { "CZQ4ONLYMMF", "2" }, { "CZQ4MMFMORE", "3" }, { "CZQ4STOCKS", "4" }, { "CZQ4OCP", "5" } });
                public static FASelectValue r185 = new FASelectValue("r185", new Dictionary<string, string>() { { "CZQ4ACCOUNT", "1" }, { "CZQ4ONLYMMF", "2" }, { "CZQ4MMFMORE", "3" }, { "CZQ4STOCKS", "4" }, { "CZQ4OCP", "5" } });
                public static FASelectValue r186 = new FASelectValue("r186", new Dictionary<string, string>() { { "CZQ6NONE", "1" }, { "CZQ6ONCEY", "2" }, { "CZQ6MORE1Y", "3" }, { "CZQ6MONTHLY", "4" }, { "CZQ6WEEKLY", "5" } });
                public static FASelectValue r187 = new FASelectValue("r187", new Dictionary<string, string>() { { "CZQ6NONE", "1" }, { "CZQ6ONCEY", "2" }, { "CZQ6MORE1Y", "3" }, { "CZQ6MONTHLY", "4" }, { "CZQ6WEEKLY", "5" } });
                public static FASelectValue r188 = new FASelectValue("r188", new Dictionary<string, string>() { { "CZQ5FIRSTINVEST", "1" }, { "CZQ5LESS1Y", "2" }, { "CZQ5B2A5Y", "3" }, { "CZQ5MORE5Y", "4" } });
                public static FASelectValue r189 = new FASelectValue("r189", new Dictionary<string, string>() { { "CZQ5FIRSTINVEST", "1" }, { "CZQ5LESS1Y", "2" }, { "CZQ5B2A5Y", "3" }, { "CZQ5MORE5Y", "4" } });

                public static FASelectValue r190 = new FASelectValue("r190", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r191 = new FASelectValue("r191", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r192 = new FASelectValue("r192", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r193 = new FASelectValue("r193", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r194 = new FASelectValue("r194", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static class ZV
        {
            public static class TextFields
            {
                public static FATextValue T1 = new FATextValue("T1");
                public static FATextValue T2 = new FATextValue("T2");
                public static FATextValue T3 = new FATextValue("T3");
                public static FATextValue T4 = new FATextValue("T4");
                public static FATextValue T5 = new FATextValue("T5");
                public static FATextValue T7 = new FATextValue("T7");
                public static FATextValue T8 = new FATextValue("T8");
                public static FATextValue T9 = new FATextValue("T9");
                public static FATextValue T10 = new FATextValue("T10");
                public static FATextValue T11 = new FATextValue("T11");
                public static FATextValue T12 = new FATextValue("T12");
                public static FATextValue T13 = new FATextValue("T13");
                public static FATextValue T14 = new FATextValue("T14");
                public static FATextValue T15 = new FATextValue("T15");
                public static FATextValue T16 = new FATextValue("T16");
                public static FATextValue T17 = new FATextValue("T17");
                public static FATextValue T19 = new FATextValue("T19");
                public static FATextValue T20 = new FATextValue("T20");
                public static FATextValue T21 = new FATextValue("T21");
                public static FATextValue T23 = new FATextValue("T23");
                public static FATextValue T24 = new FATextValue("T24");
                public static FATextValue T25 = new FATextValue("T25");
                public static FATextValue T26 = new FATextValue("T26");
                public static FATextValue T27 = new FATextValue("T27");
                public static FATextValue T28 = new FATextValue("T28");
                public static FATextValue T29 = new FATextValue("T29");
                public static FATextValue T34 = new FATextValue("T34");
                public static FATextValue T35 = new FATextValue("T35");
                public static FATextValue T36 = new FATextValue("T36");
                public static FATextValue T37 = new FATextValue("T37");
                public static FATextValue T38 = new FATextValue("T38");
                public static FATextValue T39 = new FATextValue("T39");
                public static FATextValue T40 = new FATextValue("T40");
                public static FATextValue T54 = new FATextValue("T54");
                public static FATextValue T55 = new FATextValue("T55");
                public static FATextValue T56 = new FATextValue("T56");
                public static FATextValue T57 = new FATextValue("T57");
                public static FATextValue T59 = new FATextValue("T59");
                public static FATextValue T60 = new FATextValue("T60");
                public static FATextValue T61 = new FATextValue("T61");
                public static FATextValue T62 = new FATextValue("T62");
                public static FATextValue T63 = new FATextValue("T63");
                public static FATextValue T68 = new FATextValue("T68");
                public static FATextValue T69 = new FATextValue("T69");
                public static FATextValue T70 = new FATextValue("T70");
                public static FATextValue T71 = new FATextValue("T71");
                public static FATextValue T72 = new FATextValue("T72");
                public static FATextValue T73 = new FATextValue("T73");
                public static FATextValue T112 = new FATextValue("T112");
            }

            public static class CheckBoxFields
            {
                public static FAChBValue chb30 = new FAChBValue("chb30", "Ano");
                public static FAChBValue chb32 = new FAChBValue("chb32", "Ano");
                public static FAChBValue chb33 = new FAChBValue("chb33", "Ano");
                public static FAChBValue chb41 = new FAChBValue("chb41", "Ano");
                public static FAChBValue chb42 = new FAChBValue("chb42", "Ano");
                public static FAChBValue chb43 = new FAChBValue("chb43", "Ano");
                public static FAChBValue chb44 = new FAChBValue("chb44", "Ano");
                public static FAChBValue chb45 = new FAChBValue("chb45", "Ano");
                public static FAChBValue chb46 = new FAChBValue("chb46", "Ano");
                public static FAChBValue chb47 = new FAChBValue("chb47", "Ano");
                public static FAChBValue chb48 = new FAChBValue("chb48", "Ano");
                public static FAChBValue chb49 = new FAChBValue("chb49", "Ano");
                public static FAChBValue chb50 = new FAChBValue("chb50", "Ano");
                public static FAChBValue chb51 = new FAChBValue("chb51", "Ano");
                public static FAChBValue chb52 = new FAChBValue("chb52", "Ano");
                public static FAChBValue chb53 = new FAChBValue("chb53", "Ano");
                public static FAChBValue chb56 = new FAChBValue("chb56", "Ano");
                public static FAChBValue chb57 = new FAChBValue("chb57", "Ano");
                public static FAChBValue chb58 = new FAChBValue("chb58", "Ano");
                public static FAChBValue chb64 = new FAChBValue("chb64", "Ano");
                public static FAChBValue chb65 = new FAChBValue("chb65", "Ano");
                public static FAChBValue chb66 = new FAChBValue("chb66", "Ano");
                public static FAChBValue chb67 = new FAChBValue("chb67", "Ano");
                public static FAChBValue r22_1 = new FAChBValue("r22_1", "Ano");
                public static FAChBValue r22_2 = new FAChBValue("r22_2", "Ano");
                public static FAChBValue r23_1 = new FAChBValue("r23_1", "Ano");
                public static FAChBValue r23_2 = new FAChBValue("r23_2", "Ano");
                public static FAChBValue r24_1 = new FAChBValue("r24_1", "Ano");
                public static FAChBValue r24_2 = new FAChBValue("r24_2", "Ano");

                public static FAChBValue r37_1_X = new FAChBValue("r37_1_X", "Ano");
                public static FAChBValue r37_2_X = new FAChBValue("r37_2_X", "Ano");
                public static FAChBValue r37_3_X = new FAChBValue("r37_3_X", "Ano");
                public static FAChBValue r37_4_X = new FAChBValue("r37_4_X", "Ano");
                public static FAChBValue r37_5_X = new FAChBValue("r37_5_X", "Ano");
            }

            public static class RadioButtonFields
            {
                public static FASelectValue r6 = new FASelectValue("r6", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r7 = new FASelectValue("r7", new Dictionary<string, string>() { { "PERSONALLY", "1" }, { "ATTORNEY", "2" } });
                public static FASelectValue r8 = new FASelectValue("r8", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r9 = new FASelectValue("r9", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r10 = new FASelectValue("r10", new Dictionary<string, string>() { { "M", "1" }, { "F", "2" } });
                public static FASelectValue r_X = new FASelectValue("r_X", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });
                public static FASelectValue r11 = new FASelectValue("r11", new Dictionary<string, string>() { { "Q1OptionA", "1" }, { "Q1OptionB", "2" }, { "Q1OptionC", "3" } });
                public static FASelectValue r12 = new FASelectValue("r12", new Dictionary<string, string>() { { "Q2OptionA", "1" }, { "Q2OptionB", "2" }, { "Q2OptionC", "3" }, { "Q2OptionD", "4" } });
                public static FASelectValue r13 = new FASelectValue("r13", new Dictionary<string, string>() { { "Q3OptionA", "1" }, { "Q3OptionB", "2" }, { "Q3OptionC", "3" } });
                public static FASelectValue r14 = new FASelectValue("r14", new Dictionary<string, string>() { { "Q4OptionA", "1" }, { "Q4OptionB", "2" }, { "Q4OptionC", "3" }, { "Q4OptionD", "4" } });
                public static FASelectValue r15 = new FASelectValue("r15", new Dictionary<string, string>() { { "Q5OptionA", "1" }, { "Q5OptionB", "2" }, { "Q5OptionC", "3" }, { "Q5OptionD", "4" } });
                public static FASelectValue r16 = new FASelectValue("r16", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                //public static FASelectValue r17 = new FASelectValue("r17", new Dictionary<string, string>() { { "CZDSKONZE", "1" }, { "CZDSVYVAZ", "2" }, { "CZDSDYNAM", "3" }, { "CZDSOTHER", "4" } });
                public static FASelectValue r17 = new FASelectValue("r17", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r18 = new FASelectValue("r18", new Dictionary<string, string>() { { "DPS", "1" }, { "TF", "2" } });
                public static FASelectValue r19 = new FASelectValue("r19", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r20 = new FASelectValue("r20", new Dictionary<string, string>() { { "CZDPSKON", "1" }, { "CZDPSOTH", "2" } });
                public static FASelectValue r21 = new FASelectValue("r21", new Dictionary<string, string>() { { "False", "1" }, { "True", "2" } });
                public static FASelectValue r25 = new FASelectValue("r25", new Dictionary<string, string>() { { "True", "1" }, { "False", "2" } });

            }
        }
    }
}
