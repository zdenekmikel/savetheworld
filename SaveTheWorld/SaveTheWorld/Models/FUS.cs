﻿using System;

namespace SaveTheWorld.Models
{
    public static class FusDocumentStatuses
    {
        public const int Undefined = 0;
        public const int ProtocolDownloaded = 1;
    }

    public static class FUSStatuses
    {
        public const int InProgress = 1;
        public const int Closed = 2;
        public const int Released = 3;
        public const int ClosedAMLRequired = 4;
        public const int ClosedConverted = 5;
        public const int ClosedUnrealised = 6;
        public const int ClosedConvertedBO = 7;
        public const int ClosedConvertedAmlRequired = 8;
    }

    public class FUS
    {
        public Int32? FUSID { get; set; }

        public Int32 AgentID { get; set; }

        public DateTime DateCreated { get; set; }

        public Int32 CreatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        public Int32 UpdatedBy { get; set; }

        public Boolean Active { get; set; }

        public String FUSTypeCode { get; set; }

        public Int32 FUSStatusID { get; set; }

        public string XmlContent { get; set; }

        public int? CIMasterID { get; set; }

        public string ClientPartyCode { get; set; }

        public Boolean Imported { get; set; }

        public int? TicketID { get; set; }

        /// <summary>2015-07-17 - DT - #6187</summary>
        private byte _docStatus = FusDocumentStatuses.Undefined;
        public byte? DocStatus
        {
            get
            {
                return this._docStatus;
            }
            set
            {
                if (!value.HasValue)
                    this._docStatus = FusDocumentStatuses.Undefined;
                else
                    this._docStatus = value.Value;
            }
        }

        // #6225
        public int? ParentAgentID { get; set; }

        public int? OriginalOpportunityID { get; set; }
    }
}
