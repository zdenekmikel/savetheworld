﻿using SaveTheWorld.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using static SaveTheWorld.Models.FusHelper;

namespace SaveTheWorld.Models
{
    [Serializable]
    public class FormFUSCZ
    {
        public FormFUSCZ()
        {
            FUSForClient = true;
        }

        public string PageFUS { get; set; }

        public string LastPageFUSSaved { get; set; }

        public int? FUSID { get; set; }

        public string FUSType { get; set; }

        public int? Version { get; set; }

        public bool WasFUSUpdated { get; set; }

        public bool WasProtocolPrinted { get; set; }

        public bool FirstStepEmailsSent { get; set; }

        public int? PartnerCount { get; set; }

        //////[Resource("FUSCZ.StandardFUS.ChildrenCount")]
        public int? ChildrenCount { get; set; }

        public DateTime? FirstSmsFusCloseDate { get; set; }

        public AgentDetail AgentDetail { get; set; }

        public OverallInfo OverallInfo { get; set; }

        public Client Client { get; set; }

        public Partner Partner { get; set; }

        public List<Partner> Partners { get; set; }

        public List<Child> Childs { get; set; }

        public Announcement Announcement { get; set; }

        public FinancialDataVerification FinancialDataVerification { get; set; }

        public FinancialDsDps FinancialDsDps { get; set; }

        // #6758
        //public FinancialInsurance FinancialInsurance { get; set; }

        public FinancialInvestment FinancialInvestment { get; set; }

        public InvestmentInstrucions InvestmentInstrucions { get; set; }

        public List<Sector> Sectors { get; set; }

        public List<SectorInvestment> SectorInvestments { get; set; }

        public List<SectorInsurance> SectorInsurances { get; set; }

        public FinalStatementSignatures FinalStatementSignatures { get; set; }


        //[Editable(false)]
        //////[Resource("FUSCZ.StandardFUS.FUSForClient")]
        //[BooleanMustBeTrue(ErrorMessageResourceName = "FUSCZ.FUSForClient")]
        public bool FUSForClient { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FUSForPartner")]
        public bool FUSForPartner { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FUSForChild")]
        public bool FUSForChild { get; set; }

        //Spider
        //////[Resource("FUSCZ.StandardFUS.FSBuildSaving")]
        public bool FSBuildSaving { get; set; }

        public BoolClientPartnerChild FSBuildSavingSet { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FSInsuranceLive")]
        public bool FSInsuranceLive { get; set; }

        public BoolClientPartnerChild FSInsuranceLiveSet { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FSTermDepositsOrOtherJT")]
        public bool FSTermDepositsOrOtherJT { get; set; }

        public BoolClientPartnerChild FSTermDepositsOrOtherJTSet { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FSInsuranceLiveOther")]
        public bool FSInsuranceLiveOther { get; set; }

        public BoolClientPartnerChild FSInsuranceLiveOtherSet { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FSInsurancePersonal")]
        public bool FSInsurancePersonal { get; set; }

        public BoolClientPartnerChild FSInsurancePersonalSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSInsuranceProperty")]
        public bool FSInsuranceProperty { get; set; }

        public BoolClientPartnerChild FSInsurancePropertySet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSInsuranceLiability")]
        public bool FSInsuranceLiability { get; set; }

        public BoolClientPartnerChild FSInsuranceLiabilitySet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSInsuranceTravel")]
        public bool FSInsuranceTravel { get; set; }

        public BoolClientPartnerChild FSInsuranceTravelSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSInsuranceOthers")]
        public bool FSInsuranceOthers { get; set; }

        public BoolClientPartnerChild FSInsuranceOthersSet { get; set; }

        //////[Resource("FUSCZ.StandardFUS.FSLeasingLoan")]
        public bool FSLeasingLoan { get { return FSLoanConsumer || FSLoanOther; } }

        ////[Resource("FUSCZ.StandardFUS.FSSavingDS")]
        public bool FSSavingDS { get; set; }

        //public BoolClientPartnerChild FSSavingDSSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSSavingDPS")]
        public bool FSSavingDPS { get; set; }

        public BoolClientPartnerChild FSSavingDPSSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSSavingIncreaseDPS")]
        public bool FSSavingIncreaseDPS { get; set; }

        public BoolClientPartnerChild FSSavingIncreaseDPSSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSSavingTFToDPS")]
        public bool FSSavingTFToDPS { get; set; }

        public BoolClientPartnerChild FSSavingTFToDPSSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSSavingIncreaseTF")]
        public bool FSSavingIncreaseTF { get; set; }

        public BoolClientPartnerChild FSSavingIncreaseTFSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSInvestment")]
        public bool FSInvestment { get; set; }

        public BoolClientPartnerChild FSInvestmentSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSOthers")]
        public bool FSOthers { get; set; }

        public BoolClientPartnerChild FSOthersSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSConsumerLoan")]
        public bool FSLoanConsumer { get; set; }

        public BoolClientPartnerChild FSLoanConsumerSet { get; set; }

        ////[Resource("FUSCZ.StandardFUS.FSLoanOther")]
        public bool FSLoanOther { get; set; }

        public BoolClientPartnerChild FSLoanOtherSet { get; set; }


        public bool showImportedInfo { get; set; }

        private DateTime? _minSectorDate;
        public DateTime? MinSectorDate
        {
            get
            {
                if (_minSectorDate == null)
                {
                    var listDates = GetSectorDates(this);
                    _minSectorDate = listDates.Where(p => p != null).Select(p => p).Min();
                }
                return _minSectorDate;
            }
            set
            {
                _minSectorDate = value;
            }
        }
        /*
        FSBuildSaving
        FSInsuranceLive 
        FSInsuranceLiveOther
        FSInsurancePersonal 
        FSInsuranceProperty 
        FSInsuranceLiability
        FSInsuranceTravel 
        FSInsuranceOthers
        FSSavingDS 
        FSSavingDPS 
        FSInvestment 
        FSOthers 
        FSLoanConsumer
        FSLoanOther
        */

        #region Properties RO
        public SealedConstants.PersonType ClientType()
        {
            var personType = OverallInfo.ValueOrDefault(p => p.ClientType);

            if (SealedConstants.PersonType.Individual.Value == personType)
                return SealedConstants.PersonType.Individual;
            else if (SealedConstants.PersonType.Corporation.Value == personType)
                return SealedConstants.PersonType.Corporation;
            else if (SealedConstants.PersonType.IndividualBusinessman.Value == personType)
                return SealedConstants.PersonType.IndividualBusinessman;

            return null;
        }

        public SealedConstants.PersonType PartnerType()
        {
            var personType = OverallInfo.ValueOrDefault(p => p.PartnerType);

            if (SealedConstants.PersonType.Individual.Value == personType)
                return SealedConstants.PersonType.Individual;
            else if (SealedConstants.PersonType.Corporation.Value == personType)
                return SealedConstants.PersonType.Corporation;
            else if (SealedConstants.PersonType.IndividualBusinessman.Value == personType)
                return SealedConstants.PersonType.IndividualBusinessman;

            return null;
        }

        public ShowSection GetShowSection
        {
            get
            {
                return new ShowSection(this);
            }
        }

        public bool ClientIsSingle { get { return !FUSForPartner && !FUSForChild; } }

        public bool HasFusDynamic { get { return true; } }

        public bool IsSMSType { get { return FUSType == Constants.FUSCZTypes.SMS || FUSType == Constants.FUSCZTypes.SMSZV; } }

        public bool IsSMSStandardType { get { return FUSType == Constants.FUSCZTypes.SMS; } }

        public bool IsSMSZVType { get { return FUSType == Constants.FUSCZTypes.SMSZV; } }

        public bool IsZVType { get { return FUSType == Constants.FUSCZTypes.ZV || FUSType == Constants.FUSCZTypes.SMSZV; } }

        public bool OnlyNonLifeClientInsurance { get { return OnlyNonLifeClientInsuranceChecked(x => x.Client); } }
        public bool OnlyNonLifePartnerInsurance { get { return OnlyNonLifeClientInsuranceChecked(x => x.Partner); } }
        public bool OnlyNonLifeChildInsurance { get { return OnlyNonLifeClientInsuranceChecked(x => x.Child); } }

        public bool IsClientFSInputReadonly { get { return Client != null && OnlyNonLifeClientInsurance && !Client.EditOptionalInformation; } }

        private bool OnlyNonLifeClientInsuranceChecked(Func<BoolClientPartnerChild, bool> func)
        {
            var nonLifeChecked = ExtensionMethods.AtleastOnetrue(FSInsurancePersonalSet.VoD(func), FSInsurancePropertySet.VoD(func), FSInsuranceLiabilitySet.VoD(func), FSInsuranceTravelSet.VoD(func), FSInsuranceOthersSet.VoD(func));
            var othersChecked = ExtensionMethods.AtleastOnetrue(FSLoanConsumerSet.VoD(func), FSLoanOtherSet.VoD(func), FSInsuranceLiveSet.VoD(func), FSInsuranceLiveOtherSet.VoD(func), FSBuildSavingSet.VoD(func),
                FSSavingDPSSet.VoD(func), FSSavingIncreaseDPSSet.VoD(func), FSSavingTFToDPSSet.VoD(func), FSSavingIncreaseTFSet.VoD(func), FSInvestmentSet.VoD(func), FSTermDepositsOrOtherJTSet.VoD(func), FSOthersSet.VoD(func));

            return nonLifeChecked && !othersChecked;
        }
        #endregion


        private static List<DateTime?> GetSectorDates(FormFUSCZ formFUSCZ)
        {
            List<DateTime?> listDates = new List<DateTime?>();
            if (formFUSCZ.Sectors.NullableCount() > 0)
                listDates.AddRange(formFUSCZ.Sectors.ValueOrDefault(v => v.Select(p => p.DateSigned)).ToList());

            if (formFUSCZ.SectorInsurances.NullableCount() > 0)
                listDates.AddRange(formFUSCZ.SectorInsurances.ValueOrDefault(v => v.Select(p => p.DateSigned)).ToList());

            if (formFUSCZ.SectorInvestments.NullableCount() > 0)
                listDates.AddRange(formFUSCZ.SectorInvestments.ValueOrDefault(v => v.Select(p => p.DateSigned)).ToList());
            return listDates;
        }

        //#region ValidatePartyCodeAndBirthDate
        ///// <summary>
        ///// 2015-05-25 - DT - validace rodného čísla a datumu narození, společná pro klienta a partnera
        ///// </summary>
        ///// <param name="partyCode"></param>
        ///// <param name="birthDate"></param>
        ///// <param name="resourceCode"></param>
        ///// <returns></returns>
        //static IEnumerable<ValidationResult> ValidatePartyCodeAndBirthDate(string partyCode, DateTime? birthDate, string resourceCode)
        //{
        //    if (string.IsNullOrWhiteSpace(partyCode))
        //    {
        //        yield return new ValidationResult(GetErrorResource(resourceCode + ".PartyCode"), new[] { resourceCode + ".PartyCode" });
        //    }
        //    else if (birthDate.HasValue)
        //    {
        //        if (birthDate.Value > DateTime.Now)
        //            yield return new ValidationResult(GetErrorResource(resourceCode + ".BirthDate.InFuture"), new[] { resourceCode + ".BirthDate" });

        //        if (!PartyCodeHelper.IsValidPartyCodeDate(birthDate.Value, partyCode))
        //        {
        //            yield return new ValidationResult(GetErrorResource(resourceCode + ".BirthDate.NotAsPartyCode"), new[] { resourceCode + ".PartyCode" });
        //        }
        //    }
        //    else
        //        yield return new ValidationResult(GetErrorResource(resourceCode + ".BirthDate"), new[] { resourceCode + ".BirthDate" });
        //}
        //#endregion

        //static IEnumerable<ValidationResult> ValidateInsuranceSectors(
        //    List<SectorInsurance> sectors,
        //    bool clientMustBeInsured,
        //    bool partnerMustBeInsured,
        //    bool childMustBeInsured)
        //{
        //    if (clientMustBeInsured && !FUShelper.IsClientAtLeastOne(sectors))
        //        yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual.Client",
        //            validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });

        //    else if (partnerMustBeInsured && !FUShelper.IsPartnerAtLeastOne(sectors))
        //        yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual.Partner",
        //            validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });

        //    else if (childMustBeInsured && !FUShelper.IsChildAtLeastOne(sectors))
        //        yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual.Child",
        //            validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });
        //}

        //   static IEnumerable<ValidationResult> ValidateInsuranceSector(
        //       FormFUSCZ viewForm,
        //       //FormFUSCZ dbForm,
        //       SectorInsurance sector,
        //       FUShelper.SectorSettings settings,
        //       bool clientMustBeInsured,
        //       bool partnerMustBeInsured,
        //       bool childMustBeInsured)
        //   {
        //       if (sector.InsuranceChoices.InsuranceIndividual || settings.InsuranceIndividual)
        //       {
        //           if ((FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalGuaranteed) || FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalInvestFund)) &&
        //               !sector.InsuranceChoices.RequiredInvestProfile)
        //           {
        //               if (FUShelper.IsClientSector(viewForm, sector))
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Client"), new[] { "FinancialInsurance.RequiredInvestProfile" });
        //               else if (FUShelper.IsPartnerSector(viewForm, sector))
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Partner"), new[] { "FinancialInsurance.RequiredInvestProfile" });
        //               else if (FUShelper.IsChildSector(viewForm, sector))
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Child"), new[] { "FinancialInsurance.RequiredInvestProfile" });
        //           }

        //           bool atLeastOne = true;

        //           if (!FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIDeath) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIDeathInjury) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIPermanentInjury) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIInvalidity) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IILowInjury) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IISeriousDiseases) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalGuaranteed) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalInvestFund)/* &&
        //!sector.InsuranceChoices.RequiredInvestProfile*/)
        //               atLeastOne = false;

        //           if (!atLeastOne)
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual",
        //                   validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });
        //           else
        //           {
        //               //if (!FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIDeath) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIDeathInjury) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIPermanentInjury) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIInvalidity) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IILowInjury) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IISeriousDiseases) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalGuaranteed) ||
        //               //    !FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalInvestFund) ||
        //               //    !sector.InsuranceChoices.RequiredInvestProfile)
        //               //{
        //               //    if (clientMustBeInsured && !FUShelper.IsClientSector(viewForm, sector))
        //               //        yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual.Client",
        //               //            validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });

        //               //    else if (partnerMustBeInsured && !FUShelper.IsPartnerSector(viewForm, sector))
        //               //        yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual.Partner",
        //               //            validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });

        //               //    else if (childMustBeInsured && !FUShelper.IsChildSector(viewForm, sector))
        //               //        yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceIndividual.Child",
        //               //            validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceIndividual" });
        //               //}

        //               if (FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIPermanentInjury) && !sector.InsuranceChoices.IIPermanentInjuryFree && !sector.InsuranceChoices.IIPermanentInjurySingle)
        //               {
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.IIPermanentInjury.Specify", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.IIPermanentInjury" });
        //               }

        //               if (FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIInvalidity) && !sector.InsuranceChoices.IIInvalidityFree && !sector.InsuranceChoices.IIInvaliditySingle)
        //               {
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.IIInvalidity.Specify", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.IIInvalidity" });
        //               }

        //               if (!FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIPermanentInjury) && (sector.InsuranceChoices.IIPermanentInjuryFree || sector.InsuranceChoices.IIPermanentInjurySingle))
        //               {
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.IIPermanentInjury.MustBeSet", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.IIPermanentInjury" });
        //               }

        //               if (!FUShelper.AtleastOneTrue(sector.InsuranceChoices.IIInvalidity) && (sector.InsuranceChoices.IIInvalidityFree || sector.InsuranceChoices.IIInvaliditySingle))
        //               {
        //                   yield return new ValidationResult(GetErrorResource("FinancialInsurance.IIInvalidity.MustBeSet", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.IIInvalidity" });
        //               }
        //           }

        //           if ((FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalGuaranteed) || FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalInvestFund)) && sector.InsuranceChoices.TaxAdvantage == null)
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceSurvivalGuaranteed", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceTaxAdvantage" });

        //           //if ((sector.InsuranceChoices.InsuranceSurvivalGuaranteed || sector.InsuranceChoices.InsuranceSurvivalInvestFund) && !sector.InsuranceChoices.RequiredInvestProfile)
        //           //{
        //           //	if (FUShelper.IsClientSector(viewForm, sector))
        //           //		yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Client", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.RequiredInvestProfile.Client" });
        //           //	if (FUShelper.IsPartnerSector(viewForm, sector))
        //           //		yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Partner", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.RequiredInvestProfile.Partner" });
        //           //	if (FUShelper.IsChildSector(viewForm, sector))
        //           //		yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Child", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.RequiredInvestProfile.Child" });
        //           //}

        //           if (viewForm.FSInsuranceLive && !FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceSurvivalInvestFund) && settings.InsuranceSurvivalInvestFund)
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceSurvivalInvestFund", validType: Constants.ValidateType.Error), new[] { "FinancialInsurance.InsuranceSurvivalInvestFund" });

        //           //if (model.FormFUSCZ.FSInsuranceLiveOther && !ExtensionMethods.AtleastOnetrue(formFUSCZ.FinancialInsurance.InsuranceSurvivalGuaranteed))
        //           //	yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceSurvivalGuaranteed", validType: Constants.ValidateType.Error), new[] { "FinancialInsurance.InsuranceSurvivalGuaranteed" });
        //       }

        //       if (sector.InsuranceChoices.InsuranceProperty)
        //       {
        //           if (!FUShelper.AtleastOneTrue(sector.InsuranceChoices.IPHomeReality) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IPOtherReality) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IPAllRisk) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IPTheft) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IPVehicleOthers) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IPOthers))
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceProperty",
        //                   validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceProperty" });
        //       }

        //       if (sector.InsuranceChoices.InsuranceDamage)
        //       {
        //           if (!FUShelper.AtleastOneTrue(sector.InsuranceChoices.IDPublic) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IDOwnReality) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IDPZP) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IDEmployer) &&
        //               !FUShelper.AtleastOneTrue(sector.InsuranceChoices.IDOthers))
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceDamage",
        //                   validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceDamage" });
        //       }

        //       //if (sector.InsuranceChoices.InsuranceTraveling && !sector.InsuranceChoices.InsuranceTravelingDetail)
        //       //	yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceTraveling",
        //       //		validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceTraveling" });

        //       if (FUShelper.AtleastOneTrue(sector.InsuranceChoices.InsuranceOtherDetail) && string.IsNullOrWhiteSpace(sector.InsuranceChoices.InsuranceNote))
        //       {
        //           var fusType = viewForm.FUSType == Constants.FUSCZTypes.SMS ? Constants.FUSCZTypes.STANDARDNI : viewForm.FUSType == Constants.FUSCZTypes.SMSZV ? Constants.FUSCZTypes.ZV : viewForm.FUSType;
        //           yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceNote." + fusType,
        //               validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.InsuranceNote" });
        //       }


        //       if (!viewForm.HasFusDynamic)
        //       {
        //           if (!string.IsNullOrWhiteSpace(sector.InsuranceChoices.InsuranceNote) && sector.InsuranceChoices.InsuranceNote.NullableLength() > (viewForm.FUSType == Constants.FUSCZTypes.STANDARDNI || viewForm.FUSType == Constants.FUSCZTypes.SMS ? 65 : 200))
        //           {
        //               var fusType = viewForm.FUSType == Constants.FUSCZTypes.SMS ? Constants.FUSCZTypes.STANDARDNI : viewForm.FUSType == Constants.FUSCZTypes.SMSZV ? Constants.FUSCZTypes.ZV : viewForm.FUSType;
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.InsuranceNote." + fusType,
        //                   validType: Constants.ValidateType.MaxLength), new[] { "FinancialInsurance.InsuranceNote" });
        //           }

        //           if (!string.IsNullOrWhiteSpace(sector.InsuranceChoices.Discrepancies) && sector.InsuranceChoices.Discrepancies.NullableLength() > (viewForm.FUSType == Constants.FUSCZTypes.STANDARDNI || viewForm.FUSType == Constants.FUSCZTypes.SMS ? 90 : 180))
        //           {
        //               yield return new ValidationResult(GetErrorResource("FinancialInsurance.Discrepancies",
        //                   validType: Constants.ValidateType.MaxLength), new[] { "FinancialInsurance.Discrepancies" });
        //           }
        //       }
        //       //if (sector.InsuranceChoices.RequiredInvestProfile && 
        //       //	!sector.InsuranceChoices.DynamicProfil,
        //       //	formFUSCZ.FinancialInsurance.BalancedProfil.VoD(v => v.Client), formFUSCZ.FinancialInsurance.KonzervativeProfil.VoD(v => v.Client)))
        //       //{
        //       //	yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Client", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.RequiredInvestProfile.Client" });
        //       //}

        //       //if (formFUSCZ.FinancialInsurance.RequiredInvestProfile.VoD(v => v.Partner) == true && !ExtensionMethods.AtleastOnetrue(formFUSCZ.FinancialInsurance.DynamicProfil.VoD(v => v.Partner), formFUSCZ.FinancialInsurance.BalancedProfil.VoD(v => v.Partner), formFUSCZ.FinancialInsurance.KonzervativeProfil.VoD(v => v.Partner)))
        //       //{
        //       //	yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Partner", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.RequiredInvestProfile.Partner" });
        //       //}

        //       //if (formFUSCZ.FinancialInsurance.RequiredInvestProfile.VoD(v => v.Child) == true && !ExtensionMethods.AtleastOnetrue(formFUSCZ.FinancialInsurance.DynamicProfil.VoD(v => v.Child), formFUSCZ.FinancialInsurance.BalancedProfil.VoD(v => v.Child), formFUSCZ.FinancialInsurance.KonzervativeProfil.VoD(v => v.Child)))
        //       //{
        //       //	yield return new ValidationResult(GetErrorResource("FinancialInsurance.RequiredInvestProfile.Child", validType: Constants.ValidateType.AtLeastOne), new[] { "FinancialInsurance.RequiredInvestProfile.Child" });
        //       //}
        //   }


        //#endregion Validate

        //public static string GetErrorResource(string value, string validType = null)
        //{
        //    return ResourceManager.GetResourceRaw(string.Format("Web.Model.FUSCZ.{0}.Required.{1}", value, (validType ?? Constants.ValidateType.Error)));
        //}

        //private static bool IsAgentPhone(string phone)
        //{
        //    phone = Regex.Replace(phone, @"[^0-9]", "");
        //    return DbProvider.Connection.PartyContacts(Auth.Profile.Agent.PartyID, Auth.Profile.UserID)
        //        .Any(x => (!string.IsNullOrEmpty(x.Phone) && Regex.Replace(x.Phone, @"[^0-9]", "") == phone) || (!string.IsNullOrEmpty(x.Mobile) && Regex.Replace(x.Mobile, @"[^0-9]", "") == phone));
        //}

        //private static bool IsPhoneTaken(int? clientID, string phone)
        //{
        //    var contacts = DbProvider.Connection.AgentClientsContacts(Auth.Profile.AgentID, Auth.Profile.UserID, clientID, phone: phone);

        //    return contacts.NullableCount() > int.Parse(AppCache.GetInstance().LocalSettings["PhoneCountLimit"]);
        //}

        //    private static bool IsEmailTaken(int? clientID, string email)
        //    {
        //        var contacts = DbProvider.Connection.AgentClientsContacts(Auth.Profile.AgentID, Auth.Profile.UserID, clientID, email: email);

        //        return contacts.NullableCount() > int.Parse(AppCache.GetInstance().LocalSettings["EmailCountLimit"]);
        //    }

        //    private static bool IsNewFinancialInvestment()
        //    {
        //        return bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NewFinancialInvestment"]);
        //    }
        //}
    }
}