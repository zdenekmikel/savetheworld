﻿using SaveTheWorld.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using static SaveTheWorld.Models.FusHelper;

namespace SaveTheWorld.Models
{
    public class FUShelper
    {
        public static bool AtleastOneTrue(BoolClientPartner cpc)
        {
            if (cpc == null)
                return false;

            if (cpc.ValueOrDefault(p => p.Client) || cpc.ValueOrDefault(p => p.Partner))
                return true;

            return false;
        }

        public static bool AtleastOneTrue(BoolClientPartnerChild cpc, bool clientMustBeTrue = false, bool partnerMustBeTrue = false, bool childMustBeTrue = false)
        {
            if (cpc == null)
                return false;

            if (cpc.ValueOrDefault(p => p.Client) || cpc.ValueOrDefault(p => p.Partner) || cpc.ValueOrDefault(p => p.Child))
                return true;

            return false;
        }

        public static bool AtleastOneTrue(StringClientPartner cpc, bool clientMustBeTrue = false, bool partnerMustBeTrue = false, bool clientMustHaveinsurance = false, bool partnerMustHaveinsurance = false)
        {
            if (cpc == null)
                return false;

            if (string.IsNullOrWhiteSpace(cpc.ValueOrDefault(p => p.Client)) && string.IsNullOrWhiteSpace(cpc.ValueOrDefault(p => p.Partner)))
                return false;

            if (((clientMustBeTrue || clientMustHaveinsurance) && string.IsNullOrWhiteSpace(cpc.ValueOrDefault(p => p.Client))) || ((partnerMustBeTrue || partnerMustHaveinsurance) && string.IsNullOrWhiteSpace(cpc.ValueOrDefault(p => p.Partner))))
                return false;

            return true;
        }

        //public static void AddDocument(HttpPostedFileBase file, int FusID)
        //{
        //    DocumentManager manager = new DocumentManager(RootID: DocumentManager.InterventionsRoot);
        //    Document document = manager.AddDocument(DocumentTypes.IMPORTEDFUS, Path.GetFileNameWithoutExtension(file.FileName));

        //    var extension = Path.GetExtension(file.FileName);
        //    document.CreateFile(DocumentFileTypes.FILE, file.ContentType, extension, "", (long)file.ContentLength, file.InputStream);

        //    //add document relations...
        //    document.AddRelation(FusID, DocumentRelationTypes.FUS);

        //    // add relation to contract if ticket class = partner objection
        //    //return true;

        //}
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region AddFusToCI
        public class FusAddToCIResult
        {
            public bool FUSNotValid { get; set; }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 2015-05-13 - DT - #4710
        /// </summary>
        /// <param name="fusID"></param>
        /// <param name="fusModel"></param>
        /// <param name="CIMasterID"></param>
        /// <param name="CIModel"></param>
        /// <returns></returns>
        //public static FusAddToCIResult AddFusToCI(int? fusID, StandardFUSCZModel fusModel, int CIMasterID, CIMasterCZModel CIModel, string bookerNo = null, int? tipID = null, int? userID = null)
        //{
        //    if (!fusID.HasValue) return new FusAddToCIResult() { FUSNotValid = true };

        //    if (fusModel == null)
        //    {
        //        fusModel = new StandardFUSCZModel();
        //        fusModel.LoadModel(fusID, true);
        //    }

        //    FusAddToCIResult result = new FusAddToCIResult();

        //    var fus = fusModel.FormFUSCZ;

        //    Dictionary<Guid?, int?> CiClientTempIDs = new Dictionary<Guid?, int?>();

        //    var sectors = new List<BL.FinData.Web.Models.FUSCZ.Sector>();
        //    if (fus.Sectors.NullableCount() > 0)
        //        sectors.AddRange(fus.Sectors);
        //    if (fus.SectorInsurances.NullableCount() > 0)
        //        sectors.AddRange(fus.SectorInsurances);

        //    foreach (BL.FinData.Web.Models.FUSCZ.Sector sector in sectors)
        //    {
        //        CIContractCZModel cic = new CIContractCZModel();

        //        if (!BL.FinData.Web.Models.FUSCZ.FUSCIMasterCZ.FillCIMaster(cic, fusModel, sector, CIMasterID, bookerNo, tipID, userID))
        //        {
        //            //TempData["FUSNotValid"] = this.Resource("FUSNotValid");
        //            result.FUSNotValid = true;
        //        }
        //        else
        //        {
        //            CIContractCZModel.HandleFUSClientInsert(fus, CiClientTempIDs, cic);
        //        }
        //    }

        //    if (fus.SectorInvestments.NullableCount() > 0)
        //    {
        //        foreach (BL.FinData.Web.Models.FUSCZ.SectorInvestment sector in fus.SectorInvestments)
        //        {
        //            CIContractCZModel cic = new CIContractCZModel();

        //            if (!BL.FinData.Web.Models.FUSCZ.FUSCIMasterCZ.FillCIMaster(cic, fusModel, sector, CIMasterID))
        //            {
        //                //TempData["FUSNotValid"] = this.Resource("FUSNotValid");
        //                result.FUSNotValid = true;
        //            }
        //            else
        //            {
        //                CIContractCZModel.HandleFUSClientInsert(fus, CiClientTempIDs, cic);
        //            }
        //        }
        //    }

        //    fusModel.FUS.CIMasterID = CIModel.CIMaster.CIMasterID;

        //    if (fusModel.FUS.FUSStatusID == Constants.FUSStatuses.ClosedAMLRequired)
        //        fusModel.FUS.FUSStatusID = Constants.FUSStatuses.ClosedConvertedAmlRequired;
        //    else
        //        fusModel.FUS.FUSStatusID = Constants.FUSStatuses.ClosedConverted;

        //    fusModel.TicketInsert = true;
        //    fusModel.UpdateDbFUS();

        //    if (fusModel.FormFUSCZ.Client.ValueOrDefault(p => p.EmployerID) != null)
        //        CIModel.CIMasterUpdate(fusModel.FormFUSCZ.Client.EmployerID, null);

        //    return result;
        //}

        //public static FusAddToCIResult AddFusToCI(int? fusID, StandardFUSModel fusModel, int CIMasterID, CIMasterModel CIModel)
        //{
        //    if (!fusID.HasValue) return new FusAddToCIResult() { FUSNotValid = true };

        //    if (fusModel == null)
        //    {
        //        fusModel = new StandardFUSModel();
        //        fusModel.LoadModel(fusID, true);
        //    }

        //    FusAddToCIResult result = new FusAddToCIResult();

        //    var fus = fusModel.StandardFUS;

        //    Dictionary<Guid?, int?> CiClientTempIDs = new Dictionary<Guid?, int?>();

        //    var sectors = new List<BL.FinData.Web.Models.FUSSK.Sector>();
        //    if (fus.Sectors.NullableCount() > 0)
        //        sectors.AddRange(fus.Sectors);

        //    foreach (BL.FinData.Web.Models.FUSSK.Sector sector in sectors)
        //    {
        //        CIContractModel cic = new CIContractModel();

        //        if (!BL.FinData.Web.Models.FUSSK.FUSCIMaster.FillCIMaster(cic, fusModel, sector, CIMasterID))
        //        {
        //            //TempData["FUSNotValid"] = this.Resource("FUSNotValid");
        //            result.FUSNotValid = true;
        //        }
        //        else
        //        {
        //            CIContractModel.HandleFUSClientInsert(fus, CiClientTempIDs, cic);
        //        }
        //    }

        //    fusModel.FUS.CIMasterID = CIModel.CIMaster.CIMasterID;

        //    if (fusModel.FUS.FUSStatusID == Constants.FUSStatuses.ClosedAMLRequired)
        //        fusModel.FUS.FUSStatusID = Constants.FUSStatuses.ClosedConvertedAmlRequired;
        //    else
        //        fusModel.FUS.FUSStatusID = Constants.FUSStatuses.ClosedConverted;

        //    fusModel.TicketInsert = true;
        //    fusModel.UpdateDbFUS();

        //    //if (fusModel.StandardFUS.ClientDetail.ValueOrDefault(p => p.EmployerID) != null)
        //    //	CIModel.CIMasterUpdate(fusModel.FormFUSCZ.Client.EmployerID, null);

        //    return result;
        //}
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region StornoEFUS
        /// <summary>
        /// 2015-05-14 - DT - #4772 - zrušení všech smluv v e.soupisce, které pocházejí z tohoto eFUS
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="fusID"></param>
        /// <exception cref="ArgumentNullException"></exception>
        //public static Dictionary<int, bool> StornoEFUS(StornoModel model)
        //{
        //    if (model == null)
        //        throw new ArgumentNullException("model");

        //    var ticSearch = DbProvider.Connection.TicketByEntity(model.UserID, model.FusID, Constants.TicketClasses.FUS);
        //    if (ticSearch != null)
        //    {
        //        model.Ticket = DbProvider.Connection.Ticket(model.UserID, ticSearch.TicketID);
        //        if (model.Ticket == null)
        //            model.Results.TicketNotFound = true;
        //    }
        //    else
        //    {
        //        model.Results.TicketSearchFail = true;
        //    }

        //    var abc = DbProvider.Connection.CIContractsSearchByFUSID(model.UserID, model.FusID);
        //    if (abc != null)
        //        foreach (dynamic row in abc)
        //        {
        //            int mID = (int)row.CIMasterID;
        //            int cID = (int)row.CIContractID;

        //            StornoModel.CICOpair pair = new StornoModel.CICOpair() { CImasterID = mID, ContractID = cID };

        //            if (!model.CICO.Contains(pair))
        //                model.CICO.Add(pair);
        //        }
        //    else
        //    {
        //        model.Results.ContractSearchFail = true;
        //    }

        //    Dictionary<int, bool> result = new Dictionary<int, bool>();

        //    foreach (var pair in model.CICO)
        //    {
        //        var ciContrModel = CIContractCZModel.GetCIContract(pair.CImasterID, pair.ContractID);

        //        if (ciContrModel != null && ciContrModel.CIContract != null)
        //        {
        //            pair.IsEditable = ciContrModel.CIMaster != null && CIMasterModel.IsEditable(ciContrModel.CIMaster.CIContractStatusID);

        //            if (pair.IsEditable)
        //            {
        //                try
        //                {
        //                    var res = DeleteContractFromCI(model.UserID, ciContrModel, pair.CImasterID, true);
        //                    if (res.DeletedItemsCnt > 0)
        //                        pair.StornoSuccessful = true;

        //                    model.Results.MasterDeleted = res.MasterDeleted;
        //                }
        //                catch { }
        //            }
        //        }
        //    }

        //    model.Results.DroppedAllContracts = model.CICO.TrueForAll(f => f.StornoSuccessful);

        //    if (model.Results.DroppedAllContracts)
        //    {
        //        if (model.Ticket != null)
        //        {
        //            try
        //            {
        //                UnrealizeTicket(model.Ticket, model.FusID, model.UserID);

        //            }
        //            catch { }
        //        }
        //    }

        //    return result;
        //}
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region DeleteContractFromCI
        public class DeleteContractResult
        {
            public int DeletedItemsCnt { get; set; }
            public int RemainingItemsCnt { get; set; }

            public bool MasterDeleted { get; set; }
        }
        /// <summary>
        /// <para>2015-05-14 - DT - #4772</para>
        /// <para>převzato z CIMasterCZController</para>
        /// <para>2015-05-22 - DT - přidán parametr deleteCImaster, pokud je true a vymazaná smlouva je poslední v el. soupisce, tak dojde ke smazání el. soupisky</para>
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="model"></param>
        /// <param name="CImasterID"></param>
        /// <exception cref="ArgumentNullException"></exception>
        //public static DeleteContractResult DeleteContractFromCI(int userID, CIContractCZModel model, int CImasterID, bool deleteCImaster)
        //{
        //    if (model == null)
        //        throw new ArgumentNullException("model");

        //    DeleteContractResult result = new DeleteContractResult();

        //    if (model.FUSID != null)
        //    {
        //        //Delete all contract from cimaster with same fusid as deleting cicontract
        //        var ciMasterModel = new CIMasterModel(CImasterID);

        //        if (ciMasterModel != null && ciMasterModel.CIContracts != null)
        //        {
        //            for (int i = ciMasterModel.CIContracts.Count - 1; i >= 0; --i)
        //            {
        //                var item = ciMasterModel.CIContracts[i];
        //                if (item.FUSID != model.FUSID || item.CIMasterID != model.CIMasterID)
        //                    continue;

        //                var ciCm = CIContractModel.GetCIContract(CImasterID, item.CIContractID);
        //                if (ciCm != null && ciCm.CIContract != null)
        //                {
        //                    try
        //                    {
        //                        ciCm.CIContractDelete();
        //                        ciMasterModel.CIContracts.RemoveAt(i);
        //                        result.DeletedItemsCnt++;
        //                    }
        //                    catch { }
        //                }
        //            }

        //            if (deleteCImaster && ciMasterModel.CIContracts.Count == 0 && ciMasterModel.CIMaster != null)
        //            {
        //                try
        //                {
        //                    DeleteCIMaster(ciMasterModel.CIMaster, userID);
        //                    result.MasterDeleted = true;
        //                }
        //                catch { }
        //            }
        //        }

        //        var fusModel = new BL.FinData.Web.Models.FUSCZ.StandardFUSCZModel();
        //        fusModel.LoadModel(model.FUSID, true);

        //        if (fusModel.FUS != null)
        //        {
        //            if (fusModel.FUS.FUSStatusID == Constants.FUSStatuses.ClosedConvertedAmlRequired)
        //                fusModel.FUS.FUSStatusID = Constants.FUSStatuses.ClosedAMLRequired;
        //            else if (fusModel.FUS.FUSStatusID == Constants.FUSStatuses.ClosedConverted)
        //                fusModel.FUS.FUSStatusID = Constants.FUSStatuses.Closed;
        //        }
        //        fusModel.TicketInsert = true;
        //        fusModel.UpdateDbFUS();
        //    }
        //    else
        //    {
        //        model.CIContractDelete();
        //    }

        //    // Delete documents uploaded from cicontract detail
        //    int? dirID = null;
        //    dirID = DbProvider.Connection.DocumentsFolder(CImasterID.ToString());

        //    if (dirID.HasValue)
        //    {
        //        var DocContract = DbProvider.Connection.DocumentsByRelation(userID, model.CIContractID, DocumentRelationTypes.CONTRACT_INVENTORY, DocumentTypes.CONTRACT).FirstOrDefault();
        //        if (DocContract != null)
        //            DbProvider.Connection.DocumentDelete(userID, DocContract.DocumentID, dirID.Value);

        //        var DocOthers = DbProvider.Connection.DocumentsByRelation(userID, model.CIContractID, DocumentRelationTypes.CONTRACT_INVENTORY, DocumentTypes.OTHERS).FirstOrDefault();
        //        if (DocOthers != null)
        //            DbProvider.Connection.DocumentDelete(userID, DocOthers.DocumentID, dirID.Value);
        //    }

        //    return result;
        //}
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region UnrealizeTicket
        /// <summary>
        /// <para>2015-05-14 - DT - #4772</para>
        /// <para>převzato z workflow v backoffice</para>
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="fusID"></param>
        /// <param name="userID"></param>
        /// <exception cref="ArgumentNullException"></exception>
        //static void UnrealizeTicket(Ticket ticket, int fusID, int userID)
        //{
        //    if (ticket == null)
        //        throw new ArgumentNullException("ticket");

        //    string ticketClassCode = ticket.TicketClassCode;
        //    string encEntityID = "";
        //    string parentClassCode = AppCache.Inst.TicketClasses.Single(c => c.TicketClassCode == ticketClassCode).ParentClassCode;
        //    if (parentClassCode != null)
        //    {
        //        ticketClassCode = parentClassCode;
        //    }

        //    if (ticketClassCode == Constants.TicketClasses.FUS)
        //    {
        //        encEntityID = Encryption.Encode(fusID);

        //        ticket.CreatedBy = userID;
        //        ticket.TicketStatusID = TicketStatuses.EF_CLOSEDUNREALISED;

        //        ticket.Content = BaseModel.ResourceGlobal("Workflow.TicketStatus.Content." + TicketStatuses.EF_CLOSEDUNREALISED, fusID);

        //        DbProvider.Connection.TicketResponseInsert(ticket, encEntityID, false);

        //        var fus = DbProvider.Connection.CustomerFUS(userID, 0, fusID);
        //        if (fus != null)
        //        {
        //            fus.FUSStatusID = Constants.FUSStatuses.ClosedUnrealised;
        //            DbProvider.Connection.CustomerFUSUpdate(userID, 0, fus);
        //        }
        //    }
        //}
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region DeleteCIMaster
        /// <para>2015-05-14 - DT - #4772</para>
        /// <para>převzato z CIMasterCZController</para>
        /// <exception cref="ArgumentNullException"></exception>
        //public static void DeleteCIMaster(CIMaster ciMaster, int userID)
        //{
        //    if (ciMaster == null)
        //        throw new ArgumentNullException("ciMaster");

        //    DbProvider.Connection.CIMasterDelete(ciMaster);
        //    DbProvider.Connection.TicketDeleteByCIMaster(ciMaster.CIMasterID, userID);
        //}
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static List<FUSListOptions> FUSListOptionsResource
        {
            get
            {
                var list = DbProvider.FUSListOptions();
                list.ForEach(p => p.Name = BaseModel.ResourceGlobal(p.ResourceCode)); //TOTO
                return list;
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region SplitToLines
        /// <summary>
        /// 2015-04-07 - od Hynka
        /// </summary>
        /// <param name="stringToSplit"></param>
        /// <param name="firstRow"></param>
        /// <param name="maximumLineLengthTemp"></param>
        /// <returns></returns>
        public static IEnumerable<string> SplitToLines(string stringToSplit, int firstRow, int maximumLineLengthTemp)
        {
            if (string.IsNullOrEmpty(stringToSplit)) return new List<string>();

            bool IsFirst = true;
            // bool IsLast = false;
            var words = stringToSplit.Split(' ').Concat(new[] { "" });
            int maximumLineLength;
            return
                   words
                          .Skip(1)
                          .Aggregate(
                                words.Take(1).ToList(),
                                (a, w) =>
                                {
                                    maximumLineLength = IsFirst ? firstRow : maximumLineLengthTemp;
                                    maximumLineLength = a.Count() > 2 ? 500 : maximumLineLength;
                                    var last = a.Last();
                                    while (last.Length > maximumLineLength)
                                    {
                                        a[a.Count() - 1] = last.Substring(0, maximumLineLength);
                                        last = last.Substring(maximumLineLength);
                                        a.Add(last);
                                        maximumLineLength = IsFirst ? firstRow : maximumLineLengthTemp;
                                        IsFirst = false;
                                    }
                                    var test = last + " " + w;
                                    if (test.Length > maximumLineLength)
                                    {
                                        a.Add(w);
                                        IsFirst = false;
                                    }
                                    else
                                    {
                                        a[a.Count() - 1] = test;
                                    }

                                    return a;
                                });
        }
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region SplitByChar
        public class SplitResult
        {
            public DateTime? From { get; set; }
            public DateTime? To { get; set; }
            public string Where { get; set; }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 2015-05-28 - DT - splits string if it contains two datetimes followed by text, text separator is variable splitter
        /// </summary>
        /// <param name="toSplit"></param>
        /// <param name="splitter"></param>
        /// <returns></returns>
        public static SplitResult SplitByChar(string toSplit, char splitter)
        {
            SplitResult result = new SplitResult();

            if (string.IsNullOrEmpty(toSplit)) return result;

            bool ok = false;

            try
            {
                string[] data = toSplit.Split(new char[] { '-' });

                if (data.Length == 2)
                {
                    long cnt = data[1].LongCount(f => f == splitter);

                    if (cnt == 1)
                    {
                        string[] data2 = data[1].Split(new char[] { splitter });

                        if (data2.Length == 2)
                        {
                            result.From = data[0].ToDateTime();
                            result.To = data2[0].ToDateTime();
                            result.Where = data2[1].Trim();
                            ok = true;
                        }
                    }
                    else if (cnt > 2)
                    {
                        int index = data[1].IndexOfNth('/', 3);

                        if (index > -1)
                        {
                            result.From = data[0].ToDateTime();
                            result.To = data[1].Substring(0, index).ToDateTime();
                            result.Where = data[1].Substring(index + 1).Trim();
                            ok = true;
                        }
                    }
                }
            }
            catch { }

            if (!ok) return null;

            return result;
        }
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region IsValid_DocumentDatesVsBirthDate
        /// <summary>
        /// 2015-05-28 - DT - validate document against birthdate
        /// if the document was issued before person's 15th birthdate, document is expected to have 5year duration otherwise 10year duration is expected
        /// 2015-06-04 - DT - allow 5 year duration for document issued after person's 15th birthday
        /// </summary>
        /// <param name="birthDate"></param>
        /// <param name="docFrom"></param>
        /// <param name="docTo"></param>
        /// <returns></returns>
        public static bool IsValid_DocumentDatesVsBirthDate(DateTime? birthDate, DateTime? docFrom, DateTime? docTo, DateTime? docValAgreeDate = null)
        {
            if (!birthDate.HasValue || !docFrom.HasValue || !docTo.HasValue)
                return false;

            var b15 = birthDate.Value.AddYears(15);

            DateTime requiredDate5 = docFrom.Value.AddYears(5);
            DateTime requiredDate10 = docFrom.Value.AddYears(10);

            bool result = false;

            if (b15 > docFrom)
            {
                var dif = docTo.Value.Subtract(requiredDate5);

                if (dif.Days <= 1 && dif.Days >= -1) // allow 1 day mistake
                    result |= true;

                var difDoc = b15.Subtract(docFrom.Value);

                if (difDoc.Days <= 183 && difDoc.Days >= -1 && docValAgreeDate.HasValue)
                {
                    result |= true;
                }
            }
            else
            {
                var dif = docTo.Value.Subtract(requiredDate5);

                if (dif.Days <= 1 && dif.Days >= -1) // allow 1 day mistake
                    result |= true;

                dif = docTo.Value.Subtract(requiredDate10);

                if (dif.Days <= 1 && dif.Days >= -1) // allow 1 day mistake
                    result |= true;
            }

            return result;
        }
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Ticket_WriteOnCloseFUS
        /// <summary>
        /// 2015-06-17 - DT - refactored code from CZ & SK fuses into one method for easier maintenance, adds record to fus ticket history on close action (both with or without AML)
        /// </summary>
        /// <param name="fus"></param>
        /// <param name="userid"></param>
        /// <param name="content"></param>
        //public static void Ticket_WriteOnCloseFUS(Core.Customer.FUS fus, int userid, string content)
        //{
        //    // TODO? a check for fus == null would be appropriate, but original implementation didn't have it and so something might depend on missing check
        //    if (!fus.TicketID.HasValue)
        //        return;

        //    BL.FinData.Core.Workflow.Ticket ticket = DbProvider.Connection.Ticket(userid, fus.TicketID.Value);

        //    // 2015-06-17 - DT - ticket is temporarily changed to reflect actual user in history, this setting is not saved to the original, but the new response will have this value
        //    ticket.CreatedBy = userid;

        //    ticket.TicketStatusID = FUSLists.GetTicketStatusID(fus.FUSStatusID);
        //    ticket.Content = content ?? (BaseModel.ResourceGlobal("Workflow.TicketStatus.Content." + FUSLists.GetTicketStatusID(fus.FUSStatusID).ToString(), fus.FUSID));

        //    DbProvider.Connection.TicketResponseInsert(ticket, WebEncryption.Encode(fus.FUSID), false);
        //}
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region GetCIMastersForClosedFus
        /// <summary>
        /// 2015-07-22 - DT - #6209 returns list of CIMasters for eFUS assigment
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="agentID"></param>
        /// <param name="fusType"></param>
        /// <returns></returns>
        //public static IEnumerable<dynamic> GetCIMastersForClosedFus(int userID, int agentID, string fusType)
        //{
        //    var param = new CIMasterSearchXml()
        //    {
        //        CIContractStatusID = BL.FinData.Common.Constants.CIContractStatuses.IN_PROGRESS,
        //        Type = fusType
        //    };

        //    var table = DbProvider.Connection.CIMastersXml(userID, agentID,
        //        BL.FinData.Common.ExtensionMethods.ToXml(param, typeof(CIMasterSearchXml)));

        //    var result = new List<dynamic>();

        //    foreach (var row in table)
        //    {
        //        result.Add(new { Key = row.CIMasterID, Value = string.Format("{0} [{1:dd. MM. yyyy hh:mm}]", row.CIMasterID, row.DateCreated) });
        //    }

        //    return result;
        //}
        #endregion

        #region #5778
        //public static void GetUnassignedTempClients(FormFUSCZ formFus)
        //{
        //    try
        //    {
        //        if (!formFus.FUSID.HasValue)
        //            throw new ArgumentNullException("FUSID je null, není možné vložit záznam o nepřiřazených účastnících eFUS");

        //        if (formFus.Client != null && !HasSector(formFus, formFus.Client.ClientTempID))
        //            HandleClient(formFus.Client, formFus.FUSID.Value, "C");

        //        foreach (var _partner in formFus.Partners)
        //        {
        //            if (!HasSector(formFus, _partner.ClientTempID))
        //                HandleClient(_partner, formFus.FUSID.Value, "P");
        //        }

        //        foreach (var child in formFus.Childs)
        //        {
        //            if (!HasSector(formFus, child.ClientTempID))
        //                HandleChild(child, formFus.FUSID.Value);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        App.HandleException(e);
        //    }
        //}

        //static void HandleClient(BL.FinData.Web.Models.FUSCZ.Client client, int fusID, string clientType)
        //{
        //    if (/*client.ClientID.HasValue ||*/ client.ClientTempID == Guid.Empty) return; // 2015-08-24 - condition changed

        //    DbProvider.Connection.CustomerFUSTempCLientsInsert(fusID, client.ClientTempID, clientType, Auth.Profile.UserID);
        //}

        //static void HandleChild(BL.FinData.Web.Models.FUSCZ.Child child, int fusID)
        //{
        //    if (/*child.ClientID.HasValue ||*/ child.ClientTempID == Guid.Empty) return; // 2015-08-24 - condition changed

        //    DbProvider.Connection.CustomerFUSTempCLientsInsert(fusID, child.ClientTempID, "CH", Auth.Profile.UserID);
        //}

        static bool HasSector(FormFUSCZ formFUS, Guid clientTempID)
        {
            bool result = false;

            foreach (var sector in formFUS.Sectors)
                result |= sector.ClientTempID == clientTempID;

            foreach (var sector in formFUS.SectorInsurances)
                result |= sector.ClientTempID == clientTempID;

            foreach (var sector in formFUS.SectorInvestments)
                result |= sector.ClientTempID == clientTempID;

            return result;
        }
        #endregion

        #region #6481
        //public static void GetUnassignedTempClients(StandardFUS formFus)
        //{
        //    try
        //    {
        //        if (!formFus.FUSID.HasValue)
        //            throw new ArgumentNullException("FUSID je null, není možné vložit záznam o nepřiřazených účastnících eFUS");

        //        if (formFus.ClientDetail != null && !HasSector(formFus, formFus.ClientDetail.ClientTempID))
        //            HandleClient(formFus.ClientDetail, formFus.FUSID.Value, "C");

        //        if (formFus.PartnerDetail != null && !HasSector(formFus, formFus.PartnerDetail.ClientTempID))
        //            HandleClient(formFus.PartnerDetail, formFus.FUSID.Value, "P");

        //        foreach (var child in formFus.Childs)
        //        {
        //            if (!HasSector(formFus, child.ClientTempID))
        //                HandleChild(child, formFus.FUSID.Value);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        App.HandleException(e);
        //    }
        //}

        //static void HandleClient(BL.FinData.Web.Models.FUSSK.Client client, int fusID, string clientType)
        //{
        //    if (/*client.ClientID.HasValue ||*/ client.ClientTempID == Guid.Empty) return; // 2015-08-24 - condition changed

        //    DbProvider.Connection.CustomerFUSTempCLientsInsert(fusID, client.ClientTempID, clientType, Auth.Profile.UserID);
        //}

        //static void HandleClient(BL.FinData.Web.Models.FUSSK.Partner client, int fusID, string clientType)
        //{
        //    if (/*client.ClientID.HasValue ||*/ client.ClientTempID == Guid.Empty) return; // 2015-08-24 - condition changed

        //    DbProvider.Connection.CustomerFUSTempCLientsInsert(fusID, client.ClientTempID, clientType, Auth.Profile.UserID);
        //}

        //static void HandleChild(BL.FinData.Web.Models.FUSSK.Child child, int fusID)
        //{
        //    if (/*child.ClientID.HasValue ||*/ child.ClientTempID == Guid.Empty) return; // 2015-08-24 - condition changed

        //    DbProvider.Connection.CustomerFUSTempCLientsInsert(fusID, child.ClientTempID, "CH", Auth.Profile.UserID);
        //}

        //static bool HasSector(StandardFUS formFUS, Guid clientTempID)
        //{
        //    bool result = false;

        //    foreach (var sector in formFUS.Sectors)
        //        result |= sector.ClientTempID == clientTempID;

        //    return result;
        //}
        #endregion

        //public static bool IsClientAtLeastOne(List<SectorInsurance> sectors)
        //{
        //    return IsAtLeastOne(sectors, p => p.Client);
        //}

        //public static bool IsPartnerAtLeastOne(List<SectorInsurance> sectors)
        //{
        //    return IsAtLeastOne(sectors, p => p.Partner);
        //}

        //public static bool IsChildAtLeastOne(List<SectorInsurance> sectors)
        //{
        //    return IsAtLeastOne(sectors, p => p.Child);
        //}

        //public static bool IsAtLeastOne(List<SectorInsurance> sectors, Func<BoolClientPartnerChild, bool> func)
        //{
        //    bool result = false;
        //    if (sectors != null)
        //    {
        //        foreach (var sector in sectors)
        //        {
        //            result |= sector.InsuranceChoices.IIDeath.VoD(func);
        //            result |= sector.InsuranceChoices.IIDeathInjury.VoD(func);
        //            result |= sector.InsuranceChoices.IIPermanentInjury.VoD(func);
        //            result |= sector.InsuranceChoices.IIInvalidity.VoD(func);
        //            result |= sector.InsuranceChoices.IILowInjury.VoD(func);
        //            result |= sector.InsuranceChoices.IISeriousDiseases.VoD(func);
        //            result |= sector.InsuranceChoices.InsuranceSurvivalGuaranteed.VoD(func);
        //            result |= sector.InsuranceChoices.InsuranceSurvivalInvestFund.VoD(func);
        //            result |= sector.InsuranceChoices.DynamicProfil.VoD(func);
        //            result |= sector.InsuranceChoices.BalancedProfil.VoD(func);
        //            result |= sector.InsuranceChoices.KonzervativeProfil.VoD(func);

        //            if (result)
        //                break;
        //        }
        //    }
        //    return result;
        //}

        //public static bool IsClientSector(FormFUSCZ form, SectorInsurance sector)
        //{
        //    if (form.Client != null &&
        //        sector != null &&
        //        sector.ClientTempID.HasValue &&
        //        form.Client.ClientTempID == sector.ClientTempID.Value)
        //        return true;

        //    return false;
        //}

        public static bool IsPartnerSector(FormFUSCZ form, SectorInsurance sector)
        {
            Partner partner = null;
            if (form.Partners.NullableCount() > 0)
                partner = form.Partners.Where(x => x.ClientTempID == sector.ClientTempID.Value).FirstOrDefault();

            if (form.Partners != null &&
                sector != null &&
                sector.ClientTempID.HasValue &&
                partner != null)
                return true;

            return false;
        }

        public static bool IsChildSector(FormFUSCZ form, SectorInsurance sector)
        {
            if (form.Partners != null &&
                sector != null &&
                sector.ClientTempID.HasValue)
                foreach (var child in form.Childs)
                {
                    if (child.ClientTempID == sector.ClientTempID.Value)
                        return true;
                }

            return false;
        }

        //public static bool IsChild(FormFUSCZ form, Guid tempClientID)
        //{
        //    bool isForChild = false;

        //    foreach (var child in form.Childs)
        //    {
        //        if (child.ClientTempID == tempClientID)
        //        {
        //            isForChild = true;
        //            break;
        //        }
        //    }

        //    return isForChild;
        //}

        //public static SectorSettings GetSectors(int userID, int serviceID, bool isForChild, int categoryID)
        //{
        //    var datas = DbProvider.Connection.ServiceSectors(userID, serviceID: serviceID);

        //    SectorSettings settings = null;

        //    // load custom settings
        //    if (datas != null && datas.Count > 0)
        //    {
        //        settings = new SectorSettings(datas, isForChild);
        //    }
        //    // load defaults
        //    else
        //    {
        //        var databs = DbProvider.Connection.CategorySectors(categoryID);
        //        settings = new SectorSettings(databs, isForChild);
        //    }

        //    return settings;
        //}

        public class SectorSettings
        {
            public bool InsuranceIndividual
            {
                get { return this.IIDeath || this.IIDeathInjury || this.IIPermanentInjury || this.IIInvalidity || this.IILowInjury || this.IISeriousDiseases || this.InsuranceSurvival; }
            }

            public bool IIDeath { get; set; }
            public bool IIDeathInjury { get; set; }
            public bool IIPermanentInjury { get; set; }
            public bool IIInvalidity { get; set; }
            public bool IILowInjury { get; set; }
            public bool IISeriousDiseases { get; set; }

            public bool InsuranceSurvival
            {
                get { return this.InsuranceSurvivalGuaranteed || this.InsuranceSurvivalInvestFund; }
            }

            public bool InsuranceSurvivalGuaranteed { get; set; }
            public bool InsuranceSurvivalInvestFund { get; set; }

            // props
            public bool InsuranceProperty
            {
                get { return this.IPHomeReality || this.IPOtherReality || this.IPVehicle || this.IPOthers; }
            }

            public bool IPHomeReality { get; set; }
            public bool IPOtherReality { get; set; }
            public bool IPVehicle { get; set; }
            public bool IPOthers { get; set; }

            // dmg
            public bool InsuranceDamage
            {
                get { return this.IDPublic || this.IDOwnReality || this.IDPZP || this.IDEmployer || this.IDOthers; }
            }

            public bool IDPublic { get; set; }
            public bool IDOwnReality { get; set; }
            public bool IDPZP { get; set; }
            public bool IDEmployer { get; set; }
            public bool IDOthers { get; set; }

            // rest

            public bool InsuranceTraveling { get; set; }
            public bool InsuranceOtherDetail { get; set; }

            //Selected
            public bool IsInsuranceTravelingSelected { get; set; }

            public SectorSettings() { }
            public SectorSettings(List<ProcServiceSectors_Result> data, bool isChild)
            {
                if (data == null)
                    return;

                foreach (var item in data)
                {
                    switch (item.SectorID)
                    {
                        case 1101:
                            if (isChild) break;
                            this.IIDeath = item.Value; break;
                        case 1102:
                            if (isChild) break;
                            this.IIDeathInjury = item.Value; break;
                        case 1103: this.IIPermanentInjury = item.Value; break;
                        case 1104: this.IIInvalidity = item.Value; break;
                        case 1105: this.IILowInjury = item.Value; break;
                        case 1106: this.IISeriousDiseases = item.Value; break;
                        case 1107: this.InsuranceSurvivalGuaranteed = item.Value; break;
                        case 1108: this.InsuranceSurvivalInvestFund = item.Value; break;

                        case 1201:
                            if (isChild) break;
                            this.IPHomeReality = item.Value; break;
                        case 1202:
                            if (isChild) break;
                            this.IPOtherReality = item.Value; break;
                        case 1203:
                            if (isChild) break;
                            this.IPVehicle = item.Value; break;
                        case 1204:
                            if (isChild) break;
                            this.IPOthers = item.Value; break;

                        case 1301:
                            if (isChild) break;
                            this.IDPublic = item.Value; break;
                        case 1302:
                            if (isChild) break;
                            this.IDOwnReality = item.Value; break;
                        case 1303:
                            if (isChild) break;
                            this.IDPZP = item.Value; break;
                        case 1304:
                            if (isChild) break;
                            this.IDEmployer = item.Value; break;
                        case 1305:
                            if (isChild) break;
                            this.IDOthers = item.Value; break;

                        case 1400: this.InsuranceTraveling = item.Value; break;

                        case 1500: this.InsuranceOtherDetail = item.Value; break;
                    }
                }
            }

            public SectorSettings(List<ProcCategorySectors_Result> data, bool isChild)
            {
                if (data == null)
                    return;

                foreach (var item in data)
                {
                    switch (item.SectorID)
                    {
                        case 1101:
                            if (isChild) break;
                            this.IIDeath = true; break;
                        case 1102:
                            if (isChild) break;
                            this.IIDeathInjury = true; break;
                        case 1103: this.IIPermanentInjury = true; break;
                        case 1104: this.IIInvalidity = true; break;
                        case 1105: this.IILowInjury = true; break;
                        case 1106: this.IISeriousDiseases = true; break;
                        case 1107: this.InsuranceSurvivalGuaranteed = true; break;
                        case 1108: this.InsuranceSurvivalInvestFund = true; break;

                        case 1201:
                            if (isChild) break;
                            this.IPHomeReality = true; break;
                        case 1202:
                            if (isChild) break;
                            this.IPOtherReality = true; break;
                        case 1203:
                            if (isChild) break;
                            this.IPVehicle = true; break;
                        case 1204:
                            if (isChild) break;
                            this.IPOthers = true; break;

                        case 1301:
                            if (isChild) break;
                            this.IDPublic = true; break;
                        case 1302:
                            if (isChild) break;
                            this.IDOwnReality = true; break;
                        case 1303:
                            if (isChild) break;
                            this.IDPZP = true; break;
                        case 1304:
                            if (isChild) break;
                            this.IDEmployer = true; break;
                        case 1305:
                            if (isChild) break;
                            this.IDOthers = true; break;

                        case 1400: this.InsuranceTraveling = true; break;

                        case 1500: this.InsuranceOtherDetail = true; break;
                    }
                }
            }
        }

        public static void SetBoolClientPartner(FormFUSCZ form, BoolClientPartnerChild property, BoolClientPartnerChild value)
        {
            if (property == null || value == null)
                return;

            property.Client |= value.Client;

            if (form.FUSForPartner)
                property.Partner |= value.Partner;

            if (form.FUSForChild)
                property.Child |= value.Child;
        }

        public static void SetInvest(FormFUSCZ form, BoolClientPartnerChild values, FinancialInsuranceChoices choices)
        {
            if (values == null)
                values = new BoolClientPartnerChild();
            if (choices == null || choices.BalancedProfil == null || choices.DynamicProfil == null || choices.KonzervativeProfil == null)
                return;

            values.Client |= FusHelper.LogOR(choices.BalancedProfil.Client, choices.DynamicProfil.Client, choices.KonzervativeProfil.Client);

            if (form.FUSForPartner)
            {
                values.Partner |= FusHelper.LogOR(choices.BalancedProfil.Partner, choices.DynamicProfil.Partner, choices.KonzervativeProfil.Partner);
            }

            if (form.FUSForChild)
            {
                values.Child |= FusHelper.LogOR(choices.BalancedProfil.Child, choices.DynamicProfil.Child, choices.KonzervativeProfil.Child);
            }
        }

        public static void SetBoolClientPartnerAssign(FormFUSCZ form, BoolClientPartnerChild property, BoolClientPartnerChild value)
        {
            if (value == null)
                return;

            if (property == null)
                property = new BoolClientPartnerChild();

            property.Client = value.Client;

            if (form.FUSForPartner)
                property.Partner = value.Partner;

            if (form.FUSForChild)
                property.Child = value.Child;
        }

        public static void SetBoolClientPartner(FormFUSCZ form, BoolClientPartner property, BoolClientPartner value)
        {
            if (property == null || value == null)
                return;

            property.Client |= value.Client;

            if (form.FUSForPartner)
                property.Partner |= value.Partner;
        }

        public static FinancialInsurance FIns(FormFUSCZ form)
        {
            FinancialInsurance result = new FinancialInsurance(true)
            {
                IIDeath = new BoolClientPartnerChild(),
                IIDeathInjury = new BoolClientPartnerChild(),
                IIPermanentInjury = new BoolClientPartnerChild(),
                IIPermanentInjuryFree = false,
                IIPermanentInjurySingle = false,
                IIInvalidity = new BoolClientPartnerChild(),
                IIInvalidityFree = false,
                IIInvaliditySingle = false,
                IILowInjury = new BoolClientPartnerChild(),
                IISeriousDiseases = new BoolClientPartnerChild(),
                InsuranceSurvivalGuaranteed = new BoolClientPartnerChild(),
                InsuranceSurvivalInvestFund = new BoolClientPartnerChild(),
                RequiredInvestProfile = new BoolClientPartnerChild(),
                IPHomeReality = new BoolClientPartner(),
                IPOtherReality = new BoolClientPartner(),
                IPAllRisk = new BoolClientPartner(),
                IPTheft = new BoolClientPartner(),
                IPVehicleOthers = new BoolClientPartner(),
                IPOthers = new BoolClientPartner(),
                IDPublic = new BoolClientPartner(),
                IDOwnReality = new BoolClientPartner(),
                IDPZP = new BoolClientPartner(),
                IDEmployer = new BoolClientPartnerChild(),
                IDOthers = new BoolClientPartnerChild(),
                InsuranceTravelingDetail = new BoolClientPartnerChild(),

                BalancedProfil = new BoolClientPartnerChild(),
                DynamicProfil = new BoolClientPartnerChild(),
                KonzervativeProfil = new BoolClientPartnerChild(),
                InsuranceTaxAdvantage = null,
                InsuranceOtherDetail = new BoolClientPartnerChild(),
                Discrepancies = string.Empty,
                InsuranceNote = string.Empty,
                InsuranceIndividual = false,
                InsuranceProperty = false,
                InsuranceDamage = false,
                InsuranceTraveling = false
            };

            #region functions
            Func<BoolClientPartnerChild, bool, bool?, int> SetCPC = (property, value, designation) =>
            {
                if (!designation.HasValue)
                    property.Client = value;
                else if (!designation.Value)
                    property.Partner = value;
                else
                    property.Child = value;

                return 0;
            };

            Func<BoolClientPartner, bool, bool?, int> SetCP = (property, value, designation) =>
            {
                if (!designation.HasValue)
                    property.Client = value;
                else if (!designation.Value)
                    property.Partner = value;

                return 0;
            };



            #endregion

            foreach (var sector in form.SectorInsurances)
            {
                bool? designation = null;

                if (IsPartnerSector(form, sector))
                    designation = false;
                else if (IsChildSector(form, sector))
                    designation = true;

                //SetCPC(result.IIDeath, sector.InsuranceChoices.IIDeath, designation);
                SetBoolClientPartner(form, result.IIDeath, sector.InsuranceChoices.IIDeath);
                SetBoolClientPartner(form, result.IIDeathInjury, sector.InsuranceChoices.IIDeathInjury);

                SetBoolClientPartner(form, result.IIPermanentInjury, sector.InsuranceChoices.IIPermanentInjury);
                //SetCPC(result.IIPermanentInjury, sector.InsuranceChoices.IIPermanentInjury, designation);
                result.IIPermanentInjuryFree |= sector.InsuranceChoices.IIPermanentInjuryFree;
                result.IIPermanentInjurySingle |= sector.InsuranceChoices.IIPermanentInjurySingle;

                SetBoolClientPartner(form, result.IIInvalidity, sector.InsuranceChoices.IIInvalidity);
                //SetCPC(result.IIInvalidity, sector.InsuranceChoices.IIInvalidity, designation);
                result.IIInvalidityFree |= sector.InsuranceChoices.IIInvalidityFree;
                result.IIInvaliditySingle |= sector.InsuranceChoices.IIInvaliditySingle;

                SetBoolClientPartner(form, result.IILowInjury, sector.InsuranceChoices.IILowInjury);
                //SetCPC(result.IILowInjury, sector.InsuranceChoices.IILowInjury, designation);
                SetBoolClientPartner(form, result.IISeriousDiseases, sector.InsuranceChoices.IISeriousDiseases);
                //SetCPC(result.IISeriousDiseases, sector.InsuranceChoices.IISeriousDiseases, designation);
                SetBoolClientPartner(form, result.InsuranceSurvivalGuaranteed, sector.InsuranceChoices.InsuranceSurvivalGuaranteed);
                //SetCPC(result.InsuranceSurvivalGuaranteed, sector.InsuranceChoices.InsuranceSurvivalGuaranteed, designation);
                SetBoolClientPartner(form, result.InsuranceSurvivalInvestFund, sector.InsuranceChoices.InsuranceSurvivalInvestFund);
                //SetCPC(result.InsuranceSurvivalInvestFund, sector.InsuranceChoices.InsuranceSurvivalInvestFund, designation);
                //SetCPC(result.RequiredInvestProfile, sector.InsuranceChoices.RequiredInvestProfile, designation);
                SetInvest(form, result.RequiredInvestProfile, sector.InsuranceChoices);




                SetBoolClientPartner(form, result.IPHomeReality, sector.InsuranceChoices.IPHomeReality);
                //	SetCP(result.IPHomeReality, sector.InsuranceChoices.IPHomeReality, designation);
                SetBoolClientPartner(form, result.IPOtherReality, sector.InsuranceChoices.IPOtherReality);
                //	SetCP(result.IPOtherReality, sector.InsuranceChoices.IPOtherReality, designation);
                SetBoolClientPartner(form, result.IPAllRisk, sector.InsuranceChoices.IPAllRisk);
                //SetCP(result.IPAllRisk, sector.InsuranceChoices.IPAllRisk, designation);
                SetBoolClientPartner(form, result.IPTheft, sector.InsuranceChoices.IPTheft);
                ///SetCP(result.IPTheft, sector.InsuranceChoices.IPTheft, designation);
                SetBoolClientPartner(form, result.IPVehicleOthers, sector.InsuranceChoices.IPVehicleOthers);
                //SetCP(result.IPVehicleOthers, sector.InsuranceChoices.IPVehicleOthers, designation);
                SetBoolClientPartner(form, result.IPOthers, sector.InsuranceChoices.IPOthers);
                //SetCP(result.IPOthers, sector.InsuranceChoices.IPOthers, designation);

                SetBoolClientPartner(form, result.IDPublic, sector.InsuranceChoices.IDPublic);
                //SetCP(result.IDPublic, sector.InsuranceChoices.IDPublic, designation);
                SetBoolClientPartner(form, result.IDOwnReality, sector.InsuranceChoices.IDOwnReality);
                //SetCP(result.IDOwnReality, sector.InsuranceChoices.IDOwnReality, designation);
                SetBoolClientPartner(form, result.IDPZP, sector.InsuranceChoices.IDPZP);
                //SetCP(result.IDPZP, sector.InsuranceChoices.IDPZP, designation);
                SetBoolClientPartner(form, result.IDEmployer, sector.InsuranceChoices.IDEmployer);
                //SetCPC(result.IDEmployer, sector.InsuranceChoices.IDEmployer, designation);
                SetBoolClientPartner(form, result.IDOthers, sector.InsuranceChoices.IDOthers);
                //SetCPC(result.IDOthers, sector.InsuranceChoices.IDOthers, designation);

                SetBoolClientPartner(form, result.InsuranceTravelingDetail, sector.InsuranceChoices.InsuranceTravelingDetail);
                //SetCPC(result.InsuranceTravelingDetail, sector.InsuranceChoices.InsuranceTravelingDetail, designation);

                SetBoolClientPartner(form, result.BalancedProfil, sector.InsuranceChoices.BalancedProfil);
                //SetCPC(result.BalancedProfil, sector.InsuranceChoices.BalancedProfil, designation);
                SetBoolClientPartner(form, result.DynamicProfil, sector.InsuranceChoices.DynamicProfil);
                //SetCPC(result.DynamicProfil, sector.InsuranceChoices.DynamicProfil, designation);
                SetBoolClientPartner(form, result.KonzervativeProfil, sector.InsuranceChoices.KonzervativeProfil);
                //SetCPC(result.KonzervativeProfil, sector.InsuranceChoices.KonzervativeProfil, designation);

                result.InsuranceTaxAdvantage |= sector.InsuranceChoices.TaxAdvantage;

                SetBoolClientPartner(form, result.InsuranceOtherDetail, sector.InsuranceChoices.InsuranceOtherDetail);
                //SetCPC(result.InsuranceOtherDetail, sector.InsuranceChoices.InsuranceOtherDetail, designation);

                result.Discrepancies += (sector.InsuranceChoices.Discrepancies + " ");

                result.InsuranceNote += (sector.InsuranceChoices.InsuranceNote + " ");

                result.InsuranceIndividual |= sector.InsuranceChoices.InsuranceIndividual;
                result.InsuranceProperty |= sector.InsuranceChoices.InsuranceProperty;
                result.InsuranceDamage |= sector.InsuranceChoices.InsuranceDamage;
                result.InsuranceTraveling |= sector.InsuranceChoices.InsuranceTraveling;
            }

            return result;
        }

        internal static void SetCheckedCheckBoxes(SectorSettings settings, StandardFUSCZModel model)
        {
            try
            {
                if (settings.InsuranceTraveling && !model.FormFUSCZ.FUSForChild && !model.FormFUSCZ.FUSForPartner && !ExtensionMethods.AtleastOnetrue(settings.InsuranceIndividual, settings.InsuranceSurvival, settings.InsuranceProperty, settings.InsuranceDamage))
                    settings.IsInsuranceTravelingSelected = true;
            }
            catch (Exception e)
            {
                //App.HandleException(e);
            }
        }
    }
}
