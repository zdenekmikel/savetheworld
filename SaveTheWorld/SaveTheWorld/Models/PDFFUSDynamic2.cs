﻿

using iTextSharp.text;
using iTextSharp.text.pdf;
using SaveTheWorld.Extensions;
using SaveTheWorld.Models.PDF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using static SaveTheWorld.Models.FusHelper;

namespace SaveTheWorld.Models
{
    public class PDFFUSDynamic2 : PDFBase
    {
        #region properties

        public string Name { get { return "eFUS"; } }
        public string FileName { get { return Name; } }
        string space = " "; // mezera mezi nazvem pole a hodnotou - resource ji osekaval
        double mainTablePadding = 1.5; // 0.5, 1, 1.5
        double innerTablePadding = 1;
        double leadingMultiplied = 1.2f;
        float paddingFromHeader = 1;
        float separatingPadding = 4;
        float paragraphPadding = 5;
        bool FusZV = false;

        BaseColor subBorderColor = new BaseColor(160, 160, 160);
        BaseColor sectionBTitleColor = new BaseColor(180, 180, 180);

        PageEvents helper;

        // show parts on last pages
        bool showItem1 = true;
        bool showItem2 = true;
        bool showItem3 = true;
        bool showItem4 = false;
        bool showItem5 = false;
        bool showItem6 = false;
        bool showItem7 = false;
        bool showItem9 = true;
        bool showItem8 = true;
        bool showItem8Part1 = false;
        bool showItem8Part2 = false;
        bool showItem8Part3 = false;
        bool showItem8Part4 = false;
        bool showItem8Part5 = true;

        double paddingToCompensateCheckBox = 2.5;

        private BaseFont chbfont;

        private BaseFont bfontNormal;
        private Font fn9;
        private Font fb9;
        private Font fn10;
        private Font fb10;
        private Font fbWhiteBig;
        private Font fSectionHeader;
        private Font fSectionSubHeader;
        private Font fSectionSubHeaderNormal;
        private Font fi9;
        private Font fbi9;
        private Document d;
        private PdfWriter writer;
        private NumberFormatInfo nfi;
        private Font fnCheckbox;
        private Font fnCheckboxWhite;
        private Font fnDashedLine;

        private BaseFont bfontBold;
        private BaseFont bfontDashed;

        private Font fTextLittleNormal;
        private Font fTextLittleBold;

        private Font fTextSmallNormal;
        private Font fTextSmallBold;

        private Font fLastPagesNormal;
        private Font fLastPagesBold;
        private Font fLastPagesNormalItalic;
        private Font fLastPagesBoldItalic;
        private Font fLastPagesRed;

        private Font fTextMediumNormal;
        private Font fTextMediumBold;

        private Font fTextNormal;
        private Font fTextNormalUnderline;
        private Font fTextNormalWhite;
        private Font fTextNormalItalic;
        private Font fTextBold;
        private Font fTextBoldItalic;
        private Font fTextBoldUnderline;

        private string NullValueText = "";

        private bool IsSmsFus()
        {
            if (this.FormFUSCZ != null && (this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMS))
                return true;
            else
                return false;
        }

        #endregion properties

        public PDFFUSDynamic2()
        {
            // setup fonts
            bfontDashed = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            bfontNormal = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            bfontBold = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahomabd.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            chbfont = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\WINGDNG2.TTF", BaseFont.IDENTITY_H, false);

            fn9 = new Font(bfontNormal, 9, Font.NORMAL, BaseColor.BLACK);
            fnDashedLine = new Font(bfontDashed, 3.92f, Font.NORMAL, BaseColor.BLACK);

            fb9 = new Font(bfontNormal, 9, Font.BOLD, BaseColor.BLACK);
            fn10 = new Font(bfontNormal, 10, Font.NORMAL, BaseColor.BLACK);
            fb10 = new Font(bfontNormal, 10, Font.BOLD, BaseColor.BLACK);
            fbWhiteBig = new Font(bfontBold, 10.5f, Font.NORMAL, BaseColor.WHITE);
            fi9 = new Font(bfontNormal, 9, Font.ITALIC, BaseColor.BLACK);
            fbi9 = new Font(bfontNormal, 9, Font.BOLDITALIC, BaseColor.BLACK);
            fnCheckbox = new Font(bfontNormal, 20, Font.NORMAL, BaseColor.BLACK);
            fnCheckboxWhite = new Font(bfontNormal, 20, Font.NORMAL, BaseColor.WHITE);

            fSectionHeader = new Font(bfontNormal, 9f, Font.BOLD, BaseColor.WHITE);
            fSectionSubHeader = new Font(bfontNormal, 9, Font.BOLD, BaseColor.BLACK);
            //fSectionSubHeader = new Font(bfontBold, 9, Font.UNDERLINE, BaseColor.BLACK);
            fSectionSubHeaderNormal = new Font(bfontNormal, 9, Font.NORMAL, BaseColor.BLACK);

            int lastPagesFontSize = 8;

            fTextNormal = new Font(bfontNormal, 8, Font.NORMAL, BaseColor.BLACK);
            fTextNormalUnderline = new Font(bfontNormal, 8, Font.UNDERLINE, BaseColor.BLACK);
            fTextNormalWhite = new Font(bfontNormal, 8, Font.NORMAL, BaseColor.WHITE);
            fTextNormalItalic = new Font(bfontNormal, 8, Font.ITALIC, BaseColor.BLACK);
            fTextBold = new Font(bfontNormal, 8, Font.BOLD, BaseColor.BLACK);
            fTextBoldItalic = new Font(bfontNormal, 8, Font.BOLDITALIC, BaseColor.BLACK);
            fTextBoldUnderline = new Font(bfontBold, 8, Font.UNDERLINE, BaseColor.BLACK);

            fLastPagesNormal = new Font(bfontNormal, lastPagesFontSize, Font.NORMAL, BaseColor.BLACK);
            fLastPagesBold = new Font(bfontNormal, lastPagesFontSize, Font.BOLD, BaseColor.BLACK);
            fLastPagesNormalItalic = new Font(bfontNormal, lastPagesFontSize, Font.ITALIC, BaseColor.BLACK);
            fLastPagesBoldItalic = new Font(bfontNormal, lastPagesFontSize, Font.BOLDITALIC, BaseColor.BLACK);
            fLastPagesRed = new Font(bfontNormal, lastPagesFontSize, Font.UNDERLINE, BaseColor.RED);

            //fTextMediumNormal = new Font(bfontNormal, 7, Font.NORMAL, BaseColor.BLACK);
            //fTextMediumBold = new Font(bfontNormal, 7, Font.BOLD, BaseColor.BLACK);

            //fTextSmallNormal = new Font(bfontNormal, 6, Font.NORMAL, BaseColor.BLACK);
            //fTextSmallBold = new Font(bfontNormal, 6, Font.BOLD, BaseColor.BLACK);

            //fTextLittleNormal = new Font(bfontNormal, 5, Font.NORMAL, BaseColor.BLACK);
            //fTextLittleBold = new Font(bfontNormal, 5, Font.BOLD, BaseColor.BLACK);

            fTextMediumNormal = fTextNormal;
            fTextMediumBold = fTextBold;

            fTextSmallNormal = fTextNormal;
            fTextSmallBold = fTextBold;

            fTextLittleNormal = fTextNormal;
            fTextLittleBold = fTextBold;
        }

        #region LoadFUS
        /// <summary>
        /// Vytáhne definici z db
        /// </summary>
        /// <param name="fUSID"></param>
        /// <param name="serverPath"></param>
        /// <param name="pdfType"></param>
        public void LoadFUS(int fusID, string pdfType)
        {
            this.PdfType = pdfType;

            var fus = DbProvider.CustomerFUS(0, 0, (int)fusID);
            //if (fus != null)
            //    StandardFUSCZModel.CheckAccess(fus.AgentID, WebData.Current.RequestedAgent.AgentID);

            if (fus != null && !string.IsNullOrWhiteSpace(fus.XmlContent))
            {
                var model = fus.XmlContent.DeSearialize<FormFUSCZ>();
                if (model != null)
                {
                    this.FormFUSCZ = model;
                    this.FormFUSCZ.FUSID = model.FUSID;
                    this.FormFUSCZ.FUSType = fus.FUSTypeCode;
                }
            }
            else
            {
                this.IsValid = false;
            }


            LoadPDF(pdfType);
        }

        void LoadPDF(string pdfType)
        {
            if (this.IsValid)
            {
                string fileName = string.Empty;

                if (pdfType == Constants.FUSPDFTypes.Draft)
                    fileName = Path.GetFileName(this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV ? Files.ZV.Draft : Files.FUS.Draft);
                else
                    fileName = Path.GetFileName(this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV ? Files.ZV.Raw : Files.FUS.Raw);

                //this.FilePath = Path.Combine(serverPath, fileName);
                if (this.FormFUSCZ.Client != null)
                    this.PasswordParty = this.FormFUSCZ.Client.PartyCode.ValueOrDefault(v => v.Replace("/", "").Replace(" ", "")) ?? FormFUSCZ.Client.TradeLicenceNo.ValueOrDefault(v => v.Replace("/", "").Replace(" ", ""));
            }
        }
        #endregion

        public void RenderPdf(MemoryStream mStream)
        {
            helper = new PageEvents(this.FormFUSCZ.FUSID, this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV, this.PdfType == Constants.FUSPDFTypes.Draft, version: "160609");

            // set number formating
            nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " "; //ConfigurationSettings.AppSettings["NumberGroupSeparator"];
            nfi.NumberDecimalSeparator = ","; //ConfigurationSettings.AppSettings["NumberDecimalSeparator"];

            //d = new Document(PageSize.A4, 35, 35, 60, 80); //W=595, H=842
            d = new Document(new RectangleReadOnly(563, 798), 25, 25, 45, 45);

            writer = PdfWriter.GetInstance(d, mStream);
            writer.PageEvent = helper;

            // meta data
            d.AddTitle("eFUS");
            d.AddCreator("Fincentrum");
            d.AddAuthor("Fincentrum");
            d.AddCreationDate();

            // open document
            d.Open();

            //helper.fnDashedLine = fnDashedLine;

            RenderContent(d);

            writer.CloseStream = false;
            d.Close();
            mStream.Position = 0;

        }

        private void RenderContent(Document d)
        {
            var hr = new Table();
            hr.T.WidthPercentage = 100;
            var hr1cell = new Cell().PaddingTop(0.2).PaddingBottom(0.2).BorderWidth(0.2);
            hr1cell.C.Border = Rectangle.TOP_BORDER;
            hr1cell.C.BorderColor = BaseColor.BLACK;
            hr.Add(hr1cell);

            //showItem3 =
            //    this.FormFUSCZ.FSLoanConsumer || this.FormFUSCZ.FSLoanOther // uvery
            //    || this.FormFUSCZ.FSBuildSaving // stavebni sporeni
            //    || this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther; // zivotni

            showItem4 = this.FormFUSCZ.FSLoanConsumer || this.FormFUSCZ.FSLoanOther;

            showItem8Part2 = this.FormFUSCZ.FSLoanConsumer;

            showItem8 = FormFUSCZ.FSBuildSaving || FormFUSCZ.FSInsuranceLiability || FormFUSCZ.FSInsuranceLive || FormFUSCZ.FSInsuranceLiveOther || FormFUSCZ.FSInsuranceOthers
                    || FormFUSCZ.FSInsurancePersonal || FormFUSCZ.FSInsuranceProperty || FormFUSCZ.FSInsuranceTravel || FormFUSCZ.FSInvestment
                    || FormFUSCZ.FSLoanConsumer || FormFUSCZ.FSOthers || FormFUSCZ.FSSavingDPS || FormFUSCZ.FSSavingDS || FormFUSCZ.FSSavingIncreaseDPS || FormFUSCZ.FSSavingIncreaseTF
                    || FormFUSCZ.FSSavingTFToDPS || FormFUSCZ.FSTermDepositsOrOtherJT;

            // main table
            d.Add((PdfPTable)new Table().With(mainTable =>
            {
                mainTable.T.WidthPercentage = 100;
                mainTable.Style = new Cell().Padding(mainTablePadding);
                mainTable.Style.C.SetLeading(0, 1.1f);

                FusZV = this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV;

                if (FusZV)
                {
                    RenderSection_01(mainTable);
                    RenderSection_02(mainTable);
                    RenderSection_03(mainTable);
                    RenderSection_04(mainTable);
                    RenderSection_05(mainTable);
                    RenderSection_06(mainTable);
                    RenderSection_08(mainTable);
                    RenderInformationAndAnnouncementZV(mainTable);
                }
                else
                {
                    RenderSection_01(mainTable);
                    RenderSection_02(mainTable);
                    RenderSection_03(mainTable);
                    RenderSection_04(mainTable);
                    RenderSection_05(mainTable);
                    RenderSection_06(mainTable);
                    RenderSection_07(mainTable);
                    RenderSection_08(mainTable);
                    RenderInformationAndAnnouncement(mainTable);
                }

            }));

        }

        void RenderSection_01(Table table)
        {
            SetTablePaddingVertical(table, innerTablePadding);

            AddSectionHeader(table, App_GlobalResources.FusCZ.S1Section1Header);
            AddSectionSubHeader(table, App_GlobalResources.FusCZ.S1Associate);

            string Lastname = string.IsNullOrEmpty(this.FormFUSCZ.AgentDetail.LastName) ? "" : this.FormFUSCZ.AgentDetail.LastName; // Příjmení / Obchodní firma
            string FirstName = string.IsNullOrEmpty(this.FormFUSCZ.AgentDetail.FirstName) ? "" : this.FormFUSCZ.AgentDetail.FirstName; // Jméno / u s.r.o. příjmení jednatele
            string PartyCode = string.IsNullOrEmpty(this.FormFUSCZ.AgentDetail.PartyCode) ? "" : this.FormFUSCZ.AgentDetail.PartyCode; // IČO
            string AgentNo = string.IsNullOrEmpty(this.FormFUSCZ.AgentDetail.AgentNo) ? "" : this.FormFUSCZ.AgentDetail.AgentNo; // Ev. Č.
            string Address = string.IsNullOrEmpty(this.FormFUSCZ.AgentDetail.Address) ? "" : this.FormFUSCZ.AgentDetail.Address; // Místo podnikání (sídlo) / Bydliště (pokud je odlišné)

            AddPaddingLine(table, separatingPadding);

            table.Add(new Table(table, 5, 5).With(t =>
            {
                Cell lastNameCell = new Cell(table);
                lastNameCell.Add(new Chunk(App_GlobalResources.FusCZ.S1LastName + space, fTextNormal));
                lastNameCell.Add(new Chunk(Lastname, fTextBold)).PaddingBottom(paddingToCompensateCheckBox);
                t.Add(lastNameCell);

                Cell firstNameCell = new Cell(table);
                firstNameCell.Add(new Chunk(App_GlobalResources.FusCZ.S1FirstName + space, fTextNormal));
                firstNameCell.Add(new Chunk(FirstName, fTextBold));
                t.Add(firstNameCell);

            }));

            table.Add(new Table(table, 5, 5).With(t =>
            {
                Cell partyCodeCell = new Cell(table);
                partyCodeCell.Add(new Chunk(App_GlobalResources.FusCZ.S1PartyCode + space, fTextNormal));
                partyCodeCell.Add(new Chunk(PartyCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox);
                t.Add(partyCodeCell);

                Cell agentNoCell = new Cell(table);
                agentNoCell.Add(new Chunk(App_GlobalResources.FusCZ.S1AgentNo + space, fTextNormal));
                agentNoCell.Add(new Chunk(AgentNo, fTextBold));
                t.Add(agentNoCell);
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                Cell addressCell = new Cell(table);
                addressCell.Add(new Chunk(App_GlobalResources.FusCZ.S1Address + space, fTextNormal));
                addressCell.Add(new Chunk(Address, fTextBold));
                addressCell.Indent(0, 124);
                t.Add(addressCell);
            }));


            AddPaddingLine(table, separatingPadding);
            SetTablePaddingDefault(table);
        }

        #region Section 2

        void RenderSection_02(Table table)
        {
            SetTablePaddingVertical(table, innerTablePadding);

            if (FusZV)
            {
                RenderSection_02Part1_ZV(table);
                RenderSection_02Part2_ZV(table);
            }
            else
            {
                RenderSection_02Part1(table);
                RenderSection_02Part2(table);
            }

            SetTablePaddingDefault(table);
        }

        private void RenderSection_02Part1(Table table)
        {
            #region Announcements - section 1

            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S2Section2Header);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Section2Part1, fTextBold));
                }));

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Table(table, 5.5, 294.5).With(t =>
            {
                t.Add(new Cell(t));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S2InformationOfContent, fTextBold));

            }));
            table.Add(new Table(table, 4, 1, 1, 1).With(t =>
            {
                SetTablePaddingVertical(t, innerTablePadding);

                t.Add(new Cell(t));

                if (this.FormFUSCZ.FUSForClient)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Client, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                { t.Add(new Cell(t)); }

                if (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Partner, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                { t.Add(new Cell(t)); }

                if (this.FormFUSCZ.FUSForChild)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Child, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                { t.Add(new Cell(t)); }

            }));

            table.Add(new Table(table, 4, 0.1, 0.4, 0.4, 0.1, 0.1, 0.4, 0.4, 0.1, 0.1, 0.4, 0.4, 0.1).With(t =>
            {
                SetTablePaddingVertical(t, innerTablePadding);

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                if (this.FormFUSCZ.FUSForClient)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2NO, fTextBold).Align(Element.ALIGN_CENTER));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2YES, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                {
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                }

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                if (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2NO, fTextBold).Align(Element.ALIGN_CENTER));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2YES, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                {
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                }

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                if (this.FormFUSCZ.FUSForChild)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2NO, fTextBold).Align(Element.ALIGN_CENTER));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2YES, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                {
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                }

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                t.Add(new Cell(t));
            }));

            S2_Info(table, App_GlobalResources.FusCZ.S2BuildSaving, "building");
            S2_Info(table, App_GlobalResources.FusCZ.S2InsurancePersonal, "life");
            S2_Info(table, App_GlobalResources.FusCZ.S2AnnouncementLeases, "leasing");

            AddPaddingLine(table, separatingPadding);

            #endregion Announcements - section 1
        }

        private void RenderSection_02Part1_ZV(Table table)
        {
            #region Announcements - section 1

            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S2Section2Header);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Section2Part1, fTextBold));
                }));

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Table(table, 5.5, 294.5).With(t =>
            {
                t.Add(new Cell(t));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S2InformationOfContentZV, fTextBold));

            }));
            table.Add(new Table(table, 4, 1, 1, 1).With(t =>
            {
                SetTablePaddingVertical(t, innerTablePadding);

                t.Add(new Cell(t));

                if (this.FormFUSCZ.FUSForClient)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Client, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                { t.Add(new Cell(t)); }

                t.Add(new Cell(t));
                t.Add(new Cell(t));

            }));

            table.Add(new Table(table, 4, 0.1, 0.4, 0.4, 0.1, 0.1, 0.4, 0.4, 0.1, 0.1, 0.4, 0.4, 0.1).With(t =>
            {
                SetTablePaddingVertical(t, innerTablePadding);

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                if (this.FormFUSCZ.FUSForClient)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2NO, fTextBold).Align(Element.ALIGN_CENTER));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2YES, fTextBold).Align(Element.ALIGN_CENTER));
                }
                else
                {
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                }

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                t.Add(new Cell(t));
                t.Add(new Cell(t));

                t.Add(new Cell(t));
            }));

            S2_Info(table, App_GlobalResources.FusCZ.S2InsurancePersonal, "life");

            AddPaddingLine(table, separatingPadding);

            #endregion Announcements - section 1
        }

        private void RenderSection_02Part2(Table table)
        {
            int Section2Padding = 5; // left padding, not vertical

            #region Announcements - section 2

            bool? showSecondSection = false;

            showSecondSection = (this.FormFUSCZ.FSBuildSaving && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Client.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Client : showSecondSection;
            showSecondSection = (this.FormFUSCZ.FSBuildSaving && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Partner.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Partner : showSecondSection;
            showSecondSection = (this.FormFUSCZ.FSBuildSaving && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Child.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Child : showSecondSection;
            showSecondSection = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client : showSecondSection;
            showSecondSection = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Partner.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Partner : showSecondSection;
            showSecondSection = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Child.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Child : showSecondSection;
            showSecondSection = (this.FormFUSCZ.FSLeasingLoan && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLeases != null && this.FormFUSCZ.Announcement.AnnouncementLeases.Client.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLeases.Client : showSecondSection;
            showSecondSection = (this.FormFUSCZ.FSLeasingLoan && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLeases != null && this.FormFUSCZ.Announcement.AnnouncementLeases.Partner.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLeases.Partner : showSecondSection;
            showSecondSection = (this.FormFUSCZ.FSLeasingLoan && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLeases != null && this.FormFUSCZ.Announcement.AnnouncementLeases.Child.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLeases.Child : showSecondSection;

            if (showSecondSection.HasValue && showSecondSection.Value || true) // condition removed to show whole pdf
            {
                //AddPaddingLine(table, 1f);

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell warning = new Cell(table);
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S2Warning + space, fTextBold));
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S2Part2Information, fTextNormal));
                    t.Add(warning);

                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell cell = new Cell(table);
                    cell.Add(new Chunk(App_GlobalResources.FusCZ.S2Section2Part2, fTextBold));
                    t.Add(cell);

                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell cell = new Cell(table);
                    cell.Add(new Chunk(App_GlobalResources.FusCZ.S2AnnouncementATitle + space, fTextBold));
                    cell.Add(new Chunk(App_GlobalResources.FusCZ.S2AnnouncementAText, fTextNormal));
                    t.Add(cell.PaddingLeft(Section2Padding).AlignJustify().Indent(6, 17).Leading(0f, leadingMultiplied));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell warning = new Cell(table);
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S2AnnouncementB + space, fTextBold));
                    t.Add(warning.PaddingLeft(Section2Padding).Indent(6, 17));
                }));


                bool? clientAnnouncement = null;
                bool? partnerAnnouncement = null;

                if (this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementAct != null && !string.IsNullOrEmpty(this.FormFUSCZ.Announcement.AnnouncementAct.Client))
                    clientAnnouncement = this.FormFUSCZ.Announcement.AnnouncementAct.Client == "PERSONALLY";

                if (this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementAct != null && !string.IsNullOrEmpty(this.FormFUSCZ.Announcement.AnnouncementAct.Partner))
                    partnerAnnouncement = this.FormFUSCZ.Announcement.AnnouncementAct.Partner == "PERSONALLY";

                table.Style.PaddingLeft(2f);

                table.Add(new Table(table, 7, 1, 1, 0.5).With(t =>
                {
                    t.Style.Padding(mainTablePadding);


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementActTitle, fTextBold).With(c =>
                    {
                        c.Add(new Chunk("        " + App_GlobalResources.FusCZ.S2AnnouncementActText, fTextNormal));
                    }).PaddingLeft(2 * Section2Padding));

                    if (this.FormFUSCZ.FUSForClient)
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Client, fTextBold).AlignCenter());
                    }
                    else
                    { t.Add(new Cell(t)); }

                    if (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation)
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Partner, fTextBold).AlignCenter());
                    }
                    else
                    { t.Add(new Cell(t)); }

                    t.Add(new Cell(t));
                    AddPaddingLine(t, 1);

                    AddPaddingLine(t, 1f, 4);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementActOption1, fTextNormal).AlignRight());

                    if (this.FormFUSCZ.FUSForClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? clientAnnouncement.Value : false));
                    }
                    else
                    { t.Add(new Cell(t)); }

                    if (this.FormFUSCZ.Partner != null && (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation))
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerAnnouncement.HasValue ? partnerAnnouncement.Value : false));
                    }
                    else
                    { t.Add(new Cell(t)); }

                    t.Add(new Cell(t));


                    AddPaddingLine(t, 1f, 4);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementActOption2, fTextNormal).AlignRight());

                    if (this.FormFUSCZ.FUSForClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? !clientAnnouncement.Value : false));
                    }
                    else
                    { t.Add(new Cell(t)); }

                    if (this.FormFUSCZ.Partner != null && (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation))
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerAnnouncement.HasValue ? !partnerAnnouncement.Value : false));
                    }
                    else
                    { t.Add(new Cell(t)); }

                    t.Add(new Cell(t));

                }));

                bool? expone = null;

                if ((this.FormFUSCZ.FUSForClient || this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.FUSForChild) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementExpone != null)
                    expone = this.FormFUSCZ.Announcement.AnnouncementExpone;

                AddPaddingLine(table, 1f);
                table.Add(new Table(table, 1.03, 3.765, 0.5, 0.22, 0.5, 0.5, 0.315).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeTitle, fTextBold).PaddingLeft(2 * Section2Padding));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeText1 + "\n" + App_GlobalResources.FusCZ.S2AnnouncementExponeText2, fTextNormal).Leading(0f, 1.2f));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeOption2, fTextNormal).Align(Element.ALIGN_RIGHT));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, expone.HasValue ? !expone.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeOption1, fTextNormal).Align(Element.ALIGN_RIGHT));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, expone.HasValue ? expone.Value : false));
                    t.Add(new Cell(t));
                }));

                AddPaddingLine(table, 1f);

                table.Add(new Table(table, 15, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AdditionalAnnouncement, fTextNormal));
                    t.Add(new Cell(t));
                }));

                //table.Add(new Table(table, 1).With(t =>
                //{
                //	Cell warning = new Cell(table);
                //	warning.Add(new Chunk(App_GlobalResources.FusCZ.S2Warning + space, fTextSmallBold));
                //	warning.Add(new Chunk(App_GlobalResources.FusCZ.S2WarningAboutPart2, fTextSmallNormal));
                //	t.Add(warning.AlignJustify());
                //	AddPaddingLine(t, 2);
                //}));
                AddPaddingLine(table, separatingPadding);
            }

            #endregion Announcements - section 2
        }

        private void RenderSection_02Part2_ZV(Table table)
        {
            int Section2Padding = 5; // left padding, not vertical

            #region Announcements - section 2

            bool? showSecondSection = false;

            showSecondSection = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client.HasValue && !showSecondSection.Value) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client : showSecondSection;

            if (showSecondSection.HasValue && showSecondSection.Value || true)
            {
                //AddPaddingLine(table, 1f);

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell warning = new Cell(table);
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S2Warning + space, fTextBold));
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S2Part2InformationZV, fTextNormal));
                    t.Add(warning);

                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell cell = new Cell(table);
                    cell.Add(new Chunk(App_GlobalResources.FusCZ.S2Section2Part2, fTextBold));
                    t.Add(cell);

                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell cell = new Cell(table);
                    cell.Add(new Chunk(App_GlobalResources.FusCZ.S2AnnouncementATitle + space, fTextBold));
                    cell.Add(new Chunk(App_GlobalResources.FusCZ.S2AnnouncementATextZV, fTextNormal));
                    t.Add(cell.PaddingLeft(Section2Padding).AlignJustify().Indent(6, 17).Leading(0f, leadingMultiplied));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell warning = new Cell(table);
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S2AnnouncementBZV + space, fTextBold));
                    t.Add(warning.PaddingLeft(Section2Padding).Indent(6, 17));
                }));


                bool? clientAnnouncement = null;

                if (this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementAct != null && !string.IsNullOrEmpty(this.FormFUSCZ.Announcement.AnnouncementAct.Client))
                    clientAnnouncement = this.FormFUSCZ.Announcement.AnnouncementAct.Client == "PERSONALLY";

                table.Style.PaddingLeft(2f);

                table.Add(new Table(table, 7, 1, 1, 0.5).With(t =>
                {
                    t.Style.Padding(mainTablePadding);


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementActTitle, fTextBold).With(c =>
                    {
                        c.Add(new Chunk("        " + App_GlobalResources.FusCZ.S2AnnouncementActText, fTextNormal));
                    }).PaddingLeft(2 * Section2Padding));

                    if (this.FormFUSCZ.FUSForClient)
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S2Client, fTextBold).AlignCenter());
                    }
                    else
                    { t.Add(new Cell(t)); }

                    t.Add(new Cell(t));

                    t.Add(new Cell(t));
                    AddPaddingLine(t, 1);

                    AddPaddingLine(t, 1f, 4);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementActOption1, fTextNormal).AlignRight());

                    if (this.FormFUSCZ.FUSForClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? clientAnnouncement.Value : false));
                    }
                    else
                    { t.Add(new Cell(t)); }

                    t.Add(new Cell(t));

                    t.Add(new Cell(t));


                    AddPaddingLine(t, 1f, 4);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementActOption2, fTextNormal).AlignRight());

                    if (this.FormFUSCZ.FUSForClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? !clientAnnouncement.Value : false));
                    }
                    else
                    { t.Add(new Cell(t)); }

                    t.Add(new Cell(t));

                    t.Add(new Cell(t));

                }));

                bool? expone = null;

                if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementExpone != null)
                    expone = this.FormFUSCZ.Announcement.AnnouncementExpone;

                AddPaddingLine(table, 1f);
                table.Add(new Table(table, 1.03, 3.765, 0.5, 0.22, 0.5, 0.5, 0.315).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeTitle, fTextBold).PaddingLeft(2 * Section2Padding));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeText1 + "\n" + App_GlobalResources.FusCZ.S2AnnouncementExponeText2, fTextNormal).Leading(0f, 1.2f));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeOption2, fTextNormal).Align(Element.ALIGN_RIGHT));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, expone.HasValue ? !expone.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AnnouncementExponeOption1, fTextNormal).Align(Element.ALIGN_RIGHT));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, expone.HasValue ? expone.Value : false));
                    t.Add(new Cell(t));
                }));

                AddPaddingLine(table, 1f);

                table.Add(new Table(table, 15, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S2AdditionalAnnouncement, fTextNormal));
                    t.Add(new Cell(t));
                    AddPaddingLine(t, separatingPadding);
                }));

                //table.Add(new Table(table, 1).With(t =>
                //{
                //	Cell warning = new Cell(table);
                //	warning.Add(new Chunk(App_GlobalResources.FusCZ.S2Warning + space, fTextSmallBold));
                //	warning.Add(new Chunk(App_GlobalResources.FusCZ.S2WarningAboutPart2ZV, fTextSmallNormal));
                //	t.Add(warning.AlignJustify());
                //}));
            }

            #endregion Announcements - section 2
        }

        #endregion Section 2

        #region Section 3

        void RenderSection_03(Table table)
        {
            SetTablePaddingVertical(table, innerTablePadding);

            if (FusZV)
            {
                RenderSection_03_OverAllInfo_ZV(table);
                RenderSection_03_Client(table);
            }
            else
            {
                RenderSection_03_OverAllInfo(table);
                RenderSection_03_Client(table);
                RenderSection_03_Partner(table);
                RenderSection_03_Children(table);
            }

            SetTablePaddingDefault(table);
        }

        private void RenderSection_03_OverAllInfo(Table table)
        {
            #region OverallInfo

            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S3Section3Header);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    Cell warning = new Cell(t);
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S3Warning + space, fTextSmallBold));
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S3InitialWarningText, fTextSmallNormal));
                    t.Add(warning.AlignJustify());
                }));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Announcement1Part1, fTextBold));
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Announcement1Part2, fTextBold));
            }));

            table.Add(new Table(table, 0.5, 4, 0.5, 4, 0.5, 4).With(t =>
            {
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, this.FormFUSCZ.OverallInfo.StatementClientCode == "WASSPECIFIED"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoStatementClientCodeWASSPECIFIED, fTextNormal).AlignLeft());
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, this.FormFUSCZ.OverallInfo.StatementClientCode == "REFUSESET"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoStatementClientCodeREFUSESET, fTextNormal).AlignLeft());
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, this.FormFUSCZ.OverallInfo.StatementClientCode == "WASNTSPECIFIED"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoStatementClientCodeWASNTSPECIFIEDSelected, fTextNormal).AlignLeft());

                //if (this.FormFUSCZ.OverallInfo.StatementClientCode == Constants.FUSListOptions.WASNTSPECIFIED)
                //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoStatementClientCodeWASNTSPECIFIEDSelected, fTextNormal).AlignLeft());
                //else
                //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoStatementClientCodeWASNTSPECIFIED, fTextNormal).AlignLeft());
            }));

            //if (this.FormFUSCZ.OverallInfo.StatementClientCode == Constants.FUSListOptions.WASNTSPECIFIED)
            if (true)
            {
                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoSpecifiedEffective, fTextNormal));
                }));

                AddPaddingLine(table, separatingPadding);

                table.Add(new Table(table, 4, 4, 6).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoLastName + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.OverallInfo.LastName, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoFirstName + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.OverallInfo.FirstName, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoBirthDate + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.OverallInfo.BirthDate.HasValue ? FormFUSCZ.OverallInfo.BirthDate.Value.ToString("dd'/'MM'/'yyyy") : this.NullValueText, fTextBold)));
                }));

                AddPaddingLine(table, separatingPadding);

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoSpecifiedAgreement, fTextMediumNormal));
                }));
            }

            AddPaddingLine(table, separatingPadding);

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoClientParnerAnnouncement, fTextBold));
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonal, fTextBold));
            }));

            AddPaddingLine(table, separatingPadding);

            bool? clientAnnouncement = null;
            bool? clientAllowEdit = null;
            bool? partnerAnnouncement = null;
            bool? partnerAllowEdit = null;

            if (!string.IsNullOrEmpty(this.FormFUSCZ.Client.ClientIDEnc))
            {
                clientAnnouncement = true;
                clientAllowEdit = this.FormFUSCZ.Client.AllowEdit;
            }
            else
                clientAnnouncement = false;

            if (this.FormFUSCZ.Partner != null)
                if (!string.IsNullOrEmpty(this.FormFUSCZ.Partner.ClientIDEnc))
                {
                    partnerAnnouncement = true;
                    partnerAllowEdit = this.FormFUSCZ.Partner.AllowEdit;
                }
                else
                    partnerAnnouncement = false;

            if (this.FormFUSCZ.FUSForClient)
            {
                table.Add(new Table(table, 1, 0.4, 4, 0.4, 7.7).With(t =>
                {
                    t.Style.PaddingBottom(-2);

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoClient, fTextBold));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? !clientAnnouncement.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementNo, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? clientAnnouncement.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementYes, fTextNormal).AlignJustifyAll());
                }));

                table.Add(new Table(table, 143, 7, 104).With(t =>
                {
                    t.Style.PaddingBottom(-2);

                    t.Add(new Cell(table, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart1, fTextNormal).AlignJustifyAll());
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAllowEdit.HasValue ? !clientAllowEdit.Value : false));
                    t.Add(new Cell(table, space + App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart2, fTextNormal).AlignJustifyAll());
                }));

                table.Add(new Table(table, 8.5).With(t =>
                {
                    t.Style.PaddingTop(-1);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart3, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart3OR, fTextNormalUnderline))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart3_1, fTextNormal))
                        .AlignJustifyAll().PaddingBottom(2));
                }));

                table.Add(new Table(table, 30, 7, 220).With(t =>
                {
                    t.Style.PaddingTop(-2);

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart4, fTextNormal));
                    t.Add(CreateCheckboxS3(t, Element.ALIGN_CENTER, clientAllowEdit.HasValue ? clientAllowEdit.Value : false));
                    t.Add(new Cell(t, space + App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart5, fTextNormal));
                }));

                AddPaddingLine(table, separatingPadding);
            }

            if (this.FormFUSCZ.Partner != null && (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation))
            {
                table.Add(new Table(table, 1, 0.4, 4, 0.4, 7.7).With(t =>
                {
                    t.Style.PaddingBottom(-2);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoPartner, fTextBold));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerAnnouncement.HasValue ? !partnerAnnouncement.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementNo, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerAnnouncement.HasValue ? partnerAnnouncement.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementYes, fTextNormal).AlignJustifyAll());
                }));

                table.Add(new Table(table, 143, 7, 104).With(t =>
                {
                    t.Style.PaddingBottom(-2);
                    t.Add(new Cell(table, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart1, fTextNormal).AlignJustifyAll());
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerAllowEdit.HasValue ? !partnerAllowEdit.Value : false));
                    t.Add(new Cell(table, space + App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart2, fTextNormal).AlignJustifyAll());
                }));

                table.Add(new Table(table, 8.5).With(t =>
                {
                    t.Style.PaddingTop(-1);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart3, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart3OR, fTextNormalUnderline))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart3_1, fTextNormal))
                        .AlignJustifyAll().PaddingBottom(2));
                }));

                table.Add(new Table(table, 30, 7, 220).With(t =>
                {
                    t.Style.PaddingTop(-2);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart4, fTextNormal));
                    t.Add(CreateCheckboxS3(t, Element.ALIGN_CENTER, partnerAllowEdit.HasValue ? partnerAllowEdit.Value : false));
                    t.Add(new Cell(t, space + App_GlobalResources.FusCZ.S3OverallInfoAnnouncementPersonalPart5, fTextNormal));
                }));

                AddPaddingLine(table, separatingPadding);
            }

            #endregion OverallInfo
        }

        private void RenderSection_03_OverAllInfo_ZV(Table table)
        {
            #region OverallInfo

            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S3Section3HeaderZV);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    Cell warning = new Cell(t);
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S3Warning + space, fTextSmallBold));
                    warning.Add(new Chunk(App_GlobalResources.FusCZ.S3InitialWarningText, fTextSmallNormal));
                    t.Add(warning.AlignJustify());
                }));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            AddPaddingLine(table, separatingPadding);

            bool? clientAnnouncement = null;

            if (!string.IsNullOrEmpty(this.FormFUSCZ.Client.ClientIDEnc))
            {
                clientAnnouncement = true;
            }
            else
                clientAnnouncement = false;

            if (this.FormFUSCZ.FUSForClient)
            {
                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ClientAnnouncementZV, fTextBold).AlignJustify());
                }));

                table.Add(new Table(table, 1, 2, 1, 30).With(t =>
                {
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? clientAnnouncement.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Yes, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientAnnouncement.HasValue ? !clientAnnouncement.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3No, fTextNormal));
                }));

                AddPaddingLine(table, separatingPadding);
            }

            #endregion OverallInfo
        }

        private void RenderSection_03_Client(Table table)
        {
            #region Client

            bool clientIsCompany = this.FormFUSCZ.OverallInfo.ClientType == SealedConstants.PersonType.Corporation.Value;
            var client = this.FormFUSCZ.Client;

            if (clientIsCompany)
            {
                string tradeLicenceNo = "";

                if (SealedConstants.PersonType.IndividualBusinessman.Value == this.FormFUSCZ.OverallInfo.ClientType && string.IsNullOrWhiteSpace(client.TradeLicenceNo))
                    tradeLicenceNo = "XXXXXXXX";
                else
                    tradeLicenceNo = client.TradeLicenceNo;

                bool? residentUSA = null;

                if (client.ResidentUSA.HasValue)
                    residentUSA = client.ResidentUSA;

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S3Client);

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 32.5, 20).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3LastName + space, fTextNormal).Add(new Chunk(clientIsCompany ? client.BusinessName : client.LastName, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3TradeLicenceNo + space, fTextNormal).Add(new Chunk(tradeLicenceNo, fTextBold)));
                    }));

                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 32.5, 9, 1.5, 2.5, 1.5, 5.5).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3EmailAddr + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                        .Add(new Chunk(client.EmailAddr, fTextBold)));
                    //if (FusZV)
                    //{
                    //	t.Add(new Cell(t));
                    //	t.Add(new Cell(t));
                    //	t.Add(new Cell(t));
                    //	t.Add(new Cell(t));
                    //	t.Add(new Cell(t));
                    //}
                    //else
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA + space, fTextNormal));
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? residentUSA.Value : false));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_YES, fTextBold));
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? !residentUSA.Value : false));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_NO, fTextBold));
                    }
                }));

                AddPaddingLine(table, separatingPadding);

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentAddressPart1 + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3PermanentAddressPart2 + space, fTextNormalItalic))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3PermanentAddressPart3 + space, fTextNormal))
                        .Add(new Chunk(client.PermanentAddress, fTextBold)).Indent(0, 216));
                }));

                table.Add(new Table(table, 32.5, 20).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentCity + space, fTextNormal)
                        .Add(new Chunk(client.PermanentCity, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentPostalCode + space, fTextNormal).Add(new Chunk(client.PermanentPostalCode, fTextBold)));
                }));
                AddPaddingLine(table, separatingPadding);
            }
            else
            {
                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S3Client);

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 3.5, 1.5, 2.5, 1.5, 3.5, 20, 12, 8).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Gender + space, fTextNormal).Align(Element.ALIGN_LEFT));
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, client.GenderCode == "M"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3GenderCodeMan + space, fTextBold).Align(Element.ALIGN_LEFT));
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, client.GenderCode == "F"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3GenderCodeWoman + space, fTextBold).Align(Element.ALIGN_LEFT));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3LastName + space, fTextNormal).Add(new Chunk(clientIsCompany ? client.BusinessName : client.LastName, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3FirstName + space, fTextNormal).Add(new Chunk(client.FirstName, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Prefix + space, fTextNormal).Add(new Chunk(client.Prefix, fTextBold)));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                string partyCode = "";
                string tradeLicenceNo = "";

                if (!string.IsNullOrEmpty(client.PartyCode))
                {
                    string[] subResult = client.PartyCode.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                    if (subResult.Length > 0)
                        partyCode += subResult[0];

                    if (subResult.Length > 1)
                        partyCode += "/" + subResult[1];
                }

                bool? residentUSA = null;

                if (client.ResidentUSA.HasValue)
                    residentUSA = client.ResidentUSA;

                if (SealedConstants.PersonType.IndividualBusinessman.Value == this.FormFUSCZ.OverallInfo.ClientType && string.IsNullOrWhiteSpace(client.TradeLicenceNo))
                    tradeLicenceNo = "XXXXXXXX";
                else
                    tradeLicenceNo = client.TradeLicenceNo;

                if (SealedConstants.PersonType.IndividualBusinessman.Value == this.FormFUSCZ.OverallInfo.ClientType)
                {
                    table.Add(new Table(table, 12.5, 9, 11, 9, 1.5, 2.5, 1.5, 5.5).With(t =>
                    {
                        if (string.IsNullOrWhiteSpace(partyCode) && client.CountryCode != "CZ" && client.CountryCode != "SK")
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthDate + space, fTextNormal).Add(new Chunk(client.BirthDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        }
                        else
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PartyCode + space, fTextNormal).Add(new Chunk(partyCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        }


                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3TradeLicenceNo + space, fTextNormal).Add(new Chunk(tradeLicenceNo, fTextBold)));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3CountryCode + space, fTextNormal).Add(new Chunk(client.CountryCode, fTextBold)));
                        //if (FusZV)
                        //{
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //}
                        //else
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA + space, fTextNormal));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_YES, fTextBold));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? !residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_NO, fTextBold));
                        }
                    }));
                }
                else
                {
                    table.Add(new Table(table, 12.5, 20, 9, 1.5, 2.5, 1.5, 5.5).With(t =>
                    {
                        if (string.IsNullOrWhiteSpace(partyCode) && client.CountryCode != "CZ" && client.CountryCode != "SK")
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthDate + space, fTextNormal).Add(new Chunk(client.BirthDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        }
                        else
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PartyCode + space, fTextNormal).Add(new Chunk(partyCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3CountryCode + space, fTextNormal).Add(new Chunk(client.CountryCode, fTextBold)));
                        //if (FusZV)
                        //{
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //	t.Add(new Cell(t));
                        //}
                        //else
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA + space, fTextNormal));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_YES, fTextBold));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? !residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_NO, fTextBold));
                        }
                        t.Add(new Cell(t));
                    }));
                }

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthPlace + space, fTextNormal).Add(new Chunk(client.BirthPlace, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                }));

                StringBuilder docClient = new StringBuilder();

                if (!clientIsCompany)
                {

                    if (client.DocumentDateFrom.HasValue && client.DocumentDateTo.HasValue)
                        docClient.AppendFormat("{0:dd'/'MM'/'yyyy} - {1:dd'/'MM'/'yyyy}", client.DocumentDateFrom, client.DocumentDateTo);

                    if (!string.IsNullOrWhiteSpace(client.DocumentReleased))
                    {
                        if (docClient.Length > 0) docClient.Append(" / ");
                        docClient.Append(client.DocumentReleased);
                    }
                }

                table.Add(new Table(table, 1, 2).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3DocumentCode + space, fTextNormal).Add(new Chunk(client.DocumentCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3DocumentDate + space, fTextNormal).Add(new Chunk(docClient.ToString(), fTextBold)));
                }));
                AddPaddingLine(table, separatingPadding);


                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentAddressPart1 + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3PermanentAddressPart2 + space, fTextNormalItalic))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S3PermanentAddressPart3 + space, fTextNormal))
                        .Add(new Chunk(client.PermanentAddress, fTextBold)).Indent(0, 216));
                }));

                table.Add(new Table(table, 32.5, 20).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentCity + space, fTextNormal).Add(new Chunk(client.PermanentCity, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentPostalCode + space, fTextNormal).Add(new Chunk(client.PermanentPostalCode, fTextBold)));
                }));
                AddPaddingLine(table, separatingPadding);

                if (client.IsMailingDifferent)
                {
                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MailingAddressPart1 + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S3MailingAddressPart2 + space, fTextNormalItalic))
                            .Add(new Chunk(App_GlobalResources.FusCZ.S3MailingAddressPart3 + space, fTextNormal))
                            .Add(new Chunk(client.MailingAddress, fTextBold)).Indent(0, 174));
                    }));

                    table.Add(new Table(table, 32.5, 20).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MailingCity + space, fTextNormal).Add(new Chunk(client.MailingCity, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MailingPostalCode + space, fTextNormal).Add(new Chunk(client.MailingPostalCode, fTextBold)));
                    }));
                    AddPaddingLine(table, separatingPadding);
                }
                else
                {
                    table.Add(new Table(table, 1, 30).With(t =>
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, !client.IsMailingDifferent));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3AddressSame, fTextNormal));
                    }));
                    AddPaddingLine(table, separatingPadding);
                }

                table.Add(new Table(table, 1, 2).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MaritalStatus + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                        .Add(new Chunk(FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == client.MaritalStatus).Select(p => p.Name).FirstOrDefault(), fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Occupation + space, fTextNormal)
                        .Add(new Chunk(client.Occupation, fTextBold)));
                }));

                table.Add(new Table(table, 1, 2).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Phone + space, fTextNormal)
                        .Add(new Chunk(client.Phone, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3EmailAddr + space, fTextNormal)
                        .Add(new Chunk(client.EmailAddr, fTextBold)));
                }));

                if (FusZV)
                {
                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Employer + space, fTextNormal)
                            .Add(new Chunk(client.Employer, fTextBold)));
                    }));
                }

                AddPaddingLine(table, separatingPadding);
            }

            #endregion Client
        }

        private void RenderSection_03_Partner(Table table)
        {
            #region Partner
            if (this.FormFUSCZ.Partner != null && (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation))
            {
                bool partnerIsCompany = this.FormFUSCZ.OverallInfo.PartnerType == SealedConstants.PersonType.Corporation.Value;
                var partner = this.FormFUSCZ.Partner;

                {
                    // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                    SetTableZeroPaddingLeading(table);
                    table.Add(new Table(table, 1).With(ti =>
                    {
                        AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S3Partner);

                        ResetTablePaddingVertical(ti, innerTablePadding);
                        AddPaddingLine(ti, separatingPadding);

                        ti.Add(new Table(ti, 3.5, 1.5, 2.5, 1.5, 3.5, 20, 12, 8).With(t =>
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Gender + space, fTextNormal).Align(Element.ALIGN_LEFT));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, partner.GenderCode == "M"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3GenderCodeMan + space, fTextBold).Align(Element.ALIGN_LEFT));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, partner.GenderCode == "F"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3GenderCodeWoman + space, fTextBold).Align(Element.ALIGN_LEFT));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3LastName + space, fTextNormal).Add(new Chunk(partner.LastName, fTextBold)));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3FirstName + space, fTextNormal).Add(new Chunk(partner.FirstName, fTextBold)));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Prefix + space, fTextNormal).Add(new Chunk(partner.Prefix, fTextBold)));
                        }));

                    }));
                    ResetTablePaddingVertical(table, innerTablePadding);

                    string partyCode = "";
                    string tradeLicenceNo = "";

                    if (!string.IsNullOrEmpty(partner.PartyCode))
                    {
                        string[] subResult = partner.PartyCode.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                        if (subResult.Length > 0)
                            partyCode += subResult[0];

                        if (subResult.Length > 1)
                            partyCode += "/" + subResult[1];
                    }

                    bool? residentUSA = null;

                    if (partner.ResidentUSA.HasValue)
                        residentUSA = partner.ResidentUSA;

                    if (SealedConstants.PersonType.IndividualBusinessman.Value == this.FormFUSCZ.OverallInfo.PartnerType && string.IsNullOrWhiteSpace(partner.TradeLicenceNo))
                        tradeLicenceNo = "XXXXXXXX";
                    else
                        tradeLicenceNo = partner.TradeLicenceNo;

                    if (SealedConstants.PersonType.IndividualBusinessman.Value == this.FormFUSCZ.OverallInfo.PartnerType)
                    {
                        table.Add(new Table(table, 12.5, 9, 11, 9, 1.5, 2.5, 1.5, 5.5).With(t =>
                        {
                            if (string.IsNullOrWhiteSpace(partyCode) && partner.CountryCode != "CZ" && partner.CountryCode != "SK")
                            {
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthDate + space, fTextNormal).Add(new Chunk(partner.BirthDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                            }
                            else
                            {
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PartyCode + space, fTextNormal).Add(new Chunk(partyCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                            }

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3TradeLicenceNo + space, fTextNormal).Add(new Chunk(tradeLicenceNo, fTextBold)));

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3CountryCode + space, fTextNormal).Add(new Chunk(partner.CountryCode, fTextBold)));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA + space, fTextNormal));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_YES, fTextBold));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? !residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_NO, fTextBold).PaddingBottom(2));
                        }));
                    }
                    else if (partnerIsCompany)
                    {
                        table.Add(new Table(table, 12.5, 20, 9, 1.5, 2.5, 1.5, 5.5).With(t =>
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3TradeLicenceNo + space, fTextNormal).Add(new Chunk(tradeLicenceNo, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3CountryCode + space, fTextNormal).Add(new Chunk(partner.CountryCode, fTextBold)));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA + space, fTextNormal));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_YES, fTextBold));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? !residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_NO, fTextBold).PaddingBottom(2));
                            t.Add(new Cell(t));
                        }));
                    }
                    else
                    {
                        table.Add(new Table(table, 12.5, 20, 9, 1.5, 2.5, 1.5, 5.5).With(t =>
                        {
                            if (string.IsNullOrWhiteSpace(partyCode) && partner.CountryCode != "CZ" && partner.CountryCode != "SK")
                            {
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthDate + space, fTextNormal).Add(new Chunk(partner.BirthDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)));
                            }
                            else
                            {
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PartyCode + space, fTextNormal).Add(new Chunk(partyCode, fTextBold)));
                            }

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3CountryCode + space, fTextNormal).Add(new Chunk(partner.CountryCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA + space, fTextNormal));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_YES, fTextBold));
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, residentUSA.HasValue ? !residentUSA.Value : false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ResidentUSA_NO, fTextBold));
                            t.Add(new Cell(t));
                        }));
                    }

                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthPlace + space, fTextNormal).Add(new Chunk(partner.BirthPlace, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    }));

                    StringBuilder docPartner = new StringBuilder();

                    if (partner.DocumentDateFrom.HasValue && partner.DocumentDateTo.HasValue)
                        docPartner.AppendFormat("{0:dd'/'MM'/'yyyy} - {1:dd'/'MM'/'yyyy}", partner.DocumentDateFrom, partner.DocumentDateTo);

                    if (!string.IsNullOrWhiteSpace(partner.DocumentReleased))
                    {
                        if (docPartner.Length > 0) docPartner.Append(" / ");
                        docPartner.Append(partner.DocumentReleased);
                    }

                    table.Add(new Table(table, 1, 2).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3DocumentCode + space, fTextNormal).Add(new Chunk(partner.DocumentCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3DocumentDate + space, fTextNormal).Add(new Chunk(docPartner.ToString(), fTextBold)));
                    }));

                    AddPaddingLine(table, separatingPadding);

                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentAddressPart1 + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S3PermanentAddressPart2 + space, fTextNormalItalic))
                            .Add(new Chunk(App_GlobalResources.FusCZ.S3PermanentAddressPart3 + space, fTextNormal))
                            .Add(new Chunk(partner.PermanentAddress, fTextBold)).Indent(0, 216).PaddingBottom(2));
                    }));

                    table.Add(new Table(table, 32.5, 20).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentCity + space, fTextNormal).Add(new Chunk(partner.PermanentCity, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentPostalCode + space, fTextNormal).Add(new Chunk(partner.PermanentPostalCode, fTextBold)));
                    }));
                    AddPaddingLine(table, separatingPadding);

                    if (partner.IsMailingDifferent)
                    {
                        table.Add(new Table(table, 1).With(t =>
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MailingAddressPart1 + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                                .Add(new Chunk(App_GlobalResources.FusCZ.S3MailingAddressPart2 + space, fTextNormalItalic))
                                .Add(new Chunk(App_GlobalResources.FusCZ.S3MailingAddressPart3 + space, fTextNormal))
                                .Add(new Chunk(partner.MailingAddress, fTextBold)).Indent(0, 174));
                        }));

                        table.Add(new Table(table, 32.5, 20).With(t =>
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MailingCity + space, fTextNormal).Add(new Chunk(partner.MailingCity, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MailingPostalCode + space, fTextNormal).Add(new Chunk(partner.MailingPostalCode, fTextBold)));
                        }));
                        AddPaddingLine(table, separatingPadding);
                    }
                    else
                    {
                        table.Add(new Table(table, 1, 30).With(t =>
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, !partner.IsMailingDifferent));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S3AddressSame, fTextNormal));
                        }));
                        AddPaddingLine(table, separatingPadding);
                    }

                    table.Add(new Table(table, 1, 2).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3MaritalStatus + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox)
                            .Add(new Chunk(FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == partner.MaritalStatus).Select(p => p.Name).FirstOrDefault(), fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Occupation + space, fTextNormal)
                            .Add(new Chunk(partner.Occupation, fTextBold)));
                    }));

                    table.Add(new Table(table, 1, 2).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Phone + space, fTextNormal)
                            .Add(new Chunk(partner.Phone, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3EmailAddr + space, fTextNormal)
                            .Add(new Chunk(partner.EmailAddr, fTextBold)));
                    }));
                    AddPaddingLine(table, separatingPadding);
                }

            }
            #endregion Partner
        }

        private void RenderSection_03_Children(Table table)
        {
            #region children

            int childrenCount = 0;

            foreach (var child in this.FormFUSCZ.Childs) // ach ta anglictina :)
            {
                childrenCount++;

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S3Child + space + childrenCount.ToString());

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 3.5, 1.5, 2.5, 1.5, 8.5, 15, 20).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3Gender + space, fTextNormal).Align(Element.ALIGN_LEFT).PaddingBottom(paddingToCompensateCheckBox));
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, child.GenderCode == "M"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3GenderCodeMan + space, fTextBold).Align(Element.ALIGN_LEFT));
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, child.GenderCode == "F"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3GenderCodeWoman + space, fTextBold).Align(Element.ALIGN_LEFT));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3LastNameChild + space, fTextNormal).Add(new Chunk(child.LastName, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3FirstName + space, fTextNormal).Add(new Chunk(child.FirstName, fTextBold)));
                    }));

                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                string partyCode = "";

                if (!string.IsNullOrEmpty(child.PartyCode))
                {
                    string[] subResult = child.PartyCode.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                    if (subResult.Length > 0)
                        partyCode += subResult[0];

                    if (subResult.Length > 1)
                        partyCode += "/" + subResult[1];
                }

                table.Add(new Table(table, 17.5, 15, 20).With(t =>
                {
                    if (string.IsNullOrWhiteSpace(partyCode) && child.CountryCode != "CZ" && child.CountryCode != "SK")
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthDate + space, fTextNormal).Add(new Chunk(child.BirthDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    }
                    else
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PartyCode + space, fTextNormal).Add(new Chunk(partyCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3BirthPlace + space, fTextNormal).Add(new Chunk(child.BirthPlace, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3CountryCode + space, fTextNormal).Add(new Chunk(child.CountryCode, fTextBold)));
                }));

                StringBuilder docChild = new StringBuilder();

                if (child.DocumentDateFrom.HasValue && child.DocumentDateTo.HasValue)
                    docChild.AppendFormat("{0:dd'/'MM'/'yyyy} - {1:dd'/'MM'/'yyyy}", child.DocumentDateFrom, child.DocumentDateTo);

                if (!string.IsNullOrWhiteSpace(child.DocumentReleased))
                {
                    if (docChild.Length > 0) docChild.Append(" / ");
                    docChild.Append(child.DocumentReleased);
                }

                table.Add(new Table(table, 1, 2).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3DocumentCode + space, fTextNormal).Add(new Chunk(child.DocumentCode, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S3DocumentDate + space, fTextNormal).Add(new Chunk(docChild.ToString(), fTextBold)));
                }));

                if (child.IsAddressDifferent)
                {
                    AddPaddingLine(table, separatingPadding);
                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ChildAddress + space, fTextNormal).Add(new Chunk(child.Address, fTextBold)).Indent(0, 193).PaddingBottom(paddingToCompensateCheckBox));
                    }));

                    table.Add(new Table(table, 32.5, 20).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentCity + space, fTextNormal).Add(new Chunk(child.City, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentPostalCode + space, fTextNormal).Add(new Chunk(child.PostalCode, fTextBold)));
                    }));
                }
                else
                {
                    var client = this.FormFUSCZ.Client;
                    AddPaddingLine(table, separatingPadding);
                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3ChildAddress + space, fTextNormal).Add(new Chunk(client.PermanentAddress, fTextBold)).Indent(0, 193).PaddingBottom(paddingToCompensateCheckBox));
                    }));

                    table.Add(new Table(table, 32.5, 20).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentCity + space, fTextNormal).Add(new Chunk(client.PermanentCity, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S3PermanentPostalCode + space, fTextNormal).Add(new Chunk(client.PermanentPostalCode, fTextBold)));
                    }));
                }

                AddPaddingLine(table, separatingPadding);
            }

            #endregion children
        }

        #endregion Section 3

        void RenderSection_04(Table table)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinancialDataVerification == null || (!this.FormFUSCZ.FSInsuranceLive && !this.FormFUSCZ.FSSavingDS && !this.FormFUSCZ.FSSavingDPS))
            {
                //return;
            }

            int questionsIndentSec = 10;

            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S4Section4Header);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    if (FusZV)
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4ClientAnnouncementTitleZV + space, fTextBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S4ClientAnnouncementText, fTextNormal)));
                    }
                    else
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4ClientAnnouncementTitle + space, fTextBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S4ClientAnnouncementText, fTextNormal)));
                    }
                }));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Table(table, 1).With(t =>
            {
                if (FusZV)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S4FCTRAnnouncementTitle + space, fTextBold)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S4FCTRAnnouncementTextZV, fTextNormal)));
                }
                else
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S4FCTRAnnouncementTitle + space, fTextBold)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S4FCTRAnnouncementText, fTextNormal)));
                }

            }));

            AddPaddingLine(table, separatingPadding);

            if (this.FormFUSCZ != null && (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSSavingTFToDPS || this.FormFUSCZ.FSSavingDPS))
            {
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    string investmentHeader = string.Format("{0} {1}{2}{3}",
                                App_GlobalResources.FusCZ.S4InvestmentTestHeader,
                                this.FormFUSCZ.FSSavingDPS || this.FormFUSCZ.FSSavingTFToDPS ? App_GlobalResources.FusCZ.S4InvestmentTestDPS : "",
                                ((this.FormFUSCZ.FSSavingDPS || this.FormFUSCZ.FSSavingTFToDPS) && this.FormFUSCZ.FSInsuranceLive) ? "A " : "",
                                this.FormFUSCZ.FSInsuranceLive ? App_GlobalResources.FusCZ.S4InvestmentTestIS : "");

                    AddSectionSubHeader(ti, investmentHeader);

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Warning + space, fTextSmallBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S4WarningText, fTextSmallNormal)));
                    }));

                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S4InformationLaw, fTextBold));
                    //t.Add(new Cell(t, App_GlobalResources.FusCZ.S4OtherSideInformation, fTextNormal));
                }));

                //if ((this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client) || (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                if (true)
                {

                    #region Questions

                    List<string> codesClient = new List<string>();
                    List<string> codesPartner = new List<string>();


                    table.Style.PaddingLeft(0).PaddingRight(0);
                    table.Style.C.SetLeading(0, 0);
                    table.Add(new Table(table, 1).With(ti =>
                    {
                        //ti.T.HeaderRows = 1;

                        ti.Style = new Cell().Padding(mainTablePadding);
                        ti.Style.C.SetLeading(0, 1.1f);

                        ti.Add(new Table(ti, 16.3, 2.1, 1.9).With(t =>
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4InvestmentInstrucionsInfo, fTextBold));

                            if (this.FormFUSCZ.FUSForClient)
                            {
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Client, fTextBold));
                            }
                            else
                                t.Add(new Cell(t));

                            if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification != null)
                            {
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Partner, fTextBold));
                            }
                            else
                                t.Add(new Cell(t));
                        }));

                        #region Q1

                        ti.Add(new Table(ti, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                        {
                            if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                                codesClient.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client);
                            if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                                codesPartner.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Partner);

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestGoal, fTextBold));
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestGoalQ1OptionA, fTextNormal).Indent(0, questionsIndentSec));

                            if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                            }
                            else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client == "Q1OptionA"));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                            }
                            else
                            {
                                t.Add(new Cell(t));
                                t.Add(new Cell(t));
                            }


                            if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                            }
                            else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Partner == "Q1OptionA"));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));

                            }
                            else
                            {
                                t.Add(new Cell(t));
                                t.Add(new Cell(t));
                            }

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestGoalQ1OptionB, fTextNormal).Indent(0, questionsIndentSec));

                            if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                            }
                            else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client == "Q1OptionB"));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                            }
                            else
                            {
                                t.Add(new Cell(t));
                                t.Add(new Cell(t));
                            }

                            if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                            }
                            else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Partner == "Q1OptionB"));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));

                            }
                            else
                            {
                                t.Add(new Cell(t));
                                t.Add(new Cell(t));
                            }

                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestGoalQ1OptionC, fTextNormal).Indent(0, questionsIndentSec));

                            if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                            }
                            else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client == "Q1OptionC"));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                            }
                            else
                            {
                                t.Add(new Cell(t));
                                t.Add(new Cell(t));
                            }

                            if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                            }
                            else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Partner == "Q1OptionC"));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                            }
                            else
                            {
                                t.Add(new Cell(t));
                                t.Add(new Cell(t));
                            }
                        }));

                        #endregion Q1

                    }));
                    table.Style = new Cell().Padding(mainTablePadding);
                    table.Style.C.SetLeading(0, 1.1f);

                    #region Q2
                    table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                    {
                        if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesClient.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client);
                        if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesPartner.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner);

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestTools, fTextBold));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestToolsQ2OptionA, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client == "Q2OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner == "Q2OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestToolsQ2OptionB, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client == "Q2OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner == "Q2OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestToolsQ2OptionC, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client == "Q2OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner == "Q2OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestToolsQ2OptionD, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client == "Q2OptionD"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner == "Q2OptionD"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                    }));

                    #endregion Q2

                    #region Q3

                    table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                    {
                        if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesClient.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client);
                        if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesPartner.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Partner);

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestHorizont, fTextBold));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestHorizontQ3OptionA, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client == "Q3OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Partner == "Q3OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestHorizontQ3OptionB, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client == "Q3OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Partner == "Q3OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestHorizontQ3OptionC, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client == "Q3OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Partner == "Q3OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                    }));

                    #endregion Q3

                    #region Q4

                    table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                    {
                        if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesClient.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client);
                        if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesPartner.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner);

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestExperience, fTextBold));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestExperienceQ4OptionA, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client == "Q4OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner == "Q4OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestExperienceQ4OptionB, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client == "Q4OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner == "Q4OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points1, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestExperienceQ4OptionC, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client == "Q4OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner == "Q4OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points2, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestExperienceQ4OptionD, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client == "Q4OptionD"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner == "Q4OptionD"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                    }));

                    #endregion Q4

                    #region Q5

                    table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                    {
                        if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesClient.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client);
                        if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification != null && this.FormFUSCZ.FinancialDataVerification.QEInvestGoal != null)
                            codesPartner.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner);

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestRisk, fTextBold));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestRiskExample, fTextNormal));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestRiskQ5OptionA, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client == "Q5OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner == "Q5OptionA"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points0, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestRiskQ5OptionB, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client == "Q5OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner == "Q5OptionB"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points3, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestRiskQ5OptionC, fTextNormal).Indent(0, questionsIndentSec));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client == "Q5OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner == "Q5OptionC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points6, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4QEInvestRiskQ5OptionD, fTextNormal).Indent(0, questionsIndentSec));


                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points9, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client == "Q5OptionD"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points9, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points9, fTextBold));
                        }
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner == "Q5OptionD"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4Points9, fTextBold));

                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                    }));

                    #endregion Q5

                    int? sumClient = null;
                    int? sumPartner = null;
                    sumClient = FUShelper.FUSListOptionsResource.Where(p => codesClient.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);
                    sumPartner = FUShelper.FUSListOptionsResource.Where(p => codesPartner.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

                    table.Add(new Table(table, 18.8, 2.4, 2.4, 0.7).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4PointsTotal, fTextBold));

                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client))
                            t.Add(new Cell(t, " ", fTextBold).AlignCenter());
                        else if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                            t.Add(new Cell(t, sumClient.ToString(), fTextBold).AlignCenter());
                        else
                            t.Add(new Cell(t));

                        if (this.FormFUSCZ.FUSForPartner && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner))
                            t.Add(new Cell(t, " ", fTextBold).AlignCenter());
                        else if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                            t.Add(new Cell(t, sumPartner.ToString(), fTextBold).AlignCenter());
                        else
                            t.Add(new Cell(t));

                        t.Add(new Cell(t));
                    }));


                    #endregion Questions

                    table.Add(new Table(table, 1).With(t =>
                    {
                        if (FusZV)
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4PointsSumZV, fTextNormal));
                        else
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4PointsSum, fTextNormal));
                    }));

                    table.Add(new Table(table, 1, 2).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4LegendPointsDynamic, fTextNormal));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4LegendProfileDynamic, fTextBold));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4LegendPointsBalanced, fTextNormal));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4LegendProfileBalanced, fTextBold));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4LegendPointsConservative, fTextNormal));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S4LegendProfileConservative, fTextBold));
                    }));

                    table.Style.PaddingBottom(0);
                    table.Add(new Table(table, 1, 35).With(t =>
                    {
                        if (this.FormFUSCZ.FUSForClient && (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client == null))
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationClient, fTextNormal));
                        }
                        else if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationClient, fTextNormal));
                        }
                        else
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationClient, fTextNormal));
                        }

                        // Only for standard FUS
                        if (!FusZV && this.FormFUSCZ.FUSForPartner)
                        {
                            if (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationPartner, fTextNormal));
                            }
                            else if (this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationPartner, fTextNormal));
                            }
                            else
                            {
                                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                                t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationPartner, fTextNormal));
                            }
                        }
                    }));

                }
                else
                {
                    // this code was used when poll was dynamically hiding
                    table.Style.PaddingBottom(0);
                    table.Add(new Table(table, 1, 35).With(t =>
                    {
                        if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationClient, fTextNormal));
                        }

                        if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner && !FusZV)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S4RefuseInformationPartner, fTextNormal));
                        }
                    }));

                }
                AddPaddingLine(table, separatingPadding);
            }
        }

        void RenderSection_05(Table table)
        {
            if (FusZV)
            {
                RenderSection_05_ZV(table);
            }
            else
            {
                RenderSection_05_Standard(table);
            }
        }

        private void RenderSection_05_Standard(Table table)
        {
            bool section5stdUsed = false;
            if (!(this.FormFUSCZ != null && this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FSSavingDS || this.FormFUSCZ.FSSavingDPS || this.FormFUSCZ.FSSavingIncreaseDPS || this.FormFUSCZ.FSSavingTFToDPS || this.FormFUSCZ.FSSavingIncreaseTF)))
            {
                //return;			
            }
            else
            {
                section5stdUsed = true;
                showItem6 = true;
                showItem8Part4 = true;
            }

            AddPaddingLine(table, separatingPadding);
            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is not larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S5Section5Header, 1, true, section5stdUsed);
                AddPaddingLine(ti, separatingPadding);

                if (section5stdUsed)
                {
                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S5InformationLaw + space, fTextLittleBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S5InformationWarning, fTextLittleNormal)));
                    }));
                }

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            //Don't generate content of section 5
            if (!section5stdUsed)
            {
                return;
            }

            table.Add(new Table(table, 2.5, 94.5).With(t =>
            {

                if (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || !section5stdUsed)
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                else
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client));

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5RefuseInformationClient, fTextBold));
            }));

            if ((this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation))
            {
                table.Add(new Table(table, 2.5, 94.5).With(t =>
                {

                    //t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner));
                    if (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || !section5stdUsed)
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                    else
                        t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5RefuseInformationPartner, fTextBold));
                }));
            }

            if (FormFUSCZ.FSSavingDPS)
            {
                table.Add(new Table(table, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
                {
                    // client - 3)

                    bool clientChangeStrategy = false;
                    string ClientStrategyDPSOptions = "";
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        clientChangeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS;
                        ClientStrategyDPSOptions = this.FormFUSCZ.FinancialDsDps.ClientStrategyDPSOptions;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, clientChangeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyDPS + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                        );
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, ClientStrategyDPSOptions == "CZDPSKON"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, ClientStrategyDPSOptions == "CZDPSOTH"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

                }));
            }

            if (FormFUSCZ.FUSForPartner && FormFUSCZ.FSSavingDPS)
            {
                table.Add(new Table(table, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
                {
                    // partner - 4)

                    bool partnerChangeStrategy = false;
                    string PartnerStrategyDPSOptions = "";
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        partnerChangeStrategy = this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPS;
                        PartnerStrategyDPSOptions = this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPSOptions;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Partner, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, partnerChangeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyDPS + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                        );
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, PartnerStrategyDPSOptions == "CZDPSKON"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, PartnerStrategyDPSOptions == "CZDPSOTH"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

                }));
            }

            if (FormFUSCZ.FSSavingTFToDPS)
            {
                table.Add(new Table(table, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
                {
                    // client - 5)
                    bool changeStrategy = false;
                    string strategyTFToDPSOptions = "";
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        changeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS;
                        strategyTFToDPSOptions = this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPSOptions;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart1 + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart2 + space, fTextNormal))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart3 + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                        );
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyTFToDPSOptions == "CZDPSKON"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyTFToDPSOptions == "CZDPSOTH"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

                }));
            }

            if (FormFUSCZ.FUSForPartner && FormFUSCZ.FSSavingTFToDPS)
            {
                table.Add(new Table(table, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
                {
                    // partner - 6)
                    bool changeStrategy = false;
                    string strategyTFToDPSOptions = "";
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        changeStrategy = this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFToDPS;
                        strategyTFToDPSOptions = this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFToDPSOptions;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Partner, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart1 + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart2 + space, fTextNormal))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart3 + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                        );
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyTFToDPSOptions == "CZDPSKON"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyTFToDPSOptions == "CZDPSOTH"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

                }));
            }

            if (FormFUSCZ.FSSavingIncreaseDPS)
            {
                table.Add(new Table(table, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
                {
                    // client - 7)
                    bool changeStrategy = false;
                    string strategyIncreaseDPS = "";
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        changeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS;
                        strategyIncreaseDPS = this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPSOptions;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyIncreaseDPS + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                        );
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyIncreaseDPS == "CZDPSKON"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyIncreaseDPS == "CZDPSOTH"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

                }));
            }

            if (FormFUSCZ.FUSForPartner && FormFUSCZ.FSSavingIncreaseDPS)
            {
                table.Add(new Table(table, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
                {
                    // partner - 8)
                    bool changeStrategy = false;
                    string strategyIncreaseDPS = "";
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        changeStrategy = this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseDPS;
                        strategyIncreaseDPS = this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseDPSOptions;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Partner, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyIncreaseDPS + space, fTextBold))
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                        );
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyIncreaseDPS == "CZDPSKON"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyIncreaseDPS == "CZDPSOTH"));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

                }));
            }

            if (FormFUSCZ.FSSavingIncreaseTF)
            {
                table.Add(new Table(table, 7.5, 2.5, 87).With(t =>
                {
                    // client - 9)
                    bool changeStrategy = false;
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        changeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyIncreaseTF + space, fTextBold))
                        );
                }));
            }

            if (FormFUSCZ.FUSForPartner && FormFUSCZ.FSSavingIncreaseTF)
            {
                table.Add(new Table(table, 7.5, 2.5, 87).With(t =>
                {
                    // partner - 10)
                    bool changeStrategy = false;
                    if (this.FormFUSCZ.FinancialDsDps != null)
                    {
                        changeStrategy = this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseTF;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Partner, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyIncreaseTF + space, fTextBold))
                        );
                }));
            }

            //table.Add(new Table(table, 3, 97).With(t =>
            //{
            //	// DS - 9)
            //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part9, fTextBold));
            //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S5FinancialDSService, fTextBold));

            //	IEnumerable<Sector> clientSrvs = null;
            //	IEnumerable<Sector> partnerSrvs = null;

            //	t.Add(new Cell(t));
            //	t.Add(new Table(t, 35, 35, 15, 12).With(ti =>
            //	{
            //		bool everythingIsNull = false;
            //		if (this.FormFUSCZ.Sectors != null)
            //		{
            //			List<int> dpServices = DbProvider.Services().Where(p => p.CategoryID == 100028).Select(p => p.ServiceID).ToList();

            //			if (this.FormFUSCZ.FinancialDsDps != null && this.FormFUSCZ.FinancialDsDps.ClientStrategyDS)
            //				clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpServices.Contains(p.ProductID.Value));

            //			if (this.FormFUSCZ.FinancialDsDps != null && this.FormFUSCZ.FinancialDsDps.PartnerStrategyDS)
            //				partnerSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Partner.ClientTempID && dpServices.Contains(p.ProductID.Value));

            //			if (!(clientSrvs == null || clientSrvs.Count() == 0) || !(partnerSrvs == null || partnerSrvs.Count() == 0))
            //			{
            //				if (!(clientSrvs == null || clientSrvs.Count() == 0))
            //				{
            //					ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService + space, fTextBold).Add(new Chunk(this.GetInstitution(clientSrvs.ElementAt(0).InstitutionID), fTextNormal)));
            //				}
            //				else
            //				{
            //					ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextBold));
            //				}

            //				if (!(partnerSrvs == null || partnerSrvs.Count() == 0))
            //				{
            //					ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5PartnerService + space, fTextBold).Add(new Chunk(this.GetInstitution(partnerSrvs.ElementAt(0).InstitutionID), fTextNormal)));
            //				}
            //				else
            //				{
            //					ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5PartnerService, fTextBold));
            //				}

            //				ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService + space, fTextBold).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.ClientDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextNormal)));
            //				ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService + space, fTextBold).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.ClientDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")), fTextNormal)));
            //			}
            //			else
            //			{
            //				everythingIsNull = true;
            //			}
            //		}
            //		else
            //		{
            //			everythingIsNull = true;
            //		}

            //		if (everythingIsNull)
            //		{
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextBold));
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5PartnerService, fTextBold));
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService, fTextBold));
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService, fTextBold));
            //		}
            //	}));

            //}));

            table.Add(new Table(table, 1).With(t =>
            {
                // DPS - 11)
                //t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part11, fTextBold));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5FinancialDPSService, fTextBold));

                IEnumerable<Sector> clientSrvs = null;
                IEnumerable<Sector> partnerSrvs = null;

                //t.Add(new Cell(t));
                t.Add(new Table(t, 35, 35, 15, 12).With(ti =>
                {
                    bool everythingIsNull = false;
                    if (this.FormFUSCZ.Sectors != null)
                    {
                        List<int> dpsServices = DbProvider.Services().Where(p => p.CategoryID == 100029).Select(p => p.ServiceID).ToList();
                        List<int> ppServices = DbProvider.Services().Where(p => p.CategoryID == 10102).Select(p => p.ServiceID).ToList();

                        List<IEnumerable<Sector>> childSectors = new List<IEnumerable<Sector>>();
                        if (this.FormFUSCZ.Childs != null)
                        {
                            foreach (var child in this.FormFUSCZ.Childs)
                            {
                                childSectors.Add(this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == child.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value))));
                            }
                        }

                        if (this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS || this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF))
                            clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value)));

                        if (this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPS || this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseDPS || this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFToDPS || this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseTF))
                            partnerSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Partner.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value)));

                        if (!(clientSrvs == null || clientSrvs.Count() == 0) || !(partnerSrvs == null || partnerSrvs.Count() == 0) || childSectors.Count > 0)
                        {
                            if (!(clientSrvs == null || clientSrvs.Count() == 0) || childSectors.Count > 0)
                            {
                                if (clientSrvs == null || clientSrvs.Count() == 0)
                                    ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService + space, fTextNormal).Add(new Chunk(this.GetInstitution(null, childSectors), fTextBold)));
                                else
                                    ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService + space, fTextNormal).Add(new Chunk(this.GetInstitution(clientSrvs.ElementAt(0).InstitutionID, childSectors), fTextBold)));
                            }
                            else
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextNormal));
                            }

                            if (FormFUSCZ.FUSForPartner)
                            {
                                if (!(partnerSrvs == null || partnerSrvs.Count() == 0))
                                {
                                    ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5PartnerService + space, fTextNormal).Add(new Chunk(this.GetInstitution(partnerSrvs.ElementAt(0).InstitutionID), fTextBold)));
                                }
                                else
                                {
                                    ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5PartnerService, fTextNormal));
                                }
                            }
                            else
                            {
                                ti.AddEmptyCell();
                            }

                            if (this.FormFUSCZ.FinancialDsDps != null && this.FormFUSCZ.FinancialDsDps.PartnerDSDate != null)
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)));
                            }
                            else
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService + space, fTextNormal));
                            }

                            if (this.FormFUSCZ.FinancialDsDps != null && this.FormFUSCZ.FinancialDsDps.PartnerDSTime != null)
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")), fTextBold)));
                            }
                            else
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService + space, fTextNormal));
                            }
                        }
                        else
                        {
                            everythingIsNull = true;
                        }
                    }
                    else
                    {
                        everythingIsNull = true;
                    }

                    if (everythingIsNull)
                    {
                        ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextNormal));
                        ti.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(t, App_GlobalResources.FusCZ.S5PartnerService, fTextNormal));
                        ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService, fTextNormal));
                        ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService, fTextNormal));
                    }
                }));

            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientAnnouncementTitle + space, fTextSmallBold)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ClientAnnouncementText, fTextSmallNormal))
                    .AlignJustify());
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5FCTRAnnouncementTitle + space, fTextSmallBold)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5FCTRAnnouncementText, fTextSmallNormal))
                    .AlignJustify());
            }));

            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSection_05_ZV(Table table)
        {
            bool sectionUsed = false;
            if (!(this.FormFUSCZ != null && this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FSSavingDS || this.FormFUSCZ.FSSavingDPS || this.FormFUSCZ.FSSavingIncreaseDPS || this.FormFUSCZ.FSSavingTFToDPS || this.FormFUSCZ.FSSavingIncreaseTF)))
            {
                //return;			
            }
            else
            {
                sectionUsed = true;
                showItem6 = true;
                showItem8Part4 = true;
            }


            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is not larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S5Section5Header, 1, true, sectionUsed);
                AddPaddingLine(ti, separatingPadding);

                if (sectionUsed)
                {
                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S5InformationLaw + space, fTextLittleBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S5InformationWarning, fTextLittleNormal)));
                    }));
                }

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            //Don't generate content of section 5
            if (!sectionUsed)
            {
                return;
            }

            table.Add(new Table(table, 3, 2.5, 94.5).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part1, fTextBold));

                if (this.FormFUSCZ.FinancialDataVerification == null || this.FormFUSCZ.FinancialDataVerification.RefuseInformation == null || !sectionUsed)
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, false));
                else
                    t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client));

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5RefuseInformationClient, fTextBold));
            }));

            table.Add(new Table(table, 3, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
            {
                // client - 2)

                bool clientChangeStrategy = false;
                string ClientStrategyDPSOptions = "";
                if (this.FormFUSCZ.FinancialDsDps != null)
                {
                    clientChangeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS;
                    ClientStrategyDPSOptions = this.FormFUSCZ.FinancialDsDps.ClientStrategyDPSOptions;
                }

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part2, fTextBold));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, clientChangeStrategy));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyDPS + space, fTextBold))
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                    );
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, ClientStrategyDPSOptions == "CZDPSKON"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, ClientStrategyDPSOptions == "CZDPSOTH"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));


            }));

            table.Add(new Table(table, 3, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
            {
                // client - 3)
                bool changeStrategy = false;
                string strategyTFToDPSOptions = "";
                if (this.FormFUSCZ.FinancialDsDps != null)
                {
                    changeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS;
                    strategyTFToDPSOptions = this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPSOptions;
                }

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part3, fTextBold));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart1 + space, fTextBold))
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart2 + space, fTextNormal))
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyTFPart3 + space, fTextBold))
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                    );
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyTFToDPSOptions == "CZDPSKON"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyTFToDPSOptions == "CZDPSOTH"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

            }));

            table.Add(new Table(table, 3, 7.5, 2.5, 57, 2.5, 2, 2.5, 23).With(t =>
            {
                // client - 4)
                bool changeStrategy = false;
                string strategyIncreaseDPS = "";
                if (this.FormFUSCZ.FinancialDsDps != null)
                {
                    changeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS;
                    strategyIncreaseDPS = this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPSOptions;
                }

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part4, fTextBold));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyIncreaseDPS + space, fTextBold))
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsPart2, fTextNormal))
                    );
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyIncreaseDPS == "CZDPSKON"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DSOptionsK, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, strategyIncreaseDPS == "CZDPSOTH"));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5DPSOptionsOther, fTextNormal));

            }));

            table.Add(new Table(table, 3, 7.5, 2.5, 87).With(t =>
            {
                // client - 5)
                bool changeStrategy = false;
                if (this.FormFUSCZ.FinancialDsDps != null)
                {
                    changeStrategy = this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF;
                }

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part5, fTextBold));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Client, fTextNormal));
                t.Add(CreateCheckbox(t, Element.ALIGN_LEFT, changeStrategy));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ItemsPart1 + space, fTextNormal)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ItemsStrategyIncreaseTF + space, fTextBold))
                    );

            }));

            //table.Add(new Table(table, 3, 97).With(t =>
            //{
            //	// DS - 5)
            //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part5, fTextBold));
            //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S5FinancialDSService, fTextBold));

            //	IEnumerable<Sector> clientSrvs = null;

            //	t.Add(new Cell(t));
            //	t.Add(new Table(t, 70, 15, 12).With(ti =>
            //	{
            //		bool everythingIsNull = false;
            //		if (this.FormFUSCZ.Sectors != null)
            //		{
            //			List<int> dpServices = DbProvider.Services().Where(p => p.CategoryID == 100028).Select(p => p.ServiceID).ToList();

            //			if (this.FormFUSCZ.FinancialDsDps != null && this.FormFUSCZ.FinancialDsDps.ClientStrategyDS)
            //				clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpServices.Contains(p.ProductID.Value));

            //			if (!(clientSrvs == null || clientSrvs.Count() == 0))
            //			{
            //				if (!(clientSrvs == null || clientSrvs.Count() == 0))
            //				{
            //					ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService + space, fTextBold).Add(new Chunk(this.GetInstitution(clientSrvs.ElementAt(0).InstitutionID), fTextNormal)));
            //				}
            //				else
            //				{
            //					ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextBold));
            //				}

            //				ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService + space, fTextBold).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.ClientDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextNormal)));
            //				ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService + space, fTextBold).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.ClientDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")), fTextNormal)));
            //			}
            //			else
            //			{
            //				everythingIsNull = true;
            //			}
            //		}
            //		else
            //		{
            //			everythingIsNull = true;
            //		}

            //		if (everythingIsNull)
            //		{
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextBold));
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService, fTextBold));
            //			ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService, fTextBold));
            //		}
            //	}));

            //}));

            table.Add(new Table(table, 3, 97).With(t =>
            {
                // DPS - 6)
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5Part6, fTextBold));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5FinancialDPSService, fTextBold));

                IEnumerable<Sector> clientSrvs = null;

                t.Add(new Cell(t));
                t.Add(new Table(t, 70, 15, 12).With(ti =>
                {
                    bool everythingIsNull = false;
                    if (this.FormFUSCZ.Sectors != null)
                    {
                        List<int> dpsServices = DbProvider.Services().Where(p => p.CategoryID == 100029).Select(p => p.ServiceID).ToList();

                        if (this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS || this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF))
                            clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpsServices.Contains(p.ProductID.Value));

                        if (!(clientSrvs == null || clientSrvs.Count() == 0))
                        {
                            if (!(clientSrvs == null || clientSrvs.Count() == 0))
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService + space, fTextNormal).Add(new Chunk(this.GetInstitution(clientSrvs.ElementAt(0).InstitutionID), fTextBold)));
                            }
                            else
                            {
                                ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextNormal));
                            }

                            ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)));
                            ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")), fTextBold)));
                        }
                        else
                        {
                            everythingIsNull = true;
                        }
                    }
                    else
                    {
                        everythingIsNull = true;
                    }

                    if (everythingIsNull)
                    {
                        ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientService, fTextNormal));
                        ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5DateService, fTextNormal));
                        ti.Add(new Cell(t, App_GlobalResources.FusCZ.S5TimeService, fTextNormal));
                    }
                }));

            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5ClientAnnouncementTitleZV + space, fTextSmallBold)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5ClientAnnouncementText, fTextSmallNormal))
                    .AlignJustify());
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S5FCTRAnnouncementTitle + space, fTextSmallBold)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S5FCTRAnnouncementText, fTextSmallNormal))
                    .AlignJustify());
            }));

            AddPaddingLine(table, separatingPadding);
        }

        void RenderSection_06(Table table)
        {
            bool sectionUsed = false;
            var financialInsurance = FUShelper.FIns(this.FormFUSCZ);
            if (!(this.FormFUSCZ != null && financialInsurance != null && (this.FormFUSCZ.FSInsuranceLive ||
                this.FormFUSCZ.FSInsuranceLiveOther ||
                this.FormFUSCZ.FSInsurancePersonal ||
                this.FormFUSCZ.FSInsuranceProperty ||
                this.FormFUSCZ.FSInsuranceLiability ||
                this.FormFUSCZ.FSInsuranceTravel ||
                this.FormFUSCZ.FSInsuranceOthers)))
            {
                //return;
            }
            else
            {
                sectionUsed = true;
                showItem5 = true;
                showItem8Part1 = true;
            }

            // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S6Section6Header, 1, true, sectionUsed);
                AddPaddingLine(ti, separatingPadding);

                if (sectionUsed)
                {
                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        t.Add(new Cell(ti, App_GlobalResources.FusCZ.S6InformationLaw, fTextBold));
                    }));
                }

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            //Don't generate content of section 6
            if (!sectionUsed)
            {
                return;
            }
            AddPaddingLine(table, separatingPadding);

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(table, App_GlobalResources.FusCZ.S6GeneralWarningTitle + space, fTextMediumBold).Add(new Chunk(App_GlobalResources.FusCZ.S6GeneralWarningText, fTextMediumNormal)));
            }));

            //table.Add(new Table(table, 1).With(t =>
            //{
            //	t.Add(new Cell(table, App_GlobalResources.FusCZ.S6InformationTitle + space, fTextMediumBold).Add(new Chunk(App_GlobalResources.FusCZ.S6InformationText, fTextMediumNormal)));
            //}));

            table.Add(new Table(table, 1).With(t =>
            {
                if (sectionUsed)
                    t.Add(new Cell(table, App_GlobalResources.FusCZ.S6AssociateCNBNo + space, fTextBold).Add(new Chunk(this.FormFUSCZ.AgentDetail.RegistrationNumber, fTextNormal)));
                else
                    t.Add(new Cell(table, App_GlobalResources.FusCZ.S6AssociateCNBNo + space, fTextBold));

            }));

            AddPaddingLine(table, separatingPadding);

            if (FusZV)
            {
                RenderSection_06_PartA_ZV(table, financialInsurance, sectionUsed);
            }
            else
            {
                RenderSection_06_PartA(table, financialInsurance, sectionUsed);
            }

            RenderSection_06_PartBCD(table, financialInsurance, sectionUsed);

            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSection_06_PartA(Table table, FinancialInsurance financialInsurance, bool sectionUsed)
        {
            #region SectionA

            int requestIndend = 3;
            int nextLineIndend = 12;
            bool headerShown = false;
            //if (financialInsurance.InsuranceIndividual)
            {
                #region Part1

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 35, 3, 3, 3).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceIndividual, fTextBold).PaddingBottom(2).PaddingTop(0));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(t, App_GlobalResources.FusCZ.S6Partner, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForChild, new Cell(t, App_GlobalResources.FusCZ.S6Child, fTextBold).AlignCenter());

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIDeath, fTextNormal).Indent(requestIndend, 0));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIDeath.Client));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIDeath.Partner));
                        t.Add(new Cell(t));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIDeathInjury, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIDeathInjury.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIDeathInjury.Partner));
                    t.Add(new Cell(t));
                }));

                // c
                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIPermanentInjury, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    t.Add(new Table(t, 2, 4, 46, 4, 46).With(ti =>
                    {
                        ti.Add(new Cell(ti));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjuryFree));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjuryFree, fTextNormal));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjurySingle));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjurySingle, fTextNormal));
                    }));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjury.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjury.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjury.Child));
                }));

                // d
                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIInvalidity, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    t.Add(new Table(t, 2, 4, 46, 4, 46).With(ti =>
                    {
                        ti.Add(new Cell(ti));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIInvalidityFree));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjuryFree, fTextNormal));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIInvaliditySingle));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjurySingle, fTextNormal));
                    }));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIInvalidity.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIInvalidity.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIInvalidity.Child));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IILowInjury, fTextNormal).Indent(requestIndend, nextLineIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IILowInjury.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IILowInjury.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IILowInjury.Child));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IISeriousDiseases, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IISeriousDiseases.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IISeriousDiseases.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IISeriousDiseases.Child));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceSurvival, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceSurvivalGuaranteed, fTextNormal).Indent(nextLineIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalGuaranteed.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalGuaranteed.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalGuaranteed.Child));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceSurvivalInvestFund, fTextNormal).Indent(nextLineIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalInvestFund.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalInvestFund.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalInvestFund.Child));

                    AddPaddingLine(t, 3);
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceProfile, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.RequiredInvestProfile.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.RequiredInvestProfile.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.RequiredInvestProfile.Child));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6DynamicProfil, fTextBold).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.DynamicProfil.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.DynamicProfil.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.DynamicProfil.Child));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6BalancedProfil, fTextBold).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.BalancedProfil.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.BalancedProfil.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.BalancedProfil.Child));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6KonzervativeProfil, fTextBold).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.KonzervativeProfil.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.KonzervativeProfil.Partner));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.KonzervativeProfil.Child));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6ClientAnnouncementTitle + space, fTextMediumBold).Add(new Chunk(App_GlobalResources.FusCZ.S6ClientAnnouncementText, fTextMediumNormal)).AlignJustify());
                }));

                #endregion Part1
            }

            //if (financialInsurance.InsuranceProperty)
            {
                #region Part2

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 35, 3, 3, 3).With(t =>
                    {

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceProperty, fTextBold).PaddingBottom(2));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(t, App_GlobalResources.FusCZ.S6Partner, fTextBold).AlignCenter());
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IPHomeReality, fTextNormal).Indent(requestIndend, 0));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPHomeReality.Client));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPHomeReality.Partner));
                        t.Add(new Cell(t));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);


                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IPOtherReality, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPOtherReality.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPOtherReality.Partner));
                    t.Add(new Cell(t));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Style.Padding(1);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceVehicle, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IPAllRisk, fTextNormal).Indent(requestIndend * 4));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPAllRisk.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPAllRisk.Partner));
                    t.Add(new Cell(t));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IPTheft, fTextNormal).Indent(requestIndend * 4));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPTheft.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPTheft.Partner));
                    t.Add(new Cell(t));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IPVehicleOthers, fTextNormal).Indent(requestIndend * 4));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPVehicleOthers.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPVehicleOthers.Partner));
                    t.Add(new Cell(t));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IPOthers, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPOthers.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IPOthers.Partner));
                    t.Add(new Cell(t));
                }));

                #endregion Part2
            }

            //if (financialInsurance.InsuranceDamage)
            {
                #region Part3

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 35, 3, 3, 3).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceDamage, fTextBold).PaddingBottom(2));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(t, App_GlobalResources.FusCZ.S6Partner, fTextBold).AlignCenter());
                        t.Add(new Cell(t));

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDPublic, fTextNormal).Indent(requestIndend, 0));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDPublic.Client));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDPublic.Partner));
                        t.Add(new Cell(t));
                        t.Style.PaddingBottom(0);
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Style.PaddingTop(0);
                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDOwnReality, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDOwnReality.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDOwnReality.Partner));
                    t.Add(new Cell(t));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDPZP, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDPZP.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDPZP.Partner));
                    t.Add(new Cell(t));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDEmployer, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDEmployer.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDEmployer.Partner));
                    t.Add(new Cell(t));
                }));

                table.Add(new Table(table, 35, 3, 3, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDOthers, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDOthers.Client));
                    t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDOthers.Partner));
                    t.Add(new Cell(t));
                }));

                #endregion Part3
            }

            //if (financialInsurance.InsuranceTraveling && financialInsurance.InsuranceTravelingDetail != null)
            {
                #region Part4

                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 35, 3, 3, 3).With(t =>
                    {

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceTraveling, fTextBold).PaddingBottom(2));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(t, App_GlobalResources.FusCZ.S6Partner, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForChild, new Cell(t, App_GlobalResources.FusCZ.S6Child, fTextBold).AlignCenter());

                        t.Add(new Cell(t));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTravelingDetail.Client));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTravelingDetail.Partner));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTravelingDetail.Child));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                #endregion Part4
            }

            //if (financialInsurance.InsuranceOtherDetail != null)
            {
                #region Part5

                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 35, 3, 3, 3).With(t =>
                    {

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceOther, fTextBold).PaddingBottom(2));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(t, App_GlobalResources.FusCZ.S6Partner, fTextBold).AlignCenter());
                        t.AddDataOrEmpty(FormFUSCZ.FUSForChild, new Cell(t, App_GlobalResources.FusCZ.S6Child, fTextBold).AlignCenter());

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceOtherDetail, fTextNormal).Indent(requestIndend));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceOtherDetail.Client));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForPartner, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceOtherDetail.Partner));
                        t.AddDataOrEmpty(FormFUSCZ.FUSForChild, CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceOtherDetail.Child));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                #endregion Part5
            }

            #region Part6

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                if (!headerShown)
                {
                    AddSection6AHeader(ti);
                    headerShown = true;
                }

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 33, 2.9, 1.2, 1.8, 1.2, 3.9).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceTaxAdvantage, fTextBold).PaddingBottom(2));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceTaxAdvantageInfo, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6YES, fTextBold).AlignRight());
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? financialInsurance.InsuranceTaxAdvantage.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6NO, fTextBold).AlignRight());
                    //t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? !financialInsurance.InsuranceTaxAdvantage.Value : true));

                    if (sectionUsed)
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? !financialInsurance.InsuranceTaxAdvantage.Value : true));
                    else
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? !financialInsurance.InsuranceTaxAdvantage.Value : false));

                    t.Add(new Cell(t));
                }));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            #endregion Part6

            #region Part7

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                if (!headerShown)
                {
                    AddSection6AHeader(ti);
                    headerShown = true;
                }

                ResetTablePaddingVertical(ti, innerTablePadding);
                //AddPaddingLine(ti, separatingPadding);

                ti.Style.PaddingLeft(3).PaddingRight(3);
                ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S6InsuranceNote + space, fTextBold).PaddingBottom(2)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S6InsuranceNoteExample, fTextSmallNormal))
                    .Add(new Chunk(":", fTextBold))
                    .AlignJustify()
                    );

                string note = (string.IsNullOrEmpty(financialInsurance.InsuranceNote) || string.IsNullOrEmpty(financialInsurance.InsuranceNote.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : financialInsurance.InsuranceNote;

                if (!sectionUsed) note = "";

                Cell insuranceNote = new Cell(ti, note, fTextNormal).AlignJustify();
                ti.Add(insuranceNote);
            }));

            #endregion Part7


            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceNoteWarningTitle + space, fTextSmallBold).Add(new Chunk(App_GlobalResources.FusCZ.S6InsuranceNoteWarningText, fTextSmallNormal)).AlignJustify());
                }));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);
            AddPaddingLine(table, separatingPadding);

            #endregion SectionA
        }

        private void RenderSection_06_PartA_ZV(Table table, FinancialInsurance financialInsurance, bool sectionUsed)
        {
            #region SectionA

            int requestIndend = 3;
            int nextLineIndend = 12;
            bool headerShown = false;
            //if (financialInsurance.InsuranceIndividual)
            {
                #region Part1

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 41, 3).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceIndividual, fTextBold).PaddingBottom(2).PaddingTop(0));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIDeath, fTextNormal).Indent(requestIndend, 0));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIDeath.Client));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIDeathInjury, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIDeathInjury.Client));
                }));

                // c
                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIPermanentInjury, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));

                    t.Add(new Table(t, 2, 4, 46, 4, 46).With(ti =>
                    {
                        ti.Add(new Cell(ti));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjuryFree));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjuryFree, fTextNormal));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjurySingle));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjurySingle, fTextNormal));
                    }));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIPermanentInjury.Client));
                }));

                // d
                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IIInvalidity, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));

                    t.Add(new Table(t, 2, 4, 46, 4, 46).With(ti =>
                    {
                        ti.Add(new Cell(ti));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIInvalidityFree));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjuryFree, fTextNormal));
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, financialInsurance.IIInvaliditySingle));
                        ti.Add(new Cell(ti, space + App_GlobalResources.FusCZ.S6IIPermanentInjurySingle, fTextNormal));
                    }));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IIInvalidity.Client));
                }));

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IILowInjury, fTextNormal).Indent(requestIndend, nextLineIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IILowInjury.Client));
                }));

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IISeriousDiseases, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IISeriousDiseases.Client));
                }));

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceSurvival, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceSurvivalGuaranteed, fTextNormal).Indent(nextLineIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalGuaranteed.Client));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceSurvivalInvestFund, fTextNormal).Indent(nextLineIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceSurvivalInvestFund.Client));

                    AddPaddingLine(t, 3);
                }));

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceProfile, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.RequiredInvestProfile.Client));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6DynamicProfil, fTextBold).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.DynamicProfil.Client));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6BalancedProfil, fTextBold).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.BalancedProfil.Client));

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6KonzervativeProfil, fTextBold).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.KonzervativeProfil.Client));
                }));

                #endregion Part1
            }

            //if (financialInsurance.InsuranceDamage)
            {
                #region Part2

                // this code ensures that header/subheader wont be alone on end page - as long as content of table below is nto larger than whole page, it will stay together
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 41, 3).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceDamageZV, fTextBold).PaddingBottom(2));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDPublic, fTextNormal).Indent(requestIndend, 0));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDPublic.Client));
                        t.Style.PaddingBottom(0);
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Style.PaddingTop(0);
                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDOwnReality, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDOwnReality.Client));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDPZP, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDPZP.Client));
                }));

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDEmployer, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDEmployer.Client));
                }));

                table.Add(new Table(table, 41, 3).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6IDOthers, fTextNormal).Indent(requestIndend));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.IDOthers.Client));
                }));

                #endregion Part2
            }

            //if (financialInsurance.InsuranceOtherDetail != null)
            {
                #region Part3

                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    if (!headerShown)
                    {
                        AddSection6AHeader(ti);
                        headerShown = true;
                    }

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    ti.Add(new Table(ti, 41, 3).With(t =>
                    {

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceOtherZV, fTextBold).PaddingBottom(2));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Client, fTextBold).AlignCenter());

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceOtherDetailZV, fTextNormal).Indent(requestIndend));
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceOtherDetail.Client));
                    }));
                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                #endregion Part3
            }

            #region Part4

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                if (!headerShown)
                {
                    AddSection6AHeader(ti);
                    headerShown = true;
                }

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 36, 2.9, 1.2, 1.8, 1.2, 0.9).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceTaxAdvantageZV, fTextBold).PaddingBottom(2));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceTaxAdvantageInfo, fTextNormal).Indent(requestIndend));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6YES, fTextBold).AlignRight());
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? financialInsurance.InsuranceTaxAdvantage.Value : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6NO, fTextBold).AlignRight());

                    if (sectionUsed)
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? !financialInsurance.InsuranceTaxAdvantage.Value : true));
                    else
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, financialInsurance.InsuranceTaxAdvantage.HasValue ? !financialInsurance.InsuranceTaxAdvantage.Value : false));

                    t.Add(new Cell(t));
                }));
            }));
            //ResetTablePaddingVertical(table, innerTablePadding);

            #endregion Part4

            #region Part5

            //SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                if (!headerShown)
                {
                    AddSection6AHeader(ti);
                    headerShown = true;
                }

                ResetTablePaddingVertical(ti, innerTablePadding);
                //AddPaddingLine(ti, separatingPadding);
                ti.Style.PaddingLeft(3).PaddingRight(3);
                ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S6InsuranceNoteZV + space, fTextBold).PaddingBottom(2)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S6InsuranceNoteExample, fTextSmallNormal))
                    .Add(new Chunk(":", fTextBold))
                    .AlignJustify()
                    );

                string note = (string.IsNullOrEmpty(financialInsurance.InsuranceNote) || string.IsNullOrEmpty(financialInsurance.InsuranceNote.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : financialInsurance.InsuranceNote;

                if (!sectionUsed) note = "";

                Cell insuranceNote = new Cell(ti, note, fTextNormal).AlignJustify();
                ti.Add(insuranceNote);
            }));

            #endregion Part5

            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceNoteWarningTitle + space, fTextSmallBold).Add(new Chunk(App_GlobalResources.FusCZ.S6InsuranceNoteWarningTextZV, fTextSmallNormal)).AlignJustify());
                }));
            }));

            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6ClientAnnouncementTitleZV + space, fTextMediumBold).Add(new Chunk(App_GlobalResources.FusCZ.S6ClientAnnouncementText, fTextMediumNormal)).AlignJustify());
                }));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);
            AddPaddingLine(table, separatingPadding);


            #endregion SectionA

        }

        private void RenderSection_06_PartBCD(Table table, FinancialInsurance financialInsurance, bool sectionUsed)
        {
            #region SectionB

            List<int> provideInfo = new List<int>();
            List<int> provideSoft = new List<int>();
            bool print, email;
            print = email = false;

            int insuranceCount = 0;
            List<SectorInsurance> remainingInsurances = new List<SectorInsurance>(this.FormFUSCZ.SectorInsurances);

            SectorInsurance firstInsurance = null;

            if (this.FormFUSCZ.SectorInsurances.Count > 0 && this.FormFUSCZ.SectorInsurances[0] != null)
            {
                firstInsurance = this.FormFUSCZ.SectorInsurances[0];
                remainingInsurances.RemoveAt(0);
            }

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                if (FusZV)
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S6SectionBHeaderZV);
                else
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S6SectionBHeader);
                //ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S6SectionBHeader, fSectionSubHeader).BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1));

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                if (firstInsurance != null)
                {
                    insuranceCount++;

                    if (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther)
                    {
                        if (firstInsurance.IsInfromationProvide) provideInfo.Add(insuranceCount);
                        if (firstInsurance.IsSoftwareProvide) provideSoft.Add(insuranceCount);

                        print |= firstInsurance.IsInformationPrint;
                        email |= firstInsurance.IsInformationElectronic;
                    }

                    int categoryID;
                    string pojistovna = this.GetInstitution(firstInsurance.InstitutionID);
                    string produkt = this.GetProduct(firstInsurance.ProductID, out categoryID);

                    string categoryName = string.Empty;

                    if (categoryID > -1)
                    {
                        var cat = DbProvider.Categories().FirstOrDefault(f => f.CategoryID == categoryID);
                        if (cat != null)
                            categoryName = cat.Name;
                    }

                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        Cell insuranceTitle = new Cell(ti, App_GlobalResources.FusCZ.S6RowNumber + space + insuranceCount.ToString() + space + App_GlobalResources.FusCZ.S6RequestNumber + space + GetInsuranceRequestNum(firstInsurance), fTextBold).BackgroundColor(0xf1f1f1).Leading(1, 1.1).Padding(3).PaddingBottom(3).PaddingTop(1.5).Colspan(1);

                        insuranceTitle.C.BorderWidthBottom = 1;
                        insuranceTitle.C.BorderColor = sectionBTitleColor;

                        ti.Add(insuranceTitle);
                    }));

                    ti.Add(new Table(ti, 10, 10).With(t =>
                    {
                        //t.Add(new Cell(t, App_GlobalResources.FusCZ.S6RequestNumber + space, fTextBold).Add(new Chunk(insuranceCount.ToString(), fTextNormal)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6ProductType + space, fTextNormal).Add(new Chunk(categoryName, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Institution + space, fTextNormal).Add(new Chunk(pojistovna, fTextBold)));
                    }));
                }

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            if (firstInsurance != null)
            {
                int categoryID;
                string produkt = this.GetProduct(firstInsurance.ProductID, out categoryID);

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6ProductLabel + space, fTextNormal).Add(new Chunk(produkt, fTextBold)));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Notes + space, fTextNormal)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S6NotesExamples, fTextSmallNormal))
                        .Add(new Chunk(":" + space, fTextNormal))
                        .AlignJustify()
                        );

                    string note = (string.IsNullOrEmpty(firstInsurance.Note) || string.IsNullOrEmpty(firstInsurance.Note.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : firstInsurance.Note;
                    AddPaddingLine(t, 1);

                    t.Add(new Cell(t, note, fTextBold).AlignJustify());

                }));

                AddPaddingLine(table, separatingPadding);
            }

            foreach (var insurance in remainingInsurances)
            {
                insuranceCount++;

                if (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther)
                {
                    if (insurance.IsInfromationProvide) provideInfo.Add(insuranceCount);
                    if (insurance.IsSoftwareProvide) provideSoft.Add(insuranceCount);

                    print |= insurance.IsInformationPrint;
                    email |= insurance.IsInformationElectronic;
                }

                int categoryID;
                string pojistovna = this.GetInstitution(insurance.InstitutionID);
                string produkt = this.GetProduct(insurance.ProductID, out categoryID);

                string categoryName = string.Empty;

                if (categoryID > -1)
                {
                    var cat = DbProvider.Categories().FirstOrDefault(f => f.CategoryID == categoryID);
                    if (cat != null)
                        categoryName = cat.Name;
                }

                table.Add(new Table(table, 1).With(t =>
                {
                    Cell insuranceTitle = new Cell(t, App_GlobalResources.FusCZ.S6RowNumber + space + insuranceCount.ToString() + space + App_GlobalResources.FusCZ.S6RequestNumber + space + GetInsuranceRequestNum(insurance), fTextBold).BackgroundColor(0xf1f1f1).Leading(1, 1.1).Padding(3).PaddingBottom(3).PaddingTop(1.5).Colspan(1);

                    insuranceTitle.C.BorderWidthBottom = 1;
                    insuranceTitle.C.BorderColor = sectionBTitleColor;

                    t.Add(insuranceTitle);
                    //t.Add(new Cell(t, App_GlobalResources.FusCZ.S6RequestNumber + space + insuranceCount.ToString(), fTextBold).BackgroundColor(0xf1f1f1).Leading(1, 1.1).Padding(3).PaddingBottom(3).PaddingTop(1.5).Colspan(1));
                }));

                table.Add(new Table(table, 10, 10).With(t =>
                {
                    //t.Add(new Cell(t, App_GlobalResources.FusCZ.S6RequestNumber + space, fTextBold).Add(new Chunk(insuranceCount.ToString(), fTextNormal)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6ProductType + space, fTextNormal).Add(new Chunk(categoryName, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Institution + space, fTextNormal).Add(new Chunk(pojistovna, fTextBold)));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6ProductLabel + space, fTextNormal).Add(new Chunk(produkt, fTextBold)));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6Notes + space, fTextNormal).PaddingBottom(0)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S6NotesExamples, fTextSmallNormal))
                        .Add(new Chunk(":" + space, fTextNormal))
                        .AlignJustify()
                        );



                    string note = (string.IsNullOrEmpty(insurance.Note) || string.IsNullOrEmpty(insurance.Note.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : insurance.Note;
                    AddPaddingLine(t, 1);

                    t.Add(new Cell(t, note, fTextBold).AlignJustify());

                }));

                AddPaddingLine(table, separatingPadding);
            }


            #endregion SectionB

            #region SectionC

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                //ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S6SectionCHeader, fSectionSubHeader).BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1));
                AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S6SectionCHeader);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {


                    string note = (string.IsNullOrEmpty(financialInsurance.Discrepancies) || string.IsNullOrEmpty(financialInsurance.Discrepancies.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : financialInsurance.Discrepancies;

                    if (!sectionUsed) note = "";

                    t.Add(new Cell(t, note, fTextNormal));
                }));

            }));
            ResetTablePaddingVertical(table, innerTablePadding);
            AddPaddingLine(table, separatingPadding);

            #endregion SectionC

            #region SectionD

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                Cell subHeader = new Cell(ti, App_GlobalResources.FusCZ.S6SectionDHeader + space, fSectionSubHeader)
                    .Add(new Chunk(App_GlobalResources.FusCZ.S6SectionDHeaderNote, fSectionSubHeaderNormal))
                    .BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(0).PaddingTop(1.5).Colspan(1);
                subHeader.C.BorderWidthTop = 1;
                subHeader.C.BorderColor = subBorderColor;
                ti.Add(subHeader);

                Cell subHeaderNote = new Cell(ti, App_GlobalResources.FusCZ.S6SectionDHeaderNote2, fTextMediumNormal)
                    .BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(0).Colspan(1).Indent(10);
                subHeaderNote.C.BorderWidthBottom = 1;
                subHeaderNote.C.BorderColor = subBorderColor;
                ti.Add(subHeaderNote);


                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6AssociateAnnouncement, fTextBold));
                }));
                ti.Add(new Table(ti, 1, 1, 35).With(t =>
                {
                    t.Add(new Cell(t, "1.", fTextBold));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, provideInfo.Count > 0));
                    if (FusZV)
                    {
                        string announcement1Text = String.Format(App_GlobalResources.FusCZ.S6Annoucement1TextZV,
                            string.IsNullOrEmpty(String.Join(", ", provideInfo)) ? "......" : String.Join(", ", provideInfo));
                        t.Add(new Cell(t, announcement1Text, fTextNormal).AlignJustify());
                    }
                    else
                    {
                        string announcement1Text = String.Format(App_GlobalResources.FusCZ.S6Annoucement1Text,
                            string.IsNullOrEmpty(String.Join(", ", provideInfo)) ? "......" : String.Join(", ", provideInfo));
                        t.Add(new Cell(t, announcement1Text, fTextNormal).AlignJustify());
                    }
                }));
                ti.Add(new Table(ti, 2, 1, 6, 1, 27).With(t =>
                {
                    t.Add(new Cell(t));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, provideInfo.Count > 0 ? print : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6AnnoucementOptionPrint, fTextNormal));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, provideInfo.Count > 0 ? email : false));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6AnnoucementOptionEmail, fTextNormal));
                }));

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Table(table, 1, 1, 35).With(t =>
            {
                t.Add(new Cell(t, "2.", fTextBold));
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, provideSoft.Count > 0));
                string announcement2Text = String.Format(App_GlobalResources.FusCZ.S6Annoucement2Text,
                        string.IsNullOrEmpty(String.Join(", ", provideSoft)) ? "......" : String.Join(", ", provideSoft));
                t.Add(new Cell(announcement2Text, fTextNormal).AlignJustify());
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                if (FusZV)
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6SectionDClientAnnouncementZV + space, fTextBold).Add(new Chunk(App_GlobalResources.FusCZ.S6SectionDClientAnnouncementText, fTextMediumNormal)));
                else
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S6SectionDClientAnnouncement + space, fTextBold).Add(new Chunk(App_GlobalResources.FusCZ.S6SectionDClientAnnouncementText, fTextMediumNormal)));
            }));

            table.Add(new Table(table, 1).With(t =>
            {
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S6SectionDContractTermination, fTextMediumNormal).AlignJustify());
            }));

            #endregion SectionD
        }

        void RenderSection_07(Table table)
        {
            bool sectionUsed = false;
            if (!(this.FormFUSCZ.FSInvestment || this.FormFUSCZ.FSTermDepositsOrOtherJT) || this.FormFUSCZ.FinancialInvestment == null)
            {
                //return; 
            }
            else
            {
                sectionUsed = true;
                showItem7 = true;
                showItem8Part3 = true;
            }

            bool questionsClient = true;
            bool showPartner = (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialInvestment != null); // this value decides if partner part is shown in first part of section a - if partner refused/answered test
            bool questionsPartner = (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialInvestment != null && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner == "CZQTRULY");

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                ResetTablePaddingVertical(ti, innerTablePadding);
                AddSectionHeader(ti, App_GlobalResources.FusCZ.S7Section7Header, 1, true, sectionUsed);
                AddPaddingLine(ti, separatingPadding);

                if (sectionUsed)
                {
                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7InvestmentContractTitle, fTextBold));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7InvestmentContractText, fTextNormal));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7WarningTitle + space, fTextSmallBold).Add(new Chunk(App_GlobalResources.FusCZ.S7WarningText, fTextSmallNormal)));
                    }));
                }

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            //Don't generate content of section 7
            if (!sectionUsed)
            {
                return;
            }

            AddPaddingLine(table, separatingPadding);
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S7SectionAHeader);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientAnnouncement, fTextBold));
                }));

                ti.Add(new Table(ti, 2, 2, 6, 2, 6, 1, 45).With(t =>
                {
                    bool questionClient = false;
                    bool questionPartner = false;

                    if (this.FormFUSCZ.FinancialInvestment != null && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null)
                    {
                        questionClient = this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Client == "CZQTRULY";
                        questionPartner = this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner == "CZQTRULY";
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientAnnouncementA, fTextNormal).AlignRight());
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, questionClient));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Client, fTextNormal));
                    if (showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, questionPartner));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Partner, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    t.Add(new Cell(t, "-", fTextNormal));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QTruly, fTextNormal));
                }));

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Table(table, 2, 2, 6, 2, 6, 1, 45).With(t =>
            {
                bool questionClient = false;
                bool questionPartner = false;

                if (this.FormFUSCZ.FinancialInvestment != null && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null)
                {
                    questionClient = this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Client == "CZQREFUSE";
                    questionPartner = this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner == "CZQREFUSE";
                }

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientAnnouncementB, fTextNormal).AlignRight());
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, questionClient));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Client, fTextNormal));
                if (showPartner)
                {
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, questionPartner));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Partner, fTextNormal));
                }
                else
                {
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                }
                t.Add(new Cell(t, "-", fTextNormal));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QRefused, fTextNormal));
            }));

            double investmentQuestionsIndent = 8;
            double investmentQuestionsIndentNextLine = investmentQuestionsIndent + 10;

            table.Add(new Table(table, 2, 62).With(t =>
            {
                string questionClient = "";
                string questionPartner = "";

                if (this.FormFUSCZ.FinancialInvestment != null && this.FormFUSCZ.FinancialInvestment.ExpInvetorCode != null)
                {
                    questionClient = this.FormFUSCZ.FinancialInvestment.ExpInvetorCode.Client;
                    questionPartner = this.FormFUSCZ.FinancialInvestment.ExpInvetorCode.Partner;
                }

                t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientAnnouncementC, fTextNormal).AlignRight());
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QAnswered, fTextNormal));

                t.Add(new Cell(t));

                t.Add(new Table(t, 10, 5, 5, 20).With(ti =>
                {
                    ti.Add(new Cell(ti));
                    ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7Client, fTextBold).AlignCenter());

                    if (showPartner)
                        ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7Partner, fTextBold).AlignCenter());
                    else
                        ti.Add(new Cell(ti));

                    ti.Add(new Cell(ti));

                    ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7InvestorInexperienced, fTextNormal));
                    ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionClient == "CZINVESTORINEXPERIENCED"));
                    if (showPartner)
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionPartner == "CZINVESTORINEXPERIENCED"));
                    else
                        ti.Add(new Cell(ti));
                    ti.Add(new Cell(ti));

                    ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7InvestorLow, fTextNormal));
                    ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionClient == "CZINVESTORLOW"));
                    if (showPartner)
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionPartner == "CZINVESTORLOW"));
                    else
                        ti.Add(new Cell(ti));
                    ti.Add(new Cell(ti));

                    ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7InvestorMedium, fTextNormal));
                    ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionClient == "CZINVESTORMEDIUM"));
                    if (showPartner)
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionPartner == "CZINVESTORMEDIUM"));
                    else
                        ti.Add(new Cell(ti));
                    ti.Add(new Cell(ti));

                    ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7InvestorHigh, fTextNormal));
                    ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionClient == "CZINVESTORHIGH"));
                    if (showPartner)
                        ti.Add(CreateCheckbox(ti, Element.ALIGN_CENTER, questionPartner == "CZINVESTORHIGH"));
                    else
                        ti.Add(new Cell(ti));
                    ti.Add(new Cell(ti));

                }));

                t.Add(new Cell(t));
                t.Add(new Cell(t, App_GlobalResources.FusCZ.S7FormNotRequired, fTextNormal));

            }));

            AddPaddingLine(table, separatingPadding);

            //bool questionsClient = (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Client == "CZQTRULY");
            //bool questionsPartner = (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner == "CZQTRULY");

            showPartner = false;

            //if (questionsClient || questionsPartner)
            if (true)
            {
                #region SectionAPartB

                string clientQuestion = "";
                string partnerQuestion = "";

                table.Add(new Table(table, 1).With(ti =>
                {
                    ti.Add(new Table(ti, 16.3, 2.1, 1.9).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ExperienceFinding, fTextBold));
                        if (questionsClient)
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Client, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                        }
                        if (questionsPartner || showPartner)
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Partner, fTextBold));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                        }
                    }));

                    #region Q1



                    ti.Style.PaddingLeft(0).PaddingRight(0);
                    ti.Add(new Table(ti, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QEInvestFinancialEducationCode, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));

                        clientQuestion = "";
                        partnerQuestion = "";
                        if (FormFUSCZ.FinancialInvestment != null && FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                        {
                            clientQuestion = FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.Client;
                            partnerQuestion = FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.Partner;
                        }


                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ1NONE, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                        if (questionsClient)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ1NONE"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                        if (questionsPartner || showPartner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ1NONE"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ1ECONOMIC, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                        if (questionsClient)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ1ECONOMIC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                        if (questionsPartner || showPartner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ1ECONOMIC"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ1FINANCIAL, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                        if (questionsClient)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ1FINANCIAL"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }
                        if (questionsPartner || showPartner)
                        {
                            t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ1FINANCIAL"));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t));
                        }

                    }));

                    #endregion Q1

                }));

                #region Q2

                table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QEInvestWorkPositionCode, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    clientQuestion = "";
                    partnerQuestion = "";
                    if (FormFUSCZ.FinancialInvestment != null && FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                    {
                        clientQuestion = FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.Client;
                        partnerQuestion = FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.Partner;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ2NONE, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ2NONE"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ2NONE"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ2WORKLESS1Y, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ2WORKLESS1Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ2WORKLESS1Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ2WORKING, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ2WORKING"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ2WORKING"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                }));

                #endregion Q2

                #region Q3

                table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QEInvestRiskEducationCode, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    clientQuestion = "";
                    partnerQuestion = "";
                    if (FormFUSCZ.FinancialInvestment != null && FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                    {
                        clientQuestion = FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.Client;
                        partnerQuestion = FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.Partner;
                    }


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ3DONTKNOW, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ3DONTKNOW"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ3DONTKNOW"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ3UNDERSTAND, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ3UNDERSTAND"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ3UNDERSTAND"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ3UBONDS, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ3UBONDS"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ3UBONDS"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ3UINVESTING, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ3UINVESTING"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ3UINVESTING"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ3STOCKS, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ3STOCKS"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ3STOCKS"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                }));

                #endregion Q3

                #region Q4

                table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QEInvestExpInvestInstrumetnCode, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    clientQuestion = "";
                    partnerQuestion = "";
                    if (FormFUSCZ.FinancialInvestment != null && FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                    {
                        clientQuestion = FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.Client;
                        partnerQuestion = FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.Partner;
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ4ACCOUNT, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ4ACCOUNT"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ4ACCOUNT"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ4ONLYMMF, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ4ONLYMMF"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ4ONLYMMF"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ4MMFMORE, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ4MMFMORE"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ4MMFMORE"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ4STOCKS, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ4STOCKS"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ4STOCKS"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ4OCP, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ4OCP"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ4OCP"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                }));

                #endregion Q4

                #region Q5

                table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QEInvestExpInvestCode, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    clientQuestion = "";
                    partnerQuestion = "";
                    if (FormFUSCZ.FinancialInvestment != null && FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                    {
                        clientQuestion = FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.Client;
                        partnerQuestion = FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.Partner;
                    }


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ5FIRSTINVEST, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ5FIRSTINVEST"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ5FIRSTINVEST"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ5LESS1Y, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ5LESS1Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ5LESS1Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ5B2A5Y, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ5B2A5Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ5B2A5Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ5MORE5Y, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ5MORE5Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ5MORE5Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                }));

                #endregion Q5

                #region Q6

                table.Add(new Table(table, 19.5, 0.7, 1.7, 0.7, 1.5).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7QEInvestFreqClosingDealCode, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));
                    t.Add(new Cell(t));

                    clientQuestion = "";
                    partnerQuestion = "";
                    if (FormFUSCZ.FinancialInvestment != null && FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                    {
                        clientQuestion = FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.Client;
                        partnerQuestion = FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.Partner;
                    }


                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ6NONE, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ6NONE"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ6NONE"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points0, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ6ONCEY, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ6ONCEY"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ6ONCEY"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points1, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ6MORE1Y, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ6MORE1Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ6MORE1Y"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points2, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ6MONTHLY, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ6MONTHLY"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ6MONTHLY"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points3, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7CZQ6WEEKLY, fTextNormal).Indent(investmentQuestionsIndent, investmentQuestionsIndentNextLine));
                    if (questionsClient)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, clientQuestion == "CZQ6WEEKLY"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }
                    if (questionsPartner || showPartner)
                    {
                        t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partnerQuestion == "CZQ6WEEKLY"));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Points5, fTextNormal));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                    }

                }));

                #endregion Q6

                #region Sum

                List<string> codesClient = new List<string>();
                if (this.FormFUSCZ.FinancialInvestment != null)
                {
                    codesClient.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.ValueOrDefault(v => v.Client));
                    codesClient.Add(this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.ValueOrDefault(v => v.Client));
                    codesClient.Add(this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.ValueOrDefault(v => v.Client));
                    codesClient.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.ValueOrDefault(v => v.Client));
                    codesClient.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.ValueOrDefault(v => v.Client));
                    codesClient.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.ValueOrDefault(v => v.Client));
                }

                List<string> codesPartner = new List<string>();
                if (this.FormFUSCZ.FinancialInvestment != null)
                {
                    codesPartner.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.ValueOrDefault(v => v.Partner));
                    codesPartner.Add(this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.ValueOrDefault(v => v.Partner));
                    codesPartner.Add(this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.ValueOrDefault(v => v.Partner));
                    codesPartner.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.ValueOrDefault(v => v.Partner));
                    codesPartner.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.ValueOrDefault(v => v.Partner));
                    codesPartner.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.ValueOrDefault(v => v.Partner));
                }

                int? sumClient = null;
                if (codesClient.Any(p => p != null))
                    sumClient = FUShelper.FUSListOptionsResource.Where(p => codesClient.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

                int? sumPartner = null;
                if (codesPartner.Any(p => p != null))
                    sumPartner = FUShelper.FUSListOptionsResource.Where(p => codesPartner.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

                table.Add(new Table(table, 19.7, 2.4, 2.0).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7PointsAquired, fTextBold).Indent(investmentQuestionsIndent / 2, investmentQuestionsIndent));
                    if (questionsClient)
                    {
                        t.Add(new Cell(t, sumClient.HasValue ? sumClient.Value.ToString() : "", fTextBold));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                    }

                    if (questionsPartner || showPartner)
                    {
                        t.Add(new Cell(t, sumPartner.HasValue ? sumPartner.Value.ToString() : "", fTextBold));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                    }


                }));

                #endregion Sum

                AddPaddingLine(table, separatingPadding);

                #endregion SectionAPartB

            }
            #region SectionB

            SectorInvestment firstInvestment = null;
            IEnumerable<SectorInvestment> remainingInvestments = null;
            int investmentCount = 0;

            if (this.FormFUSCZ.SectorInvestments.Count > 0)
            {
                firstInvestment = this.FormFUSCZ.SectorInvestments[0];
            }

            if (this.FormFUSCZ.SectorInvestments.Count > 1)
            {
                remainingInvestments = this.FormFUSCZ.SectorInvestments.Skip(1);
            }

            if (firstInvestment != null)
            {
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S7SectionBHeader);

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                    // this.FormFUSCZ.SectorInvestments
                    investmentCount++;

                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        Cell investmentTitle = new Cell(ti, App_GlobalResources.FusCZ.S7RequestNumber + space + investmentCount.ToString(), fTextBold).BackgroundColor(0xf1f1f1).Leading(1, 1.1).Padding(3).PaddingBottom(3).PaddingTop(1.5).Colspan(1);

                        investmentTitle.C.BorderWidthBottom = 1;
                        investmentTitle.C.BorderColor = sectionBTitleColor;

                        t.Add(investmentTitle);

                        //t.Add(new Cell(t, App_GlobalResources.FusCZ.S7RequestNumber + space, fTextBold).Add(new Chunk(investmentCount.ToString(), fTextNormal)).PaddingBottom(paddingToCompensateCheckBox));
                        t.Add(new Table(ti, 0.125, 1, 4, 1, 4, 30).With(ti2 =>
                        {
                            Guid? clientID = this.FormFUSCZ.Client.ValueOrDefault(v => v.ClientTempID);

                            Cell clientChb = CreateCheckbox(ti2, Element.ALIGN_LEFT, firstInvestment.ClientTempID == clientID);
                            clientChb.C.PaddingLeft = -1;
                            clientChb.C.PaddingBottom = 0;
                            Cell partnerChb = CreateCheckbox(ti2, Element.ALIGN_LEFT, firstInvestment.ClientTempID != clientID);
                            partnerChb.C.PaddingLeft = -1;
                            partnerChb.C.PaddingBottom = 0;

                            ti2.Add(new Cell(ti2));
                            ti2.Add(clientChb);
                            ti2.Add(new Cell(ti2, App_GlobalResources.FusCZ.S7Client, fTextBold));

                            ti2.AddDataOrEmpty(FormFUSCZ.FUSForPartner, partnerChb);
                            ti2.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(ti2, App_GlobalResources.FusCZ.S7Partner, fTextBold));

                            ti2.Add(new Cell(ti2));

                            //ti2.Add(new Cell(ti2, App_GlobalResources.FusCZ.S7Institution + space, fTextBold).Add(new Chunk(this.GetInstitution(firstInvestment.InstitutionID), fTextNormal)));
                        }));
                        t.Add(new Table(ti, 1).With(ti2 =>
                        {
                            ti.Style.PaddingLeft(-0.25);
                            ti2.Add(new Cell(ti2, App_GlobalResources.FusCZ.S7Institution + space, fTextNormal).Add(new Chunk(this.GetInstitution(firstInvestment.InstitutionID), fTextBold)));
                        }));
                    }));

                }));
                ResetTablePaddingVertical(table, innerTablePadding);

                table.Add(new Table(table, 2, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTFinancialTool + space, fTextNormal).Add(new Chunk(firstInvestment.KTFinancialTool, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTFinancialToolType + space, fTextNormal).Add(new Chunk(firstInvestment.KTFinancialToolType.Substring(2), fTextBold)));
                }));

                table.Add(new Table(table, 1, 1, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7BuySell + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox).Add(new Chunk(FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == firstInvestment.KTBuySell && p.FUSListTypeID == Constants.FUSCZListTypes.BUYSELL).Select(p => p.Name).FirstOrDefault(), fTextBold)));
                    if (firstInvestment.KTInvestAmountTypeCode == "CZONETIME")
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTInvestAmountTypeCode + space, fTextNormal).Add(new Chunk("Jednorázová", fTextBold)));
                    }
                    else if (firstInvestment.KTInvestAmountTypeCode == "CZMULTITIME")
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTInvestAmountTypeCode + space, fTextNormal).Add(new Chunk("Pravidelná", fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7AmountPeriod + space, fTextNormal).Add(new Chunk(GetPeriodText(firstInvestment.AmountPeriod), fTextBold)));
                    }
                    else
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTInvestAmountTypeCode + space, fTextNormal));
                    }
                }));
                table.Add(new Table(table, 2, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Amount + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox).Add(new Chunk(firstInvestment.Amount, fTextBold)));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTCurrency + space, fTextNormal).Add(new Chunk(FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == firstInvestment.KTCurrency).Select(p => p.Name).FirstOrDefault(), fTextBold)));
                }));

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientNote + space, fTextNormal));

                    string note = (string.IsNullOrEmpty(firstInvestment.Note) || string.IsNullOrEmpty(firstInvestment.Note.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : firstInvestment.Note;
                    t.Add(new Cell(t, note, fTextBold));
                }));

                AddPaddingLine(table, separatingPadding);
            }
            else
            {
                SetTableZeroPaddingLeading(table);
                table.Add(new Table(table, 1).With(ti =>
                {
                    AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S7SectionBHeader);

                    ResetTablePaddingVertical(ti, innerTablePadding);
                    AddPaddingLine(ti, separatingPadding);

                }));
                ResetTablePaddingVertical(table, innerTablePadding);
            }

            if (remainingInvestments != null)
                foreach (var investment in remainingInvestments)
                {
                    investmentCount++;
                    table.Add(new Table(table, 1).With(t =>
                    {
                        Cell investmentTitle = new Cell(t, App_GlobalResources.FusCZ.S7RequestNumber + space + investmentCount.ToString(), fTextBold).BackgroundColor(0xf1f1f1).Leading(1, 1.1).Padding(3).PaddingBottom(3).PaddingTop(1.5).Colspan(1);

                        investmentTitle.C.BorderWidthBottom = 1;
                        investmentTitle.C.BorderColor = sectionBTitleColor;

                        t.Add(investmentTitle);

                        t.Add(new Table(table, 1, 4, 1, 4, 30).With(ti =>
                        {
                            Guid? clientID = this.FormFUSCZ.Client.ValueOrDefault(v => v.ClientTempID);

                            Cell clientChb = CreateCheckbox(ti, Element.ALIGN_LEFT, investment.ClientTempID == clientID);
                            clientChb.C.PaddingLeft = -1;
                            clientChb.C.PaddingBottom = 0;
                            Cell partnerChb = CreateCheckbox(ti, Element.ALIGN_LEFT, investment.ClientTempID != clientID);
                            partnerChb.C.PaddingLeft = -1;
                            partnerChb.C.PaddingBottom = 0;

                            ti.Add(clientChb);
                            ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7Client, fTextBold));

                            ti.AddDataOrEmpty(FormFUSCZ.FUSForPartner, partnerChb);
                            ti.AddDataOrEmpty(FormFUSCZ.FUSForPartner, new Cell(ti, App_GlobalResources.FusCZ.S7Partner, fTextBold));

                            ti.Add(new Cell(ti));

                            //ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7Institution + space, fTextBold).Add(new Chunk(this.GetInstitution(investment.InstitutionID), fTextNormal)));
                        }));
                        t.Add(new Table(table, 1).With(ti =>
                        {
                            ti.Style.PaddingLeft(-0.25);
                            ti.Add(new Cell(ti, App_GlobalResources.FusCZ.S7Institution + space, fTextNormal).Add(new Chunk(this.GetInstitution(investment.InstitutionID), fTextBold)));
                        }));
                    }));

                    table.Add(new Table(table, 2, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTFinancialTool + space, fTextNormal).Add(new Chunk(investment.KTFinancialTool, fTextBold)).PaddingBottom(paddingToCompensateCheckBox));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTFinancialToolType + space, fTextNormal).Add(new Chunk(investment.KTFinancialToolType.Substring(2), fTextBold)));
                    }));

                    table.Add(new Table(table, 1, 1, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7BuySell + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox).Add(new Chunk(FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == investment.KTBuySell && p.FUSListTypeID == Constants.FUSCZListTypes.BUYSELL).Select(p => p.Name).FirstOrDefault(), fTextBold)));
                        if (investment.KTInvestAmountTypeCode == "CZONETIME")
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTInvestAmountTypeCode + space, fTextNormal).Add(new Chunk("Jednorázová", fTextBold)));
                        }
                        else if (investment.KTInvestAmountTypeCode == "CZMULTITIME")
                        {
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTInvestAmountTypeCode + space, fTextNormal).Add(new Chunk("Pravidelná", fTextBold)));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7AmountPeriod + space, fTextNormal).Add(new Chunk(GetPeriodText(investment.AmountPeriod), fTextBold)));
                        }
                        else
                        {
                            t.Add(new Cell(t));
                            t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTInvestAmountTypeCode + space, fTextNormal));
                        }
                    }));
                    table.Add(new Table(table, 2, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Amount + space, fTextNormal).PaddingBottom(paddingToCompensateCheckBox).Add(new Chunk(investment.Amount, fTextBold)));
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7KTCurrency + space, fTextNormal).Add(new Chunk(FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == investment.KTCurrency).Select(p => p.Name).FirstOrDefault(), fTextBold)));
                    }));


                    table.Add(new Table(table, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientNote + space, fTextNormal));

                        string note = (string.IsNullOrEmpty(investment.Note) || string.IsNullOrEmpty(investment.Note.Replace(" ", ""))) ? App_GlobalResources.FusCZ.None : investment.Note;
                        t.Add(new Cell(t, note, fTextBold));
                    }));

                    AddPaddingLine(table, separatingPadding);

                }

            if (firstInvestment != null)
            {
                table.Add(new Table(table, 1, 9).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Legend, fTextMediumNormal));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7LegendInvestmentTool, fTextMediumNormal));

                    t.Add(new Cell(t));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7LegendToolTypePart1, fTextMediumNormal));

                    t.Add(new Cell(t));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7LegendToolTypePart2, fTextMediumNormal)
                        .Indent(20.75));

                    //t.Add(new Cell(t));
                    //t.Add(new Cell(t));
                    //t.Add(new Cell(t, App_GlobalResources.FusCZ.S7LegendToolTypePart3, fTextMediumNormal)
                    //	.Indent(20.75));
                }));

                AddPaddingLine(table, separatingPadding);

                table.Add(new Table(table, 4.2, 2, 4.8).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7ClientPartnerInvestmentRequest + space, fTextNormal).PaddingBottom(0));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Date + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.FinancialInvestment.InvestDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")) + space, fTextBold)).PaddingBottom(0));
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7Time + space, fTextNormal).Add(new Chunk(this.FormFUSCZ.FinancialInvestment.InvestTime.ValueOrDefault(v => v.Value.ToString("HH:mm")) + space, fTextBold)).PaddingBottom(0));
                }));
                AddPaddingLine(table, separatingPadding);

                table.Add(new Table(table, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7SectionBWarning + space, fTextMediumNormal).PaddingTop(0));
                }));

                AddPaddingLine(table, separatingPadding);
            }


            #endregion SectionB

            #region SectionC

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                AddSectionSubHeader(ti, App_GlobalResources.FusCZ.S7SectionCHeader);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                investmentCount++;

                ti.Add(new Table(ti, 1).With(t =>
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.S7SectionCClientPartnerAnnouncementTitle + space, fTextBold)
                        .Add(new Chunk(App_GlobalResources.FusCZ.S7SectionCClientPartnerAnnouncementText, fTextNormal))
                        .AlignJustify()
                        );
                }));

            }));
            ResetTablePaddingVertical(table, innerTablePadding);

            #endregion SectionC

            AddPaddingLine(table, separatingPadding);
        }

        void RenderSection_08(Table table)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinalStatementSignatures == null) return;

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(ti =>
            {
                if (FusZV)
                    AddSectionHeader(ti, App_GlobalResources.FusCZ.S8Section8HeaderZV);
                else
                    AddSectionHeader(ti, App_GlobalResources.FusCZ.S8Section8Header);

                ResetTablePaddingVertical(ti, innerTablePadding);
                AddPaddingLine(ti, separatingPadding);

                ti.Add(new Table(ti, 1).With(t =>
                {
                    if (FusZV)
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S8ClientAnnouncementTitleZV + space, fTextBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S8ClientAnnouncementText, fTextNormal))
                            .AlignJustify());
                    }
                    else
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S8ClientAnnouncementTitle + space, fTextBold)
                            .Add(new Chunk(App_GlobalResources.FusCZ.S8ClientAnnouncementText, fTextNormal))
                            .AlignJustify());
                    }

                }));

                if (IsSmsFus())
                {
                    AddPaddingLine(ti, paragraphPadding);
                    ti.Add(new Table(ti, 1).With(t =>
                    {
                        t.Add(new Cell(t, App_GlobalResources.FusCZ.S8DateFUSSignedTitle + space, fTextNormal)
                        .Add(new Chunk(this.FormFUSCZ.FinalStatementSignatures.DateFUSSigned.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextBold)));

                    }));
                }

                //AddPaddingLine(ti, separatingPadding * 2);
                //ti.Add(new Table(ti, 1).With(t =>
                //{
                //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S8ClientSignatures + space, fTextBold)
                //		.Add(new Chunk(".............................................................................................." + space, fTextNormal)));
                //}));

                //AddPaddingLine(ti, separatingPadding * 2);
                //ti.Add(new Table(ti, 3, 2, 5).With(t =>
                //{
                //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S8SignedPlace + space, fTextBold)
                //		.Add(new Chunk(this.FormFUSCZ.FinalStatementSignatures.SignedPlace, fTextNormal)));


                //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S8DateFUSSigned + space, fTextBold)
                //		.Add(new Chunk(this.FormFUSCZ.FinalStatementSignatures.DateFUSSigned.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")), fTextNormal)));


                //	t.Add(new Cell(t, App_GlobalResources.FusCZ.S8AssociateSignature + space, fTextBold)
                //		.Add(new Chunk("......................................\n", fTextNormal))
                //		.Add(new Chunk(App_GlobalResources.FusCZ.S8AssociateSignatureNote, fTextSmallNormal)).AlignJustify());
                //}));
            }));
            ResetTablePaddingVertical(table, innerTablePadding);
        }

        void RenderInformationAndAnnouncement(Table table)
        {
            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(tiouter =>
            {
                tiouter.Add(new Cell(tiouter, "INFORMACE A POUČENÍ PRO ZÁKAZNÍKA/PARTNERA A PROHLÁŠENÍ ZPROSTŘEDKOVATELŮ A ZÁKAZNÍKA/PARTNERA", fTextBoldUnderline).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1).AlignCenter());
                tiouter.Style.Leading(0, 1.5);
                tiouter.Style.PaddingTop(0).PaddingBottom(0.5);

                tiouter.T.SplitRows = true;
                tiouter.T.SplitLate = true;

                #region 1-Definice

                if (showItem1)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "DEFINICE", fTextMediumBold));
                        }));

                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ČNB" + space, fLastPagesBold)
                                .Add(new Chunk("– Česká národní banka, Na Příkopě 28, 115 03 Praha 1, webová adresa ", fLastPagesNormal))
                                .Add(new Chunk("www.cnb.cz", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ČOI" + space, fLastPagesBold)
                                .Add(new Chunk("– Česká obchodní inspekce, Štěpánská 567/15, 120 00 Praha 2, webová adresa ", fLastPagesNormal))
                                .Add(new Chunk("www.coi.cz", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Daňovým rezidentem USA" + space, fLastPagesBold)
                                .Add(new Chunk("se pro účely tohoto formuláře rozumí fyzická nebo právnická osoba, která je daňovým rezidentem USA, nebo je u ní zjištěna jiná americká indície, např.: státní občanství USA, místo narození/registrace v USA, adresa/sídlo v USA, US telefon (předčíslí 001), pokyn k poukázání platby na US účet.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Dítětem" + space, fLastPagesBold)
                                .Add(new Chunk("se pro účely tohoto formuláře rozumí fyzická osoba, u které je Zákazník nebo Partner zákonným zástupcem nebo opatrovníkem, nebo jiná nezletilá osoba, která je účastníkem zprostředkovávaného smluvního vztahu (např. vnuk, vnučka, neteř, synovec atd.), a pro kterou Spolupracovník (resp. FINCENTRUM) zprostředkovává smlouvy pro smluvní finanční instituce (viz" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk("), nebo uzavírá Smlouvu o investičním zprostředkování nebo jí poskytuje další související služby.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Finanční arbitr" + space, fLastPagesBold)
                                .Add(new Chunk("Kancelář Finančního arbitra, Legerova 1581/69, 110 00 Praha 1, webová adresa ", fLastPagesNormal))
                                .Add(new Chunk("www.finarbitr.cz/cs/", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM" + space, fLastPagesBold)
                                .Add(new Chunk("- obchodní jméno: Fincentrum a.s., adresa sídla: Pobřežní 620/3, 186 00 Praha 8, IČO: 24260444, zápis v OR: Městský soud v Praze, oddíl B, vložka 18458", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "NOZ" + space, fLastPagesBold)
                                .Add(new Chunk("– zákon č. 89/2012 Sb., občanský zákoník v platném znění", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Partnerem" + space, fLastPagesBold)
                                .Add(new Chunk("se pro účely tohoto formuláře rozumí fyzická osoba, která je životním partnerem Zákazníka, je-li tento rovněž fyzickou osobou, příp. zákonný zástupce nebo opatrovník této osoby, nebo statutární orgán je-li Zákazníkem právnická osoba, nebo jiná plnoletá osoba se vztahem k Zákazníkovi, pro kterou Spolupracovník (resp. FINCENTRUM) zprostředkovává smlouvy pro smluvní finanční instituce (viz" + space, fTextSmallNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk("), nebo uzavírá Smlouvu o investičním zprostředkování nebo jí poskytuje další související služby.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Reklamační řád" + space, fLastPagesBold)
                                .Add(new Chunk("je dostupný na" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(". Postupy ohledně možnosti podání stížnosti případně o dalších právních možnostech jsou uvedeny v Reklamačním řádu FINCENTRA.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Spolupracovník" + space, fLastPagesBold)
                                .Add(new Chunk("– fyzická nebo právnická osoba, která pro FINCENTRUM vykonává činnosti na základě Smlouvy o spolupráci (jedná se především o zprostředkovatelskou činnost v oblasti finančních produktů a souvisejících služeb)", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazníkem" + space, fLastPagesBold)
                                .Add(new Chunk("se pro účely tohoto formuláře rozumí fyzická nebo právnická osoba, příp. zákonný zástupce osoby nebo opatrovník, pro kterou Spolupracovník (resp. FINCENTRUM) zprostředkovává smlouvy pro smluvní finanční instituce (viz" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk("), nebo uzavírá Smlouvu o investičním zprostředkování nebo jí poskytuje další související služby.", fLastPagesNormal))
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding * 2);
                    }));
                }

                #endregion 1-Definice

                #region 2-zpracovani os. udaju

                if (showItem2)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPRACOVÁNÍ OSOBNÍCH ÚDAJŮ", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM tímto v souladu se zákonem č. 101/2000 Sb., o ochraně osobních údajů (dále jen „Zákon“) oznamuje, že v souvislosti se zprostředkováním a uzavíráním smluv, jejichž obsahem jsou produkty finančních institucí nebo služby FINCENTRA, eviduje osobní údaje osob, které jeho prostřednictvím, nebo prostřednictvím jeho Spolupracovníků, nebo přímo s ním tyto smlouvy uzavřely nebo využily jeho služeb. Zákazník/Partner podpisem tohoto formuláře výslovně souhlasí s tím, že FINCENTRUM je oprávněno:", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "1. zpracovávat osobní údaje uvedené na tomto formuláři za účelem realizace smluv, jejich zprostředkování a uzavírání, a to i pokud je tato činnost prováděna prostřednictvím Spolupracovníků,", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        //AddPaddingLine(ti, separatingPadding * 2);
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "2. vytvářet databázi Zákazníků/Partnerů, a těmto případně nabízet další služby FINCENTRA.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazník/Partner byl seznámen se skutečnostmi, které konkrétní osobní údaje jsou relevantní pro uzavření předmětné smlouvy, a pokud jsou na tomto formuláři uvedeny také další osobní údaje pro uzavření předmětné smlouvy nikoliv nezbytné, Zákazník/Partner dává podpisem tohoto formuláře souhlas s jejich shromažďováním a zpracováním vzhledem k tomu, že veškeré uvedené osobní údaje jsou nezbytné k naplnění účelu vzájemné spolupráce při jednání o smluvním vztahu, respektive při zprostředkování a realizaci dalších smluv.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Za účelem realizace výše uvedených bodů bude zpracováváno jméno, příjmení, adresa bydliště, rodné číslo a případně další údaje nutné pro uzavření smluv. Rodné číslo je osobním údajem podléhajícím regulaci zákona č. 133/2000 Sb., o evidenci obyvatel a rodných čísel. FINCENTRUM je oprávněno ve smyslu ust. § 13c tohoto zákona zpracovávat rodná čísla na základě zvláštních zákonů (např. zák. č. 253/2008 Sb., o některých opatřeních proti legalizaci výnosů z trestné činnosti a financování terorismu, zák. č. 89/2012 Sb., občanského zákoníku, a dalších) respektive na základě poskytnutého souhlasu nositele rodného čísla nebo jeho zákonného zástupce. Pokud součástí zprostředkovávaných smluv od finančních institucí budou dotazy na zdravotní stav a další podobné citlivé údaje, Zákazník/Partner podpisem tohoto formuláře výslovně souhlasí i se zpracováním těchto údajů.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Údaje budou spravovány a zpracovávány FINCENTREM minimálně po dobu trvání zprostředkovaných nebo uzavřených smluv a dále po dobu 10 let po jejich skončení.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazník/Partner má právo, zjistí-li, že došlo k porušení zákonem vymezených povinností správcem nebo zpracovatelem, obrátit se na Úřad pro ochranu osobních údajů s žádostí o zajištění opatření k nápravě protiprávního stavu. Došlo-li k porušení Zákonem vymezených povinností jak u správce, tak u zpracovatele, odpovídají za ně společně a nerozdílně. Subjekt údajů se může domáhat svých nároků u kteréhokoliv z nich.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 2-zpracovani os. udaju

                #region 3-opatreni proti legalizaci...

                if (showItem3)
                {
                    AddPaddingLine(tiouter, separatingPadding * 5);
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "OPATŘENÍ PROTI LEGALIZACI VÝNOSŮ Z TRESTNÉ ČINNOSTI A FINANCOVÁNÍ TERORISMU", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM je při některých svých činnostech osobou povinnou ve smyslu zákona č. 253/2008 Sb., o některých opatřeních proti legalizaci výnosů z trestné činnosti a financování terorismu (dále jen „AML zákon“). FINCENTRUM v souladu s AML zákonem a systémem svých vnitřních AML předpisů plní své povinnosti v oblasti boje proti legalizaci výnosů z trestné činnosti a financování terorismu. Účelem naplňování těchto AML zásad a principů je minimalizovat resp. zabránit rizikům, jež vyplývají z možného zneužití společnosti FINCENTRUM k legalizaci výnosů z trestné činnosti nebo financování terorismu a rozpoznat tato rizika v rané fázi.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Před uzavřením obchodu musí být účastník obchodu prozkoumán z hlediska přijatelnosti pro FINCENTRUM provedením celkového ohodnocení, přičemž musí být vzata v úvahu veškerá Zákazníkem/Partnerem poskytnutá data. Pozadí a účel smlouvy musí být vyjasněn a dokumentován. Pokud se v průběhu kontroly přijatelnosti Zákazníka/Partnera objeví nějaké podezření nebo pokud nastanou nějaké jiné podezřelé okolnosti, bude postupováno v souladu s platnou legislativou a vnitřními předpisy FINCENTRA. Obchody smí být prováděny pouze se Zákazníky/Partnery, jejichž identita a finanční zdroje mohou být v rozumné míře prokázány.", fLastPagesNormal)
                                .AlignJustify());
                        }));

                    }));
                    tiouter.Add(new Table(tiouter, 1, 30).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "Politicky exponovanou osobou se ve smyslu ustanovení AML zákona rozumí:", fLastPagesBold)
                            .AlignJustify().PaddingTop(1.5).PaddingBottom(1.5));
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 29).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "a)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "fyzická osoba, která je ve významné veřejné funkci s celostátní působností, jako je například hlava státu nebo předseda vlády, ministr, náměstek nebo asistent ministra, člen parlamentu, člen nejvyššího soudu, ústavního soudu nebo jiného vyššího soudního orgánu, proti jehož rozhodnutí obecně až na výjimky nelze použít opravné prostředky, člen účetního dvora, člen vrcholného orgánu centrální banky, vysoký důstojník v ozbrojených silách nebo sborech, člen správního, řídícího nebo kontrolního orgánu obchodního závodu ve vlastnictví státu, velvyslanec nebo chargé d´affaires, nebo fyzická osoba, která obdobné funkce vykonává v orgánech Evropské unie nebo jiných mezinárodních organizací, a to po dobu výkonu této funkce a dále po dobu jednoho roku po ukončení výkonu této funkce, a která", fLastPagesNormal)
                            .AlignJustify());
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "i)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "má bydliště mimo Českou republiku, nebo", fLastPagesNormal)
                            .AlignJustify());

                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "ii)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "takovou významnou veřejnou funkci vykonává mimo Českou republiku,", fLastPagesNormal)
                            .AlignJustify());
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 29).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "b)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "fyzická osoba, která", fLastPagesNormal)
                            .AlignJustify());
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "i)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je k osobě uvedené v písmenu a) ve vztahu manželském, partnerském nebo v jiném obdobném vztahu nebo ve vztahu rodičovském,", fLastPagesNormal)
                            .AlignJustify());

                    }));

                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "ii)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je k osobě uvedené v písmenu a) ve vztahu syna nebo dcery nebo je k synovi nebo dceři osoby uvedené v písmenu a) osobou ve vztahu manželském (zeťové, snachy), partnerském nebo jiném obdobném vztahu,", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "iii)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je společníkem nebo skutečným majitelem stejné právnické osoby, popřípadě svěřenectví nebo jiného obdobného právního uspořádání podle cizího právního řádu jako osoba uvedená v písmenu a), nebo je o ni společnosti známo, že je v jakémkoli jiném blízkém podnikatelském vztahu s osobou uvedenou v písmeni a), nebo", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "iv)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je skutečným majitelem právnické osoby, popřípadě svěřenectví nebo jiného obdobného právního uspořádání podle cizího právního řádu, o kterém je známo, že bylo vytvořeno ve prospěch osoby uvedené v písmenu a).", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 3-opatreni proti legalizaci...

                #region 4-zprostredkovani spotrebitelskych uveru

                if (showItem4)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPROSTŘEDKOVÁNÍ SPOTŘEBITELSKÝCH ÚVĚRŮ", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "V případě, že dochází ke zprostředkování spotřebitelského úvěru, podléhá tento vztah regulaci zákona č. 145/2010 Sb., zákon o spotřebitelském úvěru. FINCENTRUM tímto informuje Zákazníka/Partnera, že zprostředkovává spotřebitelské úvěry pro více poskytovatelů spotřebitelských úvěrů (věřitelů), taktéž činí Spolupracovník, byl-li ke zprostředkování spotřebitelských úvěrů FINCENTREM zmocněn.", fLastPagesNormal)
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding * 2);
                    }));
                }

                #endregion 4-zprostredkovani spotrebitelskych uveru

                #region 5-zprostredkovani pojisteni

                if (showItem5)
                {
                    List<Insurances> Insurances = new List<Insurances>();
                    // naplneni listu pro sekci 5 - pro nasledne vycteni pres foreach cyklus - prvni polozka zde nema byt
                    #region InsuranceFill

                    Insurances generalInsurance = new Insurances();
                    generalInsurance.Title = "Pojištění obecně";
                    generalInsurance.Advantage = "Státní dozor, technické rezervy pojišťoven, pojišťovny jsou většinou pojištěny u zajišťoven. Pojistná ochrana je zpravidla účinná okamžikem uzavření smlouvy (kromě výslovně uvedených výluk).";
                    generalInsurance.Disadvantage = "Pojistné podmínky obsahují v rámci pojistné ochrany často množství výluk a omezení, kdy není pojišťovna povinna vyplatit pojistné plnění, příp. jen do určité výše.";
                    generalInsurance.Exception = "FirstItem";
                    Insurances.Add(generalInsurance);

                    Insurances personInsurance = new Insurances();
                    personInsurance.Title = "Pojištění osob";
                    personInsurance.Advantage = "Pojistné plnění v případě smrti pojištěného nepodléhá zdanění. U pojištění typu I. až VI. nemá pojišťovna právo v průběhu pojistné doby vypovědět pojistnou smlouvu z důvodu zhoršení zdravotního stavu pojištěného.";
                    personInsurance.Disadvantage = "U pojištění typu I. až VI. nejsou vklady pojištěny z titulu zákona. U pojištění typu I. až III. a V. se zhodnocení pojistného formou technické úrokové míry a podíly na hospodaření pojišťovny vztahují pouze na tu část, která zbude po odečtu nákladů pojišťovny, jejího zisku a částky pro krytí sjednaných nebezpečí.";
                    Insurances.Add(personInsurance);

                    Insurances taxAspects = new Insurances();
                    taxAspects.Title = "Daňové aspekty";
                    taxAspects.Advantage = "Daňové úlevy je možné obecně uplatnit u pojištění typu I. až IV. Pojištěný si může odečíst své pojistné pro případ dožití až do výše 12.000 Kč ročně od daňového základu.";
                    taxAspects.Disadvantage = "Pro využití daňových výhod musí pojištění trvat min. 5 let a min. do věku 60 let, v případě nedodržení těchto podmínek je nutné již uplatněné odpočty zpětně dodanit. Pojištěný musí být zároveň pojistníkem. U některých typů pojištění musí pojistná částka pro dožití odpovídat zákonným ustanovením.";
                    Insurances.Add(taxAspects);

                    Insurances employerContribution = new Insurances();
                    employerContribution.Title = "Příspěvky zaměstnavatele";
                    employerContribution.Advantage = "Příspěvky zaměstnavatele je možné obecně uplatnit u pojištění typu I. až IV. Z příspěvků zaměstnavatele až do 30 000,-Kč ročně neplatí pojištěný odvody na sociální a zdravotní pojištění ani daň z příjmu.";
                    Insurances.Add(employerContribution);

                    Insurances insurance1 = new Insurances();
                    insurance1.Title = "I. Kapitálové (smíšené) životní pojištění";
                    insurance1.Advantage = "Garance minimálního zhodnocení kapitálové hodnoty ve formě technické úrokové míry. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance1.Disadvantage = "Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. V některých případech neprůhlednost rozdělení pojistného na jednotlivé složky – skryté poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, nejčastěji až za 2 roky, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance1);

                    Insurances insurance2 = new Insurances();
                    insurance2.Title = "II. Důchodové pojištění";
                    insurance2.Advantage = "Garance minimálního zhodnocení kapitálové hodnoty ve formě technické úrokové míry. Vyšší efektivita zhodnocení (rizikové pojistné je nulové či minimální). U většiny pojišťoven se nezkoumá zdravotní stav. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance2.Disadvantage = "Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. Častá je neprůhlednost rozdělení pojistného na jednotlivé složky – skryté poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, nejčastěji až za 2 roky (u některých produktů za 1 rok), takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance2);

                    Insurances insurance3 = new Insurances();
                    insurance3.Title = "III. Flexibilní životní pojištění";
                    insurance3.Advantage = "Jedná se o kapitálové (smíšené) nebo investiční životní pojištění, které lze variabilně nastavovat v závislosti na aktuální životní situaci. Pojištění lze za daných podmínek přerušit (platební prázdniny), snížit/zvýšit pojistné a pojistnou částku, lze provádět částečné výběry apod. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí. Lepší průhlednost rozdělení pojistného na jednotlivé složky – poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod. – je obecně vyšší než u kapitálových a důchodových pojištění.";
                    insurance3.Disadvantage = "Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, u dlouhodobých smluv obvykle po dvou letech, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance3);

                    Insurances insurance4 = new Insurances();
                    insurance4.Title = "IV. Investiční životní pojištění";
                    insurance4.Advantage = "Možnost dosáhnout vyššího zhodnocení než u ostatních typů životních pojištění. Flexibilita a likvidita v některých případech, možnost měnit parametry a možnost mimořádných výběrů z kapitálové hodnoty. Průhlednost - všechny poplatky jsou uvedeny. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance4.Disadvantage = "Výnos není garantován a ani není obecně zaručeno vyplacení vloženého pojistného. Časový horizont je zpravidla dlouhý (doporučeno min. 15 let). Často relativně vysoké náklady - rozdíl mezi nákupní a prodejní cenou, poplatky za sjednání, poplatky za vedení smlouvy, správu portfolia, za změny v pojistné ochraně apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, u dlouhodobých smluv obvykle po dvou letech, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby (mj. tzv. počáteční jednotky) v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance4);

                    Insurances insurance5 = new Insurances();
                    insurance5.Title = "V. Životní pojištění ve prospěch dítěte – s garantovaným zhodnocením";
                    insurance5.Advantage = "Garance minimálního zhodnocení kapitálové hodnoty ve formě technické úrokové míry. Zajištění finančních prostředků pro dítě v případě smrti nebo plné invalidity připojištěné dospělé osoby, přičemž se v těchto případech často vyplácí i renty před uplynutím pojistné doby. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance5.Disadvantage = "Není možné uplatnit daňové úlevy pro připojištěnou dospělou osobu. Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. Neprůhlednost rozdělení pojistného na jednotlivé složky – skryté poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod.";
                    Insurances.Add(insurance5);

                    Insurances insurance6 = new Insurances();
                    insurance6.Title = "VI. Životní pojištění ve prospěch dítěte – investičního typu";
                    insurance6.Advantage = "Možnost dosáhnout vyššího zhodnocení než u pojištění ve prospěch dítěte s garantovaným zhodnocením. Zajištění finančních prostředků pro dítě v případě smrti nebo plné invalidity připojištěné dospělé osoby, přičemž se v těchto případech často vyplácí i renty před uplynutím pojistné doby. Flexibilita a likvidita - možnost měnit parametry, peníze jsou obecně rychleji k dispozici. Průhlednost - všechny poplatky jsou uvedeny. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance6.Disadvantage = "Není možné uplatnit daňové úlevy pro připojištěnou dospělou osobu. Výnos není garantován a ani není obecně zaručeno ani vyplacení vloženého pojistného. Časový horizont je zpravidla dlouhý (doporučeno min. 15 let). Často relativně vysoké náklady - rozdíl mezi nákupní a prodejní cenou, poplatky za sjednání, poplatky za vedení smlouvy, správu portfolia, za změny v pojistné ochraně apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, u dlouhodobých smluv obvykle po dvou letech, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby (mj. tzv. počáteční jednotky) v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance6);

                    Insurances insurance7 = new Insurances();
                    insurance7.Title = "VII. Rizikové životní pojištění";
                    insurance7.Advantage = "Relativně levná finanční ochrana pozůstalých v případě smrti pojištěného. Vhodné pro zajištění úvěrů. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance7.Disadvantage = "Není možné uplatnit daňové úlevy. Většina těchto pojištění neobsahuje kapitálovou hodnotu, na konci pojistné doby pojištění zaniká bez náhrady.";
                    Insurances.Add(insurance7);

                    Insurances insurance8 = new Insurances();
                    insurance8.Title = "VIII. Úrazové pojištění";
                    insurance8.Advantage = "Obsahuje pojištění pro případ smrti nebo trvalých následků nebo kombinace obojího. Relativně nízké pojistné. Možno sjednat samostatně nebo jako připojištění k jiným produktům. U trvalých následků úrazu často pojišťovny nabízejí tzv. progresivní plnění, kdy v případě těžších následků plní pojišťovna násobky sjednaných základních částek.";
                    insurance8.Disadvantage = "Příčinou pojistné události může být pouze úraz, který způsobí smrt nebo trvalé následky – menší úrazy bez trvalých následků nejsou pojištěny. Téměř vždy se plní v procentech pojistné částky podle rozsahu zdravotního poškození (u progresivního plnění ze sjednaných násobků). Trvalé následky jsou obecně prokazatelné až po 1 roce po úrazu.";
                    Insurances.Add(insurance8);

                    Insurances insurance9 = new Insurances();
                    insurance9.Title = "IX. Pojištění denních dávek";
                    insurance9.Advantage = "Vyplácí se denní dávky za dobu nezbytného léčení následků úrazu nebo nemoci, včetně případné hospitalizace. Možno sjednat samostatně nebo jako připojištění k jiným produktům.";
                    insurance9.Disadvantage = "Tzv. karenční doba - nárok na plnění vzniká, jen pokud celková doba léčení je delší než min. doba léčení stanovená v pojistných podmínkách. Je omezen celkový počet dnů léčení (nejčastěji max. 365 dnů).";
                    Insurances.Add(insurance9);

                    Insurances insurance10 = new Insurances();
                    insurance10.Title = "X. Pojištění léčení tělesného poškození";
                    insurance10.Advantage = "Pojistné plnění je vypláceno v % za dobu nezbytného léčení následků úrazu nebo nemoci, včetně případné hospitalizace.";
                    insurance10.Disadvantage = "Většinou nelze sjednat samostatně, pouze jako připojištění k jiným produktům.";
                    Insurances.Add(insurance10);

                    Insurances insurance11 = new Insurances();
                    insurance11.Title = "XI. Pojištění trvalé (plné) invalidity";
                    insurance11.Advantage = "V zásadě existují dvě formy – jednorázová výplata sjednané pojistné částky, nebo tzv. zproštění od placení pojistného po dobu trvání plné invalidity. V prvním případě získá pojištěný ihned finanční prostředky pro řešení vzniklé situace, ve druhém případě pojišťovna sama platí pojistné max. do konce sjednané pojistné doby, aniž je tím dotčena výše pojistného plnění v případě pojistné události, nebo existence pojištění. ";
                    insurance11.Disadvantage = "Pro uplatnění nároků na pojistné plnění z pojištění je nutné mít přiznaný plný invalidní důchod ze státního systému sociálního zabezpečení. Předpisy pro přiznání plného invalidního důchodu jsou obecně velmi přísné. V případě, že pojistným nárokem je zproštění od placení pojistného, pak tento nárok trvá pouze po dobu čerpání plného invalidního důchodu. U některých pojišťoven je podmínkou pro přiznání tohoto nároku pouze invalidita, která vznikla následkem úrazu.";
                    Insurances.Add(insurance11);

                    Insurances insurance12 = new Insurances();
                    insurance12.Title = "XII. Pojištění vážných chorob";
                    insurance12.Advantage = "Pojistné plnění se většinou vyplácí již při lékařské diagnóze dané choroby.";
                    insurance12.Disadvantage = "Omezené množství pojistitelných chorob, navíc velmi přesně definovaných. Tzv. karenční doba od uzavření smlouvy, po kterou se v případě diagnózy choroby neplní.";
                    Insurances.Add(insurance12);

                    Insurances insurance13 = new Insurances();
                    insurance13.Title = "XIII. Cestovní pojištění";
                    insurance13.Advantage = "Krytí léčebných výloh v souvislosti s nezbytným lékařským ošetřením při cestách do zahraničí. Součástí pojištění je i krytí nákladů na neplánovaný návrat domů, tzv. repatriace. Asistenční služby pojišťovny. Možnost sjednání některých připojištění.";
                    insurance13.Disadvantage = "Řada výluk, kdy pojišťovna neplní.";
                    Insurances.Add(insurance13);

                    Insurances propertyInsurance = new Insurances();
                    propertyInsurance.Title = "Pojištění majetku";
                    propertyInsurance.Advantage = "Krytí újmy vzniklé na vlastním nemovitém nebo movitém majetku. Možnost pojištění na tzv. novou cenu. Slevy na pojistném v případě bezškodného průběhu pojištění. Možnost připojištění hodnotnějších věcí nad standardní limity.";
                    propertyInsurance.Disadvantage = "Plní se do výše skutečně vzniklé škody, max. do výše pojistné částky. Riziko tzv. podpojištění (pojistná částka je nižší než hodnota majetku). V některých případech nutnost zvláštního zabezpečení majetku.";
                    Insurances.Add(propertyInsurance);

                    Insurances responsibilityInsurance = new Insurances();
                    responsibilityInsurance.Title = "Pojištění odpovědnosti";
                    responsibilityInsurance.Advantage = "Krytí újmy (na životě, na zdraví, na věci, na zvířeti či z duševních útrap) způsobené jiným osobám v běžném občanském životě, v pracovní činnosti, v souvislosti s provozem dopravních prostředků, v souvislosti s vlastnictvím nemovitosti apod. V některých případech slevy na pojistném v případě bezškodného průběhu pojištění.";
                    responsibilityInsurance.Disadvantage = "Plní se do výše skutečně vzniklé škody, max. do výše pojistné částky. Výrazná omezení a výluky v případě způsobení škod úmyslných, pod vlivem alkoholu či použitím osoby s nebezpečnými vlastnostmi, apod.";
                    Insurances.Add(responsibilityInsurance);

                    #endregion InsuranceFill

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPROSTŘEDKOVÁNÍ POJIŠTĚNÍ", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace a prohlášení pojišťovacích zprostředkovatelů (PZ)", fLastPagesBold)
                                .AlignJustify());
                        }));

                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM vykonává zprostředkovatelskou činnost dle zákona č. 38/2004 Sb., o pojišťovacích zprostředkovatelích a samostatných likvidátorech pojistných (“ZPZ”), § 21, odst. 6, písm. c), tudíž není při zprostředkovatelské činnosti povinno poskytovat řádnou analýzu ve smyslu § 7 ZPZ, a jeho registrační číslo je:", fLastPagesNormal)
                                .Add(new Chunk("PZ: 167194PA. Spolupracovník vykonává zprostředkovatelskou činnost taktéž dle § 21, odst. 6, písm. c) ZPZ a jeho registrační číslo je uvedeno v oddíle VI. - ZPROSTŘEDKOVÁNÍ POJIŠTĚNÍ. Registrační čísla PZ jsou evidována a zveřejněna u ČNB (", fLastPagesNormal))
                                .Add(new Chunk("www.cnb.cz", fLastPagesRed))
                                .Add(new Chunk(").", fLastPagesNormal))
                                .AlignJustify());

                            t.Add(new Cell(t, "PZ prohlašují, že nemají žádný přímý nebo nepřímý podíl na hlasovacích právech a kapitálu pojišťovny, se kterou má být pojištění sjednáno a že pojišťovna, se kterou má být pojištění sjednáno, nebo osoba ovládající danou pojišťovnu nemá žádný přímý nebo nepřímý podíl na hlasovacích právech a kapitálu PZ. PZ jsou na žádost Zákazníka/Partnera povinni sdělit způsob svého odměňování a pojišťovny, se kterými jsou oprávněni sjednávat pojištění. Případné stížnosti je možné podávat na adrese sídla FINCENTRA nebo na e-mail:" + space, fLastPagesNormal)
                                .Add(new Chunk("reklamace@fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(". Veškeré další postupy ohledně možnosti podání stížnosti případně o dalších právních možnostech jsou uvedeny v Reklamačním řádu FINCENTRA, který je dostupný na" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(". Zákazník/Partner prohlašuje, že byl s těmito postupy seznámen.", fLastPagesNormal))
                                .AlignJustify());
                        }));

                    }));

                    foreach (var insurance in Insurances)
                    {
                        if (string.IsNullOrEmpty(insurance.Exception))
                        {
                            tiouter.Add(new Table(tiouter, 1).With(t =>
                            {
                                t.T.SplitLate = true;
                                if (!string.IsNullOrEmpty(insurance.Title))
                                {
                                    t.Add(new Cell(t, insurance.Title, fLastPagesBold)
                                        .AlignCenter());
                                }

                                if (!string.IsNullOrEmpty(insurance.Advantage))
                                {
                                    t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                        .Add(new Chunk(insurance.Advantage, fLastPagesNormal))
                                        .AlignJustify());
                                }
                                t.T.SplitLate = false;

                                if (!string.IsNullOrEmpty(insurance.Disadvantage))
                                {
                                    t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                        .Add(new Chunk(insurance.Disadvantage, fLastPagesNormalItalic))
                                        .AlignJustify());
                                }
                                AddPaddingLine(t, paragraphPadding);

                            }));
                        }
                        else
                        {
                            switch (insurance.Exception)
                            {
                                case "FirstItem":

                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        t.Add(new Cell(t, "Všeobecné informace o pojistných produktech (V: – Výhody, N: – Nevýhody)", fLastPagesBold)
                                            .AlignJustify());
                                        if (!string.IsNullOrEmpty(insurance.Title))
                                        {
                                            t.Add(new Cell(t, insurance.Title, fLastPagesBold)
                                                .AlignCenter());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(insurance.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(insurance.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }

                                        AddPaddingLine(t, paragraphPadding);

                                    }));

                                    break;
                                default:
                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        if (!string.IsNullOrEmpty(insurance.Title))
                                        {
                                            t.Add(new Cell(t, insurance.Title, fLastPagesBold)
                                                .AlignCenter());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(insurance.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(insurance.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }
                                        AddPaddingLine(t, paragraphPadding);

                                    }));
                                    break;
                            }
                        }
                    }

                    //AddPaddingLine(tiouter, separatingPadding);

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Důležité upozornění: u většiny pojištění doporučujeme v průběhu jejich trvání aktualizovat pojistné částky nebo rozsah krytí v závislosti na změnách ekonomických a životních situací (inflace, zhodnocení majetku, změny v příjmech, změny rizikových kategorií atd.).", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 5-zprostredkovani pojisteni

                #region 6-zprostredkovani duchodoveho sporeni...

                if (showItem6)
                {
                    int part6LeftPadding = 0;

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPROSTŘEDKOVÁNÍ DOPLŇKOVÉHO PENZIJNÍHO SPOŘENÍ", fTextMediumBold));
                        }));

                        ti.Style.PaddingLeft(part6LeftPadding);

                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o osobách oprávněných zprostředkovávat DPS", fLastPagesBold)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM, resp. Spolupracovník prohlašují, že jsou oprávněni zprostředkovávat uzavření smluv doplňkového penzijního spoření (dále jen „DPS“) ve smyslu zákona č. 427/2011 Sb. (dále jen „Zákon o DPS“) pro smluvní penzijní společnosti (dále jen „smluvní PS“). FINCENTRUM poskytuje toto zprostředkování na základě registrace u ČNB jako investiční zprostředkovatel, a Spolupracovník na základě registrace u ČNB jako vázaný zástupce FINCENTRA.", fLastPagesNormal)
                                .AlignJustify());
                        }));

                    }));

                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "UPOZORNĚNÍ FINCENTRA:", fLastPagesBold)
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Add(new Cell(t, "a)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "V rámci zprostředkování DPS není poskytováno penzijní doporučení.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "b)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Pokud některá smluvní PS provádí srovnání s jinými, nebo jiných PS z hlediska strategií spoření, způsobu investování účastnických fondů, návrhů smluv, údajů o minulých a očekávaných výnosech u DPS, má Zákazník/Partner právo vyžádat si tyto informace od Spolupracovníka, resp. FINCENTRA, přičemž jsou mu tyto předány na materiálech příslušné smluvní PS. FINCENTRUM takováto srovnání pro Zákazníka/Partnera neprovádí.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "c)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Podrobné informace ve smyslu § 133 Zákona o DPS jsou Zákazníkovi/Partnerovi předávány Spolupracovníkem, resp. FINCENTREM na materiálech konkrétních smluvních PS.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "d)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Klíčové informace o konkrétním účastnickém fondu ve smyslu § 133 Zákona o DPS jsou Zákazníkovi/Partnerovi předávány Spolupracovníkem, resp. FINCENTREM na materiálech konkrétních smluvních PS.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1, 1, 28).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t));
                            t.Add(new Cell(t, "e)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Sdělení klíčových informací musí být Zákazníkovi/Partnerovi poskytnuto na trvalém nosiči informací a uveřejněno na internetových stránkách každé smluvní PS, která daný účastnický fond obhospodařuje.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1, 1, 28).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t));
                            t.Add(new Cell(t, "f)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Každý účastník má právo si bezúplatně vyžádat statut daného účastnického fondu a sdělení klíčových informací v listinné podobě.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Pravidla střetu zájmů", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Ve vztahu mezi FINCENTREM, resp. Spolupracovníkem a Zákazníkem/Partnerem dochází ke střetu zájmů ve smyslu Zákona. Střet zájmů je dán především způsobem odměňování FINCENTRA, resp. Spolupracovníka. Podrobnosti o způsobech výpočtu odměn za zprostředkování DPS FINCENTRUM, resp. Spolupracovník povinni Zákazníkovi/Partnerovi objasnit.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Informace o nákladech, poplatcích a pobídkách", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "FINCENTRUM, resp. Spolupracovník jsou povinni informovat Zákazníka/Partnera o všech příslušných poplatcích a nákladech spojených se zprostředkováním DPS (zejména odměny, pobídky, poplatky, daně apod.). Příslušné informace od smluvních PS jsou uvedeny v jejich písemných materiálech (např. obchodní podmínky, klíčové informace atd.), které předává Zákazníkovi/Partnerovi Spolupracovník, resp. FINCENTRUM, nebo na internetových stránkách smluvních PS. O existenci, povaze a způsobu výpočtu odměny, resp. pobídky musí být Zákazník/Partner jasně, srozumitelně a úplně informován, přičemž tuto informaci obdrží Zákazník/Partner buď přímo od Spolupracovníka, nebo od FINCENTRA na základě dotazu na e-mail nebo na telefon – obojí viz zápatí tohoto formuláře. FINCENTRUM, resp. Spolupracovník prohlašují, že pobídky Zákazníkům/Partnerům neposkytují ani pobídky od nich nepřijímají, s výhradou odměny za zprostředkování smluv o DPS přijímané od smluvních PS.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Informace a poučení k testu investičního zaměření Zákazníka/Partnera", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Zodpovězení otázek testu Zákazníkem/Partnerem umožní zjistit doporučený profil a vhodnou strategii spoření v DPS, která odpovídá jeho cílům, odborným znalostem a zkušenostem potřebným pro pochopení souvisejících rizik. V souladu se Zákonem o DPS jsou FINCENTRUM, resp. Spolupracovník povinni předložit tento test Zákazníkovi/Partnerovi k vyplnění, přičemž vyplnění je ze strany Zákazníka/Partnera dobrovolné.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Upozornění k vyhodnocení testu: a) V případě, že Zákazník/Partner si po vyplnění testu vybere jinou, než doporučenou strategii spoření, vystavuje se riziku nevhodného rozložení investice; b) V případě, že Zákazník/Partner zodpoví otázky neúplně, nepřesně nebo nepravdivě, nebude možné správně vyhodnotit doporučený profil a vhodnou strategii spoření a Zákazník/Partner se tak vystavuje riziku nevhodného rozložení investice; c) V případě, že Zákazník/Partner odmítne test vyplnit, vystavuje se riziku nevhodného rozložení investice, přičemž na něj bude pohlíženo jako na osobu s konzervativním profilem; d) V případě vzniku situací podle bodů a), b) nebo c) výše bude Spolupracovník, resp. FINCENTRUM postupovat podle rozhodnutí/volby Zákazníka/Partnera.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Jestliže vyhodnocením testu vyjde Zákazníkovi/Partnerovi jiný než konzervativní profil, FINCENTRUM při výběru konkrétní strategie odkazuje Zákazníka/Partnera na podmínky konkrétní vybrané smluvní PS. Spolupracovník, resp. FINCENTRUM bude přitom postupovat podle rozhodnutí/volby Zákazníka/Partnera.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Všeobecné informace o doplňkovém penzijním spoření", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "1) III. pilíř – doplňkové penzijní spoření", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Jedná se o možnost zvýšení svého důchodového zabezpečení formou dobrovolného spoření prostřednictvím vlastních příspěvků. Příspěvky jsou zvýhodněny státními příspěvky, daňovými úlevami; účastníkovi může přispívat i zaměstnavatel. Účastník má právo opakovaně měnit penzijní společnost, výši příspěvků i zvolenou strategii spoření. Smlouvu o DPS je možné předčasně ukončit (tzv. odbytné), přičemž ovšem zaniká nárok na výplatu státních příspěvků a v případě, že účastník čerpal daňové úlevy, je povinen je zpětně dodanit. Po dosažení důchodového věku má účastník možnost výplaty svých úspor formou penze na dobu určitou (vyplácí penzijní společnost nebo pojišťovna, kterou si účastník zvolí) nebo formou doživotní penze (vyplácí pojišťovna, kterou si účastník zvolí).", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "2) III. pilíř – transformovaný fond (dříve penzijní připojištění)", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Jedná se o fond spravovaný penzijní společností, jehož prostřednictvím penzijní společnost spravuje penzijní připojištění. Do transformovaného fondu nemohou vstupovat noví účastníci s výjimkou převodu prostředků ze zrušeného penzijního fondu, sloučení transformovaných fondů a převodu prostředků účastníků zanikajícího transformovaného fondu jiné penzijní společnosti. Práva a povinnosti účastníka v transformovaném fondu a příjemce dávky penzijního připojištění z transformovaného fondu se řídí zákonem o penzijním připojištění, sjednaným penzijním plánem a smlouvou o penzijním připojištění. Jejich nároky zůstávají s výjimkou omezení nároku na převod prostředků k jinému fondu zachovány. Nelze být zároveň účastníkem doplňkového penzijního spoření a transformovaného fondu. Převedení prostředků účastníka z transformovaného fondu je jinak možné pouze do účastnických fondů téže penzijní společnosti na základě uzavření smlouvy o doplňkovém penzijním spoření a je bezplatné.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Všeobecné informace o možných rizicích spojených s investičními nástroji", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Viz informace uvedené v oddíle SMLOUVA O INVESTIČNÍM ZPROSTŘEDKOVÁNÍ, odst. 5).", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 6-zprostredkovani duchodoveho sporeni...

                #region 7-smlouva o investicnim zprostredkovani

                if (showItem7)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "SMLOUVA O INVESTIČNÍM ZPROSTŘEDKOVÁNÍ", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o investičním zprostředkovateli a vázaném zástupci", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "FINCENTRUM poskytuje níže uvedenou investiční službu na základě registrace u ČNB jako investiční zprostředkovatel.", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Spolupracovník poskytuje níže uvedenou investiční službu na základě registrace u ČNB jako vázaný zástupce FINCENTRA.", fLastPagesNormal)
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding * 2);
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o poskytované investiční službě", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "FINCENTRUM, resp. Spolupracovník vykonávají investiční zprostředkování dle zákona č. 256/2004 Sb. o podnikání na kapitálovém trhu (dále jen Zákon), § 29, odst. 1 ve spojení s § 4, odst. 2, písm. a). Obsahem služby je přijímání investičních pokynů k obchodům s akciemi a cennými papíry kolektivního investování od Zákazníka/Partnera a jejich předávání obchodníkům s cennými papíry a investičním společnostem k vlastní realizaci (seznam smluvních partnerů – viz" + space, fLastPagesNormal)
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(" ). V RÁMCI INVESTIČNÍHO ZPROSTŘEDKOVÁNÍ NENÍ POSKYTOVÁNA SLUŽBA INVESTIČNÍHO PORADENSTVÍ ve smyslu ust. § 29, odst. 1 ve spojení s § 4, odst. 2, písm. e) Zákona. FINCENTRUM, resp. Spolupracovník rovněž neposkytují službu obhospodařování majetku Zákazníka/Partnera, konečné rozhodnutí je plně v kompetenci a na odpovědnosti Zákazníka/Partnera. Za svou činnost jsou FINCENTRUM, resp. Spolupracovník odměňováni formou provize v závislosti na Zákazníkem/Partnerem investované částce a druhu investičního nástroje. Komunikace se Zákazníkem/Partnerem probíhá osobně nebo písemně, a to v jazyce českém. Reklamace a stížnosti je možné zasílat písemně na adresu sídla FINCENTRA nebo elektronicky na adresu" + space, fLastPagesNormal))
                                .Add(new Chunk("reklamace@fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Kategorizace Zákazníka/Partnera:" + space, fLastPagesBold)
                                .Add(new Chunk("FINCENTRUM, resp. Spolupracovník tímto informují Zákazníka/Partnera, že v rámci kategorizace Zákazníka/Partnera využívají možnosti Zákona a považují všechny Zákazníky/Partnery za tzv. „neprofesionální“ (ve smyslu § 2a, odst. 4 Zákona). Při tomto zařazení do kategorie „neprofesionální“ požívá Zákazník/Partner nejvyšší stupeň ochrany ve smyslu ustanovení Zákona, a to z hlediska struktury a rozsahu informací, které jsou FINCENTRUM, resp. Spolupracovník povinni Zákazníkovi/Partnerovi poskytnout. Jedná se zejména o informace a poučení ve smyslu investičních rizik, povahy investičních nástrojů, zjišťování znalostí a zkušeností Zákazníka/Partnera, přiměřenosti investičních pokynů apod. Zákazník/Partner má právo požádat o zařazení do vyšší kategorie, pokud splní požadavky Zákona.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Pravidla střetu zájmů:" + space, fLastPagesBold)
                                .Add(new Chunk("Ve vztahu mezi FINCENTREM, resp. Spolupracovníkem a Zákazníkem/Partnerem dochází ke střetu zájmů ve smyslu Zákona. Střet zájmů je dán především způsobem odměňování FINCENTRA, resp. Spolupracovníka (viz výše). Podrobnosti o způsobech výpočtu odměn za jednotlivé investiční Produkty jsou FINCENTRUM, resp. Spolupracovník povinni Zákazníkovi/Partnerovi objasnit.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o nákladech a poplatcích:" + space, fLastPagesBold)
                                .Add(new Chunk("FINCENTRUM, resp. Spolupracovník jsou povinni informovat Zákazníka/Partnera o všech příslušných poplatcích a nákladech spojených s poskytnutím investiční služby (zejména celková cena, včetně všech odměn, provizí, poplatků a daní, měna včetně směnných kurzů a nákladů, platební podmínky apod.). Příslušné informace od smluvních partnerů FINCENTRA (viz výše) jsou uvedeny v jejich písemných materiálech (např. Prospekt nebo Statut), které předává Zákazníkovi/Partnerovi Spolupracovník, resp. FINCENTRUM, nebo na internetových stránkách smluvních partnerů. O existenci, povaze a způsobu výpočtu odměny (pobídky) musí být Zákazník/Partner jasně, srozumitelně a úplně informován, přičemž tuto informaci obdrží Zákazník/Partner buď přímo od Spolupracovníka, nebo od FINCENTRA na základě dotazu na e-mail nebo na telefon – obojí viz zápatí tohoto formuláře.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace k Investičnímu dotazníku", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "FINCENTRUM, resp. Spolupracovník v souladu se zákonem č. 256/2004 Sb., o podnikání na kapitálovém trhu ve znění pozdějších předpisů, a prováděcími vyhláškami k tomuto zákonu Zákazníkovi/Partnerovi předkládají tento Investiční dotazník. Zákazník/Partner vyplňuje tento Investiční dotazník dobrovolně. Zákazníkem/Partnerem poskytnuté informace FINCENTRUM, resp. Spolupracovník v souladu s požadavky právních předpisů využijí k vyhodnocení přiměřenosti poskytovaných služeb a investičních nástrojů z hledisek znalostí a zkušeností Zákazníka/Partnera v oblasti investic. Získání informací od Zákazníka/Partnera je předpokladem poskytování investičních služeb Zákazníkovi/Partnerovi kvalifikovaně a v jeho nejlepším zájmu.", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Pokud Zákazník/Partner odmítne na otázky v Investičním dotazníku odpovědět nebo některou z otázek odpoví nepravdivě, neúplně nebo nepřesně, vystavuje se nebezpečí, že FINCENTRUM, resp. Spolupracovník nevyhodnotí skutečné potřeby Zákazníka/Partnera zcela přesně a poskytne investiční službu, která pro něj není přiměřená. V případě, že Zákazník/Partner odmítne odpovědět nebo uvede nepravdivé, neúplné nebo nepřesné informace, mohou FINCENTRUM, resp. Spolupracovník odmítnout provést jeho pokyn(y) ohledně konkrétní investiční služby, obchodu nebo investičního nástroje.", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Na základě informací poskytnutých Zákazníkem/Partnerem v tomto Investičním dotazníku, budou FINCENTRUM, resp. Spolupracovník poskytovat Zákazníkovi/Partnerovi službu Investičního zprostředkování, tzn., že budou Zákazníkovi/Partnerovi nabízet investiční nástroje nebo nástroje kolektivního investování, které jsou přiměřené znalostem a zkušenostem Zákazníka/Partnera, a budou přijímat a předávat Investiční pokyny Zákazníka/Partnera.", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "V rámci Investičního zprostředkování budou FINCENTRUM, resp. Spolupracovník Zákazníkovi/Partnerovi nabízet investiční nástroje emitentů, se kterými má FINCENTRUM uzavřenu smlouvu o distribuci těchto investičních nástrojů, nebo investiční služby osob, se kterými má uzavřenu smlouvu o nabízení těchto služeb.", fLastPagesNormal)
                                .AlignJustify());


                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    int pollPadding = 8;

                    tiouter.Style.BackgroundColor(0xffe5e5).PaddingLeft(-5).PaddingRight(-5);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "VYHODNOCENÍ PŘIMĚŘENOSTI NA ZÁKLADĚ INVESTIČNÍHO DOTAZNÍKU", fLastPagesBold)
                                .PaddingBottom(paragraphPadding)
                                .AlignJustify());
                        }));

                        //AddPaddingLine(ti, paragraphPadding);


                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(pollPadding);
                            t.Add(new Cell(t, "0 – 5 bodů: MÉNĚ ZKUŠENÝ INVESTOR", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Poskytnutí investiční služby a využití investičních nástrojů neodpovídají Vašim odborným znalostem a zkušenostem potřebným pro pochopení souvisejících rizik. Pokud i přesto budete trvat na poskytnutí příslušné investiční služby, jsme v souladu s právními předpisy oprávněni Vám danou investiční službu poskytnout. V takovém případě Vám doporučujeme se seznámit se všemi podstatnými okolnostmi kolektivního investování. Informujte se prosím zejména o pravidlech pro určování ceny cenných papírů (jednotlivě i ve fondech), rozsahu zajištění či garancí související s konkrétními investičními nástroji, rizicích kolísání hodnoty investic, investičním horizontu jednotlivých investičních nástrojů, jakož i na měnové riziko při investování v cizí měně. Toto upozornění nepovažujte za zhodnocení vhodnosti Vaší konkrétní investice.", fLastPagesNormal)
                                .PaddingBottom(paragraphPadding)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(pollPadding);
                            t.Add(new Cell(t, "6 – 15 bodů: STŘEDNĚ ZKUŠENÝ INVESTOR", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Vyhodnocení Vašich odpovědí prokázalo dostatečné znalosti a zkušenosti s kolektivním investováním, přiměřené pro poskytnutí investiční služby v souvislosti s investováním do fondů peněžního trhu. Pokud i přesto budete trvat na poskytnutí příslušné investiční služby, jsme v souladu s právními předpisy oprávněni Vám danou investiční službu poskytnout. V takovém případě Vám doporučujeme se seznámit se všemi podstatnými okolnostmi kolektivního investování. Před investicí do ostatních fondů, akcií či dluhopisů se prosím informujte zejména o rizicích kolísání hodnoty investic, investičním horizontu jednotlivých fondů, rizicích koncentrace a likvidity speciálně zaměřených fondů a kreditním riziku zajištěných fondů, o měnovém riziku při investování do fondů v cizí měně a o ostatních zvláštních rizicích příslušejících k investičnímu nástroji, do kterého chcete investovat. Toto upozornění nepovažujte za zhodnocení vhodnosti Vaší konkrétní investice.", fLastPagesNormal)
                                .PaddingBottom(paragraphPadding)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(pollPadding);
                            t.Add(new Cell(t, "16 – 24 bodů: ZKUŠENÝ INVESTOR", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Vyhodnocení Vašich odpovědí prokázalo dostatečné znalosti a zkušenosti s kolektivním investováním, přiměřené pro poskytnutí investiční služby v souvislosti s investováním do fondů peněžního trhu, dluhopisů, fondů dluhopisových, zajištěných, smíšených a akciových. Pokud i přesto budete trvat na poskytnutí příslušné investiční služby, jsme v souladu s právními předpisy oprávněni Vám danou investiční službu poskytnout. V takovém případě Vám doporučujeme se seznámit se všemi podstatnými okolnostmi kolektivního investování. Před investicí do ostatních fondů či akcií se prosím informujte zejména o rizicích kolísání hodnoty investic, investičním horizontu jednotlivých fondů, rizicích koncentrace a likvidity speciálně zaměřených fondů, o měnovém riziku při investování do fondů v cizí měně a o ostatních zvláštních rizicích příslušejících k investičnímu nástroji, do kterého chcete investovat. Toto upozornění nepovažujte za zhodnocení vhodnosti Vaší konkrétní investice.", fLastPagesNormal)
                                .PaddingBottom(paragraphPadding)
                                .AlignJustify());
                        }));
                    }));
                    //AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(pollPadding);
                            t.Add(new Cell(t, "25 – 30 bodů: VELMI ZKUŠENÝ INVESTOR", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Na základě vyhodnocení Vašich odpovědí, které prokázaly vysoký stupeň znalostí a zkušeností s kolektivním investováním si Vás dovolujeme informovat, že Vaše znalosti a zkušenosti považujeme za přiměřené pro investování do fondů kolektivního investování akcií či dluhopisů. Pokud i přesto budete trvat na poskytnutí příslušné investiční služby, jsme v souladu s právními předpisy oprávněni Vám danou investiční službu poskytnout. V takovém případě Vám doporučujeme se seznámit se všemi podstatnými okolnostmi kolektivního investování. I přes výše uvedené věnujte vždy pozornost popisu rizik příslušejících k jednotlivým investičním nástrojům, které chcete využít. Toto upozornění nepovažujte za zhodnocení vhodnosti Vaší konkrétní investice.", fLastPagesNormal)
                                .PaddingBottom(paragraphPadding / 2)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding / 2);

                    tiouter.Style.BackgroundColor(null).PaddingLeft(0).PaddingRight(0);
                    #region FillInvestments

                    List<ListInvestments> Investments = new List<ListInvestments>();
                    // naplneni listu pro sekci 7 - pro nasledne vzcteni pres foreach cyklus - prvni polozka zde nema byt, ale ma byt 

                    ListInvestments investment_1 = new ListInvestments();
                    investment_1.TitlePrefix = "1)";
                    investment_1.Title = "Daňové aspekty";
                    investment_1.Advantage = "Výnosy z investičního nástroje jsou osvobozeny od daně z příjmu fyzických osob, pokud dojde k jeho prodeji za dobu delší než tři roky od jeho nákupu.";
                    investment_1.Disadvantage = "Výnosy vyplácené formou dividend je investor povinen zdanit vždy.";
                    investment_1.Exception = "FirstItem";
                    Investments.Add(investment_1);

                    ListInvestments investment_2 = new ListInvestments();
                    investment_2.TitlePrefix = "2)";
                    investment_2.Title = "Garanční systémy";
                    investment_2.Exception = "ListItem1";
                    Investments.Add(investment_2);

                    ListInvestments investment_3 = new ListInvestments();
                    investment_3.TitlePrefix = "3)";
                    investment_3.Title = "Akcie";
                    investment_3.Advantage = "Transparentnost – investor má maximální kontrolu nad tím, do kterých akciových titulů své prostředky investuje. Dividenda – pravidelný podíl na zisku společnosti tvoří ve většině případů akciových titulů nezanedbatelnou částku z celkových výnosů akcie. Likvidita – u akcií, které jsou obchodovatelné na regulovaných trzích, obecně platí, že je možné je kdykoliv prodat zpět za aktuální tržní cenu. Tato možnost ovšem není nijak zaručena. Zajímavé zisky - výnosový potenciál akcií je větší než u jiných investičních nástrojů, zejména na dlouhém investičním horizontu jsou výnosy akcií v porovnání s jinými investičními nástroji nepřekonatelné. Globalizace akciových trhů – v současnosti je možné nakupovat nepřeberné množství akciových titulů z celého světa. Je možné vydělávat jak na růstu cen akcií, tak na jejich poklesu. Velký výběr obchodníků s cennými papíry – každý investor si může vybrat svého obchodníka, který mu zprostředkuje nákup a prodej konkrétních cenných papírů. Velká konkurence, zejména v zahraničí, pak umožňuje najít si obchodníka, který bude splňovat všechny představy investora (nízké poplatky, kvalitní servis, dobré jméno apod.).";
                    investment_3.Disadvantage = "Volatilita – akcie se vyznačují větší volatilitou (výkyvy cen akcií), než jiné druhy investičních nástrojů, což s sebou přináší větší riziko ztrát, zejména v kratším investičním horizontu. Pro korunové investory je nevýhodou malý výběr likvidních titulů dostupných v české koruně. U akcií je kladen větší důraz na znalosti a zkušenosti samotného investora. Je také nutné aktivně sledovat chování na trzích, což může být časově náročné, pokud tuto službu nemá investor sjednánu ve smlouvě s obchodníkem. U vyplácení dividend dochází prakticky ke dvojímu zdanění – jednou na straně dané akciové společnosti - emitenta (dividenda se vyplácí ze zdaněného zisku), podruhé na straně investora, pokud není výnos osvobozen od daní (viz výše Daňové aspekty).";
                    Investments.Add(investment_3);

                    ListInvestments investment_4 = new ListInvestments();
                    investment_4.TitlePrefix = "4)";
                    investment_4.Title = "Otevřené podílové fondy";
                    investment_4.Advantage = " Nízké riziko nekorektního jednání ze strany správce fondu (investiční společnosti) - majetek podílového fondu je oddělen od majetku společnosti, která ho spravuje, podílový fond nemá právní subjektivitu; dohled nad investičními společnostmi vykonává stát. Investor se stává podílníkem fondu. Hodnota jeho podílu se vyjadřuje počtem podílových listů. Aktuální cenu podílového listu stanovuje investiční společnost nejčastěji na denní bázi a vypočítá se jako podíl celkového čistého obchodního jmění a počtu podílových listů. Profesionální správa (není třeba aktivně sledovat vývoj trhů) Vyšší výnosový potenciál oproti jiným finančním produktům (vklady v bankách apod.). Diverzifikace - investice do podílového fondu zajišťuje rozložení rizika, tedy minimalizaci výkyvů cen jednotlivých instrumentů. Vysoká likvidita investovaných prostředků - podílové listy lze kdykoli odprodat zpět investiční společnosti za aktuální tržní cenu. U některých fondů nízké minimální investiční částky.";
                    investment_4.Disadvantage = "Investice není pojištěna z titulu zákona. Výnos není až na výjimky garantován, hodnota může klesnout i pod úroveň původně investované částky. Není možné se aktivně podílet na sestavení portfolia v rámci konkrétního fondu. Vyšší poplatky u některých fondů - vstupní, výstupní a správní. U investic v cizí měně je nutné počítat s měnovým rizikem. Čím nižší je rizikovost a výkonnost fondu, tím větší pozornost je potřeba věnovat měnovému riziku.";
                    Investments.Add(investment_4);

                    ListInvestments investment_a = new ListInvestments();
                    investment_a.TitlePrefix = "a)";
                    investment_a.Title = "Akciové fondy";
                    investment_a.Advantage = "Na dlouhodobém investičním horizontu dosahují nejvyšších výnosů. Velký výběr fondů podle investičního zaměření, podle sektorů, regionů apod.";
                    investment_a.Disadvantage = "Akciové fondy mají obecně vysoké tržní riziko – kolísání hodnoty podílového listu. Investice nebo výnosy se mohou ocitnout i v záporných hodnotách. Investiční horizont musí být delší (doporučeno min. 5 let, raději více). Relativně vysoké vstupní a správní poplatky";
                    investment_a.Exception = "LetterItem";
                    Investments.Add(investment_a);

                    ListInvestments investment_b = new ListInvestments();
                    investment_b.TitlePrefix = "b)";
                    investment_b.Title = "Dluhopisové fondy";
                    investment_b.Advantage = "Výnosy jsou střední (vyšší než u fondů peněžního trhu). Menší riziko než akciové fondy. Investiční horizont střední (doporučeno 2 až 4 roky). Velký výběr fondů podle investičního zaměření, podle sektorů, regionů apod.";
                    investment_b.Disadvantage = "Dluhopisy představují vyšší riziko než investice na peněžním trhu. Investice či výnosy se mohou ocitnout i v záporných hodnotách. U některých fondů relativně vysoké vstupní poplatky.";
                    investment_b.Exception = "LetterItem";
                    Investments.Add(investment_b);

                    ListInvestments investment_c = new ListInvestments();
                    investment_c.TitlePrefix = "c)";
                    investment_c.Title = "Fondy peněžního trhu";
                    investment_c.Advantage = "Krátký investiční horizont (doporučeno 6 měsíců až 1 rok). Nízké riziko - pouze mírné kolísání hodnoty podílového listu. Nízké nebo nulové poplatky.";
                    investment_c.Disadvantage = "Výnosy jsou nízké, jen mírně překonávají inflaci. S ohledem na zdanění výnosů je doporučeno setrvat v investici min. 6 měsíců.";
                    investment_c.Exception = "LetterItem";
                    Investments.Add(investment_c);

                    ListInvestments investment_d = new ListInvestments();
                    investment_d.TitlePrefix = "d)";
                    investment_d.Title = "Smíšené fondy";
                    investment_d.Advantage = "Výnosy jsou střední, Investiční horizont střední až dlouhý (doporučeno 3 až 5 let). Smíšené fondy rozkládají riziko v rámci více tříd aktiv, čímž dochází ke snížení rizika. Mohou pružněji reagovat na změny na trhu. Při dlouhém investičním horizontu se uplatní jako alternativa ke spořícím programům.";
                    investment_d.Disadvantage = "Nelze zjistit přesné složení portfolia z hlediska tříd aktiv (portfolio smíšených fondů omezují pouze dané statuty). Relativně vysoké vstupní poplatky.";
                    investment_d.Exception = "LetterItem";
                    Investments.Add(investment_d);

                    ListInvestments investment_e = new ListInvestments();
                    investment_e.TitlePrefix = "e)";
                    investment_e.Title = "Fondy fondů";
                    investment_e.Advantage = "Fond fondů investuje do podílových listů jiných fondů a tím snižují celkové riziko. Nízké náklady na straně fondu přinášejí vyšší zhodnocení, celkový výnos závisí od investiční strategie a zaměření. Investiční horizont střední až dlouhý (doporučeno 3 a více let).";
                    investment_e.Disadvantage = "Nutnost seznámit se detailně s investiční strategií a statutem fondu (mezi jednotlivými fondy fondů mohou být značné rozdíly). Obtížně se odhaduje míra rizika investice do fondu. Obtížné porovnání s benchmarkem a srovnání jednotlivých fondů. Vyšší náklady na straně podílníka (zdvojení vstupních a správních poplatků).";
                    investment_e.Exception = "LetterItem";
                    Investments.Add(investment_e);

                    ListInvestments investment_f = new ListInvestments();
                    investment_f.TitlePrefix = "f)";
                    investment_f.Title = "Zajištěné fondy";
                    investment_f.Advantage = "Nejčastěji je zde garance návratnosti investované částky a určitého minimálního výnosu. Výnosy jsou zpravidla vyšší než u bankovních produktů.";
                    investment_f.Disadvantage = "Fondy jsou otevírány na přesnou dobu trvání fondu, celkový objem emise a jejich nákup je časově omezen. Investiční horizont je střední až dlouhý (3 až 6 let). Výnos se uvádí za celé období trvání fondu (často je stanoven i maximální výnos). Při dřívější výpovědi ztráta garance, příp. i sankční poplatky. Výnosy jsou obecně nižší než u fondů bez zajištění. Relativně vysoké vstupní a správní poplatky.";
                    investment_f.Exception = "LetterItem";
                    Investments.Add(investment_f);

                    ListInvestments investment_g = new ListInvestments();
                    investment_g.TitlePrefix = "g)";
                    investment_g.Title = "Nemovitostní fondy";
                    investment_g.Advantage = "Možnost podílet se na vývoji cen komodit bez návaznosti na vývoj cen akcií společností působících v daném sektoru. Nízká korelace s jinými druhy aktiv";
                    investment_g.Disadvantage = "Komodity nepřináší na rozdíl od dluhopisů či akcií žádné pravidelné výnosy, hodnota investice je tak plně závislá na vývoji ceny dané komodity. Na volatilitu má značný vliv i sezónnost. Rizika jsou zvýšena též způsobem investování, kdy fondy využívají derivátové nástroje.";
                    investment_g.Exception = "LetterItem";
                    Investments.Add(investment_g);

                    ListInvestments investment_h = new ListInvestments();
                    investment_h.TitlePrefix = "h)";
                    investment_h.Title = "Komoditní fondy";
                    investment_h.Advantage = "Možnost podílet se na vývoji cen komodit bez návaznosti na vývoj cen akcií společností působících v daném sektoru. Nízká korelace s jinými druhy aktiv.";
                    investment_h.Disadvantage = "Komodity nepřináší na rozdíl od dluhopisů či akcií žádné pravidelné výnosy, hodnota investice je tak plně závislá na vývoji ceny dané komodity. Na volatilitu má značný vliv i sezónnost. Rizika jsou zvýšena též způsobem investování, kdy fondy využívají derivátové nástroje.";
                    investment_h.Exception = "LetterItem";
                    Investments.Add(investment_h);

                    #endregion FillInvestments

                    #region WriteInvestments

                    foreach (var investment in Investments)
                    {
                        if (string.IsNullOrEmpty(investment.Exception))
                        {
                            tiouter.Add(new Table(tiouter, 1).With(t =>
                            {
                                if (!string.IsNullOrEmpty(investment.Title))
                                {
                                    t.Add(new Table(t, 1, 1, 40).With(tinner =>
                                    {
                                        tinner.Add(new Cell(tinner));
                                        tinner.Add(new Cell(tinner, investment.TitlePrefix, fLastPagesBold));
                                        tinner.Add(new Cell(tinner, investment.Title, fLastPagesBold));
                                    }));

                                    //t.Add(new Cell(t, investment.Title, fLastPagesBold)
                                    //	.AlignCenter());
                                }

                                if (!string.IsNullOrEmpty(investment.Advantage))
                                {
                                    t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                        .Add(new Chunk(investment.Advantage, fLastPagesNormal))
                                        .AlignJustify());
                                }

                                if (!string.IsNullOrEmpty(investment.Disadvantage))
                                {
                                    t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                        .Add(new Chunk(investment.Disadvantage, fLastPagesNormalItalic))
                                        .AlignJustify());
                                }
                                AddPaddingLine(t, paragraphPadding);

                            }));
                        }
                        else
                        {
                            switch (investment.Exception)
                            {
                                case "FirstItem":

                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        t.T.SplitLate = true;
                                        t.Add(new Cell(t, "Všeobecné informace o pojistných produktech (V: – Výhody, N: – Nevýhody)", fLastPagesBold)
                                            .AlignJustify());
                                        if (!string.IsNullOrEmpty(investment.Title))
                                        {
                                            t.Add(new Table(t, 1, 1, 40).With(tinner =>
                                            {
                                                tinner.Add(new Cell(tinner));
                                                tinner.Add(new Cell(tinner, investment.TitlePrefix, fLastPagesBold));
                                                tinner.Add(new Cell(tinner, investment.Title, fLastPagesBold));
                                            }));
                                        }

                                        if (!string.IsNullOrEmpty(investment.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(investment.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }
                                        t.T.SplitLate = false;

                                        if (!string.IsNullOrEmpty(investment.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(investment.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }

                                        AddPaddingLine(t, paragraphPadding);

                                    }));

                                    break;

                                case "LetterItem":

                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        if (!string.IsNullOrEmpty(investment.Title))
                                        {
                                            t.Add(new Table(t, 1.5, 1, 45).With(tinner =>
                                            {
                                                tinner.Add(new Cell(tinner));
                                                tinner.Add(new Cell(tinner, investment.TitlePrefix, fLastPagesBold));
                                                tinner.Add(new Cell(tinner, investment.Title, fLastPagesBold));
                                            }));
                                        }

                                        if (!string.IsNullOrEmpty(investment.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(investment.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }

                                        if (!string.IsNullOrEmpty(investment.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(investment.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }

                                        AddPaddingLine(t, paragraphPadding);

                                    }));

                                    break;

                                case "ListItem1":

                                    iTextSharp.text.List secList = new List(iTextSharp.text.List.UNORDERED, 10f);
                                    secList.SetListSymbol("\u25E6"); // white bullet
                                    secList.IndentationLeft = 10f;
                                    iTextSharp.text.ListItem secItem1 = new ListItem("Pro české OCP platí ustanovení o tzv. Garančním fondu OCP, které jsou obsaženy hlavně v zákoně č. 256/2004 Sb. (zákon o podnikání na kapitálovém trhu)", fLastPagesNormal);
                                    secItem1.Alignment = Element.ALIGN_JUSTIFIED;
                                    iTextSharp.text.ListItem secItem2 = new ListItem("Pro zahraniční OCP mohou platit i jiné garanční systémy, např. mohou vycházet ze zákona o bankách v příslušném státě nebo z jiných zákonných norem (podrobnosti viz obchodní podmínky daného OCP)", fLastPagesNormal);
                                    secItem2.Alignment = Element.ALIGN_JUSTIFIED;
                                    secList.Add(secItem1);
                                    secList.Add(secItem2);
                                    Cell secCell = new Cell();
                                    secCell.C.AddElement(secList);

                                    iTextSharp.text.List mainList = new List(iTextSharp.text.List.UNORDERED, 10f);
                                    mainList.SetListSymbol("\u2022"); // bullet
                                    iTextSharp.text.ListItem mainItem1 = new ListItem("U investic do podílových fondů spravovaných investiční společností neexistuje žádný garanční fond (nebo jiný podobný systém) ve smyslu pojištění hodnoty investovaného majetku zákazníka", fLastPagesNormal);
                                    mainItem1.Alignment = Element.ALIGN_JUSTIFIED;
                                    iTextSharp.text.ListItem mainItem2 = new ListItem("U investic do cenných papírů prostřednictvím obchodníka s cennými papíry (OCP) platí zákonná ustanovení o pojištění hodnoty investovaného majetku zákazníka, především pak pro případ, že OCP není schopen z důvodu své finanční situace plnit své závazky vůči majetku svých zákazníků, nebo soud vyhlásil na OCP konkurz. Pozor! V žádném případě se nejedná o pojištění standardních investičních rizik, jako např. tržní riziko, měnové riziko apod. (viz i níže bod 5)).", fLastPagesNormal);
                                    mainItem2.Alignment = Element.ALIGN_JUSTIFIED;
                                    mainList.Add(mainItem1);
                                    mainList.Add(mainItem2);
                                    mainList.Add(secList);
                                    Cell mainCell = new Cell();
                                    mainCell.C.AddElement(mainList);


                                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                                    {
                                        ti.Add(new Table(ti, 1).With(t =>
                                        {
                                            if (!string.IsNullOrEmpty(investment.Title))
                                            {
                                                t.Add(new Table(t, 1, 1, 40).With(tinner =>
                                                {
                                                    tinner.Add(new Cell(tinner));
                                                    tinner.Add(new Cell(tinner, investment.TitlePrefix, fLastPagesBold));
                                                    tinner.Add(new Cell(tinner, investment.Title, fLastPagesBold));
                                                }));
                                            }

                                            t.Add(mainCell);
                                            AddPaddingLine(t, paragraphPadding);
                                        }));
                                    }));

                                    break;
                                default:
                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        if (!string.IsNullOrEmpty(investment.Title))
                                        {
                                            t.Add(new Table(t, 1, 1, 40).With(tinner =>
                                            {
                                                tinner.Add(new Cell(tinner));
                                                tinner.Add(new Cell(tinner, investment.TitlePrefix, fLastPagesBold));
                                                tinner.Add(new Cell(tinner, investment.Title, fLastPagesBold));
                                            }));

                                            //t.Add(new Cell(t, investment.Title, fLastPagesBold)
                                            //	.AlignCenter());
                                        }

                                        if (!string.IsNullOrEmpty(investment.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(investment.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }

                                        if (!string.IsNullOrEmpty(investment.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(investment.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }
                                        AddPaddingLine(t, paragraphPadding);

                                    }));
                                    break;
                            }
                        }
                    }

                    #endregion WriteInvestments

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Table(t, 1, 1, 40).With(tinner =>
                        {
                            tinner.Add(new Cell(tinner));
                            tinner.Add(new Cell(tinner, "5)", fLastPagesBold));
                            tinner.Add(new Cell(tinner, "Všeobecné informace o možných rizicích, spojených s investičními nástroji", fLastPagesBold));
                        }));

                        t.Add(new Cell(t, "Upozorňujeme, že tyto informace nelze považovat za úplné a vyčerpávající poučení o všech aspektech spojených s riziky investování do Investičních nástrojů. Zákazník je povinen se v co největší míře seznámit s riziky jím zamýšlené investice, popř. jiné transakce, a v případě nejasností nebo otázek je povinen se podrobněji seznámit s jednotlivými riziky spojenými s investováním do investičních nástrojů.", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Tržní riziko" + space, fLastPagesBold)
                            .Add(new Chunk("= Pravděpodobnost změny tržní ceny investičního nástroje vlivem některého z tržních faktorů (úroková sazba, měnový kurs, cena podkladových aktiv, apod.) Vlivem tržního rizika může hodnota investice do investičních nástrojů stoupat či klesat a není zaručena návratnost investované částky. Minulé výnosy nejsou zárukou výnosů budoucích.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Měnové riziko" + space, fLastPagesBold)
                            .Add(new Chunk("= Investiční nástroje denominované v cizích měnách jsou vystaveny fluktuacím vyplývajícím ze změn devizových kurzů, které mohou mít jak pozitivní, tak i negativní vliv na jejich kurzy, ceny, zhodnocení či výnosy z nich plynoucí v jiných měnách, popřípadě jejich jiné parametry.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Riziko likvidity" + space, fLastPagesBold)
                            .Add(new Chunk("= Dostupnost nebo prodejnost investičních nástrojů se může lišit, a z toho důvodu může být obtížné koupit nebo prodat určitý investiční nástroj v souladu s paramentry pokynu. U investic do investičních nástrojů, které nejsou obchodovány na regulovaných trzích, je nutné počítat s rizikem, že kurz bude nízkou likviditou negativně ovlivněn nebo že daný investiční nástroj nebude možné ve zvoleném okamžiku prodat či koupit.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Kreditní riziko" + space, fLastPagesBold)
                            .Add(new Chunk("= Riziko, že emitent investičního nástroje nedostojí svým závazkům vůči vlastníkům těchto nástrojů. Týká se především dluhopisů a jim podobných investičních nástrojů, jako jsou např. zajištěné podílové fondy.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Úrokové riziko" + space, fLastPagesBold)
                            .Add(new Chunk("= Vyjadřuje pravděpodobnost změny tržní ceny investičního nástroje v závislosti na změně úrokových sazeb. Úrokovému riziku jsou vystaveny především obchody s dluhovými cennými papíry, jejichž cena se pohybuje nepřímo úměrně k pohybu úrokových sazeb a úrokové deriváty.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Riziko koncentrace" + space, fLastPagesBold)
                            .Add(new Chunk("= Riziko ztráty vyplývající z významné koncentrace expozic vůči protistranám nebo skupinám protistran, kde pravděpodobnost jejich selhání je ovlivněna společným faktorem rizika, např. protistranám podnikajícím ve stejném odvětví hospodářství či stejné zeměpisné oblasti, vykonávajícím stejnou činnost nebo obchodujícím se stejnou komoditou, nebo z používání technik snižování úvěrového rizika, zejména riziko spojené s velkou nepřímou angažovaností, např. vůči stejnému vydavateli kolaterálu.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Operační riziko" + space, fLastPagesBold)
                            .Add(new Chunk("= Spočívá v neočekávaných selháních tržní infrastruktury při obchodování s investičními nástroji, zejména ve včasném nebo řádném nedodání investičních nástrojů nebo finančních prostředků.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    AddPaddingLine(tiouter, paragraphPadding);
                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Právní riziko" + space, fLastPagesBold)
                            .Add(new Chunk("= Obchody s investičními nástroji realizované na zahraničních trzích mohou podléhat právním rizikům, která vyplývají z odlišné právní úpravy obchodování na finančním trhu a ochrany investora.", fLastPagesNormal))
                            .AlignJustify());
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 7-smlouva o investicnim zprostredkovani

                #region 8-prohlaseni zakaznika/partnera

                if (showItem8)
                {
                    tiouter.Style.BackgroundColor(0xffe5e5).PaddingLeft(-5).PaddingRight(-5);
                    bool headerShown = true;

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                        }));
                    }));

                    if (showItem8Part1)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VIII.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Prohlášení ke zprostředkování pojištění:" + space, fLastPagesBold)
                                    .Add(new Chunk("Své požadavky na pojištění jsem uvedl(a) pravdivě a na základě mých skutečných potřeb. Byl(a) jsem seznámen(a) se všemi podmínkami požadovaných pojištění, s jejich výlukami a omezeními a u pojistných produktů tvořící kapitálovou hodnotu mi byl objasněn princip tvorby kapitálové rezervy, včetně principu odkupní hodnoty. Příslušné sjednané pojistné Produkty mi byly řádně a jasně vysvětleny a jsou v souladu s mými potřebami a požadavky. Byl(a) jsem poučen(a), že u většiny pojištění je doporučeno v průběhu jejich trvání aktualizovat pojistné částky nebo rozsah krytí v závislosti na změnách mé ekonomické a životní situace (např.: inflace, zhodnocení majetku, změny v příjmech, změny rizikových kategorií atd.).", fLastPagesNormal))
                                    .AlignJustify());
                            }));
                        }));
                    }

                    if (showItem8Part2)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VIII.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Prohlášení ke zprostředkování spotřebitelských úvěrů:" + space, fLastPagesBold)
                                    .Add(new Chunk("Veškeré informace související se sjednáním spotřebitelského úvěru mi byly poskytnuty s dostatečným předstihem před uzavřením smlouvy o spotřebitelském úvěru prostřednictvím kopie návrhu smlouvy o spotřebitelském úvěru a dalších souvisejících dokumentů vydaných věřitelem.", fLastPagesNormal))
                                    .AlignJustify());
                            }));
                        }));
                    }

                    if (showItem8Part3)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VIII.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Prohlášení ke Smlouvě o investičním zprostředkování:" + space, fLastPagesBold)
                                    .Add(new Chunk("Byl(a) jsem poučen(a) o tom, že zodpovězení otázek uvedených v Investičního dotazníku je z mé strany dobrovolné. Zároveň jsem byl(a) poučen(a) o důsledcích vyplývajících z odpovědí nepravdivých nebo neúplných nebo z odmítnutí zodpovězení všech otázek Investičního dotazníku. Na základě těchto poučení jsem zvolil(a) jednu z možností uvedených v úvodu Investičního dotazníku. Potvrzuji svým podpisem pravdivost a úplnost informací uvedených v Investičním dotazníku (jestliže jsem jeho otázky zodpověděl nyní nebo v minulosti), jeho obsahu jsem porozuměl(a), zodpověděl(a) všechny jeho otázky v souladu se svým nejlepším vědomím a při jakékoli podstatné změně uvedených údajů o této změně bez zbytečného odkladu vyrozumím FINCENTRUM, resp. Spolupracovníka. Beru na vědomí, že pokud FINCENTRUM, resp. Spolupracovníka o tomto včas nevyrozumím, vystavuji se nebezpečí, že mi FINCENTRUM, resp. Spolupracovník poskytnou investiční službu, která pro mě není přiměřená. Byl(a) jsem seznámen(a) s možnými riziky, která obnášejí investice do cenných papírů a cenných papírů kolektivního investování, se způsobem odměňování FINCENTRA, resp. Spolupracovníka u příslušných investičních Produktů a se všemi příslušnými poplatky a náklady. O existenci, povaze a způsobu výpočtu odměny (pobídky) jsem byl(a) jasně, srozumitelně a úplně informován(a). Souhlasím s uvedenými Investičními pokyny. Příslušné sjednané investiční Produkty mi byly řádně a jasně vysvětleny a jsou v souladu s mými potřebami a požadavky.", fLastPagesNormal))
                                    .AlignJustify());
                            }));
                        }));
                    }

                    if (showItem8Part4)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VIII.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Prohlášení ke zprostředkování doplňkového penzijního spoření:" + space, fLastPagesBold)
                                    .Add(new Chunk("Byl(a) jsem poučen(a) o tom, že zodpovězení otázek uvedených v testu je z mé strany dobrovolné. Zároveň jsem byl(a) poučen(a) o důsledcích vyplývajících z odpovědí nepravdivých nebo neúplných nebo z odmítnutí zodpovězení všech otázek testu. Na základě těchto poučení jsem zvolil(a) strategii spoření uvedenou na druhé straně tohoto formuláře. Byl(a) jsem seznámen(a) s možnými riziky, která souvisí s doplňkovým penzijním spořením, Byl(a) jsem jasně, srozumitelně a úplně informován(a) o existenci, povaze a způsobu výpočtu odměny FINCENTRA, resp. Spolupracovníka, o všech nákladech, poplatcích a daňových aspektech. Příslušné sjednané investiční Produkty mi byly řádně a jasně vysvětleny a jsou v souladu s mými potřebami a požadavky. Od Spolupracovníka, resp. FINCENTRA jsem před zprostředkováním smlouvy DPS převzal(la) všechny podklady a materiály vybrané smluvní PS, včetně klíčových informací vybraných účastnických fondů.", fLastPagesNormal))
                                    .AlignJustify());
                            }));
                        }));
                    }

                    if (showItem8Part5)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VIII.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Ostatní prohlášení:" + space, fLastPagesBold)
                                    .Add(new Chunk("Byl(a) jsem seznámen(a) s účelem a rozsahem zpracování mých osobních údajů a se svými právy v případě, že by došlo k porušení zákonem vymezených povinností správcem nebo zpracovatelem těchto osobních údajů. Byl(a) jsem informován(a) o tom, že na všechny mnou uvedené informace se vztahuje ze strany FINCENTRA, resp. Spolupracovníka povinnost mlčenlivosti a další povinnosti dle v tomto formuláři zmíněných zákonných ustanovení.", fLastPagesNormal))
                                    .PaddingBottom(3)
                                    .AlignJustify());
                            }));
                        }));
                    }
                    AddPaddingLine(tiouter, separatingPadding);
                    tiouter.Style.BackgroundColor(null).PaddingLeft(0).PaddingRight(0);

                }

                #endregion 8-prohlaseni zakaznika/partnera

                #region 9-informace pro zakaznika/partnera

                if (showItem9)
                {

                    int part9LeftPadding = 5;
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "INFORMACE PRO ZÁKAZNÍKA/PARTNERA", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o možnosti mimosoudního řešení spotřebitelských sporů pro poskytované a zprostředkované služby Spolupracovníkem FINCENTRA v rozsahu stanoveném ust. § 14 zákona č. 634/1992 Sb., o ochraně spotřebitele ve znění pozdějších předpisů („Zákon“).", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazník/Partner je oprávněn se obrátit v případě nespokojenosti s poskytnutou či zprostředkovanou službou na příslušný orgán mimosoudního řešení spotřebitelských sporů, a to po vyřízení reklamace dle Reklamačního řádu FINCENTRA. Orgánem mimosoudního řešení spotřebitelských sporů ve smyslu Zákona je:", fLastPagesNormal)
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding * 2);
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part9LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part9LeftPadding);
                            t.Add(new Cell(t, "a)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "v oblasti finančních služeb zprostředkovaných FINCENTREM finanční arbitr, a to v rozsahu působnosti stanoveném právním předpisem upravujícím finančního arbitra (zák. č. 229/2002 Sb., o finančním arbitrovi, viz též webová adresa finančního arbitra ", fLastPagesNormal)
                                .Add(new Chunk("www.finarbitr.cz/cs/", fLastPagesRed))
                                .Add(new Chunk("),", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part9LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part9LeftPadding);
                            t.Add(new Cell(t, "b)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "v případech, kdy není dána působnost orgánu uvedeného v písmenu a) je pro jiné služby poskytované a zprostředkované FINCENTREM příslušná Česká obchodní inspekce (více informací k působnosti tohoto orgánu naleznete též na webové adrese ", fLastPagesNormal)
                                .Add(new Chunk("www.coi.cz.", fLastPagesRed))
                                .Add(new Chunk(").", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                }

                #endregion

            }));
            ResetTablePaddingVertical(table, innerTablePadding);
            AddPaddingLine(table, separatingPadding);
        }

        void RenderInformationAndAnnouncementZV(Table table)
        {
            //showItem3 = true;
            //showItem4 = true;

            SetTableZeroPaddingLeading(table);
            table.Add(new Table(table, 1).With(tiouter =>
            {
                tiouter.Add(new Cell(tiouter, "INFORMACE A POUČENÍ PRO ZÁKAZNÍKA A PROHLÁŠENÍ ZPROSTŘEDKOVATELŮ A ZÁKAZNÍKA", fTextBoldUnderline).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1).AlignCenter());

                //ResetTablePaddingVertical(tiouter, innerTablePadding);
                //AddPaddingLine(tiouter, separatingPadding);

                tiouter.Style.Leading(0, 1.65);
                tiouter.Style.PaddingTop(0).PaddingBottom(0.5);
                tiouter.T.SplitRows = true;
                tiouter.T.SplitLate = true;

                #region 1-Definice

                if (showItem1)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "DEFINICE", fTextMediumBold));
                        }));

                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM" + space, fLastPagesBold)
                                .Add(new Chunk("- obchodní jméno: Fincentrum a.s., adresa sídla: Pobřežní 620/3, 186 00 Praha 8, IČO: 24260444, zápis v OR: Městský soud v Praze, oddíl B, vložka 18458", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Spolupracovník" + space, fLastPagesBold)
                                .Add(new Chunk("– fyzická nebo právnická osoba, která pro FINCENTRUM vykonává činnosti na základě Smlouvy o spolupráci (jedná se především o zprostředkovatelskou činnost v oblasti finančních produktů a souvisejících služeb)", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazníkem" + space, fLastPagesBold)
                                .Add(new Chunk("se pro účely tohoto formuláře rozumí fyzická nebo právnická osoba, příp. zákonný zástupce osoby nebo opatrovník, pro kterou Spolupracovník (resp. FINCENTRUM) zprostředkovává smlouvy pro smluvní finanční instituce (viz" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk("), nebo jí poskytuje další související služby.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ČNB" + space, fLastPagesBold)
                                .Add(new Chunk("– Česká národní banka, Na Příkopě 28, 115 03 Praha 1, webová adresa ", fLastPagesNormal))
                                .Add(new Chunk("www.cnb.cz", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ČOI" + space, fLastPagesBold)
                                .Add(new Chunk("– Česká obchodní inspekce, Štěpánská 567/15, 120 00 Praha 2, webová adresa ", fLastPagesNormal))
                                .Add(new Chunk("www.coi.cz", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Daňovým rezidentem USA" + space, fLastPagesBold)
                                .Add(new Chunk("se pro účely tohoto formuláře rozumí fyzická nebo právnická osoba, která je daňovým rezidentem USA, nebo je u ní zjištěna jiná americká indície, např.: státní občanství USA, místo narození/registrace v USA, adresa/sídlo v USA, US telefon (předčíslí 001), pokyn k poukázání platby na US účet.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Finanční arbitr" + space, fLastPagesBold)
                                .Add(new Chunk("Kancelář Finančního arbitra, Legerova 1581/69, 110 00 Praha 1, webová adresa ", fLastPagesNormal))
                                .Add(new Chunk("www.finarbitr.cz/cs/", fLastPagesRed))
                                .Add(new Chunk(".", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Reklamační řád" + space, fLastPagesBold)
                                .Add(new Chunk("je dostupný na" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(". Postupy ohledně možnosti podání stížnosti případně o dalších právních možnostech jsou uvedeny v Reklamačním řádu FINCENTRA.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "NOZ" + space, fLastPagesBold)
                                .Add(new Chunk("– zákon č. 89/2012 Sb., občanský zákoník v platném znění", fLastPagesNormal))
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding * 2);
                    }));
                }

                #endregion 1-Definice

                #region 2-zpracovani os. udaju

                if (showItem2)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPRACOVÁNÍ OSOBNÍCH ÚDAJŮ", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM tímto v souladu se zákonem č. 101/2000 Sb., o ochraně osobních údajů (dále jen „Zákon“) oznamuje, že v souvislosti se zprostředkováním a uzavíráním smluv, jejichž obsahem jsou produkty finančních institucí nebo služby FINCENTRA, eviduje osobní údaje osob, které jeho prostřednictvím, nebo prostřednictvím jeho Spolupracovníků, nebo přímo s ním tyto smlouvy uzavřely nebo využily jeho služeb. Zákazník podpisem tohoto formuláře výslovně souhlasí s tím, že FINCENTRUM je oprávněno:", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "1. zpracovávat osobní údaje uvedené na tomto formuláři za účelem realizace smluv, jejich zprostředkování a uzavírání, a to i pokud je tato činnost prováděna prostřednictvím Spolupracovníků,", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "2. vytvářet databázi Zákazníků, a těmto případně nabízet další služby FINCENTRA.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazník byl seznámen se skutečnostmi, které konkrétní osobní údaje jsou relevantní pro uzavření předmětné smlouvy, a pokud jsou na tomto formuláři uvedeny také další osobní údaje pro uzavření předmětné smlouvy nikoliv nezbytné, Zákazník dává podpisem tohoto formuláře souhlas s jejich shromažďováním a zpracováním vzhledem k tomu, že veškeré uvedené osobní údaje jsou nezbytné k naplnění účelu vzájemné spolupráce při jednání o smluvním vztahu, respektive při zprostředkování a realizaci dalších smluv", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Za účelem realizace výše uvedených bodů bude zpracováváno jméno, příjmení, adresa bydliště, rodné číslo a případně další údaje nutné pro uzavření smluv. Rodné číslo je osobním údajem podléhajícím regulaci zákona č. 133/2000 Sb., o evidenci obyvatel a rodných čísel. FINCENTRUM je oprávněno ve smyslu ust. § 13c tohoto zákona zpracovávat rodná čísla na základě zvláštních zákonů (např. zák. č. 253/2008 Sb., o některých opatřeních proti legalizaci výnosů z trestné činnosti a financování terorismu, zák. č. 89/2012 Sb., občanský zákoník, a dalších) respektive na základě poskytnutého souhlasu nositele rodného čísla nebo jeho zákonného zástupce. Pokud součástí zprostředkovávaných smluv od finančních institucí budou dotazy na zdravotní stav a další podobné citlivé údaje, Zákazník podpisem tohoto formuláře výslovně souhlasí i se zpracováním těchto údajů.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Údaje budou spravovány a zpracovávány FINCENTREM minimálně po dobu trvání zprostředkovaných nebo uzavřených smluv a dále po dobu 10 let po jejich skončení.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazník má právo, zjistí-li, že došlo k porušení zákonem vymezených povinností správcem nebo zpracovatelem, obrátit se na Úřad pro ochranu osobních údajů s žádostí o zajištění opatření k nápravě protiprávního stavu. Došlo-li k porušení Zákonem vymezených povinností jak u správce, tak u zpracovatele, odpovídají za ně společně a nerozdílně. Subjekt údajů se může domáhat svých nároků u kteréhokoliv z nich.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 2-zpracovani os. udaju

                #region 3-opatreni proti legalizaci...

                if (showItem3)
                {
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "OPATŘENÍ PROTI LEGALIZACI VÝNOSŮ Z TRESTNÉ ČINNOSTI A FINANCOVÁNÍ TERORISMU", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM je při některých svých činnostech osobou povinnou ve smyslu zákona č. 253/2008 Sb., o některých opatřeních proti legalizaci výnosů z trestné činnosti a financování terorismu (dále jen „AML zákon“). FINCENTRUM v souladu s AML zákonem a systémem svých vnitřních AML předpisů plní své povinnosti v oblasti boje proti legalizaci výnosů z trestné činnosti a financování terorismu. Účelem naplňování těchto AML zásad a principů je minimalizovat resp. zabránit rizikům, jež vyplývají z možného zneužití společnosti FINCENTRUM k legalizaci výnosů z trestné činnosti nebo financování terorismu a rozpoznat tato rizika v rané fázi.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Před uzavřením obchodu musí být účastník obchodu prozkoumán z hlediska přijatelnosti pro FINCENTRUM provedením celkového ohodnocení, přičemž musí být vzata v úvahu veškerá Zákazníkem poskytnutá data. Pozadí a účel smlouvy musí být vyjasněn a dokumentován. Pokud se v průběhu kontroly přijatelnosti Zákazníka objeví nějaké podezření nebo pokud nastanou nějaké jiné podezřelé okolnosti, bude postupováno v souladu s platnou legislativou a vnitřními předpisy FINCENTRA. Obchody smí být prováděny pouze se Zákazníky, jejichž identita a finanční zdroje mohou být v rozumné míře prokázány.", fLastPagesNormal)
                                .AlignJustify());
                        }));

                    }));
                    tiouter.Add(new Table(tiouter, 1, 30).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "Politicky exponovanou osobou se ve smyslu ustanovení AML zákona rozumí:", fLastPagesBold)
                            .AlignJustify().PaddingTop(1.5).PaddingBottom(1.5));
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 29).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "a)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "fyzická osoba, která je ve významné veřejné funkci s celostátní působností, jako je například hlava státu nebo předseda vlády, ministr, náměstek nebo asistent ministra, člen parlamentu, člen nejvyššího soudu, ústavního soudu nebo jiného vyššího soudního orgánu, proti jehož rozhodnutí obecně až na výjimky nelze použít opravné prostředky, člen účetního dvora, člen vrcholného orgánu centrální banky, vysoký důstojník v ozbrojených silách nebo sborech, člen správního, řídícího nebo kontrolního orgánu obchodního závodu ve vlastnictví státu, velvyslanec nebo chargé d´affaires, nebo fyzická osoba, která obdobné funkce vykonává v orgánech Evropské unie nebo jiných mezinárodních organizací, a to po dobu výkonu této funkce a dále po dobu jednoho roku po ukončení výkonu této funkce, a která", fLastPagesNormal)
                            .AlignJustify());
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "i)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "má bydliště mimo Českou republiku, nebo", fLastPagesNormal)
                            .AlignJustify());

                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "ii)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "takovou významnou veřejnou funkci vykonává mimo Českou republiku,", fLastPagesNormal)
                            .AlignJustify());
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 29).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "b)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "fyzická osoba, která", fLastPagesNormal)
                            .AlignJustify());
                    }));
                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "i)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je k osobě uvedené v písmenu a) ve vztahu manželském, partnerském nebo v jiném obdobném vztahu nebo ve vztahu rodičovském,", fLastPagesNormal)
                            .AlignJustify());

                    }));

                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "ii)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je k osobě uvedené v písmenu a) ve vztahu syna nebo dcery nebo je k synovi nebo dceři osoby uvedené v písmenu a) osobou ve vztahu manželském (zeťové, snachy), partnerském nebo jiném obdobném vztahu,", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "iii)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je společníkem nebo skutečným majitelem stejné právnické osoby, popřípadě svěřenectví nebo jiného obdobného právního uspořádání podle cizího právního řádu jako osoba uvedená v písmenu a), nebo je o ni společnosti známo, že je v jakémkoli jiném blízkém podnikatelském vztahu s osobou uvedenou v písmeni a), nebo", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    tiouter.Add(new Table(tiouter, 1, 1, 1, 28).With(t =>
                    {
                        t.Add(new Cell(t));
                        t.Add(new Cell(t));
                        t.Add(new Cell(t, "iv)", fLastPagesNormal)
                            .AlignJustify());
                        t.Add(new Cell(t, "je skutečným majitelem právnické osoby, popřípadě svěřenectví nebo jiného obdobného právního uspořádání podle cizího právního řádu, o kterém je známo, že bylo vytvořeno ve prospěch osoby uvedené v písmenu a).", fLastPagesNormal)
                            .AlignJustify());
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 3-opatreni proti legalizaci...

                #region 4-zprostredkovani pojisteni

                if (showItem5)
                {
                    List<Insurances> Insurances = new List<Insurances>();
                    // naplneni listu pro sekci 5 - pro nasledne vzcteni pres foreach cyklus
                    #region InsuranceFill

                    Insurances generalInsurance = new Insurances();
                    generalInsurance.Title = "Pojištění obecně";
                    generalInsurance.Advantage = "Státní dozor, technické rezervy pojišťoven, pojišťovny jsou většinou pojištěny u zajišťoven. Pojistná ochrana je zpravidla účinná okamžikem uzavření smlouvy (kromě výslovně uvedených výluk).";
                    generalInsurance.Disadvantage = "Pojistné podmínky obsahují v rámci pojistné ochrany často množství výluk a omezení, kdy není pojišťovna povinna vyplatit pojistné plnění, příp. jen do určité výše.";
                    generalInsurance.Exception = "FirstItem";
                    Insurances.Add(generalInsurance);

                    Insurances personInsurance = new Insurances();
                    personInsurance.Title = "Pojištění osob";
                    personInsurance.Advantage = "Pojistné plnění v případě smrti pojištěného nepodléhá zdanění. U pojištění typu I. až IV. nemá pojišťovna právo v průběhu pojistné doby vypovědět pojistnou smlouvu z důvodu zhoršení zdravotního stavu pojištěného.";
                    personInsurance.Disadvantage = "U pojištění typu I. až IV. nejsou vklady pojištěny z titulu zákona. U pojištění typu I. až III. se zhodnocení pojistného formou technické úrokové míry a podíly na hospodaření pojišťovny vztahují pouze na tu část, která zbude po odečtu nákladů pojišťovny, jejího zisku a částky pro krytí sjednaných nebezpečí.";
                    Insurances.Add(personInsurance);

                    Insurances taxAspects = new Insurances();
                    taxAspects.Title = "Daňové aspekty";
                    taxAspects.Advantage = "Daňové úlevy je možné obecně uplatnit u pojištění typu I. až IV. Pojištěný si může odečíst své pojistné pro případ dožití až do výše 12 000 Kč ročně od daňového základu.";
                    taxAspects.Disadvantage = "Pro využití daňových výhod musí pojištění trvat min. 5 let a min. do věku 60 let, v případě nedodržení těchto podmínek je nutné již uplatněné odpočty zpětně dodanit. Pojištěný musí být zároveň pojistníkem. U některých typů pojištění musí pojistná částka pro dožití odpovídat zákonným ustanovením.";
                    Insurances.Add(taxAspects);

                    Insurances employerContribution = new Insurances();
                    employerContribution.Title = "Příspěvky zaměstnavatele";
                    employerContribution.Advantage = "Příspěvky zaměstnavatele je možné obecně uplatnit u pojištění typu I. až IV. Z příspěvků zaměstnavatele až do 30 000 Kč ročně neplatí pojištěný odvody na sociální a zdravotní pojištění ani daň z příjmu.";
                    Insurances.Add(employerContribution);

                    Insurances insurance1 = new Insurances();
                    insurance1.Title = "I. Kapitálové (smíšené) životní pojištění";
                    insurance1.Advantage = "Garance minimálního zhodnocení kapitálové hodnoty ve formě technické úrokové míry. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance1.Disadvantage = "Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. V některých případech neprůhlednost rozdělení pojistného na jednotlivé složky – skryté poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, nejčastěji až za 2 roky, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance1);

                    Insurances insurance2 = new Insurances();
                    insurance2.Title = "II. Důchodové pojištění";
                    insurance2.Advantage = "Garance minimálního zhodnocení kapitálové hodnoty ve formě technické úrokové míry. Vyšší efektivita zhodnocení (rizikové pojistné je nulové či minimální). U většiny pojišťoven se nezkoumá zdravotní stav. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance2.Disadvantage = "Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. Častá je neprůhlednost rozdělení pojistného na jednotlivé složky – skryté poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, nejčastěji až za 2 roky (u některých produktů za 1 rok), takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance2);

                    Insurances insurance3 = new Insurances();
                    insurance3.Title = "III. Flexibilní životní pojištění";
                    insurance3.Advantage = "Jedná se o kapitálové (smíšené) nebo investiční životní pojištění, které lze variabilně nastavovat v závislosti na aktuální životní situaci. Pojištění lze za daných podmínek přerušit (platební prázdniny), snížit/zvýšit pojistné a pojistnou částku, lze provádět částečné výběry apod. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí. Lepší průhlednost rozdělení pojistného na jednotlivé složky – poplatky, poměr pojistného alokovaného na pojistná nebezpeční a poměr pojistného alokovaného do kapitálové hodnoty apod. – je obecně vyšší než u kapitálových a důchodových pojištění.";
                    insurance3.Disadvantage = "Výnos bývá zpravidla nižší až střední, časový horizont většinou dlouhý. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, u dlouhodobých smluv obvykle po dvou letech, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance3);

                    Insurances insurance4 = new Insurances();
                    insurance4.Title = "IV. Investiční životní pojištění";
                    insurance4.Advantage = "Možnost dosáhnout vyššího zhodnocení než u ostatních typů životních pojištění. Flexibilita a likvidita v některých případech, možnost měnit parametry a možnost mimořádných výběrů z kapitálové hodnoty. Průhlednost - všechny poplatky jsou uvedeny. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance4.Disadvantage = "Výnos není garantován a ani není obecně zaručeno vyplacení vloženého pojistného. Časový horizont je zpravidla dlouhý (doporučeno min. 15 let). Často relativně vysoké náklady - rozdíl mezi nákupní a prodejní cenou, poplatky za sjednání, poplatky za vedení smlouvy, správu portfolia, za změny v pojistné ochraně apod. Kladná kapitálová hodnota se nevytváří ihned od počátku platnosti pojištění, u dlouhodobých smluv obvykle po dvou letech, takže v případě dřívější výpovědi je tzv. odkupné sníženo i o platby (mj. tzv. počáteční jednotky) v této lhůtě (do konce této lhůty není na odkupné žádný nárok).";
                    Insurances.Add(insurance4);

                    Insurances insurance5 = new Insurances();
                    insurance5.Title = "V. Rizikové životní pojištění";
                    insurance5.Advantage = "Relativně levná finanční ochrana pozůstalých v případě smrti pojištěného. Vhodné pro zajištění úvěrů. Možnost sjednání některých připojištění, která pojišťovny samostatně nenabízejí.";
                    insurance5.Disadvantage = "Není možné uplatnit daňové úlevy. Většina těchto pojištění neobsahuje kapitálovou hodnotu, na konci pojistné doby pojištění zaniká bez náhrady.";
                    Insurances.Add(insurance5);

                    Insurances insurance6 = new Insurances();
                    insurance6.Title = "VI. Úrazové pojištění";
                    insurance6.Advantage = "Obsahuje pojištění pro případ smrti nebo trvalých následků nebo kombinace obojího. Relativně nízké pojistné. Možno sjednat samostatně nebo jako připojištění k jiným produktům. U trvalých následků úrazu často pojišťovny nabízejí tzv. progresivní plnění, kdy v případě těžších následků plní pojišťovna násobky sjednaných základních částek.";
                    insurance6.Disadvantage = "Příčinou pojistné události může být pouze úraz, který způsobí smrt nebo trvalé následky – menší úrazy bez trvalých následků nejsou pojištěny. Téměř vždy se plní v procentech pojistné částky podle rozsahu zdravotního poškození (u progresivního plnění ze sjednaných násobků). Trvalé následky jsou obecně prokazatelné až po 1 roce po úrazu.";
                    Insurances.Add(insurance6);

                    Insurances insurance7 = new Insurances();
                    insurance7.Title = "VII. Pojištění denních dávek";
                    insurance7.Advantage = "Vyplácí se denní dávky za dobu nezbytného léčení následků úrazu nebo nemoci, včetně případné hospitalizace. Možno sjednat samostatně nebo jako připojištění k jiným produktům.";
                    insurance7.Disadvantage = "Tzv. karenční doba - nárok na plnění vzniká, jen pokud celková doba léčení je delší než min. doba léčení stanovená v pojistných podmínkách. Je omezen celkový počet dnů léčení (nejčastěji max. 365 dnů).";
                    Insurances.Add(insurance7);

                    Insurances insurance8 = new Insurances();
                    insurance8.Title = "VIII. Pojištění léčení tělesného poškození";
                    insurance8.Advantage = "Pojistné plnění je vypláceno v % za dobu nezbytného léčení následků úrazu nebo nemoci, včetně případné hospitalizace.";
                    insurance8.Disadvantage = "Většinou nelze sjednat samostatně, pouze jako připojištění k jiným produktům.";
                    Insurances.Add(insurance8);

                    Insurances insurance9 = new Insurances();
                    insurance9.Title = "IX. Pojištění trvalé (plné) invalidity";
                    insurance9.Advantage = "V zásadě existují dvě formy – jednorázová výplata sjednané pojistné částky, nebo tzv. zproštění od placení pojistného po dobu trvání plné invalidity. V prvním případě získá pojištěný ihned finanční prostředky pro řešení vzniklé situace, ve druhém případě pojišťovna sama platí pojistné max. do konce sjednané pojistné doby, aniž je tím dotčena výše pojistného plnění v případě pojistné události, nebo existence pojištění. ";
                    insurance9.Disadvantage = "Pro uplatnění nároků na pojistné plnění z pojištění je nutné mít přiznaný plný invalidní důchod ze státního systému sociálního zabezpečení. Předpisy pro přiznání plného invalidního důchodu jsou obecně velmi přísné. V případě, že pojistným nárokem je zproštění od placení pojistného, pak tento nárok trvá pouze po dobu čerpání plného invalidního důchodu. U některých pojišťoven je podmínkou pro přiznání tohoto nároku pouze invalidita, která vznikla následkem úrazu.";
                    Insurances.Add(insurance9);

                    Insurances insurance10 = new Insurances();
                    insurance10.Title = "X. Pojištění vážných chorob";
                    insurance10.Advantage = "Pojistné plnění se většinou vyplácí již při lékařské diagnóze dané choroby.";
                    insurance10.Disadvantage = "Omezené množství pojistitelných chorob, navíc velmi přesně definovaných. Tzv. karenční doba od uzavření smlouvy, po kterou se v případě diagnózy choroby neplní.";
                    Insurances.Add(insurance10);

                    Insurances responsibilityInsurance = new Insurances();
                    responsibilityInsurance.Title = "Pojištění odpovědnosti";
                    responsibilityInsurance.Advantage = "Krytí újmy (na životě, na zdraví, na věci, na zvířeti či z duševních útrap) způsobené jiným osobám v běžném občanském životě, v pracovní činnosti, v souvislosti s provozem dopravních prostředků, v souvislosti s vlastnictvím nemovitosti apod. V některých případech slevy na pojistném v případě bezškodného průběhu pojištění.";
                    responsibilityInsurance.Disadvantage = "Plní se do výše skutečně vzniklé škody, max. do výše pojistné částky. Výrazná omezení a výluky v případě způsobení škod úmyslných, pod vlivem alkoholu či použitím osoby s nebezpečnými vlastnostmi, apod.";
                    Insurances.Add(responsibilityInsurance);

                    #endregion InsuranceFill

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPROSTŘEDKOVÁNÍ POJIŠTĚNÍ", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace a prohlášení pojišťovacích zprostředkovatelů (PZ)", fLastPagesBold)
                                .AlignJustify());
                        }));

                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM vykonává zprostředkovatelskou činnost dle zákona č. 38/2004 Sb., o pojišťovacích zprostředkovatelích a samostatných likvidátorech pojistných (“ZPZ”), § 21, odst. 6, písm. c), tudíž není při zprostředkovatelské činnosti povinno poskytovat řádnou analýzu ve smyslu § 7 ZPZ, a jeho registrační číslo je: PZ: 167194PA. Spolupracovník vykonává zprostředkovatelskou činnost taktéž dle § 21, odst. 6, písm. c) ZPZ a jeho registrační číslo je uvedeno na druhé straně tohoto formuláře. Registrační čísla PZ jsou evidována a zveřejněna u ČNB (", fLastPagesNormal)
                                .Add(new Chunk("www.cnb.cz", fLastPagesRed))
                                .Add(new Chunk(").", fLastPagesNormal))
                                .AlignJustify());

                            t.Add(new Cell(t, "PZ prohlašují, že nemají žádný přímý nebo nepřímý podíl na hlasovacích právech a kapitálu pojišťovny, se kterou má být pojištění sjednáno a že pojišťovna, se kterou má být pojištění sjednáno, nebo osoba ovládající danou pojišťovnu nemá žádný přímý nebo nepřímý podíl na hlasovacích právech a kapitálu PZ. PZ jsou na žádost Zákazníka povinni sdělit způsob svého odměňování a pojišťovny, se kterými jsou oprávněni sjednávat pojištění. Případné stížnosti je možné podávat na adrese sídla FINCENTRA nebo na e-mail:" + space, fLastPagesNormal)
                                .Add(new Chunk("reklamace@fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(". Veškeré další postupy ohledně možnosti podání stížnosti případně o dalších právních možnostech jsou uvedeny v Reklamačním řádu FINCENTRA, který je dostupný na" + space, fLastPagesNormal))
                                .Add(new Chunk("www.fincentrum.com", fLastPagesRed))
                                .Add(new Chunk(". Zákazník prohlašuje, že byl s těmito postupy seznámen.", fLastPagesNormal))
                                .AlignJustify());
                        }));

                    }));

                    foreach (var insurance in Insurances)
                    {
                        if (string.IsNullOrEmpty(insurance.Exception))
                        {
                            tiouter.Add(new Table(tiouter, 1).With(t =>
                            {
                                t.T.SplitLate = true;
                                if (!string.IsNullOrEmpty(insurance.Title))
                                {
                                    t.Add(new Cell(t, insurance.Title, fLastPagesBold)
                                        .AlignCenter());
                                }

                                if (!string.IsNullOrEmpty(insurance.Advantage))
                                {
                                    t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                        .Add(new Chunk(insurance.Advantage, fLastPagesNormal))
                                        .AlignJustify());
                                }
                                t.T.SplitLate = false;

                                if (!string.IsNullOrEmpty(insurance.Disadvantage))
                                {
                                    t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                        .Add(new Chunk(insurance.Disadvantage, fLastPagesNormalItalic))
                                        .AlignJustify());
                                }
                                AddPaddingLine(t, paragraphPadding);

                            }));
                        }
                        else
                        {
                            switch (insurance.Exception)
                            {
                                case "FirstItem":

                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        t.Add(new Cell(t, "Všeobecné informace o pojistných produktech (V: – Výhody, N: – Nevýhody)", fLastPagesBold)
                                            .AlignJustify());
                                        if (!string.IsNullOrEmpty(insurance.Title))
                                        {
                                            t.Add(new Cell(t, insurance.Title, fLastPagesBold)
                                                .AlignCenter());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(insurance.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(insurance.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }

                                        AddPaddingLine(t, paragraphPadding);

                                    }));

                                    break;
                                default:
                                    tiouter.Add(new Table(tiouter, 1).With(t =>
                                    {
                                        if (!string.IsNullOrEmpty(insurance.Title))
                                        {
                                            t.Add(new Cell(t, insurance.Title, fLastPagesBold)
                                                .AlignCenter());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Advantage))
                                        {
                                            t.Add(new Cell(t, "V:" + space, fLastPagesBold)
                                                .Add(new Chunk(insurance.Advantage, fLastPagesNormal))
                                                .AlignJustify());
                                        }

                                        if (!string.IsNullOrEmpty(insurance.Disadvantage))
                                        {
                                            t.Add(new Cell(t, "N:" + space, fLastPagesBoldItalic)
                                                .Add(new Chunk(insurance.Disadvantage, fLastPagesNormalItalic))
                                                .AlignJustify());
                                        }
                                        AddPaddingLine(t, paragraphPadding);

                                    }));
                                    break;
                            }
                        }
                    }

                    //AddPaddingLine(tiouter, separatingPadding);

                    tiouter.Add(new Table(tiouter, 1).With(t =>
                    {
                        t.Add(new Cell(t, "Důležité upozornění: u většiny pojištění doporučujeme v průběhu jejich trvání aktualizovat pojistné částky nebo rozsah krytí v závislosti na změnách ekonomických a životních situací (inflace, zhodnocení majetku, změny v příjmech, změny rizikových kategorií atd.).", fLastPagesNormal)
                            .AlignJustify());
                    }));


                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 4-zprostredkovani pojisteni

                #region 5-zprostredkovani duchodoveho sporeni...

                if (showItem6)
                {
                    int part6LeftPadding = 0;

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "ZPROSTŘEDKOVÁNÍ DOPLŇKOVÉHO PENZIJNÍHO SPOŘENÍ", fTextMediumBold));
                        }));

                        ti.Style.PaddingLeft(part6LeftPadding);

                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o osobách oprávněných zprostředkovávat DPS", fLastPagesBold)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "FINCENTRUM, resp. Spolupracovník prohlašují, že jsou oprávněni zprostředkovávat uzavření smluv doplňkového penzijního spoření (dále jen „DPS“) ve smyslu zákona č. 427/2011 Sb. (dále jen „Zákon o DPS“) pro smluvní penzijní společnosti (dále jen „smluvní PS“). FINCENTRUM poskytuje toto zprostředkování na základě registrace u ČNB jako investiční zprostředkovatel, a Spolupracovník na základě registrace u ČNB jako vázaný zástupce FINCENTRA.", fLastPagesNormal)
                                .AlignJustify());
                        }));

                    }));

                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "UPOZORNĚNÍ FINCENTRA:", fLastPagesBold)
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Add(new Cell(t, "a)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "V rámci zprostředkování DPS není poskytováno penzijní doporučení.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "b)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Pokud některá smluvní PS provádí srovnání s jinými, nebo jiných PS z hlediska strategií spoření, způsobu investování účastnických fondů, návrhů smluv, údajů o minulých a očekávaných výnosech u DPS, má Zákazník právo vyžádat si tyto informace od Spolupracovníka, resp. FINCENTRA, přičemž jsou mu tyto předány na materiálech příslušné smluvní PS. FINCENTRUM takováto srovnání pro Zákazníka neprovádí.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "c)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Podrobné informace ve smyslu § 133 Zákona o DPS jsou Zákazníkovi předávány Spolupracovníkem, resp. FINCENTREM na materiálech konkrétních smluvních PS.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "d)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Klíčové informace o konkrétním účastnickém fondu ve smyslu § 133 Zákona o DPS jsou Zákazníkovi předávány Spolupracovníkem, resp. FINCENTREM na materiálech konkrétních smluvních PS.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1, 1, 28).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t));
                            t.Add(new Cell(t, "e)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Sdělení klíčových informací musí být Zákazníkovi poskytnuto na trvalém nosiči informací a uveřejněno na internetových stránkách každé smluvní PS, která daný účastnický fond obhospodařuje.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1, 1, 28).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t));
                            t.Add(new Cell(t, "f)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "Každý účastník má právo si bezúplatně vyžádat statut daného účastnického fondu a sdělení klíčových informací v listinné podobě.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Pravidla střetu zájmů", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Ve vztahu mezi FINCENTREM, resp. Spolupracovníkem a Zákazníkem dochází ke střetu zájmů ve smyslu Zákona. Střet zájmů je dán především způsobem odměňování FINCENTRA, resp. Spolupracovníka. Podrobnosti o způsobech výpočtu odměn za zprostředkování DPS FINCENTRUM, resp. Spolupracovník povinni Zákazníkovi objasnit.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Informace o nákladech, poplatcích a pobídkách", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "FINCENTRUM, resp. Spolupracovník jsou povinni informovat Zákazníka o všech příslušných poplatcích a nákladech spojených se zprostředkováním DPS (zejména odměny, pobídky, poplatky, daně apod.). Příslušné informace od smluvních PS jsou uvedeny v jejich písemných materiálech (např. obchodní podmínky, klíčové informace atd.), které předává Zákazníkovi Spolupracovník, resp. FINCENTRUM, nebo na internetových stránkách smluvních PS. O existenci, povaze a způsobu výpočtu odměny, resp. pobídky musí být Zákazník jasně, srozumitelně a úplně informován, přičemž tuto informaci obdrží Zákazník buď přímo od Spolupracovníka, nebo od FINCENTRA na základě dotazu na e-mail nebo na telefon – obojí viz zápatí tohoto formuláře. FINCENTRUM, resp. Spolupracovník prohlašují, že pobídky Zákazníkům neposkytují ani pobídky od nich nepřijímají, s výhradou odměny za zprostředkování smluv o DPS přijímané od smluvních PS.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Informace a poučení k testu investičního zaměření Zákazníka", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Zodpovězení otázek testu Zákazníkem umožní zjistit doporučený profil a vhodnou strategii spoření v DPS, která odpovídá jeho cílům, odborným znalostem a zkušenostem potřebným pro pochopení souvisejících rizik. V souladu se Zákonem o DPS jsou FINCENTRUM, resp. Spolupracovník povinni předložit tento test Zákazníkovi k vyplnění, přičemž vyplnění je ze strany Zákazníka dobrovolné.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Upozornění k vyhodnocení testu: a) V případě, že Zákazník si po vyplnění testu vybere jinou, než doporučenou strategii spoření, vystavuje se riziku nevhodného rozložení investice; b) V případě, že Zákazník zodpoví otázky neúplně, nepřesně nebo nepravdivě, nebude možné správně vyhodnotit doporučený profil a vhodnou strategii spoření a Zákazník se tak vystavuje riziku nevhodného rozložení investice; c) V případě, že Zákazník odmítne test vyplnit, vystavuje se riziku nevhodného rozložení investice, přičemž na něj bude pohlíženo jako na osobu s konzervativním profilem; d) V případě vzniku situací podle bodů a), b) nebo c) výše bude Spolupracovník, resp. FINCENTRUM postupovat podle rozhodnutí/volby Zákazníka.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Jestliže vyhodnocením testu vyjde Zákazníkovi jiný než konzervativní profil, FINCENTRUM při výběru konkrétní strategie odkazuje Zákazníka na podmínky konkrétní vybrané smluvní PS. Spolupracovník, resp. FINCENTRUM bude přitom postupovat podle rozhodnutí/volby Zákazníka.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    AddPaddingLine(tiouter, paragraphPadding);

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Všeobecné informace o doplňkovém penzijním spoření", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "1) III. pilíř – doplňkové penzijní spoření", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Jedná se o možnost zvýšení svého důchodového zabezpečení formou dobrovolného spoření prostřednictvím vlastních příspěvků. Příspěvky jsou zvýhodněny státními příspěvky, daňovými úlevami; účastníkovi může přispívat i zaměstnavatel. Účastník má právo opakovaně měnit penzijní společnost, výši příspěvků i zvolenou strategii spoření. Smlouvu o DPS je možné předčasně ukončit (tzv. odbytné), přičemž ovšem zaniká nárok na výplatu státních příspěvků a v případě, že účastník čerpal daňové úlevy, je povinen je zpětně dodanit. Po dosažení důchodového věku má účastník možnost výplaty svých úspor formou penze na dobu určitou (vyplácí penzijní společnost nebo pojišťovna, kterou si účastník zvolí) nebo formou doživotní penze (vyplácí pojišťovna, kterou si účastník zvolí).", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "3) III. pilíř – transformovaný fond (dříve penzijní připojištění)", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Jedná se o fond spravovaný penzijní společností, jehož prostřednictvím penzijní společnost spravuje penzijní připojištění. Do transformovaného fondu nemohou vstupovat noví účastníci s výjimkou převodu prostředků ze zrušeného penzijního fondu, sloučení transformovaných fondů a převodu prostředků účastníků zanikajícího transformovaného fondu jiné penzijní společnosti. Práva a povinnosti účastníka v transformovaném fondu a příjemce dávky penzijního připojištění z transformovaného fondu se řídí zákonem o penzijním připojištění, sjednaným penzijním plánem a smlouvou o penzijním připojištění. Jejich nároky zůstávají s výjimkou omezení nároku na převod prostředků k jinému fondu zachovány. Nelze být zároveň účastníkem doplňkového penzijního spoření a transformovaného fondu. Převedení prostředků účastníka z transformovaného fondu je jinak možné pouze do účastnických fondů téže penzijní společnosti na základě uzavření smlouvy o doplňkovém penzijním spoření a je bezplatné.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Všeobecné informace o možných rizicích spojených s investičními nástroji", fLastPagesBold)
                                .AlignJustify());
                            t.Add(new Cell(t, "Upozorňujeme, že tyto informace nelze považovat za úplné a vyčerpávající poučení o všech aspektech spojených s riziky investování do Investičních nástrojů. Zákazník je povinen se v co největší míře seznámit s riziky jím zamýšlené investice, popř. jiné transakce, a v případě nejasností nebo otázek je povinen se podrobněji seznámit s jednotlivými riziky spojenými s investováním do investičních nástrojů.", fLastPagesNormal)
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Tržní riziko", fLastPagesBold)
                                .Add(new Chunk(" = Pravděpodobnost změny tržní ceny investičního nástroje vlivem některého z tržních faktorů (úroková sazba, měnový kurs, cena podkladových aktiv, apod.) Vlivem tržního rizika může hodnota investice do investičních nástrojů stoupat či klesat a není zaručena návratnost investované částky. Minulé výnosy nejsou zárukou výnosů budoucích.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Měnové riziko", fLastPagesBold)
                                .Add(new Chunk(" = Investiční nástroje denominované v cizích měnách jsou vystaveny fluktuacím vyplývajícím ze změn devizových kurzů, které mohou mít jak pozitivní, tak i negativní vliv na jejich kurzy, ceny, zhodnocení či výnosy z nich plynoucí v jiných měnách, popřípadě jejich jiné parametry.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Riziko likvidity", fLastPagesBold)
                                .Add(new Chunk(" = Investiční nástroje denominované v cizích měnách jsou vystaveny fluktuacím vyplývajícím ze změn devizových kurzů, které mohou mít jak pozitivní, tak i negativní vliv na jejich kurzy, ceny, zhodnocení či výnosy z nich plynoucí v jiných měnách, popřípadě jejich Dostupnost nebo prodejnost investičních nástrojů se může lišit, a z toho důvodu může být obtížné koupit nebo prodat určitý investiční nástroj v souladu s paramentry pokynu. U investic do investičních nástrojů, které nejsou obchodovány na regulovaných trzích, je nutné počítat s rizikem, že kurz bude nízkou likviditou negativně ovlivněn nebo že daný investiční nástroj nebude možné ve zvoleném okamžiku prodat či koupit. Toto neplatí pro investice do podílových listů otevřených podílových fondů.jiné parametry.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Kreditní riziko", fLastPagesBold)
                                .Add(new Chunk(" = Riziko, že emitent investičního nástroje nedostojí svým závazkům vůči vlastníkům těchto nástrojů. Týká se především dluhopisů a jim podobných investičních nástrojů, jako jsou např. zajištěné podílové fondy.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Úrokové riziko", fLastPagesBold)
                                .Add(new Chunk(" = Vyjadřuje pravděpodobnost změny tržní ceny investičního nástroje v závislosti na změně úrokových sazeb. Úrokovému riziku jsou vystaveny především obchody s dluhovými cennými papíry, jejichž cena se pohybuje nepřímo úměrně k pohybu úrokových sazeb a úrokové deriváty.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Riziko koncentrace", fLastPagesBold)
                                .Add(new Chunk(" = Riziko ztráty vyplývající z významné koncentrace expozic vůči protistranám nebo skupinám protistran, kde pravděpodobnost jejich selhání je ovlivněna společným faktorem rizika, např. protistranám podnikajícím ve stejném odvětví hospodářství či stejné zeměpisné oblasti, vykonávajícím stejnou činnost nebo obchodujícím se stejnou komoditou, nebo z používání technik snižování úvěrového rizika, zejména riziko spojené s velkou nepřímou angažovaností, např. vůči stejnému vydavateli kolaterálu.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Operační riziko", fLastPagesBold)
                                .Add(new Chunk(" = Spočívá v neočekávaných selháních tržní infrastruktury při obchodování s investičními nástroji, zejména ve včasném nebo řádném nedodání investičních nástrojů nebo finančních prostředků.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part6LeftPadding);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Style.PaddingLeft(part6LeftPadding);
                            t.Add(new Cell(t, "Právní riziko", fLastPagesBold)
                                .Add(new Chunk(" = Obchody s investičními nástroji realizované na zahraničních trzích mohou podléhat právním rizikům, která vyplývají z odlišné právní úpravy obchodování na finančním trhu a ochrany investora.", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));

                    AddPaddingLine(tiouter, separatingPadding * 2);
                }

                #endregion 6-zprostredkovani duchodoveho sporeni...

                #region 6-prohlaseni zakaznika/partnera

                if (true)
                {
                    tiouter.Style.BackgroundColor(0xffe5e5).PaddingLeft(-5).PaddingRight(-5);
                    bool headerShown = true;

                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA", fTextMediumBold));
                        }));
                    }));

                    if (showItem8Part1)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VI.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Prohlášení ke zprostředkování pojištění:" + space, fLastPagesBold)
                                    .Add(new Chunk("Své požadavky na pojištění jsem uvedl(a) pravdivě a na základě mých skutečných potřeb. Byl(a) jsem seznámen(a) se všemi podmínkami požadovaných pojištění, s jejich výlukami a omezeními a u pojistných produktů tvořící kapitálovou hodnotu mi byl objasněn princip tvorby kapitálové rezervy, včetně principu odkupní hodnoty. Příslušné sjednané pojistné Produkty mi byly řádně a jasně vysvětleny a jsou v souladu s mými potřebami a požadavky. Byl(a) jsem poučen( a), že u většiny pojištění je doporučeno v průběhu jejich trvání aktualizovat pojistné částky nebo rozsah krytí v závislosti na změnách mé ekonomické a životní situace (např.: inflace, zhodnocení majetku, změny v příjmech, změny rizikových kategorií atd.).", fLastPagesNormal))
                                    .AlignJustify());
                            }));
                        }));
                    }

                    if (showItem8Part4)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VI.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Prohlášení ke zprostředkování doplňkového penzijního spoření:" + space, fLastPagesBold)
                                    .Add(new Chunk("Byl(a) jsem poučen(a) o tom, že zodpovězení otázek uvedených v testu je z mé strany dobrovolné. Zároveň jsem byl(a) poučen(a) o důsledcích vyplývajících z odpovědí nepravdivých nebo neúplných nebo z odmítnutí zodpovězení všech otázek testu. Na základě těchto poučení jsem zvolil(a) strategii spoření uvedenou na druhé straně tohoto formuláře. Byl(a) jsem seznámen(a) s možnými riziky, která souvisí s doplňkovým penzijním spořením. Byl(a) jsem jasně, srozumitelně a úplně informován(a) o existenci, povaze a způsobu výpočtu odměny FINCENTRA, resp. Spolupracovníka, o všech nákladech, poplatcích a daňových aspektech. Příslušné sjednané investiční Produkty mi byly řádně a jasně vysvětleny a jsou v souladu s mými potřebami a požadavky. Od Spolupracovníka, resp. FINCENTRA jsem před zprostředkováním smlouvy DPS převzal(la) všechny podklady a materiály vybrané smluvní PS, včetně klíčových informací vybraných účastnických fondů.", fLastPagesNormal))
                                    .AlignJustify());
                            }));
                        }));
                    }

                    if (showItem8Part5)
                    {
                        tiouter.Add(new Table(tiouter, 1).With(ti =>
                        {
                            if (!headerShown)
                            {
                                ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                                ti.Add(new Table(ti, 1, 15).With(t =>
                                {
                                    t.Add(new Cell(t, "VI.", fTextMediumBold).PaddingBottom(2));
                                    t.Add(new Cell(t, "PROHLÁŠENÍ ZÁKAZNÍKA", fTextMediumBold));
                                }));
                                headerShown = true;
                            }

                            ti.Style.PaddingLeft(2.5).PaddingRight(2.5);
                            ti.Add(new Table(ti, 1).With(t =>
                            {
                                t.Add(new Cell(t, "Ostatní prohlášení:" + space, fLastPagesBold)
                                    .Add(new Chunk("Byl(a) jsem seznámen(a) s účelem a rozsahem zpracování mých osobních údajů a se svými právy v případě, že by došlo k porušení zákonem vymezených povinností správcem nebo zpracovatelem těchto osobních údajů. Byl(a) jsem informován(a) o tom, že na všechny mnou uvedené informace se vztahuje ze strany FINCENTRA, resp. Spolupracovníka povinnost mlčenlivosti a další povinnosti dle v tomto formuláři zmíněných zákonných ustanovení.", fLastPagesNormal))
                                    .PaddingBottom(3)
                                    .AlignJustify());
                            }));
                        }));
                    }
                    AddPaddingLine(tiouter, separatingPadding);
                    tiouter.Style.BackgroundColor(null).PaddingLeft(0).PaddingRight(0);

                }

                #endregion 6-prohlaseni zakaznika/partnera

                #region 7-informace pro zakaznika/partnera

                if (showItem9)
                {

                    int part9LeftPadding = 5;
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "INFORMACE PRO ZÁKAZNÍKA", fTextMediumBold));
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Informace o možnosti mimosoudního řešení spotřebitelských sporů pro poskytované a zprostředkované služby Spolupracovníkem FINCENTRA v rozsahu stanoveném ust. § 14 zákona č. 634/1992 Sb., o ochraně spotřebitele ve znění pozdějších předpisů („Zákon“).", fLastPagesNormal)
                                .AlignJustify());
                        }));
                        ti.Add(new Table(ti, 1).With(t =>
                        {
                            t.Add(new Cell(t, "Zákazník je oprávněn se obrátit v případě nespokojenosti s poskytnutou či zprostředkovanou službou na příslušný orgán mimosoudního řešení spotřebitelských sporů, a to po vyřízení reklamace dle Reklamačního řádu FINCENTRA. Orgánem mimosoudního řešení spotřebitelských sporů ve smyslu Zákona je:", fLastPagesNormal)
                                .AlignJustify());
                        }));

                        AddPaddingLine(ti, separatingPadding * 2);
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part9LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part9LeftPadding);
                            t.Add(new Cell(t, "a)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "v oblasti finančních služeb zprostředkovaných FINCENTREM finanční arbitr, a to v rozsahu působnosti stanoveném právním předpisem upravujícím finančního arbitra (zák. č. 229/2002 Sb., o finančním arbitrovi, viz též webová adresa finančního arbitra ", fLastPagesNormal)
                                .Add(new Chunk("www.finarbitr.cz/cs/", fLastPagesRed))
                                .Add(new Chunk("),", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                    tiouter.Add(new Table(tiouter, 1).With(ti =>
                    {
                        ti.Style.PaddingLeft(part9LeftPadding);
                        ti.Add(new Table(ti, 1, 29).With(t =>
                        {
                            t.Style.PaddingLeft(part9LeftPadding);
                            t.Add(new Cell(t, "b)", fLastPagesNormal)
                                .AlignJustify());
                            t.Add(new Cell(t, "v případech, kdy není dána působnost orgánu uvedeného v písmenu a) je pro jiné služby poskytované a zprostředkované FINCENTREM příslušná Česká obchodní inspekce (více informací k působnosti tohoto orgánu naleznete též na webové adrese ", fLastPagesNormal)
                                .Add(new Chunk("www.coi.cz.", fLastPagesRed))
                                .Add(new Chunk(").", fLastPagesNormal))
                                .AlignJustify());
                        }));
                    }));
                }

                #endregion
            }));
            ResetTablePaddingVertical(table, innerTablePadding);
            AddPaddingLine(table, separatingPadding);
        }

        // ---------------------------------------------------------------------
        #region helperFunctions

        void AddSection6AHeader(Table t)
        {
            if (FusZV)
                AddSectionSubHeader(t, App_GlobalResources.FusCZ.S6SectionAHeaderZV);
            else
                AddSectionSubHeader(t, App_GlobalResources.FusCZ.S6SectionAHeader);

            t.Add(new Cell(t, App_GlobalResources.FusCZ.S6InsuranceType, fTextBold).Colspan(1).Leading(1, 1).Padding(2));
        }

        private string GetInsuranceRequestNum(SectorInsurance insurance)
        {
            List<string> requests = new List<string>();

            if (!FusZV)
            {
                if (insurance.InsuranceChoices.InsuranceIndividual) requests.Add("1");
                if (insurance.InsuranceChoices.InsuranceProperty) requests.Add("2");
                if (insurance.InsuranceChoices.InsuranceDamage) requests.Add("3");
                if (insurance.InsuranceChoices.InsuranceTraveling) requests.Add("4");
                if ((insurance.InsuranceChoices.InsuranceOtherDetail.Client)
                    || (insurance.InsuranceChoices.InsuranceOtherDetail.Partner)
                    || (insurance.InsuranceChoices.InsuranceOtherDetail.Child))
                {
                    requests.Add("5");
                }
            }
            else
            {
                if (insurance.InsuranceChoices.InsuranceIndividual) requests.Add("1");
                if (insurance.InsuranceChoices.InsuranceDamage) requests.Add("2");
                if (insurance.InsuranceChoices.InsuranceOtherDetail.Client)
                {
                    requests.Add("3");
                }
            }

            return String.Join(", ", requests.ToArray());
        }

        private string GetPeriodText(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            switch (code)
            {
                case Constants.FUSListOptions.AMONTHLY:
                    return App_GlobalResources.FusCZ.S7Monthly;
                case Constants.FUSListOptions.AQAERTERLY:
                    return App_GlobalResources.FusCZ.S7Quaterly;
                case Constants.FUSListOptions.AHALFYEAR:
                    return App_GlobalResources.FusCZ.S7HalfYear;
                case Constants.FUSListOptions.AYEAR:
                    return App_GlobalResources.FusCZ.S7Year;
                case Constants.FUSListOptions.AONCE:
                    return "J";
                case Constants.FUSListOptions.AFINAL:
                    return "CČ";
                case Constants.FUSListOptions.AHYPO:
                    return "VU";
                default: return null;
            }
        }

        string GetProduct(int? id, out int categoryID)
        {
            categoryID = -1;

            if (id == null)
                return string.Empty;

            var product = DbProvider.Services().Where(p => p.ServiceID == id).FirstOrDefault();
            if (product == null) return string.Empty;

            categoryID = product.CategoryID;

            if (string.IsNullOrWhiteSpace(product.AltName))
            {
                return string.Empty;
            }

            return product.AltName;
        }

        string GetInstitution(int? id, List<IEnumerable<Sector>> childSectors = null)
        {
            PartnerSearch institution = null;

            List<string> childInstitutions = new List<string>();

            if (childSectors != null)
                foreach (var child in childSectors)
                {
                    if (child.Count() > 0)
                    {
                        var partner = DbProvider.Partners().Where(p => p.PartnerID == child.ElementAt(0).InstitutionID).FirstOrDefault();
                        if (partner != null)
                        {
                            childInstitutions.Add(partner.AltName ?? partner.PartyName);
                        }
                    }
                }

            if (id != null && id.HasValue)
                institution = DbProvider.Partners().Where(p => p.PartnerID == id).FirstOrDefault();

            string result = "";

            if (institution == null && childInstitutions.Count == 0)
                return string.Empty;
            else if (institution != null && childInstitutions.Count == 0)
            {
                result += institution.AltName ?? institution.PartyName;
            }
            else if (institution == null && childInstitutions.Count != 0)
            {
                foreach (string chInst in childInstitutions)
                {
                    result += ", " + chInst;
                }
                result = result.Remove(0, 2);
            }
            else if (institution != null && childInstitutions.Count != 0)
            {
                result += institution.AltName ?? institution.PartyName;
                foreach (string chInst in childInstitutions)
                {
                    result += ", " + chInst;
                }
            }

            return result;
        }

        private static void AddPaddingLine(Table t, float height = 4, int colspan = 1)
        {
            t.Add(new Cell().PaddingBottom(height).Colspan(colspan));
        }

        private void SetTablePaddingDefault(Table t)
        {
            t.Style = new Cell().Padding(mainTablePadding);
            t.Style.C.SetLeading(0, 1.1f);
        }

        private void SetTablePaddingVertical(Table t, double padding)
        {
            t.Style.PaddingBottom(padding).PaddingTop(padding);
        }

        private void ResetTablePaddingVertical(Table t, double padding)
        {
            t.Style = new Cell().Padding(mainTablePadding);
            t.Style.C.SetLeading(0, 1.1f);
            SetTablePaddingVertical(t, padding);
        }

        private void SetTableZeroPaddingLeading(Table t)
        {
            t.Style.PaddingLeft(0).PaddingRight(0);
            t.Style.C.SetLeading(0, 0);
        }

        private void AddSectionHeader(Table t, string text, int colspan = 1, bool showChb = false, bool chbVal = false)
        {
            if (showChb == true)
            {
                //t.Add(new Cell(t, text, fSectionHeader).BackgroundColor(0x808080).Leading(1, 1.1).Padding(4).PaddingBottom(3).PaddingTop(1.5).Colspan(1));
                t.Style.Padding(0).PaddingBottom(2);
                t.Add(new Table(t, 15, 1, 1, 1, 12).With(ti =>
                {
                    t.Style.BackgroundColor(0x808080);
                    ti.Add(new Cell(t, text, fSectionHeader).BackgroundColor(0x808080).Leading(1, 1.1).Padding(4).PaddingBottom(3).PaddingTop(1.5).Colspan(1).Indent(0, 17));
                    ti.Add(CreateCheckbox(ti, Element.ALIGN_RIGHT, !chbVal, Element.ALIGN_UNDEFINED, BaseColor.WHITE).PaddingRight(2).PaddingTop(7));
                    ti.Add(new Cell(ti, "NE", fTextNormalWhite).PaddingTop(3));
                    ti.Add(CreateCheckbox(ti, Element.ALIGN_RIGHT, chbVal, Element.ALIGN_UNDEFINED, BaseColor.WHITE).PaddingRight(2).PaddingTop(7));
                    ti.Add(new Cell(ti, "ANO (nutno vyplnit příslušné údaje v tomto oddíle níže)", fTextNormalWhite).PaddingTop(3));
                }));
                ResetTablePaddingVertical(t, innerTablePadding);
            }
            else
            {
                t.Add(new Cell(t, text, fSectionHeader).BackgroundColor(0x808080).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(colspan));
            }
        }

        private void AddSectionSubHeader(Table t, string text, int colspan = 1)
        {
            Cell c = new Cell(t, text, fSectionSubHeader).BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(colspan);
            c.C.BorderWidthBottom = 1f;
            //c.C.BorderWidthLeft = 1f;
            c.C.BorderWidthTop = 1f;
            //c.C.BorderWidthRight = 1f;
            c.C.BorderColor = subBorderColor;
            t.Add(c);
        }

        private Chunk CreateDashedLine(int dashes)
        {
            var content = string.Join(space, Enumerable.Range(0, dashes).Select(p => "_"));
            var dashedLine = new Chunk(content, fnDashedLine);
            return dashedLine;
        }

        private void S2_Info(Table t, string text, string type)
        {
            var style = new Cell().Padding(0.5);

            bool? client = false;
            bool? partner = false;
            bool? child = false;

            switch (type)
            {
                case "building":
                    client = (this.FormFUSCZ.FSBuildSaving && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null) ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Client : false;
                    partner = (this.FormFUSCZ.FSBuildSaving && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null) ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Partner : false;
                    child = (this.FormFUSCZ.FSBuildSaving && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null) ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Child : false;
                    break;
                case "life":
                    client = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client : false;
                    partner = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Partner : false;
                    child = ((this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Child : false;
                    break;
                case "leasing":
                    client = (this.FormFUSCZ.FSLeasingLoan && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLeases != null) ? this.FormFUSCZ.Announcement.AnnouncementLeases.Client : false;
                    partner = (this.FormFUSCZ.FSLeasingLoan && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLeases != null) ? this.FormFUSCZ.Announcement.AnnouncementLeases.Partner : false;
                    child = (this.FormFUSCZ.FSLeasingLoan && this.FormFUSCZ.Announcement != null && this.FormFUSCZ.Announcement.AnnouncementLeases != null) ? this.FormFUSCZ.Announcement.AnnouncementLeases.Child : false;
                    break;
            }

            var info = new Table(style,
                0.15, 3.87,
                0.2, 0.2, 0.2, 0.2, 0.2,
                0.2, 0.2, 0.2, 0.2, 0.2,
                0.21, 0.2, 0.2, 0.2, 0.2
            );

            info.Add(new Cell(style));
            info.Add(new Cell(style, text, fTextNormal).Leading(0f, 1.2f));

            SetTablePaddingVertical(info, 0);

            if (this.FormFUSCZ.FUSForClient)
            {
                info.Add(new Cell(style));
                info.Add(CreateCheckbox(t, Element.ALIGN_CENTER, client.HasValue ? !client.Value : true));
                info.Add(new Cell(style));
                info.Add(CreateCheckbox(t, Element.ALIGN_CENTER, client.HasValue ? client.Value : false));
                info.Add(new Cell(style));
            }
            else
            {
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
            }

            if ((this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation) && !FusZV)
            {
                info.Add(new Cell(t));
                info.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partner.HasValue ? !partner.Value : true));
                info.Add(new Cell(t));
                info.Add(CreateCheckbox(t, Element.ALIGN_CENTER, partner.HasValue ? partner.Value : false));
                info.Add(new Cell(t));
            }
            else
            {
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
            }

            if ((this.FormFUSCZ.FUSForChild) && !FusZV)
            {
                info.Add(new Cell(t));
                info.Add(CreateCheckbox(t, Element.ALIGN_CENTER, child.HasValue ? !child.Value : true));
                info.Add(new Cell(t));
                info.Add(CreateCheckbox(t, Element.ALIGN_CENTER, child.HasValue ? child.Value : false));
                info.Add(new Cell(t));
            }
            else
            {
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
                info.Add(new Cell(t));
            }

            t.Add(info);
        }

        private Cell CreateCheckbox(Table style = null, int align = Element.ALIGN_CENTER, bool value = false, int verticalAlign = Element.ALIGN_UNDEFINED, BaseColor color = null)
        {
            color = color == null ? BaseColor.BLACK : BaseColor.WHITE;
            Font chbFont = new Font(chbfont, 11, Font.NORMAL, color);
            //font = font == null ? fnCheckbox : font;
            return CreateCheckbox(chbFont, value, style, align, verticalAlign);
        }

        private Cell CreateCheckboxS3(Table style = null, int align = Element.ALIGN_CENTER, bool value = false, int verticalAlign = Element.ALIGN_UNDEFINED, BaseColor color = null)
        {
            color = color == null ? BaseColor.BLACK : BaseColor.WHITE;
            Font chbFont = new Font(chbfont, 11, Font.NORMAL, color);
            //font = font == null ? fnCheckbox : font;
            return CreateCheckboxS3(chbFont, value, style, align, verticalAlign);
        }

        private Cell CreateCheckbox(iTextSharp.text.Font font, bool value, Table style = null, int align = Element.ALIGN_CENTER, int verticalAlign = Element.ALIGN_UNDEFINED)
        {
            string trueSymbol = "S"; // v danem fontu je toto zaskrtnuty checkbox
            string falseSymbol = "£"; // v danem fontu je toto nezaskrtnuty checkbox

            double chbPadding = 5.2;

            if (value)
            {

                Cell checkbox = new Cell(style);
                checkbox.Add(new Chunk(trueSymbol, font));
                checkbox.Leading(0, 0.5f).Align(align).PaddingTop(chbPadding);
                checkbox.C.VerticalAlignment = verticalAlign;

                return checkbox;
            }
            else
            {
                Cell checkbox = new Cell(style);
                checkbox.Add(new Chunk(falseSymbol, font));
                checkbox.Leading(0, 0.5f).Align(align).PaddingTop(chbPadding);
                checkbox.C.VerticalAlignment = verticalAlign;

                return checkbox;
            }
        }

        private Cell CreateCheckboxS3(iTextSharp.text.Font font, bool value, Table style = null, int align = Element.ALIGN_CENTER, int verticalAlign = Element.ALIGN_UNDEFINED)
        {
            string trueSymbol = "S"; // v danem fontu je toto zaskrtnuty checkbox
            string falseSymbol = "£"; // v danem fontu je toto nezaskrtnuty checkbox

            double chbPadding = 2.5;

            if (value)
            {

                Cell checkbox = new Cell(style);
                checkbox.Add(new Chunk(trueSymbol, font));
                checkbox.Leading(0, 0.5f).Align(align).PaddingTop(chbPadding);
                checkbox.C.VerticalAlignment = verticalAlign;

                return checkbox;
            }
            else
            {
                Cell checkbox = new Cell(style);
                checkbox.Add(new Chunk(falseSymbol, font));
                checkbox.Leading(0, 0.5f).Align(align).PaddingTop(chbPadding);
                checkbox.C.VerticalAlignment = verticalAlign;

                return checkbox;
            }
        }

        private Cell CreateCheckbox(Cell style, int align = Element.ALIGN_CENTER)
        {
            return this.CreateCheckbox(style, fnCheckbox, align);
        }

        private Cell CreateCheckbox(Cell style, iTextSharp.text.Font font, int align = Element.ALIGN_CENTER)
        {
            return new Cell(style, "□", font).Leading(0, 0.5f).Align(align);
        }

        #endregion helperFunctions
    }

    public struct Insurances
    {
        public string Title { get; set; }
        public string Advantage { get; set; }
        public string Disadvantage { get; set; }
        public string Exception { get; set; } // if specified insurance has something different compared to others so it can be changed in foreach - leave empty by default
    }

    public struct ListInvestments
    {
        public string TitlePrefix { get; set; }
        public string Title { get; set; }
        public string Advantage { get; set; }
        public string Disadvantage { get; set; }
        public string Exception { get; set; } // if specified insurance has something different compared to others so it can be changed in foreach - leave empty by default
    }


}