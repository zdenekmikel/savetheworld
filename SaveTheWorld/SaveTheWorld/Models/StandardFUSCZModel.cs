﻿using System.IO;
using SaveTheWorld.Models.PDF;
using System;
using SaveTheWorld.Extensions;
using static SaveTheWorld.Constants;

namespace SaveTheWorld.Models
{
    public class StandardFUSCZModel : CZBaseModel
    {
        public FUS FUS { get; set; }
        public int? FUSID { get; set; }
        public int? FUSStatusID { get; set; }
        public void LoadModel(int? fUSID, out int? createdBy, bool isROForNetwork = true, int userID = 0, int agentID = 0)
        {
            if (fUSID == null)
                throw new ArgumentException("Value cannot be null or empty.", "fUSID");

            this.FUSID = fUSID;

            FUS = DbProvider.CustomerFUS(userID, agentID, FUSID.Value);
            if (FUS != null && !string.IsNullOrWhiteSpace(FUS.XmlContent))
            {
                FormFUSCZ fusModel = FUS.XmlContent.DeSearialize<FormFUSCZ>();
                if (fusModel != null)
                {
                    FormFUSCZ = fusModel;
                }
            }

            if (!FormFUSCZ.Version.HasValue)
                FormFUSCZ.Version = 1;

            FormFUSCZ.FUSID = fUSID;
            FormFUSCZ.FUSType = FUS.FUSTypeCode;
            this.FUSStatusID = FUS.FUSStatusID;
            createdBy = FUS?.CreatedBy;
            //CheckAccess(FUS.AgentID, Auth.Profile.AgentID, isROForNetwork);
        }
        public byte[] GetFusPDF(int version)
        {
            byte[] pdfFuz;
            if (version == 160301)
            {
                string serverPath = AppDomain.CurrentDomain.BaseDirectory + "Content\\Files\\CZFuses";
                pdfFuz = GeneratePdf(serverPath).GetPdf();
            }
            else if (version == 160609)
            {
                var pdf = new PDFFUSDynamic2();
                pdf.LoadFUS(FUSID.Value, Constants.FUSPDFTypes.Classic);
                //pdf.GeneratePdf();
                byte[] pdfData;

                using (var ms = new MemoryStream())
                {
                    pdf.RenderPdf(ms);

                    byte[] pdftoSave = ms.ToArray();
                    pdfData = ms.ToArray();
                }
                pdfFuz = pdfData;
            }
            else
            {
                var pdf = new PDFFUSDynamic();
                pdf.LoadFUS(FUSID.Value, Constants.FUSPDFTypes.Classic);
                //pdf.GeneratePdf();
                byte[] pdfData;

                using (var ms = new MemoryStream())
                {
                    pdf.RenderPdf(ms);

                    byte[] pdftoSave = ms.ToArray();
                    pdfData = ms.ToArray();
                }
                pdfFuz = pdfData;
            }  

            return pdfFuz;
        }

        public PDFFUS GeneratePdf(string serverPath)
        {
            //byte[] dataPdf = null;
            var pdf = new PDFFUS();
            if (FUSID.HasValue)
            {
                pdf.LoadFUS(FUSID.Value, serverPath, Constants.FUSPDFTypes.Classic);

                if (pdf.IsValid)
                {
                    pdf.GeneratePdf();
                    return pdf;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static int? SaveFUZDocumentToDb(int FUSID, byte[] pdfFuz, int? docID, int userID)
        {
            pdfFuz = PDFExtensions.LockAcroFieldPdfForms(pdfFuz).ToArray();
            //int? docID = DbProvider.Connection.WebDocumentInsert(DocumentTypes.FUS, string.Format("{0}_FUS", FUSID.ToString()), "", CurrentProfile.UserID, BL.FinData.Web.Classes.Documents.DocumentManager.ContractsRoot, 1);

            if (docID != null)
            {
                DocumentFileInfo docInfo = new DocumentFileInfo()
                {
                    contentType = "application/pdf",
                    documentFileTypeCode = DocumentFileTypes.FILE,
                    documentID = docID,
                    documentType = ".pdf",
                    index = 1,
                    note = "",
                    size = (long)pdfFuz.Length,
                    userID = userID,
                    FileByteStream = new MemoryStream(pdfFuz)
                };

                Guid? fileID = DbProvider.DocumentFileInsert(docInfo).documentFileID;

                //DbProvider.DocumentRelationInsert(userID, docID.Value, FUSID, DocumentRelationTypes.FUS);

                return docID;
                //}
            }
            return null;
        }
    }
}