﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace SaveTheWorld.Models
{
    public static class FusHelper
    {
        public static bool LogOR(params bool?[] options)
        {
            bool result = false;

            for (int i = options.Length - 1; i >= 0; --i)
            {
                bool? option = options[i];
                result |= option ?? false;
            }

            return result;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static int IndexOfNth(this string str, char character, int nth)
        {
            int result = -1;

            if (string.IsNullOrEmpty(str)) return result;

            int count = 1;

            for (int i = 0; i < str.Length; ++i)
            {
                if (str[i] == character)
                {
                    if (count < nth) ++count;
                    else
                    {
                        result = i;
                        break;
                    }
                }
            }

            return result;
        }
        public class AgentDetail
        {
            // public int? AgentID { get { return WebEncryption.DecodeAsNullableInt(AgentIDEnc); } set { AgentIDEnc = WebEncryption.Encode(value); } }
            public int? AgentID { get; set; }
            [XmlIgnore]
            public string AgentIDEnc { get; private set; }

            ////[Resource("FUSCZ.AgentDetail.FirstName")]
            public string FirstName { get; set; }

            ////[Resource("FUSCZ.AgentDetail.LastName")]
            //[RequiredLocalize(ModelType = typeof(AgentDetail), ErrorMessage = "FUSCZ.LastName")]
            public string LastName { get; set; }

            ////[Resource("FUSCZ.AgentDetail.Prefix")]
            public string Prefix { get; set; }

            ////[Resource("FUSCZ.AgentDetail.AgentNo")]
            //[RequiredLocalize(ModelType = typeof(AgentDetail), ErrorMessage = "FUSCZ.AgentNo")]
            public string AgentNo { get; set; }

            ////[Resource("FUSCZ.AgentDetail.PartyCode")]
            ////[RequiredLocalize(ModelType = typeof(AgentDetail), ErrorMessage = "FUSCZ.PartyCode")]
            public string PartyCode { get; set; }

            ////[Resource("FUSCZ.AgentDetail.Address")]
            //[RequiredLocalize(ModelType = typeof(AgentDetail), ErrorMessage = "FUSCZ.Address")]
            public string Address { get; set; }

            public List<Registration> Registration { get; set; }

            ////[Resource("FUSCZ.AgentDetail.RegistrationNumber")]
            public string RegistrationNumber { get; set; }

            public bool IsCompany { get; set; }

            ////[Resource("FUSCZ.AgentDetail.HasCustomerLoan")]
            public bool HasCustomerLoan { get; set; }

            ////[Resource("FUSCZ.AgentDetail.HasOtherLoan")]
            public bool HasOtherLoan { get; set; }
        }

        public class OverallInfo
        {
            public string ClientType { get; set; }

            public string PartnerType { get; set; }

            ////[Resource("FUSCZ.OverallInfo.StatementClientCode")]
            public string StatementClientCode { get; set; }

            ////[Resource("FUSCZ.OverallInfo.LastName")]
            public string LastName { get; set; }

            ////[Resource("FUSCZ.OverallInfo.FirstName")]
            public string FirstName { get; set; }

            ////[Resource("FUSCZ.OverallInfo.BirthDate")]
            public DateTime? BirthDate { get; set; }
        }

        public class Client
        {
            public Client()
            {
                AllowEdit = true;
                ClientTempID = Guid.NewGuid();
            }

            public Guid ClientTempID { get; set; }

            public int? ClientID { get; set; }

            [XmlIgnore]
            public string ClientIDEnc { get { return ClientID.HasValue ? ClientID.Value.ToString() : string.Empty; } }
            //public string ClientIDEnc { get { return WebEncryption.Encode(ClientID); } set { ClientID = WebEncryption.DecodeAsNullableInt(value); } }

            ////[Resource("FUSCZ.Client.AllowEdit")]
            public bool AllowEdit { get; set; }

            ////[Resource("FUSCZ.Client.EditOptionalInformation")]
            public bool EditOptionalInformation { get; set; }

            //Client details
            public string PartyTypeCode { get; set; }

            public string GenderCode { get; set; }

            //PO
            ////[Resource("FUSCZ.Client.FirstName")]
            public string FirstName { get; set; }

            ////[Resource("FUSCZ.Client.LastName")]
            public string LastName { get; set; }

            ////[Resource("FUSCZ.Client.Prefix")]
            public string Prefix { get; set; }

            ////[Resource("FUSCZ.Client.PartyCode")] //Birth number
            public string PartyCode { get; set; }

            ////[Resource("FUSCZ.Client.CountryCode")]
            public string CountryCode { get; set; }

            ////[Resource("FUSCZ.Client.ResidentUSA")]
            public bool? ResidentUSA { get; set; }

            ////[Resource("FUSCZ.Client.BirthDate")]
            public DateTime? BirthDate { get; set; }

            ////[Resource("FUSCZ.Client.BirthPlace")]
            public string BirthPlace { get; set; }

            ////[Resource("FUSCZ.Client.DocumentTypeCode")]
            public string DocumentTypeCode { get; set; }

            ////[Resource("FUSCZ.Client.DocumentCode")]
            public string DocumentCode { get; set; }

            ////[Resource("FUSCZ.Client.DocumentReleased")]
            public string DocumentReleased { get; set; }

            public int DocumentPlaceReleasedID { get; set; }

            ////[Resource("FUSCZ.Client.DocumentDateFrom")]
            public DateTime? DocumentDateFrom { get; set; }

            ////[Resource("FUSCZ.Client.DocumentDateTo")]
            public DateTime? DocumentDateTo { get; set; }

            // Permanent address
            public string PermanentAddress { get; set; }

            ////[Resource("FUSCZ.Client.PermanentCity")]
            public string PermanentCity { get; set; }

            ////[Resource("FUSCZ.Client.PermanentPostalCode")]
            public string PermanentPostalCode { get; set; }

            ////[Resource("FUSCZ.Client.IsMailingDifferent")]
            public bool IsMailingDifferent { get; set; }

            // Mailing address									
            ////[Resource("FUSCZ.Client.MailingAddress")]
            public string MailingAddress { get; set; }

            ////[Resource("FUSCZ.Client.MailingCity")]
            public string MailingCity { get; set; }

            ////[Resource("FUSCZ.Client.MailingPostalCode")]
            public string MailingPostalCode { get; set; }

            ////[Resource("FUSCZ.Client.MaritalStatus")]
            public string MaritalStatus { get; set; }

            ////[Resource("FUSCZ.Client.Occupation")]
            public string Occupation { get; set; }

            ////[Resource("FUSCZ.Client.Phone")]
            public string Phone { get; set; }

            //Both
            //[RequiredLocalalize("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessageResourceName = "Client.NotValidEmail")]
            ////[Resource("FUSCZ.Client.EmailAddr")]
            public string EmailAddr { get; set; }

            //PO
            ////[Resource("FUSCZ.Client.BusinessName")]
            public string BusinessName { get; set; }

            ////[Resource("FUSCZ.Client.TradeLicenceNo")] //ICO
            public string TradeLicenceNo { get; set; }

            //ZV
            ////[Resource("FUSCZ.Client.Employer")]
            public string Employer { get; set; }

            public int? EmployerID { get; set; }

            ////[Resource("FUSCZ.Client.FirstMeetingDate")]
            public DateTime? FirstMeetingDate { get; set; }

            ////[Resource("FUSCZ.Client.FirstMeetingPlace")]
            public string FirstMeetingPlace { get; set; }

            public int FirstMeetingPlaceID { get; set; }

            public bool IsSMSCodeValidated { get; set; }

            ////[Resource("FUSCZ.Client.IsSMSCodeRequired")]
            public bool IsSMSCodeRequired { get; set; }

            public DateTime? DocumentValidationAgreeDate { get; set; }

            //public string EmployerIDEnc { get { return WebEncryption.Encode(EmployerID); } set { EmployerID = WebEncryption.DecodeAsNullableInt(value); } }
        }

        public class Partner : Client
        {
            //public SelectList GetSelectListDocument(int id, bool nullable = false, object selectedValue = null)
            //{
            //    var selectListOptions = FUSLists.FUSListOptions.Where(p => p.FUSListTypeID == id && p.FUSListOptionCode != "FOWITHOUT").OrderBy(p => p.Rank).ToList();

            //    if (nullable)
            //        selectListOptions.Insert(0, new Core.Customer.FUSListOptions()
            //        {
            //            FUSListOptionCode = null,
            //            ResourceCode = "Customer.FUSListOption.NotSelected"
            //        });

            //    return new SelectList(selectListOptions.Select(p => new { Key = p.FUSListOptionCode, Value = BaseModel.ResourceGlobal(p.ResourceCode) }).ToList(), "Key", "Value", selectedValue);
            //}
        }

        public class Announcement
        {
            ////[Resource("FUSCZ.OverallInfo.AnnouncementBuildSaving")]
            public YesNoClientPartnerChild AnnouncementBuildSaving { get; set; }

            ////[Resource("FUSCZ.OverallInfo.AnnouncementLiveSaving")]
            public YesNoClientPartnerChild AnnouncementLiveSaving { get; set; }

            ////[Resource("FUSCZ.OverallInfo.AnnouncementLeases")]
            public YesNoClientPartnerChild AnnouncementLeases { get; set; }

            ////[Resource("FUSCZ.OverallInfo.AnnouncementSavingDPS")]
            public YesNoClientPartnerChild AnnouncementSavingDPS { get; set; }

            ////[Resource("FUSCZ.OverallInfo.AnnouncementInvestment")]
            public YesNoClientPartnerChild AnnouncementInvestment { get; set; }

            public StringClientPartner AnnouncementAct { get; set; }
            public YesNoClientPartnerChild AnnouncementBanking { get; set; }

            public bool? AnnouncementExpone { get; set; }

            ///////////////////////////////////////////////////////////////////////////////
            /// <summary>
            /// 2015-04-08 - DT - std konstruktor, nutný pro serializaci, nedal jsem sem inicializaci, protože nevím jestli třeba někdo nepočítal s výchozími null hodnotami
            /// </summary>
            public Announcement() { }
            /// <summary>
            /// 2015-04-08 - DT - dal jsem sem inicializační konstruktor, abych to nemusel vypisovat
            /// </summary>
            public Announcement(bool init)
            {
                this.AnnouncementBuildSaving = new YesNoClientPartnerChild();
                this.AnnouncementLiveSaving = new YesNoClientPartnerChild();
                this.AnnouncementLeases = new YesNoClientPartnerChild();
                this.AnnouncementSavingDPS = new YesNoClientPartnerChild();
                this.AnnouncementInvestment = new YesNoClientPartnerChild();
                this.AnnouncementAct = new StringClientPartner();
            }
        }


        public class Child
        {
            public Child()
            {
                AllowEdit = true;
                ClientTempID = Guid.NewGuid();
            }

            //[XmlIgnore]
            //public string ClientIDEnc { get { return WebEncryption.Encode(ClientID); } set { ClientID = WebEncryption.DecodeAsNullableInt(value); } }

            public int? ClientID { get; set; }

            public Guid ClientTempID { get; set; }

            ////[Resource("FUSCZ.Child.AllowEdit")]
            public bool AllowEdit { get; set; }

            ////[Resource("FUSCZ.Client.EditOptionalInformation")]
            public bool EditOptionalInformation { get; set; }

            //[RequiredLocalize(ModelType = typeof(Child), ErrorMessage = "FUSCZ.GenderCode")]
            public string GenderCode { get; set; }

            ////[Resource("FUSCZ.Child.FirstName")]
            public string FirstName { get; set; }

            ////[Resource("FUSCZ.Child.LastName")]
            //[RequiredLocalize(ModelType = typeof(Child), ErrorMessage = "FUSCZ.LastName")]
            public string LastName { get; set; }

            //[RequiredLocalize(ModelType = typeof(Child), ErrorMessage = "FUSCZ.CountryCode")]
            ////[Resource("FUSCZ.Child.CountryCode")]
            public string CountryCode { get; set; }

            ////[RequiredLocalize(ModelType = typeof(Child), ErrorMessage = "FUSCZ.PartyCode")]
            ////[Resource("FUSCZ.Child.PartyCode")]
            public string PartyCode { get; set; }

            ////[Resource("FUSCZ.Child.BirthDate")]
            //[RequiredLocalize(ModelType = typeof(Child), ErrorMessage = "FUSCZ.BirthDate")]
            public DateTime? BirthDate { get; set; }

            ////[Resource("FUSCZ.Client.IsAddressDifferent")]
            public bool IsAddressDifferent { get; set; }

            ////[Resource("FUSCZ.Child.Address")]
            public string Address { get; set; }

            ////[Resource("FUSCZ.Child.City")]
            public string City { get; set; }

            ////[Resource("FUSCZ.Child.PostalCode")]
            public string PostalCode { get; set; }

            ////[Resource("FUSCZ.Child.BirthPlace")]
            public string BirthPlace { get; set; }

            ////[Resource("FUSCZ.Child.DocumentTypeCode")]
            public string DocumentTypeCode { get; set; }

            ////[Resource("FUSCZ.Child.DocumentCode")]
            public string DocumentCode { get; set; }

            ////[Resource("FUSCZ.Client.DocumentReleased")]
            public string DocumentReleased { get; set; }

            public int DocumentPlaceReleasedID { get; set; }

            ////[Resource("FUSCZ.Client.DocumentDateFrom")]
            public DateTime? DocumentDateFrom { get; set; }

            ////[Resource("FUSCZ.Client.DocumentDateTo")]
            public DateTime? DocumentDateTo { get; set; }

            public DateTime? DocumentValidationAgreeDate { get; set; }
        }

        public class FinancialDataVerification
        {
            ////[Resource("FUSCZ.DataVerification.QEInvestGoal")]
            public StringClientPartner QEInvestGoal { get; set; }

            ////[Resource("FUSCZ.DataVerification.QEInvestTools")]
            public StringClientPartner QEInvestTools { get; set; }

            ////[Resource("FUSCZ.DataVerification.QEInvestHorizont")]
            public StringClientPartner QEInvestHorizont { get; set; }

            ////[Resource("FUSCZ.DataVerification.QEInvestExperience")]
            public StringClientPartner QEInvestExperience { get; set; }

            ////[Resource("FUSCZ.DataVerification.QEInvestRisk")]
            public StringClientPartner QEInvestRisk { get; set; }

            //////[Resource("FUSCZ.DataVerification.RefuseInformation")]
            //public bool RefuseInformation { get; set; }

            ////[Resource("FUSCZ.DataVerification.RefuseInformation")]
            public BoolClientPartner RefuseInformation { get; set; }

            ///////////////////////////////////////////////////////////////////////////////
            /// <summary>
            /// 2015-04-09 - DT - std konstruktor, nutný pro serializaci, nedal jsem sem inicializaci, protože nevím jestli třeba někdo nepočítal s výchozími null hodnotami
            /// </summary>
            public FinancialDataVerification() { }
            /// <summary>
            /// 2015-04-09 - DT - dal jsem sem inicializační konstruktor, abych to nemusel vypisovat
            /// </summary>
            public FinancialDataVerification(bool init)
            {
                this.QEInvestExperience = new StringClientPartner();
                this.QEInvestGoal = new StringClientPartner();
                this.QEInvestHorizont = new StringClientPartner();
                this.QEInvestRisk = new StringClientPartner();
                this.QEInvestTools = new StringClientPartner();
                this.RefuseInformation = new BoolClientPartner();
            }
        }

        public class FinancialDsDps
        {
            ////[Resource("FUSCZ.FinancialDsDps.ClientPartnerRefuse")]
            public bool ClientPartnerRefuse { get; set; }

            public bool ClientStrategyDS { get; set; }

            public bool PartnerStrategyDS { get; set; }

            public bool ClientStrategyDPS { get; set; }

            public bool PartnerStrategyDPS { get; set; }

            public bool ClientStrategyIncreaseDPS { get; set; }

            public bool PartnerStrategyIncreaseDPS { get; set; }

            public bool ClientStrategyTFToDPS { get; set; }

            public bool PartnerStrategyTFToDPS { get; set; }

            public bool ClientStrategyIncreaseTF { get; set; }

            public bool PartnerStrategyIncreaseTF { get; set; }

            public bool ClientStrategyTF { get; set; }

            public bool PartnerStrategyTF { get; set; }

            public string ClientStrategyDSOptions { get; set; }

            public string PartnerStrategyDSOptions { get; set; }

            public string ClientStrategyDPSOptions { get; set; }

            public string PartnerStrategyDPSOptions { get; set; }

            public string ClientStrategyIncreaseDPSOptions { get; set; }

            public string PartnerStrategyIncreaseDPSOptions { get; set; }

            public string ClientStrategyTFToDPSOptions { get; set; }

            public string PartnerStrategyTFToDPSOptions { get; set; }

            public string ClientStrategyTFOptions { get; set; }

            public string PartnerStrategyTFOptions { get; set; }

            ////[Resource("FUSCZ.FinancialDsDps.ClientDSDate")]
            public DateTime? ClientDSDate { get; set; }

            ////[Resource("FUSCZ.FinancialDsDps.ClientDSTime")]
            public DateTime? ClientDSTime { get; set; }

            ////[Resource("FUSCZ.FinancialDsDps.PartnerDSDate")]
            public DateTime? PartnerDSDate { get; set; }

            ////[Resource("FUSCZ.FinancialDsDps.PartnerDSTime")]
            public DateTime? PartnerDSTime { get; set; }

        }

        public class FinancialInsurance
        {
            ////[Resource("FUSCZ.FinancialInsurance.InsuranceIndividual")]
            public bool InsuranceIndividual { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIDeath")]
            public BoolClientPartnerChild IIDeath { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIDeathInjury")]
            public BoolClientPartnerChild IIDeathInjury { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIPermanentInjury")]
            public BoolClientPartnerChild IIPermanentInjury { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIPermanentInjuryFree")]
            public bool IIPermanentInjuryFree { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIPermanentInjurySingle")]
            public bool IIPermanentInjurySingle { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIInvalidity")]
            public BoolClientPartnerChild IIInvalidity { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIInvalidityFree")]
            public bool IIInvalidityFree { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIInvaliditySingle")]
            public bool IIInvaliditySingle { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.IILowInjury")]
            public BoolClientPartnerChild IILowInjury { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IISeriousDiseases")]
            public BoolClientPartnerChild IISeriousDiseases { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.InsuranceSurvivalGuaranteed")]
            public BoolClientPartnerChild InsuranceSurvivalGuaranteed { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceSurvivalInvestFund")]
            public BoolClientPartnerChild InsuranceSurvivalInvestFund { get; set; }

            // 2015-05-21 - DT - odstraněno v reakci na požadavek #4890
            ////[Resource("FUSCZ.FinancialInsurance.RequiredInvestProfile")]
            public BoolClientPartnerChild RequiredInvestProfile { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.DynamicProfil")]
            public BoolClientPartnerChild DynamicProfil { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.BalancedProfil")]
            public BoolClientPartnerChild BalancedProfil { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.KonzervativeProfil")]
            public BoolClientPartnerChild KonzervativeProfil { get; set; }



            ////[Resource("FUSCZ.FinancialInsurance.InsuranceProperty")]
            public bool InsuranceProperty { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPHomeReality")]
            public BoolClientPartner IPHomeReality { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPOtherReality")]
            public BoolClientPartner IPOtherReality { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPAllRisk")]
            public BoolClientPartner IPAllRisk { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPTheft")]
            public BoolClientPartner IPTheft { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPVehicleOthers")]
            public BoolClientPartner IPVehicleOthers { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPOthers")]
            public BoolClientPartner IPOthers { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.InsuranceDamage")]
            public bool InsuranceDamage { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDPublic")]
            public BoolClientPartner IDPublic { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDOwnReality")]
            public BoolClientPartner IDOwnReality { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDPZP")]
            public BoolClientPartner IDPZP { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDEmployer")]
            public BoolClientPartnerChild IDEmployer { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDOthers")]
            public BoolClientPartnerChild IDOthers { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.InsuranceTraveling")]
            public bool InsuranceTraveling { get; set; }

            public BoolClientPartnerChild InsuranceTravelingDetail { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceOtherDetail")]
            public BoolClientPartnerChild InsuranceOtherDetail { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceTaxAdvantage")]
            public bool? InsuranceTaxAdvantage { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceNote")]
            public string InsuranceNote { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.Discrepancies")]
            public string Discrepancies { get; set; }

            ///////////////////////////////////////////////////////////////////////////////
            /// <summary>
            /// 2015-04-09 - DT - std konstruktor, nutný pro serializaci, nedal jsem sem inicializaci, protože nevím jestli třeba někdo nepočítal s výchozími null hodnotami
            /// </summary>
            public FinancialInsurance() { }
            /// <summary>
            /// 2015-04-09 - DT - dal jsem sem inicializační konstruktor, abych to nemusel vypisovat
            /// </summary>
            public FinancialInsurance(bool init)
            {
                this.IIDeath = new BoolClientPartnerChild();
                this.IIDeathInjury = new BoolClientPartnerChild();
                this.IIPermanentInjury = new BoolClientPartnerChild();
                this.IIInvalidity = new BoolClientPartnerChild();
                this.IILowInjury = new BoolClientPartnerChild();
                this.IISeriousDiseases = new BoolClientPartnerChild();
                this.InsuranceSurvivalGuaranteed = new BoolClientPartnerChild();
                this.InsuranceSurvivalInvestFund = new BoolClientPartnerChild();
                this.RequiredInvestProfile = new BoolClientPartnerChild();
                this.DynamicProfil = new BoolClientPartnerChild();
                this.BalancedProfil = new BoolClientPartnerChild();
                this.KonzervativeProfil = new BoolClientPartnerChild();
                this.IPHomeReality = new BoolClientPartner();
                this.IPOtherReality = new BoolClientPartner();
                this.IPAllRisk = new BoolClientPartner();
                this.IPTheft = new BoolClientPartner();
                this.IPVehicleOthers = new BoolClientPartner();
                this.IPOthers = new BoolClientPartner();
                this.IDPublic = new BoolClientPartner();
                this.IDOwnReality = new BoolClientPartner();
                this.IDPZP = new BoolClientPartner();
                this.IDEmployer = new BoolClientPartnerChild();
                this.IDOthers = new BoolClientPartnerChild();
                this.InsuranceTravelingDetail = new BoolClientPartnerChild();
                this.InsuranceOtherDetail = new BoolClientPartnerChild();
            }
        }

        public class FinancialInvestment
        {
            ////[Resource("FUSCZ.Investment.InvestTowardQuestionnaireCode")]
            public StringClientPartner InvestTowardQuestionnaireCode { get; set; }

            public StringClientPartner ExpInvetorCode { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestFinancialEducation")]
            public StringClientPartner QEInvestFinancialEducationCode { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestWorkPosition")]
            public StringClientPartner QEInvestWorkPositionCode { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestRiskEducation")]
            public StringClientPartner QEInvestRiskEducationCode { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestExpInvestInstrumetn")]
            public StringClientPartner QEInvestExpInvestInstrumetnCode { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestExpInvest")]
            public StringClientPartner QEInvestExpInvestCode { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestFreqClosingDeal")]
            public StringClientPartner QEInvestFreqClosingDealCode { get; set; }

            ////[Resource("FUSCZ.Investment.InvestDate")]
            public DateTime? InvestDate { get; set; }

            ////[Resource("FUSCZ.Investment.InvestTime")]
            public DateTime? InvestTime { get; set; }

            ////[Resource("FUSCZ.Investment.QEInvestKnowledgeServicesA")]
            public BoolClientPartner QEInvestKnowledgeServicesA { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestKnowledgeServicesB")]
            public BoolClientPartner QEInvestKnowledgeServicesB { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestKnowledgeServicesC")]
            public BoolClientPartner QEInvestKnowledgeServicesC { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestKnowledgeServicesD")]
            public BoolClientPartner QEInvestKnowledgeServicesD { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestPrincipleToolsA")]
            public BoolClientPartner QEInvestPrincipleToolsA { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestPrincipleToolsB")]
            public BoolClientPartner QEInvestPrincipleToolsB { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestPrincipleToolsC")]
            public BoolClientPartner QEInvestPrincipleToolsC { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestPrincipleToolsD")]
            public BoolClientPartner QEInvestPrincipleToolsD { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestPrincipleToolsE")]
            public BoolClientPartner QEInvestPrincipleToolsE { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeTypesA")]
            public BoolClientPartner QEInvestTradeTypesA { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeTypesB")]
            public BoolClientPartner QEInvestTradeTypesB { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeTypesC")]
            public BoolClientPartner QEInvestTradeTypesC { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeTypesD")]
            public BoolClientPartner QEInvestTradeTypesD { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeCharacterA")]
            public BoolClientPartner QEInvestTradeCharacterA { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeCharacterB")]
            public BoolClientPartner QEInvestTradeCharacterB { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeCharacterC")]
            public BoolClientPartner QEInvestTradeCharacterC { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeCharacterD")]
            public BoolClientPartner QEInvestTradeCharacterD { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeCharacterE")]
            public BoolClientPartner QEInvestTradeCharacterE { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesA")]
            public BoolClientPartner QEInvestTradingVolumesA { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesB")]
            public BoolClientPartner QEInvestTradingVolumesB { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesC")]
            public BoolClientPartner QEInvestTradingVolumesC { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesD")]
            public BoolClientPartner QEInvestTradingVolumesD { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesE")]
            public BoolClientPartner QEInvestTradingVolumesE { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesF")]
            public BoolClientPartner QEInvestTradingVolumesF { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradingVolumesG")]
            public BoolClientPartner QEInvestTradingVolumesG { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeLastThreeYearsA")]
            public BoolClientPartner QEInvestTradeLastThreeYearsA { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeLastThreeYearsB")]
            public BoolClientPartner QEInvestTradeLastThreeYearsB { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeLastThreeYearsC")]
            public BoolClientPartner QEInvestTradeLastThreeYearsC { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeLastThreeYearsD")]
            public BoolClientPartner QEInvestTradeLastThreeYearsD { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeLastThreeYearsE")]
            public BoolClientPartner QEInvestTradeLastThreeYearsE { get; set; }
            ////[Resource("FUSCZ.Investment.QEInvestTradeLastThreeYearsF")]
            public BoolClientPartner QEInvestTradeLastThreeYearsF { get; set; }

            ///////////////////////////////////////////////////////////////////////////////
            /// <summary>
            /// 2015-04-09 - DT - std konstruktor, nutný pro serializaci, nedal jsem sem inicializaci, protože nevím jestli třeba někdo nepočítal s výchozími null hodnotami
            /// </summary>
            public FinancialInvestment() { }
            /// <summary>
            /// 2015-04-09 - DT - dal jsem sem inicializační konstruktor, abych to nemusel vypisovat
            /// </summary>
            public FinancialInvestment(bool init)
            {
                this.InvestTowardQuestionnaireCode = new StringClientPartner();
                this.ExpInvetorCode = new StringClientPartner();
                this.QEInvestFinancialEducationCode = new StringClientPartner();
                this.QEInvestWorkPositionCode = new StringClientPartner();
                this.QEInvestRiskEducationCode = new StringClientPartner();
                this.QEInvestExpInvestInstrumetnCode = new StringClientPartner();
                this.QEInvestExpInvestCode = new StringClientPartner();
                this.QEInvestFreqClosingDealCode = new StringClientPartner();
                this.QEInvestKnowledgeServicesA = new BoolClientPartner();
                this.QEInvestKnowledgeServicesB = new BoolClientPartner();
                this.QEInvestKnowledgeServicesC = new BoolClientPartner();
                this.QEInvestKnowledgeServicesD = new BoolClientPartner();
                this.QEInvestPrincipleToolsA = new BoolClientPartner();
                this.QEInvestPrincipleToolsB = new BoolClientPartner();
                this.QEInvestPrincipleToolsC = new BoolClientPartner();
                this.QEInvestPrincipleToolsD = new BoolClientPartner();
                this.QEInvestPrincipleToolsE = new BoolClientPartner();
                this.QEInvestTradeTypesA = new BoolClientPartner();
                this.QEInvestTradeTypesB = new BoolClientPartner();
                this.QEInvestTradeTypesC = new BoolClientPartner();
                this.QEInvestTradeTypesD = new BoolClientPartner();
                this.QEInvestTradeCharacterA = new BoolClientPartner();
                this.QEInvestTradeCharacterB = new BoolClientPartner();
                this.QEInvestTradeCharacterC = new BoolClientPartner();
                this.QEInvestTradeCharacterD = new BoolClientPartner();
                this.QEInvestTradeCharacterE = new BoolClientPartner();
                this.QEInvestTradingVolumesA = new BoolClientPartner();
                this.QEInvestTradingVolumesB = new BoolClientPartner();
                this.QEInvestTradingVolumesC = new BoolClientPartner();
                this.QEInvestTradingVolumesD = new BoolClientPartner();
                this.QEInvestTradingVolumesE = new BoolClientPartner();
                this.QEInvestTradingVolumesF = new BoolClientPartner();
                this.QEInvestTradingVolumesG = new BoolClientPartner();
                this.QEInvestTradeLastThreeYearsA = new BoolClientPartner();
                this.QEInvestTradeLastThreeYearsB = new BoolClientPartner();
                this.QEInvestTradeLastThreeYearsC = new BoolClientPartner();
                this.QEInvestTradeLastThreeYearsD = new BoolClientPartner();
                this.QEInvestTradeLastThreeYearsE = new BoolClientPartner();
                this.QEInvestTradeLastThreeYearsF = new BoolClientPartner();
            }
        }

        public class Sector
        {
            public Sector()
            {
                SectorID = Guid.NewGuid();
            }

            public Guid? SectorID { get; set; }

            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.ClientTempID")]
            ////[Resource("FUSCZ.ProposedSolution.ClientTempID")]
            public Guid? ClientTempID { get; set; }

            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.SectorCode")]
            ////[Resource("FUSCZ.ProposedSolution.SectorCode")]
            public string SectorCode { get; set; }

            ////[Resource("FUSCZ.ProposedSolution.Institution")]
            public int? InstitutionID { get; set; }

            //[XmlIgnore]
            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.Institution")]
            //public string InstitutionIDEnc { get { return WebEncryption.Encode(InstitutionID); } set { InstitutionID = WebEncryption.DecodeAsNullableInt(value); } }

            ////[Resource("FUSCZ.ProposedSolution.ServiceSetID")]
            public int? ServiceSetID { get; set; }

            //[XmlIgnore]
            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.ServiceSetIDEnc")]
            //public string ServiceSetIDEnc { get { return WebEncryption.Encode(ServiceSetID); } set { ServiceSetID = WebEncryption.DecodeAsNullableInt(value); } }

            ////[Resource("FUSCZ.ProposedSolution.Product")]
            public int? ProductID { get; set; }

            [XmlIgnore]
            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.Product")]
            //public string ProductIDEnc { get { return WebEncryption.Encode(ProductID); } set { ProductID = WebEncryption.DecodeAsNullableInt(value); } }

            ////[Resource("FUSCZ.ProposedSolution.ContractNo")]
            //[RequiredLocalAttribute(15, ModelType = typeof(Sector), ErrorMessage = "FUSCZ.ProposedSolution.ContractNo")]
            public string ContractNo { get; set; }

            ////[Resource("FUSCZ.ProposedSolution.Note")]
            //[RequiredLocalAttribute(35, ModelType = typeof(Sector), ErrorMessage = "FUSCZ.ProposedSolution.Note")]
            public string Note { get; set; }

            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.DateSigned")]
            ////[Resource("FUSCZ.ProposedSolution.DateSigned")]
            public DateTime? DateSigned { get; set; }
        }


        public class SectorInvestment : Sector
        {
            public string KTRequirementOther { get; set; }

            //[RequiredLocalize(ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.KTFinancialTool")]
            ////[Resource("FUSCZ.InvestmentInstrucions.KTFinancialTool")]
            public string KTFinancialTool { get; set; }

            //[RequiredLocalize(ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.KTFinancialToolType")]
            ////[Resource("FUSCZ.InvestmentInstrucions.KTFinancialToolType")]
            public string KTFinancialToolType { get; set; }

            //[RequiredLocalize(ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.KTBuySell")]
            ////[Resource("FUSCZ.InvestmentInstrucions.KTBuySell")]
            public string KTBuySell { get; set; }

            //[RequiredLocalize(ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.KTInvestAmountTypeCode")]
            ////[Resource("FUSCZ.InvestmentInstrucions.KTInvestAmountType")]
            public string KTInvestAmountTypeCode { get; set; }

            //[RequiredLocalize(ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.KTCurrency")]
            ////[Resource("FUSCZ.InvestmentInstrucions.KTCurrency")]
            public string KTCurrency { get; set; }

            //[RequiredLocalize(ModelType = typeof(Sector), ErrorMessage = "FUSCZ.Amount")]
            ////[Resource("FUSCZ.SectorInvestment.Amount")]
            //[RequiredLocalAttribute(20, ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.ProposedSolution.Amount")]
            //[RequiredLocalalize(@"^((\d| )*(,\d*[0-9])?)?$", ErrorMessageResourceName = "FUSCZ.Amount")]
            public string Amount { get; set; }

            ////[RequiredLocalize(ModelType = typeof(SectorInvestment), ErrorMessage = "FUSCZ.AmountPeriod")]
            ////[Resource("FUSCZ.InvestmentInstrucions.AmountPeriod")]
            public string AmountPeriod { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.Note")]
            //[RequiredLocalAttribute(65, ModelType = typeof(Sector), ErrorMessage = "FUSCZ.InvestmentInstrucions.Note")]
            public string Note { get; set; }
        }

        public class SectorInsurance : Sector
        {
            ////[Resource("FUSCZ.SectorInsurance.IsInfromationProvide")]
            public bool IsInfromationProvide { get; set; }

            ////[Resource("FUSCZ.SectorInsurance.IsInformationPrint")]
            public bool IsInformationPrint { get; set; }

            ////[Resource("FUSCZ.SectorInsurance.IsInformationElectronic")]
            public bool IsInformationElectronic { get; set; }

            ////[Resource("FUSCZ.SectorInsurance.IsSoftwareProvide")]
            public bool IsSoftwareProvide { get; set; }

            public FinancialInsuranceChoices InsuranceChoices { get; set; }

            ////[Resource("FUSCZ.StandardFUS.FUSForClient")]
            public bool FUSForClient { get; set; }

            ////[Resource("FUSCZ.StandardFUS.FUSForPartner")]
            public bool FUSForPartner { get; set; }

            ////[Resource("FUSCZ.StandardFUS.FUSForChild")]
            public bool FUSForChild { get; set; }

            public SectorInsurance()
            {
                this.InsuranceChoices = new FinancialInsuranceChoices();
            }

            public SectorInsurance(bool fusForClient, bool fusForPartner, bool fusForChild)
            {
                this.InsuranceChoices = new FinancialInsuranceChoices();
                FUSForClient = fusForClient;
                FUSForPartner = fusForPartner;
                FUSForChild = fusForChild;
            }
        }

        public class FinalStatementSignatures
        {
            public bool DataVerification { get; set; }

            ////[Resource("FUSCZ.FinalStatementSignatures.DocumentSendedEmail")]
            public bool DocumentSendedEmail { get; set; }

            ////[Resource("FUSCZ.FinalStatementSignatures.DocumentSendedDisc")]
            public bool DocumentSendedDisc { get; set; }

            ////[Resource("FUSCZ.FinalStatementSignatures.DocumentSendedPaper")]
            public bool DocumentSendedPaper { get; set; }

            ////[Resource("FUSCZ.FinalStatementSignatures.DateFUSSigned")]
            public DateTime? DateFUSSigned { get; set; }

            //[RequiredLocalize(ModelType = typeof(FinalStatementSignatures), ErrorMessage = "FUSCZ.SignedPlace")]
            ////[Resource("FUSCZ.FinalStatementSignatures.SignedPlace")]
            public string SignedPlace { get; set; }

            public int SignedPlaceID { get; set; }
        }

        public class InvestmentInstrucions
        {
            public string KTRequirementOther { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.KTFinancialTool")]
            public string KTFinancialTool { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.KTFinancialToolType")]
            public string KTFinancialToolType { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.KTBuySell")]
            public string KTBuySell { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.KTInvestAmountType")]
            public string KTInvestAmountTypeCode { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.KTCurrency")]
            public string KTCurrency { get; set; }

            ////[Resource("FUSCZ.InvestmentInstrucions.KTDateInvestment")]
            public DateTime? KTDateInvestment { get; set; }
        }

        public class FinancialInsuranceChoices
        {
            ////[Resource("FUSCZ.FinancialInsuranceChoices.InsuranceIndividual")]
            public bool InsuranceIndividual { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIDeath")]
            public BoolClientPartnerChild IIDeath { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIDeathInjury")]
            public BoolClientPartnerChild IIDeathInjury { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIPermanentInjury")]
            public BoolClientPartnerChild IIPermanentInjury { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIPermanentInjuryFree")]
            public bool IIPermanentInjuryFree { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIPermanentInjurySingle")]
            public bool IIPermanentInjurySingle { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIInvalidity")]
            public BoolClientPartnerChild IIInvalidity { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIInvalidityFree")]
            public bool IIInvalidityFree { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IIInvaliditySingle")]
            public bool IIInvaliditySingle { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.IILowInjury")]
            public BoolClientPartnerChild IILowInjury { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IISeriousDiseases")]
            public BoolClientPartnerChild IISeriousDiseases { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.InsuranceSurvivalGuaranteed")]
            public BoolClientPartnerChild InsuranceSurvivalGuaranteed { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceSurvivalInvestFund")]
            public BoolClientPartnerChild InsuranceSurvivalInvestFund { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.RequiredInvestProfile")]
            public bool RequiredInvestProfile
            {
                get { return FusHelper.LogOR(FUShelper.AtleastOneTrue(this.BalancedProfil), FUShelper.AtleastOneTrue(this.DynamicProfil), FUShelper.AtleastOneTrue(this.KonzervativeProfil)); }
            }

            ////[Resource("FUSCZ.FinancialInsurance.DynamicProfil")]
            public BoolClientPartnerChild DynamicProfil { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.BalancedProfil")]
            public BoolClientPartnerChild BalancedProfil { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.KonzervativeProfil")]
            public BoolClientPartnerChild KonzervativeProfil { get; set; }



            ////[Resource("FUSCZ.FinancialInsurance.InsuranceProperty")]
            public bool InsuranceProperty { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPHomeReality")]
            public BoolClientPartner IPHomeReality { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPOtherReality")]
            public BoolClientPartner IPOtherReality { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPAllRisk")]
            public BoolClientPartner IPAllRisk { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPTheft")]
            public BoolClientPartner IPTheft { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPVehicleOthers")]
            public BoolClientPartner IPVehicleOthers { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IPOthers")]
            public BoolClientPartner IPOthers { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.InsuranceDamage")]
            public bool InsuranceDamage { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDPublic")]
            public BoolClientPartner IDPublic { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDOwnReality")]
            public BoolClientPartner IDOwnReality { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDPZP")]
            public BoolClientPartner IDPZP { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDEmployer")]
            public BoolClientPartnerChild IDEmployer { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.IDOthers")]
            public BoolClientPartnerChild IDOthers { get; set; }


            ////[Resource("FUSCZ.FinancialInsurance.InsuranceTraveling")]
            public bool InsuranceTraveling { get; set; }

            public BoolClientPartnerChild InsuranceTravelingDetail { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceOtherDetail")]
            public BoolClientPartnerChild InsuranceOtherDetail { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceTaxAdvantage")]
            public bool? TaxAdvantage { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.InsuranceNote")]
            public string InsuranceNote { get; set; }

            ////[Resource("FUSCZ.FinancialInsurance.Discrepancies")]
            public string Discrepancies { get; set; }
        }
    }
}