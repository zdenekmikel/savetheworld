﻿using SaveTheWorld.Extensions;

namespace SaveTheWorld.Models
{
    public class FUSCZBase
    {
        //public static List<Core.Customer.FUSType> GetCustomerFUSTypes()
        //{
        //    var fusTypes = DbProvider.Connection.CustomerFUSTypes();
        //    fusTypes.ForEach(p => p.ResourceCode = ResourceManager.GetResource(p.ResourceCode));
        //    return fusTypes;
        //}

        //public static void NewFUSTicket(int? newFUSID, string content = null)
        //{
        //    if (newFUSID != null)
        //    {
        //        var ticket = new BL.FinData.Core.Workflow.Ticket()
        //        {
        //            CreatedBy = Auth.Profile.UserID,
        //            TicketClassCode = "EF",
        //            PriorityLevelID = 1,
        //            TicketStatusID = TicketStatuses.EF_INPROGRESS,
        //            Content = content,
        //            Subject = string.Format("eFUS: {0}", newFUSID),
        //            IsPublic = 1,
        //        };

        //        DbProvider.Connection.TicketInsert(ticket, newFUSID, WebEncryption.Encode(newFUSID), "EF", null, null, null);
        //    }
        //}
    }

    public class ShowSection
    {
        public bool AnnouncementBuildSaving { get; set; }

        public bool AnnouncementLiveSaving { get; set; }

        public bool AnnouncementLeases { get; set; }

        public bool AnnouncementSavingDPS { get; set; }

        public bool AnnouncementInvestment { get; set; }

        public bool FIDataVerification { get; set; }

        public bool FIDsDps { get; set; }

        // #6758
        //public bool FIInsurance { get; set; }

        public bool FIInvestment { get; set; }


        public bool AtLeastOneSection { get; set; }


        public bool InsuranceIndividual { get; set; }

        public bool InsuranceProperty { get; set; }

        public bool InsuranceDamage { get; set; }

        public bool InsuranceTraveling { get; set; }

        public bool InsuranceOtherDetail { get; set; }

        public bool InsuranceTaxAdvantage { get; set; }

        public bool IsOnlyInsuranceTraveling
        {
            get
            {
                return InsuranceTraveling
                    && !InsuranceIndividual
                    && !InsuranceProperty
                    && !InsuranceDamage
                    && !InsuranceOtherDetail
                    && !InsuranceTaxAdvantage;
            }
        }
        //InsuranceNote

        public ShowSection(FormFUSCZ formFUS)
        {
            if (formFUS.FSBuildSaving)
                AnnouncementBuildSaving = true;

            if (formFUS.FSInsuranceLive || formFUS.FSInsuranceLiveOther) // 2015-06-18 - DT - formFUS.FSInsurancePersonal no AML
                AnnouncementLiveSaving = true;

            if (formFUS.FSLeasingLoan)
                AnnouncementLeases = true;

            if (formFUS.FSSavingDPS || formFUS.FSSavingIncreaseDPS || formFUS.FSSavingTFToDPS)
                AnnouncementSavingDPS = true;

            if (formFUS.FSInvestment || formFUS.FSTermDepositsOrOtherJT)
                AnnouncementInvestment = true;

            if (formFUS.FSInsuranceLive || formFUS.FSSavingDS || formFUS.FSSavingDPS || formFUS.FSSavingTFToDPS)
                FIDataVerification = true;

            if (formFUS.FSSavingDS || formFUS.FSSavingDPS || formFUS.FSSavingIncreaseDPS || formFUS.FSSavingTFToDPS || formFUS.FSSavingIncreaseTF)
                FIDsDps = true;

            // #6758
            /*if (formFUS.FSInsurancePersonal || formFUS.FSInsuranceLive ||
				formFUS.FSInsuranceProperty || formFUS.FSInsuranceLiability ||
				formFUS.FSInsuranceTravel || formFUS.FSInsuranceOthers || formFUS.FSInsuranceLiveOther)
				FIInsurance = true;*/

            if (formFUS.FSInvestment || formFUS.FSTermDepositsOrOtherJT) // #6217
                FIInvestment = true;

            if (ExtensionMethods.AtleastOnetrue(FIDataVerification, FIDsDps,
                //FIInsurance, // #6758
                FIInvestment, formFUS.FSLoanConsumer, formFUS.FSLoanOther))
                AtLeastOneSection = true;

            if (ExtensionMethods.AtleastOnetrue(formFUS.FSInsuranceLive, formFUS.FSInsuranceLiveOther, formFUS.FSInsurancePersonal))
                InsuranceIndividual = true;

            if (ExtensionMethods.AtleastOnetrue(formFUS.FSInsuranceProperty))
                InsuranceProperty = true;

            if (ExtensionMethods.AtleastOnetrue(formFUS.FSInsuranceLiability))
                InsuranceDamage = true;

            if (ExtensionMethods.AtleastOnetrue(formFUS.FSInsuranceTravel))
                InsuranceTraveling = true;

            if (ExtensionMethods.AtleastOnetrue(formFUS.FSInsuranceLive, formFUS.FSInsuranceLiveOther))
                InsuranceTaxAdvantage = true;

            //InsuranceIndividual	
            //InsuranceProperty	  
            //InsuranceDamage
            //InsuranceTraveling
            //InsuranceOtherDetail 
            //InsuranceTaxAdvantage 
        }


    }
}