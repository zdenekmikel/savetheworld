﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveTheWorld.Models
{
    public class DocumentFileInfo
    {
        public Guid? documentFileID;
        public int? documentID;
        public int index;
        public long? size;
        public string contentType;
        public string documentType;
        
        public string note;
        
        public int? userID;
        
        public string documentFileTypeCode;
        public System.IO.Stream FileByteStream;
    }
}
