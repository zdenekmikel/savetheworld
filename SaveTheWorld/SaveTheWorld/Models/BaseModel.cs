﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Runtime.Serialization;

namespace SaveTheWorld.Models
{
    [DataContract]
    [Serializable]
    public abstract class BaseModel
    {
        //public static void UpdateUserFunctions()
        //{
        //    Auth.Profile.UserFunctions = new SortedSet<string>(DbProvider.Connection.UserFunctions(Auth.Profile.UserID, Auth.Profile.AgentID).Select(r => r.FunctionCode).Distinct());
        //}
        private static List<Resource> _resources = DbProvider.GetResources();
        public static List<Resource> Resources
        {
            get
            {
                return _resources;
            }
        }
        public static string ResourceGlobal(string expression)
        {
            var resource = Resources.Where(x => x.ResourceCode == expression).FirstOrDefault();
            if (resource == null)
                return "";
            else return resource.Value;
        }

        //public static string HtmlResourceGlobal(string expression)
        //{
        //    if (string.IsNullOrWhiteSpace(expression))
        //        return null;
        //    var x = CultureManager.GetCurrentCulture();

        //    string text = string.Empty;

        //    if (Utils.IsCountryCodeCZ())
        //        return AppCache.Inst.HtmlResources.Where(p => p.HtmlResourceCode == expression && p.CultureCode == "cs").Select(p => p.Value).FirstOrDefault();
        //    else
        //        return AppCache.Inst.HtmlResources.Where(p => p.HtmlResourceCode == expression && p.CultureCode == "sk").Select(p => p.Value).FirstOrDefault();
        //}

        //public static string ResourceGlobalDual(string value, params object[] args)
        //{
        //    return ResourceManager.GetDualResource(value, args);
        //}

        //public static string ResourceGlobalWeb(string expression, params object[] args)
        //{
        //    expression = string.Format(Constants.Resources.ResourceGlobalFormat, expression);
        //    return ResourceManager.GetResource(expression, args);
        //}

        //public static string ModelResource(string modelName, string expression, params object[] args)
        //{
        //    string text = string.Format(DbResourceHelper.GetStringResource("Web.Model." + modelName + "." + expression + ".Text"), args);
        //    return text;
        //}

        //protected static AuthProfile CurrentProfile
        //{
        //    get
        //    {
        //        return Auth.Profile;
        //    }
        //}
        //protected static Core.IService Db { get { return DbProvider.Connection; } }

        // public static AppCache AppCache { get { return AppCache.GetInstance(); } }

        //public static bool CanViewAll { get { return Acl("Web.Agent.View.All"); } }
        // static int? UserAgentID { get { return CanViewAll ? (int?)null : CurrentProfile.AgentID; } }
        //public static AgentDetail RequestedAgent { get { return WebData.Current.RequestedAgent; } }
        //public static int RequestedAgentID { get { return RequestedAgent.AgentID; } }

        private static List<int> AllowedPositions
        {
            get
            {
                return new List<int>()
                {
                    Constants.AgentPositions.Zemsky,
                    Constants.AgentPositions.Oblastni,
                    Constants.AgentPositions.SeniorPoradce,
                    Constants.AgentPositions.Regionalni,
                    Constants.AgentPositions.SamostatnyOsobniPoradce,
                    Constants.AgentPositions.TOPPoradce,
                    Constants.AgentPositions.ExkluzivniPoradce,
                    Constants.AgentPositions.Manazer
                };
            }
        }

        //public static List<Division> DivisionsWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<Division>();
        //        list.Add(new Division() { DivisionCode = "NA", Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.Divisions);
        //        return list;
        //    }
        //}

        //public static List<Region> RegionsWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<Region>();
        //        list.Add(new Region() { RegionID = 0, Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.Regions);
        //        return list;
        //    }
        //}

        //public static List<District> DistrictsWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<District>();
        //        list.Add(new District() { DistrictID = 0, Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.Districts);
        //        return list;
        //    }
        //}

        //public static List<AgentStatus> AgentStatusesWithNotSelectedItem
        //{
        //    get
        //    {
        //        //temporary removed Applicant
        //        return AppCache.AgentStatuses.Where(p => p.StatusCode != "P").ToList();
        //    }
        //}

        //public static List<Position> AgentPositionsWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<Position>();
        //        list.Add(new Position() { PositionID = 0, Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.AgentPositionsActive);
        //        return list;
        //    }
        //}

        //public static List<PartnerSearch> PartnersWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<PartnerSearch>();
        //        list.Add(new PartnerSearch { PartyName = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.PartnersWebClaims.Where(p => AppCache.ServicesWebClaims.Select(q => q.PartnerID).Contains(p.PartnerID)));
        //        return list;
        //    }
        //}

        //public static List<CategoryWeb> ParentCategoriesWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<CategoryWeb>();
        //        list.Add(new CategoryWeb { Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.CategoriesWeb.Where(p => p.ParentCategoryID == 10000));
        //        return list;
        //    }
        //}

        //public static List<CategoryWeb> CategoriesWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<CategoryWeb>();
        //        list.Add(new CategoryWeb { Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.CategoriesWeb.Where(p => p.ParentCategoryID != 10000 && p.CategoryTypeCode == "S"));
        //        return list;
        //    }
        //}

        //public static List<Service> ServicesWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<Service>();
        //        list.Add(new Service { Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.ServicesWebClaims);
        //        return list;
        //    }
        //}

        //public static List<Priority> ClientPrioritiesWithNotSelectedItem
        //{
        //    get
        //    {
        //        var list = new List<Priority>();
        //        list.Add(new Priority { Name = ResourceGlobal("Com.Text.NotSelected") });
        //        list.AddRange(AppCache.ClientPriorities);
        //        return list;
        //    }
        //}

        //public static IEnumerable<object> GetClientsForAutocomplete(string term, int count, int agentID)
        //{
        //    var result = DbProvider.Call(p => p.ClientsAutocomplete(CurrentProfile.UserID, agentID, term.ToSearchBoxValue())).Take(count).ToList();
        //    return result.Select(p => new { value = p.PartyName, id = WebEncryption.Encode(p.ClientID) });
        //}

        //public static string GetAgentNameWithPersonalNumber(AgentDetail a)
        //{
        //    return string.Format("{0} [{1}]", a.PartyName, a.AgentNo);
        //}

        //public static bool ShowWebProduct()
        //{
        //    if (Utils.IsCountryCodeSK() && Auth.Profile.Agent.PositionID.HasValue && !AllowedPositions.Contains(Auth.Profile.Agent.PositionID.Value))
        //        return false;

        //    return true;
        //}
    }
}
