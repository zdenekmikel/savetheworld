﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SaveTheWorld.Extensions;
using System;
using System.Linq;

namespace SaveTheWorld.Models.PDF
{
    public class PageEvents : PdfPageEventHelper
    {
        private int? FusID { get; set; }
        private bool? FusZV { get; set; }
        private bool? Draft { get; set; }
        public bool IsAml { get; set; }
        public bool IsADK { get; set; }
        public string Version { get; set; }
        public PageEvents(int? fusID, bool? fusZV, bool? draft, bool isAdk = false, string version = "161201")
        {
            this.FusID = fusID;
            this.FusZV = fusZV;
            this.Draft = draft;
            IsADK = isAdk;
            Version = version;
        }

        public PageEvents(int? fusID, bool? fusZV, bool isAml = true, bool isAdk = false, string version = "161201")
        {
            this.FusID = fusID;
            this.FusZV = fusZV;
            IsAml = isAml;
            IsADK = isAdk;
            Version = version;
        }

        public override void OnGenericTag(PdfWriter writer, Document document, iTextSharp.text.Rectangle rect, string text)
        {
            var cb = writer.DirectContent;
            switch (text)
            {
                case "dashed":

                    //Save the current state so that we don't affect future canvas drawings
                    cb.SaveState();

                    //Set a line color
                    cb.SetColorStroke(BaseColor.BLACK);

                    //Set a dashes
                    //See this for more details: http://api.itextpdf.com/itext/com/itextpdf/text/pdf/PdfContentByte.html#setLineDash(float)
                    cb.SetLineDash(3, 1);

                    //Move the cursor to the left edge with the "pen up"
                    cb.MoveTo(rect.Left - 2, rect.Bottom - 2);

                    //Move to cursor to the right edge with the "pen down"
                    cb.LineTo(rect.Right + 2, rect.Bottom - 2);

                    //Actually draw the lines (the "pen downs")
                    cb.Stroke();

                    //Reset the drawing states to what it was before we started messing with it
                    cb.RestoreState();

                    break;

                case "cross":
                    cb.SaveState();

                    cb.SetColorStroke(BaseColor.BLACK);

                    cb.SetLineDash(1);
                    cb.MoveTo(rect.Left + 3.1f, rect.Top - 12.8f);
                    cb.LineTo(rect.Right - 3.2f, rect.Bottom + 1.7f);
                    cb.Stroke();
                    cb.MoveTo(rect.Right - 3.2f, rect.Top - 12.8f);
                    cb.LineTo(rect.Left + 3.1f, rect.Bottom + 1.7f);
                    cb.Stroke();

                    cb.RestoreState();

                    break;

                case "whitecross":
                    cb.SaveState();

                    cb.SetColorStroke(BaseColor.WHITE);


                    cb.SetLineDash(1);
                    cb.MoveTo(rect.Left + 3.1f, rect.Top - 12.8f);
                    cb.LineTo(rect.Right - 3.2f, rect.Bottom + 1.7f);
                    cb.Stroke();
                    cb.MoveTo(rect.Right - 3.2f, rect.Top - 12.8f);
                    cb.LineTo(rect.Left + 3.1f, rect.Bottom + 1.7f);
                    cb.Stroke();

                    cb.RestoreState();

                    break;
            }

            base.OnGenericTag(writer, document, rect, text);
        }

        public override void OnStartPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            PdfContentByte cb = writer.DirectContent;
            new Table().With(t =>
            {
                if (FusZV.HasValue && FusZV.Value)
                {
                    var f = new Font(BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10, Font.NORMAL, BaseColor.BLACK);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.HeaderTitleZV, f).Leading(0, 1.15).AlignRight());
                    t.T.SetTotalWidth(new[] { document.PageSize.Width });
                    t.T.WriteSelectedRows(0, 1, 0 - document.LeftMargin, document.Top + 25, cb);
                }
                else
                {
                    var f = new Font(BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 12, Font.NORMAL, BaseColor.BLACK);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.HeaderTitle, f).Leading(0, 1.15).AlignRight());
                    t.T.SetTotalWidth(new[] { document.PageSize.Width });
                    t.T.WriteSelectedRows(0, 1, 0 - document.LeftMargin, document.Top + 27, cb);
                }

            });

            string imageFilePath = string.Format(AppDomain.CurrentDomain.BaseDirectory + "Content\\images\\fus\\{0}.png", IsADK ? "logo-premier" : "logo");
            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(imageFilePath);
            logo.ScalePercent(25);
            logo.Alignment = Image.ALIGN_TOP | Image.ALIGN_LEFT;
            logo.SetAbsolutePosition(document.LeftMargin - 6, document.PageSize.Height - 40);

            string prodCode = this.FusID.ToString();
            var br = new BarcodeLib.Barcode();
            br.IncludeLabel = true;
            br.LabelFont = new System.Drawing.Font("Microsoft Sans Serif", 7, System.Drawing.FontStyle.Bold);
            br.LabelWidth = 58;
            br.PaddingLeft = 62;
            br.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            var bar = br.Encode(BarcodeLib.TYPE.CODE128, prodCode, System.Drawing.Color.Black, System.Drawing.Color.White, 180, 23);
            var barCode = iTextSharp.text.Image.GetInstance(bar, System.Drawing.Imaging.ImageFormat.Bmp);
            barCode.Alignment = Image.ALIGN_TOP | Image.ALIGN_LEFT;

            if (FusZV.HasValue && FusZV.Value)
                barCode.SetAbsolutePosition(document.LeftMargin + 60, document.PageSize.Height - 38); // - 34 to get into same lane as fctr logo
            else
                barCode.SetAbsolutePosition(document.LeftMargin + 100, document.PageSize.Height - 38); // - 34 to get into same lane as fctr logo

            if (!IsAml)
                document.Add(barCode);

            document.Add(logo);

        }

        public void FillEAN()
        {
            string prodCode = this.FusID.ToString();

            var br = new BarcodeLib.Barcode();
            br.IncludeLabel = true;
            br.LabelFont = new System.Drawing.Font("Microsoft Sans Serif", 7, System.Drawing.FontStyle.Bold);
            br.LabelWidth = 58;
            br.PaddingLeft = 62;
            br.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;

            var bar = br.Encode(BarcodeLib.TYPE.CODE128, prodCode, System.Drawing.Color.Black, System.Drawing.Color.White, 180, 23);
            var itextImage = iTextSharp.text.Image.GetInstance(bar, System.Drawing.Imaging.ImageFormat.Bmp);

        }


        // This is the contentbyte object of the writer
        PdfContentByte cb;

        // we will put the final number of pages in a template
        PdfTemplate template;

        // this is the BaseFont we are going to use for the header / footer
        BaseFont footerBaseFont = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        int footerFontSize = 5;

        // This keeps track of the creation time
        DateTime PrintTime;

        private Chunk CreateDashedLine(int dashes)
        {
            var content = string.Join(" ", Enumerable.Range(0, dashes).Select(p => "_"));
            var dashedLine = new Chunk(content, fnDashedLine);
            return dashedLine;
        }

        public iTextSharp.text.Font fnDashedLine { get; set; }

        #region Properties

        #endregion
        // we override the onOpenDocument method
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now;
                footerBaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.DirectContent;
                template = cb.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
            }
            catch (System.IO.IOException ioe)
            {
            }
        }
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            var marginTop = 11.5f;
            var marginLeft = 200f;

            new Table(new Cell(), 1).With(t =>
            {
                var f = new Font(footerBaseFont, footerFontSize, Font.NORMAL, BaseColor.BLACK);
                t.Add(new Cell(t, App_GlobalResources.FusCZ.FooterInformation, f).AlignCenter());

                t.T.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                t.T.WriteSelectedRows(0, 2, 0 + document.LeftMargin, document.BottomMargin - marginTop, cb);
            });

            if (Draft.HasValue && Draft.Value)
            {
                var transf = new System.Drawing.Drawing2D.Matrix();
                transf.RotateAt(55, new System.Drawing.PointF(100, 100), System.Drawing.Drawing2D.MatrixOrder.Append);
                writer.DirectContent.Transform(transf);

                PdfGState graphicsState = new PdfGState();
                graphicsState.FillOpacity = 0.15F;
                //graphicsState.StrokeOpacity = 0F;
                cb.SaveState();
                cb.SetGState(graphicsState);

                new Table().With(t =>
                {
                    var f = new Font(BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 195, Font.NORMAL, BaseColor.BLACK);
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.Draft, f).Leading(0, 1.15).AlignCenter().AlignMiddle());
                    t.T.SetTotalWidth(new[] { document.PageSize.Width * 1.3f });
                    t.T.WriteSelectedRows(0, 1, 90, 270, cb);
                });

                cb.RestoreState();
                transf.Invert();
                writer.DirectContent.Transform(transf);
            }
            new Table(new Cell(), 1).With(t =>
            {
                var f = new Font(footerBaseFont, footerFontSize, Font.NORMAL, BaseColor.BLACK);

                if (FusZV.HasValue && FusZV.Value)
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.DocumentDestinationZV, f).AlignRight());
                    t.T.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                    t.T.WriteSelectedRows(0, 2, 0 + document.LeftMargin, document.BottomMargin - marginTop - 8, cb);
                }
                else
                {
                    t.Add(new Cell(t, App_GlobalResources.FusCZ.DocumentDestination, f).AlignRight());
                    t.T.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
                    t.T.WriteSelectedRows(0, 2, 0 + document.LeftMargin, document.BottomMargin - marginTop - 8, cb);
                }

            });

            base.OnEndPage(writer, document);
            int pageN = writer.PageNumber;
            String text = App_GlobalResources.FusCZ.Version + (IsADK ? " ADK " : " ") + this.Version + " / " + App_GlobalResources.FusCZ.Page + " " + pageN + " z ";

            if (FusZV.HasValue && FusZV.Value)
                text = App_GlobalResources.FusCZ.VersionZV + " 170101 / " + App_GlobalResources.FusCZ.Page + " " + pageN + " z ";

            float len = footerBaseFont.GetWidthPoint(text, footerFontSize);
            Rectangle pageSize = document.PageSize;
            cb.SetRGBColorFill(0, 0, 0);
            cb.BeginText();
            cb.SetFontAndSize(footerBaseFont, footerFontSize);
            cb.SetTextMatrix(pageSize.GetLeft(40 + marginLeft), document.BottomMargin - marginTop - 13.5f);
            cb.ShowText(text);
            cb.EndText();
            cb.AddTemplate(template, pageSize.GetLeft(40 + marginLeft) + len, pageSize.GetBottom(20));
        }
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            template.BeginText();
            template.SetFontAndSize(footerBaseFont, footerFontSize);
            template.SetTextMatrix(0, 0);
            template.ShowText("" + (writer.PageNumber - 1));
            template.EndText();
        }
    }
}
