﻿using SaveTheWorld.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace SaveTheWorld.Models.PDF
{
    public class PDFBase
    {
        protected byte[] FinalPdfMemory { get; set; }
        protected string FilePath { get; set; }

        public string PdfType { get; set; }
        public string PasswordParty { get; set; }

        public FormFUSCZ FormFUSCZ { get; set; }
        public bool IsValid { get; set; }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        public PDFBase()
        {
            this.IsValid = true;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region GetPdf
        public byte[] GetPdf()
        {
            var output = this.FinalPdfMemory;

            if (output == null)
                return null;

            output = PDFExtensions.MergePdfFormFiles(new List<byte[]>() { output }).ToArray();

            return output.ToArray();
        }
        #endregion
    }
}