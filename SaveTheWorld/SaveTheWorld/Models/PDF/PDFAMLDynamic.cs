﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using SaveTheWorld.App_GlobalResources;
using SaveTheWorld.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace SaveTheWorld.Models.PDF
{
    public class PDFAMLDynamic : PDFBase
    {
        private Document d;
        private PdfWriter writer;
        private NumberFormatInfo nfi;
        private double mainTablePadding = 1.5;
        private double textPadding = 2.0;
        private PageEvents helper;
        private double innerTablePadding = 1;
        private float separatingPadding = 4;
        private double headerIndent = 12.0;
        private double textIndent = 10.0;

        private BaseFont bfontNormal;
        private BaseFont bfontBold;
        private BaseFont bfontDashed;
        private BaseFont chbfont;

        private Font fSectionHeader;
        private Font fTextNormal;
        private Font fTextBold;
        private Font fTextLink;

        public string Name { get { return "AML"; } }
        public string Space { get { return " "; } }
        public string Author { get { return "Fincentrum"; } }

        private int? FUSID { get; set; }
        private FormAML FormAML { get; set; }
        private AMLModel AMLModel { get; set; }

        private bool IsADKFus
        {
            get
            {
                return FormFUSCZ != null && FormFUSCZ.FUSType == Constants.FUSCZTypes.ADK;
            }
        }

        private bool IsZVFus
        {
            get
            {
                return FormFUSCZ != null && (FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV);
            }
        }

        private bool IsStdFus
        {
            get { return this.FormFUSCZ != null && (this.FormFUSCZ.FUSType == Constants.FUSCZTypes.STANDARDNI || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMS); }
        }

        private Dictionary<string, bool> RiskCountries
        {
            get
            {
                return new Dictionary<string, bool> {
                    { AMLCZ.FooterRiskCountriesA, FormAML.RiskUSA },
                    { AMLCZ.FooterRiskCountriesB, FormAML.RiskSouthAmerica },
                    { AMLCZ.FooterRiskCountriesC, FormAML.RiskSSSR },
                    { AMLCZ.FooterRiskCountriesD, FormAML.RiskPacific },
                    { AMLCZ.FooterRiskCountriesE, FormAML.RiskAsia },
                    { AMLCZ.FooterRiskCountriesF, FormAML.RiskEurope }
                };
            }
        }

        private List<string> RiskFactors
        {
            get
            {
                return new List<string> { AMLCZ.FooterRiskFactorA, AMLCZ.FooterRiskFactorB, AMLCZ.FooterRiskFactorC, AMLCZ.FooterRiskFactorD };
            }
        }

        private List<string> RiskTrades
        {
            get
            {
                return new List<string> { AMLCZ.FooterTradeA, AMLCZ.FooterTradeB, AMLCZ.FooterTradeC, AMLCZ.FooterTradeD, AMLCZ.FooterTradeE,
                    AMLCZ.FooterTradeF, AMLCZ.FooterTradeG, AMLCZ.FooterTradeH, AMLCZ.FooterTradeI, AMLCZ.FooterTradeJ, AMLCZ.FooterTradeK,
                    AMLCZ.FooterTradeL, AMLCZ.FooterTradeM, AMLCZ.FooterTradeN, AMLCZ.FooterTradeO, AMLCZ.FooterTradeP, AMLCZ.FooterTradeQ,
                    AMLCZ.FooterTradeR, AMLCZ.FooterTradeS, AMLCZ.FooterTradeT, AMLCZ.FooterTradeU, AMLCZ.FooterTradeV, AMLCZ.FooterTradeW,
                    AMLCZ.FooterTradeX, AMLCZ.FooterTradeY };
            }
        }


        private List<string> RiskTradesStd
        {
            get
            {
                return new List<string> { AMLCZ.FooterTradeA, AMLCZ.FooterTradeB, AMLCZ.FooterTradeC, AMLCZ.FooterTradeD, AMLCZ.FooterTradeE,
                    AMLCZ.FooterTradeF, AMLCZ.FooterTradeG, AMLCZ.FooterTradeH, AMLCZ.FooterTradeI, AMLCZ.FooterTradeJ, AMLCZ.FooterTradeK,
                    AMLCZ.FooterTradeL_Std, AMLCZ.FooterTradeM, AMLCZ.FooterTradeN, AMLCZ.FooterTradeO, AMLCZ.FooterTradeP, AMLCZ.FooterTradeQ,
                    AMLCZ.FooterTradeR, AMLCZ.FooterTradeS, AMLCZ.FooterTradeT, AMLCZ.FooterTradeU, AMLCZ.FooterTradeV, AMLCZ.FooterTradeW,
                    AMLCZ.FooterTradeX, AMLCZ.FooterTradeY };
            }
        }

        private List<string> RiskTradesZV
        {
            get
            {
                return new List<string> { AMLCZ.FooterTradeA_ZV, AMLCZ.FooterTradeB_ZV, AMLCZ.FooterTradeC_ZV, AMLCZ.FooterTradeD_ZV, AMLCZ.FooterTradeE,
                    AMLCZ.FooterTradeF_ZV, AMLCZ.FooterTradeG_ZV, AMLCZ.FooterTradeH_ZV, AMLCZ.FooterTradeI, AMLCZ.FooterTradeJ_ZV, AMLCZ.FooterTradeK,
                    AMLCZ.FooterTradeL_ZV, AMLCZ.FooterTradeM, AMLCZ.FooterTradeN_ZV, AMLCZ.FooterTradeO_ZV, AMLCZ.FooterTradeP, AMLCZ.FooterTradeQ,
                    AMLCZ.FooterTradeR, AMLCZ.FooterTradeS, AMLCZ.FooterTradeT_ZV, AMLCZ.FooterTradeU, AMLCZ.FooterTradeV, AMLCZ.FooterTradeW,
                    AMLCZ.FooterTradeX, AMLCZ.FooterTradeY };
            }
        }

        private List<string> PersonLinks
        {
            get
            {
                return new List<string> { AMLCZ.FooterPersonsLink_1, AMLCZ.FooterPersonsLink_2 };
            }
        }

        private List<string> PersonPEPLinks
        {
            get
            {
                return new List<string> { AMLCZ.FooterPersonsLink_3, AMLCZ.FooterPersonsLink_4, AMLCZ.FooterPersonsLink_5,
                    AMLCZ.FooterPersonsLink_6, AMLCZ.FooterPersonsLink_7, AMLCZ.FooterPersonsLink_8, AMLCZ.FooterPersonsLink_9 };
            }
        }

        private List<string> PersonSanctionsLinks
        {
            get
            {
                return new List<string> { AMLCZ.FooterPersonsLink_10, AMLCZ.FooterPersonsLink_11, AMLCZ.FooterPersonsLink_12 };
            }
        }

        private List<string> Risks
        {
            get
            {
                return new List<string> { AMLCZ.SGQ1, AMLCZ.SGQ2, AMLCZ.SGQ3, AMLCZ.SGQ4, AMLCZ.SGQ5, AMLCZ.SGQ6 };
            }
        }

        private Dictionary<string, string> IncomeLevelRegularCodes
        {
            get
            {
                return new Dictionary<string, string>
                {
                    { AMLCZ.SFQ1A_1, "CZIRLESS500" },
                    { AMLCZ.SFQ1A_2, "CZIRLESS1300" },
                    { AMLCZ.SFQ1A_3, "CZIRMORE1300" },
                };
            }
        }

        private Dictionary<string, string> IncomeLevelIrregularCodes
        {
            get
            {
                return new Dictionary<string, string>
                {
                    { AMLCZ.SFQ1B_1, "CZIILESS6" },
                    { AMLCZ.SFQ1B_2, "CZIILESS17" },
                    { AMLCZ.SFQ1B_3, "CZIIMORE17" },
                };
            }
        }

        private Dictionary<string, string> SourceIncomeLevelRegularCodes
        {
            get
            {
                return new Dictionary<string, string>
                {
                    { AMLCZ.SFQ2A, "CZSRDEPEND" },
                    { AMLCZ.SFQ2B, "CZSRINDEPEND" },
                    { AMLCZ.SFQ2C, "CZSRCOMB" },
                };
            }
        }

        private Dictionary<string, BoolClientPartner> SourceIncomeLevelIrregulars
        {
            get
            {
                return new Dictionary<string, BoolClientPartner>
                {
                    { AMLCZ.SFQ3A, FormAML.SourceIncomeLevelIrregularA }, { AMLCZ.SFQ3B, FormAML.SourceIncomeLevelIrregularB },
                    { AMLCZ.SFQ3C, FormAML.SourceIncomeLevelIrregularC }, { AMLCZ.SFQ3D, FormAML.SourceIncomeLevelIrregularD },
                    { AMLCZ.SFQ3E, FormAML.SourceIncomeLevelIrregularE },
                };
            }
        }

        private Dictionary<string, BoolClientPartner> AMLCheckboxes
        {
            get
            {
                return new Dictionary<string, BoolClientPartner>
                {
                    { AMLCZ.SCQuestion1,  FormAML.Q1TradeOver }, { AMLCZ.SCQuestion2,  FormAML.Q2Exponed },
                    { AMLCZ.SCQuestion3,  FormAML.Q3SelectProduct }, { AMLCZ.SCQuestion4,  FormAML.Q4LiveRisk },
                    { AMLCZ.SCQuestion5,  FormAML.Q5HQRisk }, { AMLCZ.SCQuestion6,  FormAML.Q8HQBusinessPartner },
                    { AMLCZ.SCQuestion7,  FormAML.Q9OwnerShip }, { AMLCZ.SCQuestion8,  FormAML.Q10Source },
                    { AMLCZ.SCQuestion9,  FormAML.Q11ExistFact }, { AMLCZ.SCQuestion10,  FormAML.Q12Unusual },
                    { AMLCZ.SCQuestion11,  FormAML.Q13UnusualFC }, { AMLCZ.SCQuestion12,  FormAML.Q14Identificiation }
                };
            }
        }

        private Dictionary<string, BoolClientPartner> AMLCheckboxesZV
        {
            get
            {
                return new Dictionary<string, BoolClientPartner>
                {
                    { AMLCZ.SCQuestion1_ZV,  FormAML.Q1TradeOver }, { AMLCZ.SCQuestion2_ZV,  FormAML.Q2Exponed },
                    { AMLCZ.SCQuestion3_ZV,  FormAML.Q3SelectProduct }, { AMLCZ.SCQuestion4_ZV,  FormAML.Q4LiveRisk },
                    { AMLCZ.SCQuestion5_ZV,  FormAML.Q5HQRisk }, { AMLCZ.SCQuestion6_ZV,  FormAML.Q8HQBusinessPartner },
                    { AMLCZ.SCQuestion7_ZV,  FormAML.Q9OwnerShip }, { AMLCZ.SCQuestion8_ZV,  FormAML.Q10Source },
                    { AMLCZ.SCQuestion9_ZV,  FormAML.Q11ExistFact }, { AMLCZ.SCQuestion10_ZV,  FormAML.Q12Unusual },
                    { AMLCZ.SCQuestion11_ZV,  FormAML.Q13UnusualFC }, { AMLCZ.SCQuestion12_ZV,  FormAML.Q14Identificiation }
                };
            }
        }

        public PDFAMLDynamic(int fusID, out int? createdBy)
        {
            FUSID = fusID;
            SetUpFonts();
            LoadFus(out createdBy);
        }

        private void SetUpFonts()
        {
            bfontDashed = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            bfontNormal = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahoma.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            bfontBold = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\tahomabd.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            chbfont = BaseFont.CreateFont(AppDomain.CurrentDomain.BaseDirectory + "Content\\WINGDNG2.TTF", BaseFont.IDENTITY_H, false);

            fSectionHeader = new Font(bfontNormal, 9f, Font.BOLD, BaseColor.WHITE);
            fTextNormal = new Font(bfontNormal, 8, Font.NORMAL, BaseColor.BLACK);
            fTextBold = new Font(bfontNormal, 8, Font.BOLD, BaseColor.BLACK);
            fTextLink = new Font(bfontNormal, 8, Font.BOLD, BaseColor.RED);
        }

        private void LoadFus(out int? createdBy)
        {
            var fus = DbProvider.CustomerFUS(0, 0, FUSID.Value);

            if (fus != null && !string.IsNullOrWhiteSpace(fus.XmlContent))
            {
                var fusModel = fus.XmlContent.DeSearialize<FormFUSCZ>();
                if (fusModel != null)
                {
                    this.FormFUSCZ = fusModel;
                    this.FormFUSCZ.FUSID = FUSID;
                    this.FormFUSCZ.FUSType = fus.FUSTypeCode;
                }

                var aml = DbProvider.CustomerFUSAML(0, 0, FUSID.Value);

                if (aml != null && !string.IsNullOrWhiteSpace(aml.XmlContent))
                {
                    var amlModel = aml.XmlContent.DeSearialize<FormAML>();
                    this.FormAML = amlModel;
                }
                else
                    this.IsValid = false;

                AMLModel = new AMLModel();
                AMLModel.LoadAMLData(FUSID.Value);
            }
            else
            {
                this.IsValid = false;
            }

            createdBy = fus?.CreatedBy;
        }

        public bool GeneratePdf(string version)
        {
            if (!this.IsValid || this.FormAML == null) return false;

            using (MemoryStream ms = new MemoryStream())
            {
                RenderPdf(ms, version: version);
                this.FinalPdfMemory = ms.ToArray();
            }

            return true;
        }

        public void RenderPdf(MemoryStream mStream, string version)
        {
            if (!IsValid) return;

            helper = new PageEvents(this.FormFUSCZ.FUSID, this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV, version: version);

            // set number formating
            nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            nfi.NumberDecimalSeparator = ",";

            d = new Document(new RectangleReadOnly(563, 798), 25, 25, 45, 45);

            writer = PdfWriter.GetInstance(d, mStream);
            writer.PageEvent = helper;

            // meta data
            d.AddTitle(Name);
            d.AddCreator(Author);
            d.AddAuthor(Author);
            d.AddCreationDate();

            // open document
            d.Open();

            RenderContent(d);

            writer.CloseStream = false;
            d.Close();
            mStream.Position = 0;
        }

        private void RenderContent(Document d)
        {
            var hr = new Table();
            hr.T.WidthPercentage = 100;
            var hr1cell = new Cell().PaddingTop(0.2).PaddingBottom(0.2).BorderWidth(0.2);
            hr1cell.C.Border = Rectangle.TOP_BORDER;
            hr1cell.C.BorderColor = BaseColor.BLACK;
            hr.Add(hr1cell);

            // main table
            d.Add((PdfPTable)new Table().With(mainTable =>
            {
                mainTable.T.WidthPercentage = 100;
                mainTable.Style = new Cell().Padding(mainTablePadding);
                mainTable.Style.C.SetLeading(0, 1.1f);

                RenderHeader(mainTable);
                RenderSections(mainTable);
                RenderFooter(mainTable);
            }));
        }

        private void RenderSections(Table mainTable)
        {
            if (AMLModel.IsAmlForClient && AMLModel.ClientType() == SealedConstants.PersonType.Corporation || AMLModel.IsAmlForPartner && AMLModel.PartnerType() == SealedConstants.PersonType.Corporation)
                RenderSectionA(mainTable);
            RenderSectionB(mainTable);
            RenderSectionC(mainTable);
            RenderSectionD(mainTable);
            RenderSectionE(mainTable);
            RenderSectionF(mainTable);
            RenderSectionG(mainTable);
            RenderSectionH(mainTable);
            RenderSignatures(mainTable);
        }

        private void RenderHeader(Table table)
        {
            SetTablePaddingVertical(table, innerTablePadding);

            table.Add(new Cell(table, IsZVFus ? AMLCZ.Header_ZV : AMLCZ.Header, fSectionHeader).BackgroundColor(0x808080).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1));
            table.Add(new Cell(table, AMLCZ.HeaderDescription, fTextNormal));

            AddPaddingLine(table, separatingPadding);
        }

        private void RenderFooter(Table table)
        {
            RenderCountries(table);
            RenderTrades(table);
            RenderLinks(table);
        }

        private void RenderSectionA(Table table)
        {
            table.Add(new Cell(table, AMLCZ.SAHeader + Space, fTextBold)
                .Add(new Chunk(AMLCZ.SAHeader_2, fTextNormal))
                .Add(new Chunk(AMLCZ.SAHeader_3, fTextBold)).AlignLeft().Indent(0, textIndent));

            table.Add(new Cell(table, FormAML.ActualFinancialOwner, fTextNormal).Indent(textIndent));
            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionB(Table table)
        {
            string sbheader = AMLCZ.SBHeader;

            if (IsZVFus)
                sbheader = AMLCZ.SBHeader_ZV;
            else if (IsStdFus)
                sbheader = AMLCZ.SBHeader_Std;

            table.Add(new Cell(table, sbheader, fTextBold).AlignLeft().Indent(0, textIndent));
            table.Add(new Cell(table, FormAML.Details, fTextNormal).Indent(textIndent));
            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionC(Table table)
        {
            table.Add(new Table(table, 19.5, 1.8, 1.8).With(t =>
            {
                t.Add(new Cell(t, AMLCZ.SCHeader, fTextBold).AlignLeft()); ;
                if (AMLModel.AMLForClient)
                    t.Add(new Cell(table, AMLCZ.Client, fTextBold));
                else
                    t.Add(new Cell(table));
                if (AMLModel.IsAmlForPartner)
                    t.Add(new Cell(table, AMLCZ.Partner, fTextBold));
                else
                    t.Add(new Cell(table));

                if (IsZVFus)
                {
                    foreach (var item in AMLCheckboxesZV)
                        RenderQuestion(t, item.Key, item.Value, textIndent);
                }
                else
                {
                    foreach (var item in AMLCheckboxes)
                        RenderQuestion(t, item.Key, item.Value, textIndent);
                }

            }));

            table.Add(new Table(table).With(tt =>
            {
                tt.Style.BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1);
                tt.Add(new Table(tt, 2.6, 17.0).With(t =>
                {
                    tt.Add(new Cell(tt, AMLCZ.Notice, fTextBold));
                    tt.Add(new Cell(tt, IsZVFus ? AMLCZ.SCNotice_1_ZV : AMLCZ.SCNotice_1, fTextBold).Indent(0, textIndent));
                    tt.Add(new Cell(tt));
                    tt.Add(new Cell(tt, AMLCZ.SCNotice_2, fTextBold).Indent(0, textIndent));
                }));
            }));

            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionD(Table table)
        {
            string sDDescription_1 = AMLCZ.SDDescription_1;

            if (IsStdFus)
                sDDescription_1 = AMLCZ.SDDescription_1_Std;
            else if (IsADKFus)
                sDDescription_1 = AMLCZ.SDDescription_1_ZV;

            table.Add(new Cell(table, AMLCZ.SDHeader + Space, fTextBold).Add(new Chunk(sDDescription_1, fTextNormal)).AlignLeft().Indent(0, textIndent));
            table.Add(new Cell(table, IsZVFus ? AMLCZ.SDDescription_2_ZV : AMLCZ.SDDescription_2, fTextNormal).Indent(textIndent));
            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionE(Table table)
        {
            table.Add(new Cell(table, AMLCZ.SEHeader, fTextBold));
            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionF(Table table)
        {
            table.Add(new Table(table, 16.5, 1.8, 1.8).With(t =>
            {
                t.Add(new Cell(t, AMLCZ.SFHeader + Space, fTextBold).Add(new Chunk(AMLCZ.SFDescription, fTextNormal)).AlignLeft());
                AddEmptyCells(t, 2);

                t.Add(new Cell(t, AMLCZ.SFQ1Header, fTextBold).Indent(headerIndent));
                if (AMLModel.AMLForClient)
                    t.Add(new Cell(t, AMLCZ.Client, fTextBold));
                else
                    t.Add(new Cell(t));
                if (AMLModel.IsAmlForPartner)
                    t.Add(new Cell(t, AMLCZ.Partner, fTextBold));
                else
                    t.Add(new Cell(t));

                t.Add(new Cell(t, AMLCZ.SFQ1A, fTextNormal).Indent(headerIndent * 2));
                AddEmptyCells(t, 2);

                foreach (var item in IncomeLevelRegularCodes)
                    RenderQuestion(t, item.Key, FormAML.IncomeLevelRegularCode, item.Value, headerIndent * 3);

                t.Add(new Cell(t, AMLCZ.SFQ1B, fTextNormal).Indent(headerIndent * 2));
                AddEmptyCells(t, 2);

                foreach (var item in IncomeLevelIrregularCodes)
                    RenderQuestion(t, item.Key, FormAML.IncomeLevelIrregularCode, item.Value, headerIndent * 3);
            }));

            table.Add(new Table(table, 16.5, 1.8, 1.8).With(t =>
            {
                t.Add(new Cell(t, AMLCZ.SFQ2Header, fTextBold).Indent(headerIndent));
                AddEmptyCells(t, 2);

                foreach (var item in SourceIncomeLevelRegularCodes)
                    RenderQuestion(t, item.Key, FormAML.SourceIncomeLevelRegularCode, item.Value, headerIndent * 2);

                t.Add(new Cell(t, AMLCZ.SFQ2D, fTextNormal).Indent(headerIndent * 2));
                if (AMLModel.AMLForClient)
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, FormAML.SourceIncomeLevelRegularD.VoD(x => x.Client)));
                else AddEmptyCells(t);
                if (AMLModel.IsAmlForPartner)
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, FormAML.SourceIncomeLevelRegularD.VoD(x => x.Partner)));
                else AddEmptyCells(t);

                if (AMLModel.AMLForClient && FormAML.SourceIncomeLevelRegularD.VoD(x => x.Client))
                    t.Add(new Cell(t, AMLCZ.Client + ":" + Space, fTextNormal).Add(new Chunk(FormAML.SourceIncomeLevelRegularOthers.VoD(x => x.Client), fTextNormal)).Indent(headerIndent * 2));
                else AddEmptyCells(t);
                AddEmptyCells(t, 2);
                if (AMLModel.IsAmlForPartner && FormAML.SourceIncomeLevelRegularD.VoD(x => x.Partner))
                    t.Add(new Cell(t, AMLCZ.Partner + ":" + Space, fTextNormal).Add(new Chunk(FormAML.SourceIncomeLevelRegularOthers.VoD(x => x.Partner), fTextNormal)).Indent(headerIndent * 2));
                else AddEmptyCells(t);
                AddEmptyCells(t, 2);
            }));

            table.Add(new Table(table, 16.5, 1.8, 1.8).With(t =>
            {
                t.Add(new Cell(t, AMLCZ.SFQ3Header, fTextBold).Indent(headerIndent));
                AddEmptyCells(t, 2);

                foreach (var item in SourceIncomeLevelIrregulars)
                    RenderQuestion(t, item.Key, item.Value, headerIndent * 2);

                t.Add(new Cell(t, AMLCZ.SFQ3F, fTextNormal).Indent(headerIndent * 2));
                if (AMLModel.AMLForClient)
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, FormAML.SourceIncomeLevelIrregularF.VoD(x => x.Client)));
                else AddEmptyCells(t);
                if (AMLModel.IsAmlForPartner)
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, FormAML.SourceIncomeLevelIrregularF.VoD(x => x.Partner)));
                else AddEmptyCells(t);

                if (AMLModel.AMLForClient && FormAML.SourceIncomeLevelIrregularF.VoD(x => x.Client))
                    t.Add(new Cell(t, AMLCZ.Client + ":" + Space, fTextNormal).Add(new Chunk(FormAML.SourceIncomeLevelIrregularOthers.VoD(x => x.Client), fTextNormal)).Indent(headerIndent * 2));
                else AddEmptyCells(t);
                AddEmptyCells(t, 2);

                if (AMLModel.IsAmlForPartner && FormAML.SourceIncomeLevelIrregularF.VoD(x => x.Partner))
                    t.Add(new Cell(t, AMLCZ.Partner + ":" + Space, fTextNormal).Add(new Chunk(FormAML.SourceIncomeLevelIrregularOthers.VoD(x => x.Partner), fTextNormal)).Indent(headerIndent * 2));
                else AddEmptyCells(t);
                AddEmptyCells(t, 2);
            }));

            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionG(Table table)
        {
            table.Add(new Cell(table, AMLCZ.SGHeader, fTextBold));

            foreach (var item in Risks)
                table.Add(new Cell(table, item, fTextNormal).Indent(textIndent, textIndent * 2));

            AddPaddingLine(table, separatingPadding);
        }

        private void RenderSectionH(Table table)
        {
            table.Add(new Cell(table, IsADKFus ? AMLCZ.SHHeader_ADK : AMLCZ.SHHeader, fTextBold));
            table.Add(new Cell(table, FormAML.OtherRisks, fTextNormal).Indent(textIndent));
            AddPaddingLine(table, separatingPadding);
        }

        private void RenderCountries(Table table)
        {
            table.Add(new Cell(table, AMLCZ.S1FooterHeader, fTextBold));

            table.Add(new Table(table, 17.0, 2).With(t =>
            {
                foreach (var country in RiskCountries)
                {
                    t.Add(new Cell(t, country.Key, fTextNormal).Indent(textIndent, textIndent * 2));
                    t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, country.Value));
                }
            }));

            table.Add(new Cell(table, AMLCZ.S2FooterHeader + Space, fTextBold).Add(new Chunk(AMLCZ.S2FooterHeaderDescription, fTextNormal)));
            table.Add(new Table(table, 17.0, 2).With(t =>
            {
                t.Add(new Cell(t, AMLCZ.S2TaxParadise, fTextNormal).Indent(textIndent));
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, IsTaxParadise()));
            }));

            table.Add(new Cell(table, AMLCZ.S3FooterHeader, fTextBold));
            table.Add(new Cell(table, IsStdFus ? AMLCZ.S3BlackList_Std : AMLCZ.S3BlackList, fTextNormal).PaddingLeft(textPadding).Indent(textIndent));
            table.Add(new Cell(table, AMLCZ.FooterRisk + Space, fTextBold).Add(new Chunk(AMLCZ.FooterRiskUrl, fTextLink)).Add(new Chunk(".", fTextNormal)));
            table.Add(new Cell(table, AMLCZ.Note + Space, fTextBold).Add(new Chunk(AMLCZ.FooterCountriesDescription, fTextNormal)));
            table.Add(new Cell(table, AMLCZ.FooterRiskFactors, fTextBold));

            foreach (var factor in RiskFactors)
                table.Add(new Cell(table, factor, fTextNormal).Indent(0, textIndent));
        }

        private void RenderTrades(Table table)
        {
            table.Add(new Cell(table, AMLCZ.FooterTradesHeader, fTextBold));

            if (IsStdFus)
            {
                foreach (var trade in RiskTradesStd)
                    table.Add(new Cell(table, trade, fTextNormal).Indent(0, textIndent));
            }
            else if (IsZVFus)
            {
                foreach (var trade in RiskTradesZV)
                    table.Add(new Cell(table, trade, fTextNormal).Indent(0, textIndent));
            }
            else
            {
                foreach (var trade in RiskTrades)
                    table.Add(new Cell(table, trade, fTextNormal).Indent(0, textIndent));
            }
            AddPaddingLine(table, separatingPadding);
        }

        private void RenderLinks(Table table)
        {
            table.Add(new Cell(table, AMLCZ.FooterLinksHeader, fTextBold));
            table.Add(new Cell(table, AMLCZ.FooterLinksDescription, fTextNormal));
            table.Add(new Cell(table, AMLCZ.FooterLinksDocuments, fTextNormal));
            table.Add(new Cell(table, AMLCZ.FooterDocumentsLink_1, fTextLink).Indent(textIndent));
            table.Add(new Cell(table, IsZVFus ? AMLCZ.FooterLinksPersons_ZV : AMLCZ.FooterLinksPersons, fTextNormal));

            foreach (var link in PersonLinks)
                table.Add(new Cell(table, link, fTextLink).Indent(textIndent));

            table.Add(new Cell(table, AMLCZ.FooterPEPList, fTextNormal).Indent(textIndent));
            foreach (var link in PersonPEPLinks)
                table.Add(new Cell(table, link, fTextLink).Indent(textIndent));

            table.Add(new Cell(table, AMLCZ.FooterSanctionsList, fTextNormal).Indent(textIndent));
            foreach (var link in PersonSanctionsLinks)
                table.Add(new Cell(table, link, fTextLink).Indent(textIndent));
        }

        private void RenderSignatures(Table table)
        {
            table.Add(new Table(table, 10.0, 7.0, 5.0).With(t =>
            {
                t.Style.BackgroundColor(0xdedede).Leading(1, 1.1).Padding(4).PaddingBottom(5).PaddingTop(1.5).Colspan(1);
                AddEmptyCells(t, 3);
                t.Add(new Cell(t, AMLCZ.Place + Space, fTextBold).Add(new Chunk(FormAML.Place, fTextNormal)));
                t.Add(new Cell(t, AMLCZ.Date + Space, fTextBold).Add(new Chunk(FormAML.SignedDate.HasValue ? FormAML.SignedDate.ToShortDateString() : string.Empty, fTextNormal)));
                AddEmptyCells(t, 4);
            }));
            AddPaddingLine(table, separatingPadding);
        }

        private void SetTablePaddingVertical(Table t, double padding)
        {
            t.Style.PaddingBottom(padding).PaddingTop(padding);
        }

        private static void AddPaddingLine(Table t, float height = 4, int colspan = 1)
        {
            t.Add(new Cell().PaddingBottom(height).Colspan(colspan));
        }

        private static void AddPaddingLine(Table t, int numberOfCells = 1, float height = 4, int colspan = 1)
        {
            for (int i = 0; i < numberOfCells; i++)
                t.Add(new Cell().PaddingBottom(height).Colspan(colspan));
        }

        private static void AddEmptyCells(Table t, int numberOfCells = 1)
        {
            for (int i = 0; i < numberOfCells; i++)
                t.Add(new Cell(t));
        }

        private bool IsTaxParadise()
        {
            return FormAML != null && (FormAML.VatParadiseA || FormAML.VatParadiseB || FormAML.VatParadiseC || FormAML.VatParadiseF || FormAML.VatParadiseG ||
                 FormAML.VatParadiseH || FormAML.VatParadiseK || FormAML.VatParadiseL || FormAML.VatParadiseM || FormAML.VatParadiseN || FormAML.VatParadiseO ||
                  FormAML.VatParadiseP || FormAML.VatParadiseS || FormAML.VatParadiseT);
        }

        private void RenderQuestion(Table t, string question, BoolClientPartner answer, double indent = 0)
        {
            t.Add(new Cell(t, question, fTextNormal).Indent(indent, indent * 2));
            if (AMLModel.AMLForClient)
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, answer != null ? answer.Client : false));
            else
                t.Add(new Cell(t));
            if (AMLModel.IsAmlForPartner)
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, answer != null ? answer.Partner : false));
            else
                t.Add(new Cell(t));

            if (question == AMLCZ.SCQuestion3)
            {
                AddPaddingLine(t, 3, separatingPadding);

                t.Add(new Cell(t, AMLCZ.SCQuestion3Declared + Space, fTextNormal).Add(new Chunk(FormAML.Q3DeclarePurpose, fTextNormal)).Indent(indent, indent * 2));
                t.Add(new Cell(t));
                t.Add(new Cell(t));
                AddPaddingLine(t, 3, separatingPadding);

                t.Add(new Cell(t, AMLCZ.SCQuestion3Real + Space, fTextNormal).Add(new Chunk(FormAML.Q3ActualPurpose, fTextNormal)).Indent(indent, indent * 2));
                t.Add(new Cell(t));
                t.Add(new Cell(t));
                AddPaddingLine(t, 3, separatingPadding);
            }
        }

        private void RenderQuestion(Table t, string question, StringClientPartner answer, string code, double indent = 0)
        {
            t.Add(new Cell(t, question, fTextNormal).Indent(indent, indent * 2));
            if (AMLModel.AMLForClient)
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, answer != null ? answer.Client == code : false));
            else
                t.Add(new Cell(t));
            if (AMLModel.IsAmlForPartner)
                t.Add(CreateCheckbox(t, Element.ALIGN_CENTER, answer != null ? answer.Partner == code : false));
            else
                t.Add(new Cell(t));
        }

        private Cell CreateCheckbox(Table style = null, int align = Element.ALIGN_CENTER, bool value = false, int verticalAlign = Element.ALIGN_UNDEFINED, BaseColor color = null)
        {
            color = color == null ? BaseColor.BLACK : BaseColor.WHITE;
            Font chbFont = new Font(chbfont, 11, Font.NORMAL, color);
            return CreateCheckbox(chbFont, value, style, align, verticalAlign);
        }

        private Cell CreateCheckbox(iTextSharp.text.Font font, bool value, Table style = null, int align = Element.ALIGN_CENTER, int verticalAlign = Element.ALIGN_UNDEFINED)
        {
            string trueSymbol = "S"; // v danem fontu je toto zaskrtnuty checkbox
            string falseSymbol = "£"; // v danem fontu je toto nezaskrtnuty checkbox

            double chbPadding = 5.2;

            Cell checkbox = new Cell(style);
            checkbox.Add(new Chunk(value ? trueSymbol : falseSymbol, font));
            checkbox.Leading(0, 0.5f).Align(align).PaddingTop(chbPadding);
            checkbox.C.VerticalAlignment = verticalAlign;

            return checkbox;
        }
    }
}