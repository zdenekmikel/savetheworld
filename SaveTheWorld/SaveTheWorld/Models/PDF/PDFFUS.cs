﻿using iTextSharp.text.pdf;
using SaveTheWorld.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static SaveTheWorld.Models.FusHelper;

namespace SaveTheWorld.Models.PDF
{
    public class PDFFUS : PDFBase
    {
        #region Properties
        //public bool FFP = false;
        public string ImportedFUSXML { get; private set; }
        public string errorString { get; set; }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        public PDFFUS() : base() { }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region LoadFUS
        /// <summary>
        /// Vytáhne definici z db
        /// </summary>
        /// <param name="fUSID"></param>
        /// <param name="serverPath"></param>
        /// <param name="pdfType"></param>
        public void LoadFUS(int fusID, string serverPath, string pdfType)
        {
            this.PdfType = pdfType;

            var fus = DbProvider.CustomerFUS(0, 0, (int)fusID);
            
            if (fus != null && !string.IsNullOrWhiteSpace(fus.XmlContent))
            {
                var model = fus.XmlContent.DeSearialize<FormFUSCZ>();
                if (model != null)
                {
                    this.FormFUSCZ = model;
                    this.FormFUSCZ.FUSID = model.FUSID;
                    this.FormFUSCZ.FUSType = fus.FUSTypeCode;
                }
            }
            else
            {
                this.IsValid = false;
            }

            LoadPDF(serverPath, pdfType);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        //public void LoadFUS(FUS fus, FormFUSCZ formFUSCZ, string serverPath, string pdfType) {
        //	if (fus == null)
        //		throw new ArgumentNullException("fus");
        //	if (formFUSCZ == null)
        //		throw new ArgumentNullException("formFUSCZ");

        //	if (formFUSCZ != null)
        //	{
        //		this.FormFUSCZ = formFUSCZ;
        //		this.FormFUSCZ.FUSID = fus.FUSID;
        //		this.FormFUSCZ.FUSType = fus.FUSTypeCode;
        //	}
        //	else
        //	{
        //		this.IsValid = false;
        //	}

        //	this.LoadPDF(serverPath, pdfType);
        //}
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void LoadPDF(string serverPath, string pdfType)
        {
            if (this.IsValid)
            {
                string fileName = string.Empty;

                if (pdfType == Constants.FUSPDFTypes.Draft)
                {
                    fileName = Path.GetFileName(this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV ? Files.ZV.Draft : this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ADK ? Files.ADK.Draft : Files.FUS.Draft);
                }
                else
                {
                    fileName = Path.GetFileName(this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV ? Files.ZV.Raw : this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ADK ? Files.ADK.Raw : Files.FUS.Raw);
                }

                this.FilePath = Path.Combine(serverPath, fileName);
                if (this.FormFUSCZ.Client != null)
                    this.PasswordParty = this.FormFUSCZ.Client.PartyCode.ValueOrDefault(v => v.Replace("/", "").Replace(" ", "")) ?? FormFUSCZ.Client.TradeLicenceNo.ValueOrDefault(v => v.Replace("/", "").Replace(" ", ""));
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region GeneratePdf
        public bool GeneratePdf()
        {
            if (!this.IsValid || this.FormFUSCZ == null) return false;

            using (var ms = new MemoryStream())
            {
                PdfReader reader = new PdfReader(this.FilePath);

                using (PdfStamper stamper = new PdfStamper(reader, ms, '\n', false)) // last TRUE is appent and very important for leaving document editable
                {

                    if (this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ADK)
                    {
                        this.Write_Part1_ADK(stamper.AcroFields);
                    }
                    else
                    {
                        this.Write_Part1(stamper.AcroFields);
                    }

                    if (this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ZV || this.FormFUSCZ.FUSType == Constants.FUSCZTypes.SMSZV)
                    {
                        this.Write_Part2_ZV(stamper.AcroFields);
                        this.Write_Part3_ZV(stamper.AcroFields);
                        this.Write_Part4_ZV(stamper.AcroFields);
                        this.Write_Part5_ZV(stamper.AcroFields);
                        this.Write_Part6_ZV(stamper.AcroFields);

                        this.Write_Part8_ZV(stamper.AcroFields);
                        if (PdfType == Constants.FUSPDFTypes.Draft)
                            FillEANZVDraft(reader, stamper);
                        else
                            FillEANZV(reader, stamper);
                    }
                    else
                    {
                        this.Write_Part2_STD(stamper.AcroFields);
                        this.Write_Part3_STD(stamper.AcroFields);
                        this.Write_Part4_STD(stamper.AcroFields);
                        this.Write_Part5_STD(stamper.AcroFields);
                        this.Write_Part6_STD(stamper.AcroFields);
                        this.Write_Part7_STD(stamper.AcroFields);
                        this.Write_Part8_STD(stamper.AcroFields);
                        if (this.FormFUSCZ.FUSType == Constants.FUSCZTypes.ADK)
                        {
                            FillEANADK(reader, stamper);
                        }
                        else
                        {
                            FillEAN(reader, stamper);
                        }
                    }
                }

                this.FinalPdfMemory = ms.ToArray();

                reader.Close();
            }

            return true;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part1
        /// <summary>
        /// I. ÚDAJE O ZPROSTŘEDKOVATELÍCH
        /// </summary>
        /// <param name="ff"></param>
        void Write_Part1(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.AgentDetail == null) return;

            ff.MySetField(DynFields.FUS.TextFields.T1, this.FormFUSCZ.AgentDetail.LastName);
            ff.MySetField(DynFields.FUS.TextFields.T2, this.FormFUSCZ.AgentDetail.FirstName);
            ff.MySetField(DynFields.FUS.TextFields.T3, this.FormFUSCZ.AgentDetail.PartyCode); // ičo
            ff.MySetField(DynFields.FUS.TextFields.T4, this.FormFUSCZ.AgentDetail.AgentNo); // ev číslo
            ff.MySetField(DynFields.FUS.TextFields.T5, this.FormFUSCZ.AgentDetail.Address); // sídlo, bydliště
        }

        void Write_Part1_ADK(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.AgentDetail == null) return;

            ff.MySetField(DynFields.FUS.TextFields.T1, this.FormFUSCZ.AgentDetail.FirstName + " " + this.FormFUSCZ.AgentDetail.LastName);
            ff.MySetField(DynFields.FUS.TextFields.T2, this.FormFUSCZ.AgentDetail.AgentNo);
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part2
        /// <summary>
        /// II. Informace a prohlášení
        /// </summary>
        /// <param name="ff"></param>
        void Write_Part2_STD(AcroFields ff)
        {
            if (this.FormFUSCZ == null) return;

            if (this.FormFUSCZ.Announcement == null)
            {
                if (this.FormFUSCZ.FUSForClient)
                {
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r1, false);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r2, false);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r3, false);
                }
                if (this.FormFUSCZ.FUSForPartner)
                {
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r4, false);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r5, false);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r6, false);
                }
                if (this.FormFUSCZ.FUSForChild)
                {
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r7, false);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r8, false);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r9, false);
                }
                return;
            }

            if (this.FormFUSCZ.FUSForClient)
            {
                if (this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r1,
                        this.FormFUSCZ.FSBuildSaving ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Client : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r1, false);

                if (this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r2,
                        (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r2, false);

                if (this.FormFUSCZ.Announcement.AnnouncementLeases != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r3,
                        this.FormFUSCZ.FSLeasingLoan ? this.FormFUSCZ.Announcement.AnnouncementLeases.Client : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r3, false);

                if (this.FormFUSCZ.Announcement.AnnouncementAct != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r10, this.FormFUSCZ.Announcement.AnnouncementAct.Client);
            }

            if (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation)
            {
                if (this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r4,
                        this.FormFUSCZ.FSBuildSaving ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Partner : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r4, false);

                if (this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r5,
                        (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Partner : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r5, false);

                if (this.FormFUSCZ.Announcement.AnnouncementLeases != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r6,
                        this.FormFUSCZ.FSLeasingLoan ? this.FormFUSCZ.Announcement.AnnouncementLeases.Partner : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r6, false);

                if (this.FormFUSCZ.Announcement.AnnouncementAct != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r11, this.FormFUSCZ.Announcement.AnnouncementAct.Partner);
            }

            if (this.FormFUSCZ.FUSForChild)
            {
                if (this.FormFUSCZ.Announcement.AnnouncementBuildSaving != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r7,
                        this.FormFUSCZ.FSBuildSaving ? this.FormFUSCZ.Announcement.AnnouncementBuildSaving.Child : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r7, false);

                if (this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r8,
                        (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) ? this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Child : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r8, false);

                if (this.FormFUSCZ.Announcement.AnnouncementLeases != null)
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r9,
                        this.FormFUSCZ.FSLeasingLoan ? this.FormFUSCZ.Announcement.AnnouncementLeases.Child : false);
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r9, false);
            }

            if ((this.FormFUSCZ.FUSForClient || this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.FUSForChild) && this.FormFUSCZ.Announcement.AnnouncementExpone != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r12, this.FormFUSCZ.Announcement.AnnouncementExpone);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Write_Part2_ZV(AcroFields ff)
        {
            if (this.FormFUSCZ == null) return;

            if (this.FormFUSCZ.Announcement == null)
            {
                if (this.FormFUSCZ.FUSForClient)
                {
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r6, false);

                    // 2015-06-02 - DT - if field r6 is false these two fields shouldnt be set
                    //ff.MySetField(DynFields.ZV.RadioButtonFields.r7, false);
                    //ff.MySetField(DynFields.ZV.RadioButtonFields.r8, false);
                }
                return;
            }

            if (this.FormFUSCZ.FUSForClient)
            {
                //2015-05-25 - DT - změna, protože se vyplnily závislé čekboxy, i když neměly
                bool? set = (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal) && this.FormFUSCZ.Announcement.AnnouncementLiveSaving != null ?
                    this.FormFUSCZ.Announcement.AnnouncementLiveSaving.Client : false;

                ff.MySetField(DynFields.ZV.RadioButtonFields.r6, set);

                if (!set.HasValue || !set.Value) return;

                if (this.FormFUSCZ.Announcement.AnnouncementAct != null)
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r7, this.FormFUSCZ.Announcement.AnnouncementAct.Client);
                else
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r7, false);

                if (this.FormFUSCZ.Announcement.AnnouncementExpone != null)
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r8, this.FormFUSCZ.Announcement.AnnouncementExpone);
                else
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r8, false);
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part3
        /// <summary>III. ÚDAJE O ZÁKAZNÍKOVI/PARTNEROVI/DÍTĚTI (DĚTECH)</summary>
        /// <param name="ff"></param>
        void Write_Part3_STD(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.OverallInfo == null) return;

            ff.MySetField(DynFields.FUS.RadioButtonFields.r14, this.FormFUSCZ.OverallInfo.StatementClientCode);

            if (this.FormFUSCZ.OverallInfo.StatementClientCode == Constants.FUSListOptions.WASNTSPECIFIED)
            {
                ff.MySetField(DynFields.FUS.TextFields.T7, this.FormFUSCZ.OverallInfo.LastName);
                ff.MySetField(DynFields.FUS.TextFields.T8, this.FormFUSCZ.OverallInfo.FirstName);
                ff.MySetField(DynFields.FUS.TextFields.T9, this.FormFUSCZ.OverallInfo.BirthDate.HasValue ? FormFUSCZ.OverallInfo.BirthDate.Value.ToString("dd'/'MM'/'yyyy") : string.Empty);
            }

            if (this.FormFUSCZ.Client != null)
            {
                if (!string.IsNullOrEmpty(this.FormFUSCZ.Client.ClientIDEnc))
                {
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r15, true);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r16, this.FormFUSCZ.Client.AllowEdit);
                }
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r15, false);
            }

            if (this.FormFUSCZ.Partner != null)
            {
                if (!string.IsNullOrEmpty(this.FormFUSCZ.Partner.ClientIDEnc))
                {
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r17, true);
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r18, this.FormFUSCZ.Partner.AllowEdit);
                }
                else
                    ff.MySetField(DynFields.FUS.RadioButtonFields.r17, false);
            }

            this.FormFUSCZ.Client.Form_Fill(ff,
                this.FormFUSCZ.OverallInfo.ClientType == SealedConstants.PersonType.Corporation.Value,
                DynFields.FUS.RadioButtonFields.r19,
                DynFields.FUS.RadioButtonFields.r20,
                DynFields.FUS.TextFields.T10,
                DynFields.FUS.TextFields.T11,
                DynFields.FUS.TextFields.T12,
                DynFields.FUS.TextFields.T13,
                DynFields.FUS.TextFields.T14,
                DynFields.FUS.TextFields.T15,
                DynFields.FUS.TextFields.T16,
                DynFields.FUS.TextFields.T17,
                DynFields.FUS.TextFields.T18,
                DynFields.FUS.TextFields.T19,
                DynFields.FUS.TextFields.T20,
                DynFields.FUS.TextFields.T22,
                DynFields.FUS.TextFields.T23,
                DynFields.FUS.TextFields.T24,
                DynFields.FUS.TextFields.T26,
                DynFields.FUS.TextFields.T27,
                DynFields.FUS.TextFields.T28,
                DynFields.FUS.TextFields.T29,
                DynFields.FUS.TextFields.T30,
                DynFields.FUS.TextFields.T31,
                null,
                this.FormFUSCZ.OverallInfo.ClientType);

            if (this.FormFUSCZ.Partner != null && (this.FormFUSCZ.FUSForPartner || this.FormFUSCZ.ClientType() == SealedConstants.PersonType.Corporation))
            {
                this.FormFUSCZ.Partner.Form_Fill(ff,
                    this.FormFUSCZ.OverallInfo.PartnerType == SealedConstants.PersonType.Corporation.Value,
                    DynFields.FUS.RadioButtonFields.r21,
                    DynFields.FUS.RadioButtonFields.r22,
                    DynFields.FUS.TextFields.T32,
                    DynFields.FUS.TextFields.T33,
                    DynFields.FUS.TextFields.T34,
                    DynFields.FUS.TextFields.T35,
                    DynFields.FUS.TextFields.T36,
                    DynFields.FUS.TextFields.T37,
                    DynFields.FUS.TextFields.T38,
                    DynFields.FUS.TextFields.T39,
                    DynFields.FUS.TextFields.T40,
                    DynFields.FUS.TextFields.T41,
                    DynFields.FUS.TextFields.T42,
                    DynFields.FUS.TextFields.T44,
                    DynFields.FUS.TextFields.T45,
                    DynFields.FUS.TextFields.T46,
                    DynFields.FUS.TextFields.T48,
                    DynFields.FUS.TextFields.T49,
                    DynFields.FUS.TextFields.T50,
                    DynFields.FUS.TextFields.T51,
                    DynFields.FUS.TextFields.T52,
                    DynFields.FUS.TextFields.T53,
                    null,
                    this.FormFUSCZ.OverallInfo.PartnerType);
            }

            if (this.FormFUSCZ.Childs == null || this.FormFUSCZ.Childs.Count == 0) return;

            if (this.FormFUSCZ.Childs.Count > 0)
                this.FormFUSCZ.Childs[0].Form_Fill(ff,
                    DynFields.FUS.RadioButtonFields.r23,
                    DynFields.FUS.TextFields.T54,
                    DynFields.FUS.TextFields.T55,
                    DynFields.FUS.TextFields.T56,
                    DynFields.FUS.TextFields.T57,
                    DynFields.FUS.TextFields.T58,
                    DynFields.FUS.TextFields.T59,
                    DynFields.FUS.TextFields.T60,
                    DynFields.FUS.TextFields.T61,
                    DynFields.FUS.TextFields.T62,
                    DynFields.FUS.TextFields.T63,
                    DynFields.FUS.TextFields.T65);

            if (this.FormFUSCZ.Childs.Count > 1)
                this.FormFUSCZ.Childs[1].Form_Fill(ff,
                    DynFields.FUS.RadioButtonFields.r24,
                    DynFields.FUS.TextFields.T66,
                    DynFields.FUS.TextFields.T67,
                    DynFields.FUS.TextFields.T68,
                    DynFields.FUS.TextFields.T69,
                    DynFields.FUS.TextFields.T70,
                    DynFields.FUS.TextFields.T71,
                    DynFields.FUS.TextFields.T72,
                    DynFields.FUS.TextFields.T73,
                    DynFields.FUS.TextFields.T74,
                    DynFields.FUS.TextFields.T75,
                    DynFields.FUS.TextFields.T77);

            if (this.FormFUSCZ.Childs.Count > 2)
                this.FormFUSCZ.Childs[2].Form_Fill(ff,
                    DynFields.FUS.RadioButtonFields.r25,
                    DynFields.FUS.TextFields.T78,
                    DynFields.FUS.TextFields.T79,
                    DynFields.FUS.TextFields.T80,
                    DynFields.FUS.TextFields.T81,
                    DynFields.FUS.TextFields.T82,
                    DynFields.FUS.TextFields.T83,
                    DynFields.FUS.TextFields.T84,
                    DynFields.FUS.TextFields.T85,
                    DynFields.FUS.TextFields.T86,
                    DynFields.FUS.TextFields.T87,
                    DynFields.FUS.TextFields.T89);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Write_Part3_ZV(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.OverallInfo == null) return;

            if (this.FormFUSCZ.Client != null)
            {
                if (!string.IsNullOrEmpty(this.FormFUSCZ.Client.ClientIDEnc))
                {
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r9, true);
                    //ff.MySetField(DynFields.FUS.RadioButtonFields.r16, this.FormFUSCZ.Client.AllowEdit);
                }
                else
                    ff.MySetField(DynFields.ZV.RadioButtonFields.r9, false);
            }

            // TODO r9
            this.FormFUSCZ.Client.Form_Fill(ff,
                this.FormFUSCZ.OverallInfo.ClientType == SealedConstants.PersonType.Corporation.Value,
                DynFields.ZV.RadioButtonFields.r10,
                DynFields.ZV.RadioButtonFields.r_X,
                DynFields.ZV.TextFields.T7,
                DynFields.ZV.TextFields.T8,
                DynFields.ZV.TextFields.T9,
                DynFields.ZV.TextFields.T10,
                DynFields.ZV.TextFields.T11,
                DynFields.ZV.TextFields.T12,
                DynFields.ZV.TextFields.T13,
                DynFields.ZV.TextFields.T14,
                DynFields.ZV.TextFields.T15,
                DynFields.ZV.TextFields.T16,
                DynFields.ZV.TextFields.T17,
                DynFields.ZV.TextFields.T19,
                DynFields.ZV.TextFields.T20,
                DynFields.ZV.TextFields.T21,
                DynFields.ZV.TextFields.T23,
                DynFields.ZV.TextFields.T24,
                DynFields.ZV.TextFields.T25,
                DynFields.ZV.TextFields.T26,
                DynFields.ZV.TextFields.T27,
                DynFields.ZV.TextFields.T28,
                DynFields.ZV.TextFields.T112,
                this.FormFUSCZ.OverallInfo.ClientType);
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part4
        /// <summary>IV. OVĚŘENÍ ÚDAJŮ</summary>
        /// <param name="ff"></param>
        void Write_Part4_STD(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinancialDataVerification == null || (!this.FormFUSCZ.FSInsuranceLive && !this.FormFUSCZ.FSSavingDS && !this.FormFUSCZ.FSSavingDPS))
                return;

            if (this.FormFUSCZ.FinancialDataVerification.RefuseInformation.LogicOR)
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb91, true);

            if (this.FormFUSCZ.FUSForClient && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Client)
                this.Part4_FillClient_STD(ff);
            if (this.FormFUSCZ.FUSForPartner && !this.FormFUSCZ.FinancialDataVerification.RefuseInformation.Partner)
                this.Part4_FillPartner(ff);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Write_Part4_ZV(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinancialDataVerification == null || (!this.FormFUSCZ.FSInsuranceLive && !this.FormFUSCZ.FSSavingDS && !this.FormFUSCZ.FSSavingDPS))
                return;

            if (this.FormFUSCZ.FinancialDataVerification.RefuseInformation.LogicOR)
                ff.MySetField(DynFields.ZV.CheckBoxFields.chb30, true);
            else if (this.FormFUSCZ.FUSForClient)
                this.Part4_FillClient_ZV(ff);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Part4_FillClient_STD
        void Part4_FillClient_STD(AcroFields ff)
        {
            List<string> codes = new List<string>();

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r26, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r28, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r30, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestTools.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r32, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r34, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.ValueOrDefault(v => v.Client));
            }

            int? sum = null;
            if (codes.Any(p => p != null))
                sum = FUShelper.FUSListOptionsResource.Where(p => codes.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

            if (sum.HasValue)
                ff.MySetField(DynFields.FUS.TextFields.T92, sum.ToString());
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Part4_FillClient_ZV
        void Part4_FillClient_ZV(AcroFields ff)
        {
            List<string> codes = new List<string>();

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client != null)
            {
                ff.MySetField(DynFields.ZV.RadioButtonFields.r11, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client != null)
            {
                ff.MySetField(DynFields.ZV.RadioButtonFields.r12, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestTools.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client != null)
            {
                ff.MySetField(DynFields.ZV.RadioButtonFields.r13, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client != null)
            {
                ff.MySetField(DynFields.ZV.RadioButtonFields.r14, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.ValueOrDefault(v => v.Client));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client != null)
            {
                ff.MySetField(DynFields.ZV.RadioButtonFields.r15, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Client);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.ValueOrDefault(v => v.Client));
            }

            int? sum = null;
            if (codes.Any(p => p != null))
                sum = FUShelper.FUSListOptionsResource.Where(p => codes.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

            if (sum.HasValue)
                ff.MySetField(DynFields.ZV.TextFields.T29, sum.ToString());
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Part4_FillPartner
        void Part4_FillPartner(AcroFields ff)
        {
            List<string> codes = new List<string>();

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Partner != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r27, this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.Partner);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestGoal.ValueOrDefault(v => v.Partner));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Partner != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r29, this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.Partner);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestHorizont.ValueOrDefault(v => v.Partner));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r31, this.FormFUSCZ.FinancialDataVerification.QEInvestTools.Partner);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestTools.ValueOrDefault(v => v.Partner));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r33, this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.Partner);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestExperience.ValueOrDefault(v => v.Partner));
            }

            if (this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner != null)
            {
                ff.MySetField(DynFields.FUS.RadioButtonFields.r35, this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.Partner);
                codes.Add(this.FormFUSCZ.FinancialDataVerification.QEInvestRisk.ValueOrDefault(v => v.Partner));
            }

            int? sum = null;
            if (codes.Any(p => p != null))
                sum = FUShelper.FUSListOptionsResource.Where(p => codes.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

            if (sum.HasValue)
                ff.MySetField(DynFields.FUS.TextFields.T93, sum.ToString());
        }
        #endregion
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part5
        /// <summary>V. ZPROSTŘEDKOVÁNÍ DŮCHODOVÉHO SPOŘENÍ (DS) A/NEBO DOPLŇKOVÉHO PENZIJNÍHO SPOŘENÍ (DPS)</summary>
        /// <param name="ff"></param>
        void Write_Part5_STD(AcroFields ff)
        {
            bool enabled = this.FormFUSCZ != null && this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FSSavingDS || this.FormFUSCZ.FSSavingDPS || this.FormFUSCZ.FSSavingIncreaseDPS || this.FormFUSCZ.FSSavingTFToDPS || this.FormFUSCZ.FSSavingIncreaseTF);

            ff.MySetField(DynFields.FUS.RadioButtonFields.r36, enabled);

            if (!enabled) return;

            ff.MySetField(DynFields.FUS.CheckBoxFields.r37_1_X, this.FormFUSCZ.FinancialDsDps.ClientPartnerRefuse);

            List<int> dpServices = DbProvider.Services().Where(p => p.CategoryID == 100028).Select(p => p.ServiceID).ToList();

            //if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDS)
            //{
            //	ff.MySetField(DynFields.FUS.CheckBoxFields.r37_2, this.FormFUSCZ.FinancialDsDps.ClientStrategyDS);

            //	ff.MySetField(DynFields.FUS.RadioButtonFields.r38, this.FormFUSCZ.FinancialDsDps.ClientStrategyDSOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.FUS.TextFields.T94))
            //		{
            //			ff.MySetField(DynFields.FUS.TextFields.T222, this.FormFUSCZ.FinancialDsDps.ClientDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.FUS.TextFields.T223, this.FormFUSCZ.FinancialDsDps.ClientDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}

            //if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyDS)
            //{
            //	ff.MySetField(DynFields.FUS.CheckBoxFields.r37_3, this.FormFUSCZ.FinancialDsDps.PartnerStrategyDS);

            //	ff.MySetField(DynFields.FUS.RadioButtonFields.r39, this.FormFUSCZ.FinancialDsDps.PartnerStrategyDSOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var partnerSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Partner.ClientTempID && dpServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, partnerSrvs, DynFields.FUS.TextFields.T95))
            //		{
            //			ff.MySetField(DynFields.FUS.TextFields.T222, this.FormFUSCZ.FinancialDsDps.ClientDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.FUS.TextFields.T223, this.FormFUSCZ.FinancialDsDps.ClientDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}

            List<int> dpsServices = DbProvider.Services().Where(p => p.CategoryID == 100029 || p.CategoryID == 10102).Select(p => p.ServiceID).ToList();
            List<int> ppServices = DbProvider.Services().Where(p => p.CategoryID == 10102).Select(p => p.ServiceID).ToList();

            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_2_X, true);

                ff.MySetField(DynFields.FUS.RadioButtonFields.r38, this.FormFUSCZ.FinancialDsDps.ClientStrategyDPSOptions);
            }
            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_4_X, true);

                ff.MySetField(DynFields.FUS.RadioButtonFields.r40, this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPSOptions);
            }
            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_6_X, true);

                ff.MySetField(DynFields.FUS.RadioButtonFields.r42, this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPSOptions);
            }
            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_8_X, true);
            }

            if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPS)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_3_X, true);

                ff.MySetField(DynFields.FUS.RadioButtonFields.r39, this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPSOptions);
            }
            else if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFToDPS)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_5_X, true);

                ff.MySetField(DynFields.FUS.RadioButtonFields.r41, this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFToDPSOptions);

            }
            else if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseDPS)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_7_X, true);

                ff.MySetField(DynFields.FUS.RadioButtonFields.r43, this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseDPSOptions);
            }
            else if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseTF)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.r37_9_X, true);
            }

            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS
                || this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS
                || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS
                || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF
                || this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPS
                || this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFToDPS
                || this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseDPS
                || this.FormFUSCZ.FinancialDsDps.PartnerStrategyIncreaseTF)
            {
                if (this.FormFUSCZ.Sectors != null)
                {
                    List<IEnumerable<Sector>> childSectors = new List<IEnumerable<Sector>>();
                    if (this.FormFUSCZ.Childs != null)
                    {
                        foreach (var child in this.FormFUSCZ.Childs)
                        {
                            childSectors.Add(this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == child.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value))));
                        }
                    }
                    //foreach (var sec)

                    var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value)));
                    if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.FUS.TextFields.T94, childSectors))
                    {
                        ff.MySetField(DynFields.FUS.TextFields.T222, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
                        ff.MySetField(DynFields.FUS.TextFields.T223, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
                    }
                }

                if (this.FormFUSCZ.Sectors != null && this.FormFUSCZ.Partner != null)
                {
                    var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Partner.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value)));
                    if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.FUS.TextFields.T95))
                    {
                        ff.MySetField(DynFields.FUS.TextFields.T222, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
                        ff.MySetField(DynFields.FUS.TextFields.T223, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
                    }
                }
            }




            //if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPS)
            //{
            //	ff.MySetField(DynFields.FUS.RadioButtonFields.x37, "DPS");

            //	ff.MySetField(DynFields.FUS.RadioButtonFields.r41, this.FormFUSCZ.FinancialDsDps.PartnerStrategyDPSOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Partner.ClientTempID && dpsServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.FUS.TextFields.T97))
            //		{
            //			ff.MySetField(DynFields.FUS.TextFields.T224, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.FUS.TextFields.T225, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}
            //else if (this.FormFUSCZ.FinancialDsDps.PartnerStrategyTF)
            //{
            //	ff.MySetField(DynFields.FUS.RadioButtonFields.x37, "TF");

            //	ff.MySetField(DynFields.FUS.RadioButtonFields.r43, this.FormFUSCZ.FinancialDsDps.PartnerStrategyTFOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Partner.ClientTempID && dpsServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.FUS.TextFields.T97))
            //		{
            //			ff.MySetField(DynFields.FUS.TextFields.T224, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.FUS.TextFields.T225, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Write_Part5_ZV(AcroFields ff)
        {
            bool enabled = this.FormFUSCZ != null && this.FormFUSCZ.FinancialDsDps != null && (this.FormFUSCZ.FSSavingDS || this.FormFUSCZ.FSSavingDPS || this.FormFUSCZ.FSSavingIncreaseDPS || this.FormFUSCZ.FSSavingTFToDPS || this.FormFUSCZ.FSSavingIncreaseTF);

            ff.MySetField(DynFields.ZV.RadioButtonFields.r16, enabled);

            if (!enabled) return;

            ff.MySetField(DynFields.ZV.CheckBoxFields.r37_1_X, this.FormFUSCZ.FinancialDsDps.ClientPartnerRefuse);

            List<int> dpServices = DbProvider.Services().Where(p => p.CategoryID == 100028).Select(p => p.ServiceID).ToList();

            //if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDS)
            //{
            //	ff.MySetField(DynFields.ZV.CheckBoxFields.chb33, this.FormFUSCZ.FinancialDsDps.ClientStrategyDS);

            //	ff.MySetField(DynFields.ZV.RadioButtonFields.r17, this.FormFUSCZ.FinancialDsDps.ClientStrategyDSOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.ZV.TextFields.T34))
            //		{
            //			ff.MySetField(DynFields.ZV.TextFields.T35, this.FormFUSCZ.FinancialDsDps.ClientDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.ZV.TextFields.T36, this.FormFUSCZ.FinancialDsDps.ClientDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}

            List<int> dpsServices = DbProvider.Services().Where(p => p.CategoryID == 100029).Select(p => p.ServiceID).ToList();
            List<int> ppServices = DbProvider.Services().Where(p => p.CategoryID == 10102).Select(p => p.ServiceID).ToList();

            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS)
            {
                ff.MySetField(DynFields.ZV.CheckBoxFields.r37_2_X, true);

                ff.MySetField(DynFields.ZV.RadioButtonFields.r17, this.FormFUSCZ.FinancialDsDps.ClientStrategyDPSOptions);
            }
            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS)
            {
                ff.MySetField(DynFields.ZV.CheckBoxFields.r37_3_X, true);

                ff.MySetField(DynFields.ZV.RadioButtonFields.r19, this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPSOptions);
            }
            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS)
            {
                ff.MySetField(DynFields.ZV.CheckBoxFields.r37_4_X, true);

                ff.MySetField(DynFields.ZV.RadioButtonFields.r20, this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPSOptions);
            }
            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF)
            {
                ff.MySetField(DynFields.ZV.CheckBoxFields.r37_5_X, true);
            }

            if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS
                || this.FormFUSCZ.FinancialDsDps.ClientStrategyTFToDPS
                || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseDPS
                || this.FormFUSCZ.FinancialDsDps.ClientStrategyIncreaseTF)
            {
                if (this.FormFUSCZ.Sectors != null)
                {

                    var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && (dpsServices.Contains(p.ProductID.Value) || ppServices.Contains(p.ProductID.Value)));
                    if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.ZV.TextFields.T34))
                    {
                        ff.MySetField(DynFields.ZV.TextFields.T35, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
                        ff.MySetField(DynFields.ZV.TextFields.T36, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
                    }
                }
            }

            //if (this.FormFUSCZ.FinancialDsDps.ClientStrategyDPS)
            //{
            //	ff.MySetField(DynFields.ZV.RadioButtonFields.r18, "DPS");

            //	ff.MySetField(DynFields.ZV.RadioButtonFields.r19, this.FormFUSCZ.FinancialDsDps.ClientStrategyDPSOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpsServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.ZV.TextFields.T37))
            //		{
            //			ff.MySetField(DynFields.ZV.TextFields.T38, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.ZV.TextFields.T39, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}
            //else if (this.FormFUSCZ.FinancialDsDps.ClientStrategyTF)
            //{
            //	ff.MySetField(DynFields.ZV.RadioButtonFields.r18, "TF");

            //	ff.MySetField(DynFields.ZV.RadioButtonFields.r20, this.FormFUSCZ.FinancialDsDps.ClientStrategyTFOptions);

            //	if (this.FormFUSCZ.Sectors != null)
            //	{
            //		var clientSrvs = this.FormFUSCZ.Sectors.Where(p => p.ProductID != null && p.ClientTempID == this.FormFUSCZ.Client.ClientTempID && dpsServices.Contains(p.ProductID.Value));
            //		if (this.Part5_FillInstitution(ff, clientSrvs, DynFields.ZV.TextFields.T37))
            //		{
            //			ff.MySetField(DynFields.ZV.TextFields.T38, this.FormFUSCZ.FinancialDsDps.PartnerDSDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //			ff.MySetField(DynFields.ZV.TextFields.T39, this.FormFUSCZ.FinancialDsDps.PartnerDSTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            //		}
            //	}
            //}
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        bool Part5_FillInstitution(AcroFields ff, IEnumerable<Sector> sectors, FATextValue textField/*, FATextValue dateF, FATextValue timeF*/, List<IEnumerable<Sector>> childSectors = null)
        {
            if ((sectors == null || sectors.Count() == 0) && (childSectors == null || childSectors.Count == 0)) return false;

            string institution = "";
            string origInstitution = "";

            if (sectors != null && sectors.Count() != 0)
            {
                var sector = sectors.ElementAt(0);
                institution = this.GetInstitution(sector.InstitutionID);
                origInstitution = institution;
            }


            if (childSectors != null && childSectors.Count > 0)
                foreach (var childsector in childSectors)
                {
                    if (childsector.Count() > 0)
                    {
                        string childInstitution = this.GetInstitution(childsector.ElementAt(0).InstitutionID);
                        if (!institution.Contains(childInstitution))
                            institution += ", " + childInstitution;

                        if (string.IsNullOrEmpty(origInstitution))
                        {
                            institution = institution.Remove(0, 2);
                        }
                    }
                }

            ff.MySetField(textField, institution);
            //ff.MySetField(dateF, sector.DateSigned.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            //ff.MySetField(timeF, sector.DateSigned.ValueOrDefault(v => v.Value.ToString("HH:mm")));

            return true;
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part6
        /// <summary>VI. ZPROSTŘEDKOVÁNÍ POJIŠTĚNÍ</summary>
        void Write_Part6_STD(AcroFields ff)
        {
            var financialInsurance = FUShelper.FIns(this.FormFUSCZ); // this.FormFUSCZ.FinancialInsurance

            bool enabled = this.FormFUSCZ != null && financialInsurance != null && (this.FormFUSCZ.FSInsuranceLive ||
                this.FormFUSCZ.FSInsuranceLiveOther ||
                this.FormFUSCZ.FSInsurancePersonal ||
                this.FormFUSCZ.FSInsuranceProperty ||
                this.FormFUSCZ.FSInsuranceLiability ||
                this.FormFUSCZ.FSInsuranceTravel ||
                this.FormFUSCZ.FSInsuranceOthers);

            ff.MySetField(DynFields.FUS.RadioButtonFields.r44, enabled); // || this.FormFUSCZ.SectorInsurances.Count > 0

            if (!enabled) return;

            if (this.FormFUSCZ.AgentDetail != null)
                ff.MySetField(DynFields.FUS.TextFields.T98, this.FormFUSCZ.AgentDetail.RegistrationNumber); // čnb

            #region 1. pojištění osob
            if (/*this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal &&*/ financialInsurance.InsuranceIndividual)
            {
                if (financialInsurance.IIDeath != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb99, financialInsurance.IIDeath.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb100, financialInsurance.IIDeath.Partner);
                }

                if (financialInsurance.IIDeathInjury != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb101, financialInsurance.IIDeathInjury.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb102, financialInsurance.IIDeathInjury.Partner);
                }

                if (financialInsurance.IIPermanentInjury != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.r45, financialInsurance.IIPermanentInjuryFree);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.r45_2, financialInsurance.IIPermanentInjurySingle);
                    //--
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb105, financialInsurance.IIPermanentInjury.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb106, financialInsurance.IIPermanentInjury.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb107, financialInsurance.IIPermanentInjury.Child);
                }

                if (financialInsurance.IIInvalidity != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.r46, financialInsurance.IIInvalidityFree);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.r46_2, financialInsurance.IIInvaliditySingle);
                    //--
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb108, financialInsurance.IIInvalidity.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb109, financialInsurance.IIInvalidity.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb110, financialInsurance.IIInvalidity.Child);
                }

                if (financialInsurance.IILowInjury != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb111, financialInsurance.IILowInjury.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb112, financialInsurance.IILowInjury.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb113, financialInsurance.IILowInjury.Child);
                }

                if (financialInsurance.IISeriousDiseases != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb114, financialInsurance.IISeriousDiseases.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb115, financialInsurance.IISeriousDiseases.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb116, financialInsurance.IISeriousDiseases.Child);
                }

                if (financialInsurance.InsuranceSurvivalGuaranteed != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb117, financialInsurance.InsuranceSurvivalGuaranteed.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb118, financialInsurance.InsuranceSurvivalGuaranteed.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb119, financialInsurance.InsuranceSurvivalGuaranteed.Child);
                }

                if (financialInsurance.InsuranceSurvivalInvestFund != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb120, financialInsurance.InsuranceSurvivalInvestFund.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb121, financialInsurance.InsuranceSurvivalInvestFund.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb122, financialInsurance.InsuranceSurvivalInvestFund.Child);
                }

                if (financialInsurance.RequiredInvestProfile != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb123, financialInsurance.RequiredInvestProfile.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb124, financialInsurance.RequiredInvestProfile.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb125, financialInsurance.RequiredInvestProfile.Child);
                }

                if (financialInsurance.DynamicProfil != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb126, financialInsurance.DynamicProfil.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb127, financialInsurance.DynamicProfil.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb128, financialInsurance.DynamicProfil.Child);
                }

                if (financialInsurance.BalancedProfil != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb129, financialInsurance.BalancedProfil.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb130, financialInsurance.BalancedProfil.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb131, financialInsurance.BalancedProfil.Child);
                }

                if (financialInsurance.KonzervativeProfil != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb132, financialInsurance.KonzervativeProfil.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb133, financialInsurance.KonzervativeProfil.Partner);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb134, financialInsurance.KonzervativeProfil.Child);
                }
            }
            #endregion

            #region 2. pojištění majetku
            if (/*this.FormFUSCZ.FSInsuranceProperty &&*/ financialInsurance.InsuranceProperty)
            {
                if (financialInsurance.IPHomeReality != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb135, financialInsurance.IPHomeReality.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb136, financialInsurance.IPHomeReality.Partner);
                }

                if (financialInsurance.IPOtherReality != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb137, financialInsurance.IPOtherReality.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb138, financialInsurance.IPOtherReality.Partner);
                }

                if (financialInsurance.IPAllRisk != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb139, financialInsurance.IPAllRisk.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb140, financialInsurance.IPAllRisk.Partner);
                }

                if (financialInsurance.IPTheft != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb141, financialInsurance.IPTheft.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb142, financialInsurance.IPTheft.Partner);
                }

                if (financialInsurance.IPVehicleOthers != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb143, financialInsurance.IPVehicleOthers.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb144, financialInsurance.IPVehicleOthers.Partner);
                }

                if (financialInsurance.IPOthers != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb145, financialInsurance.IPOthers.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb146, financialInsurance.IPOthers.Partner);
                }
            }
            #endregion

            #region 3. pojištění odpovědnosti za újmu
            if (/*this.FormFUSCZ.FSInsuranceLiability &&*/ financialInsurance.InsuranceDamage)
            {
                if (financialInsurance.IDPublic != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb147, financialInsurance.IDPublic.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb148, financialInsurance.IDPublic.Partner);
                }

                if (financialInsurance.IDOwnReality != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb149, financialInsurance.IDOwnReality.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb150, financialInsurance.IDOwnReality.Partner);
                }

                if (financialInsurance.IDPZP != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb151, financialInsurance.IDPZP.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb152, financialInsurance.IDPZP.Partner);
                }

                if (financialInsurance.IDEmployer != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb153, financialInsurance.IDEmployer.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb154, financialInsurance.IDEmployer.Partner);
                }

                if (financialInsurance.IDOthers != null)
                {
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb155, financialInsurance.IDOthers.Client);
                    ff.MySetField(DynFields.FUS.CheckBoxFields.chb156, financialInsurance.IDOthers.Partner);
                }
            }
            #endregion

            #region 4. cestovní pojištění
            if (/*this.FormFUSCZ.FSInsuranceTravel &&*/ financialInsurance.InsuranceTraveling && financialInsurance.InsuranceTravelingDetail != null)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb157, financialInsurance.InsuranceTravelingDetail.Client);
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb158, financialInsurance.InsuranceTravelingDetail.Partner);
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb159, financialInsurance.InsuranceTravelingDetail.Child);
            }
            #endregion

            // TODO - FormFUSCZ.FSInsuranceOthers, je ok? ta část se objeví vždy

            #region 5. Jiné / další požadavky k pojištění
            if (financialInsurance.InsuranceOtherDetail != null)
            {
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb160, financialInsurance.InsuranceOtherDetail.Client);
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb161, financialInsurance.InsuranceOtherDetail.Partner);
                ff.MySetField(DynFields.FUS.CheckBoxFields.chb162, financialInsurance.InsuranceOtherDetail.Child);
            }
            #endregion

            // 6. Výše požadovaná pojištění by měla být daňově zvýhodněná
            ff.MySetField(DynFields.FUS.RadioButtonFields.r47, financialInsurance.InsuranceTaxAdvantage.HasValue ? financialInsurance.InsuranceTaxAdvantage : false);

            #region 7. Podrobnější vyjádření Zákazníka/Partnera k výše uvedeným požadavkům
            var lines = FUShelper.SplitToLines(financialInsurance.InsuranceNote, 60, 60);

            int cnt = lines.Count();
            if (cnt > 0)
                ff.MySetField(DynFields.FUS.TextFields.T163, lines.ElementAt(0));
            #endregion

            this.Part6_FillSectors(ff, DynFields.FUS.TextFields.T164, DynFields.FUS.TextFields.T165, DynFields.FUS.TextFields.T166,
                DynFields.FUS.TextFields.T172, DynFields.FUS.TextFields.T173, DynFields.FUS.CheckBoxFields.chb168, DynFields.FUS.CheckBoxFields.chb169,
                DynFields.FUS.CheckBoxFields.chb170, DynFields.FUS.CheckBoxFields.chb171, DynFields.FUS.TextFields.T167, null);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Write_Part6_ZV(AcroFields ff)
        {
            var financialInsurance = FUShelper.FIns(this.FormFUSCZ); // this.FormFUSCZ.FinancialInsurance

            bool enabled = this.FormFUSCZ != null && financialInsurance != null && (this.FormFUSCZ.FSInsuranceLive ||
                this.FormFUSCZ.FSInsuranceLiveOther ||
                this.FormFUSCZ.FSInsurancePersonal ||
                this.FormFUSCZ.FSInsuranceProperty ||
                this.FormFUSCZ.FSInsuranceLiability ||
                this.FormFUSCZ.FSInsuranceTravel ||
                this.FormFUSCZ.FSInsuranceOthers);

            ff.MySetField(DynFields.ZV.RadioButtonFields.r21, enabled); // || this.FormFUSCZ.SectorInsurances.Count > 0

            if (!enabled) return;

            if (this.FormFUSCZ.AgentDetail != null)
                ff.MySetField(DynFields.ZV.TextFields.T40, this.FormFUSCZ.AgentDetail.RegistrationNumber); // čnb

            #region 1. pojištění osob
            if (/*this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther || this.FormFUSCZ.FSInsurancePersonal &&*/ financialInsurance.InsuranceIndividual)
            {
                if (financialInsurance.IIDeath != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb41, financialInsurance.IIDeath.Client);

                if (financialInsurance.IIDeathInjury != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb42, financialInsurance.IIDeathInjury.Client);

                if (financialInsurance.IIPermanentInjury != null)
                {
                    ff.MySetField(DynFields.ZV.CheckBoxFields.r22_1, financialInsurance.IIPermanentInjuryFree);
                    ff.MySetField(DynFields.ZV.CheckBoxFields.r22_2, financialInsurance.IIPermanentInjurySingle);
                    //---
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb43, financialInsurance.IIPermanentInjury.Client);
                }

                if (financialInsurance.IIInvalidity != null)
                {
                    ff.MySetField(DynFields.ZV.CheckBoxFields.r23_1, financialInsurance.IIInvalidityFree);
                    ff.MySetField(DynFields.ZV.CheckBoxFields.r23_2, financialInsurance.IIInvaliditySingle);
                    //--
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb44, financialInsurance.IIInvalidity.Client);
                }

                if (financialInsurance.IILowInjury != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb45, financialInsurance.IILowInjury.Client);

                if (financialInsurance.IISeriousDiseases != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb46, financialInsurance.IISeriousDiseases.Client);

                if (financialInsurance.InsuranceSurvivalGuaranteed != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.r24_1, financialInsurance.InsuranceSurvivalGuaranteed.Client);

                if (financialInsurance.InsuranceSurvivalInvestFund != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.r24_2, financialInsurance.InsuranceSurvivalInvestFund.Client);

                if (financialInsurance.RequiredInvestProfile != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb47, financialInsurance.RequiredInvestProfile.Client);

                if (financialInsurance.DynamicProfil != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb56, financialInsurance.DynamicProfil.Client);

                if (financialInsurance.BalancedProfil != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb57, financialInsurance.BalancedProfil.Client);

                if (financialInsurance.KonzervativeProfil != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb58, financialInsurance.KonzervativeProfil.Client);
            }
            #endregion

            #region 2. pojištění odpovědnosti za újmu
            if (/*this.FormFUSCZ.FSInsuranceLiability &&*/ financialInsurance.InsuranceDamage)
            {
                if (financialInsurance.IDPublic != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb48, financialInsurance.IDPublic.Client);

                if (financialInsurance.IDOwnReality != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb49, financialInsurance.IDOwnReality.Client);

                if (financialInsurance.IDPZP != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb50, financialInsurance.IDPZP.Client);

                if (financialInsurance.IDEmployer != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb51, financialInsurance.IDEmployer.Client);

                if (financialInsurance.IDOthers != null)
                    ff.MySetField(DynFields.ZV.CheckBoxFields.chb52, financialInsurance.IDOthers.Client);
            }
            #endregion

            // 3. Jiné / další požadavky k pojištění
            if (financialInsurance.InsuranceOtherDetail != null)
                ff.MySetField(DynFields.ZV.CheckBoxFields.chb53, financialInsurance.InsuranceOtherDetail.Client);

            // 4. Výše požadovaná pojištění by měla být daňově zvýhodněná
            ff.MySetField(DynFields.ZV.RadioButtonFields.r25, financialInsurance.InsuranceTaxAdvantage.HasValue ? financialInsurance.InsuranceTaxAdvantage : false);

            #region 5. Podrobnější vyjádření Zákazníka/Partnera k výše uvedeným požadavkům
            var lines = FUShelper.SplitToLines(financialInsurance.InsuranceNote, 60, 60);

            int cnt = lines.Count();
            if (cnt > 0)
                ff.MySetField(DynFields.ZV.TextFields.T54, lines.ElementAt(0));

            if (cnt > 1)
                ff.MySetField(DynFields.ZV.TextFields.T55, lines.ElementAt(1));

            if (cnt > 2)
                ff.MySetField(DynFields.ZV.TextFields.T56, lines.ElementAt(2));

            if (cnt > 3)
                ff.MySetField(DynFields.ZV.TextFields.T57, lines.ElementAt(3));
            #endregion

            this.Part6_FillSectors(ff, DynFields.ZV.TextFields.T59, DynFields.ZV.TextFields.T60, DynFields.ZV.TextFields.T61,
                DynFields.ZV.TextFields.T68, DynFields.ZV.TextFields.T69, DynFields.ZV.CheckBoxFields.chb64, DynFields.ZV.CheckBoxFields.chb67,
                DynFields.ZV.CheckBoxFields.chb65, DynFields.ZV.CheckBoxFields.chb66, DynFields.ZV.TextFields.T62, DynFields.ZV.TextFields.T63);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Part6_FillSectors
        void Part6_FillSectors(AcroFields ff,
            FATextValue sector0F,
            FATextValue sector1F,
            FATextValue sector2F,
            FATextValue provideTxF,
            FATextValue softTxF,
            FAChBValue provideChBF,
            FAChBValue softChF,
            FAChBValue printChF,
            FAChBValue emailChF,
            FATextValue discrepencyTx0F,
            FATextValue discrepencyTx1F)
        {
            if (this.FormFUSCZ == null) return;

            List<int> provideInfo = new List<int>();
            List<int> provideSoft = new List<int>();

            bool print, email;
            print = email = false;

            if (this.FormFUSCZ.SectorInsurances != null)
            {
                Func<SectorInsurance, FATextValue, int, bool> AddInsurace = (sector, field, index) =>
                {
                    if (sector == null || field == null)
                        return false;

                    this.Part6_FillSector(ff, sector, field);

                    if (this.FormFUSCZ.FSInsuranceLive || this.FormFUSCZ.FSInsuranceLiveOther)
                    {
                        if (sector.IsInfromationProvide) provideInfo.Add(index);
                        if (sector.IsSoftwareProvide) provideSoft.Add(index);

                        print |= sector.IsInformationPrint;
                        email |= sector.IsInformationElectronic;
                    }

                    return true;
                };

                if (this.FormFUSCZ.SectorInsurances.Count > 0 && this.FormFUSCZ.SectorInsurances[0] != null)
                    AddInsurace(this.FormFUSCZ.SectorInsurances[0], sector0F, 1);

                if (this.FormFUSCZ.SectorInsurances.Count > 1 && this.FormFUSCZ.SectorInsurances[1] != null)
                    AddInsurace(this.FormFUSCZ.SectorInsurances[1], sector1F, 2);

                if (this.FormFUSCZ.SectorInsurances.Count > 2 && this.FormFUSCZ.SectorInsurances[2] != null)
                    AddInsurace(this.FormFUSCZ.SectorInsurances[2], sector2F, 3);
            }

            var financialInsurance = FUShelper.FIns(this.FormFUSCZ);

            if (financialInsurance != null)
            {
                IEnumerable<string> lines = FUShelper.SplitToLines(financialInsurance.Discrepancies, 100, 100);

                int cnt = lines.Count();

                if (cnt > 0)
                    ff.MySetField(discrepencyTx0F, lines.ElementAt(0));

                if (cnt > 1 && discrepencyTx1F != null)
                    ff.MySetField(discrepencyTx1F, lines.ElementAt(1));

                if (provideInfo.Count > 0)
                {
                    ff.MySetField(provideChBF, true);
                    ff.MySetField(provideTxF, String.Join(", ", provideInfo));

                    ff.MySetField(printChF, print);
                    ff.MySetField(emailChF, email);
                }

                if (provideSoft.Count > 0)
                {
                    ff.MySetField(softChF, true);
                    ff.MySetField(softTxF, String.Join(", ", provideSoft));
                }
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Part6_FillSector(AcroFields ff, SectorInsurance data, FATextValue textF)
        {
            if (data == null) return;

            int categoryID;
            string pojistovna = this.GetInstitution(data.InstitutionID);
            string produkt = this.GetProduct(data.ProductID, out categoryID);

            string categoryName = string.Empty;

            if (categoryID > -1)
            {
                var cat = DbProvider.Categories().FirstOrDefault(f => f.CategoryID == categoryID);
                if (cat != null)
                    categoryName = cat.Name;
            }

            string filedtext = TextFormater.FormatColumnText(new string[] { categoryName, pojistovna, produkt, data.Note }, new int[] { 256, 130, 140, 1 });

            ff.MySetField(textF, filedtext);
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part7
        /// <summary>VII. ZPROSTŘEDKOVÁNÍ INVESTIC</summary>
        void Write_Part7_STD(AcroFields ff)
        {
            if (this.FormFUSCZ == null) return;

            ff.MySetField(DynFields.FUS.RadioButtonFields.r175, this.FormFUSCZ.FSInvestment || this.FormFUSCZ.FSTermDepositsOrOtherJT);

            // neplníme, pokud si to klient nepřál
            if (!(this.FormFUSCZ.FSInvestment || this.FormFUSCZ.FSTermDepositsOrOtherJT) || this.FormFUSCZ.FinancialInvestment == null) return;

            #region sekce A - hotovo
            if (this.FormFUSCZ.FUSForClient && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null)
            {
                switch (this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Client)
                {
                    case "CZQTRULY":
                        ff.MySetField(DynFields.FUS.RadioButtonFields.r176, this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Client);
                        this.Part7_FillFormClient(ff); // plníme jen pokud klient odpovídá
                        break;
                    case "CZQREFUSE":
                        ff.MySetField(DynFields.FUS.RadioButtonFields.r176, this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Client);
                        break;
                    case "CZQANSWERED":
                        switch (FormFUSCZ.FinancialInvestment.ExpInvetorCode.Client)
                        {
                            case "CZINVESTORINEXPERIENCED":
                            case "CZINVESTORLOW":
                            case "CZINVESTORMEDIUM":
                            case "CZINVESTORHIGH":
                                ff.MySetField(DynFields.FUS.RadioButtonFields.r176, this.FormFUSCZ.FinancialInvestment.ExpInvetorCode.Client);
                                break;
                        }
                        break;
                }
            }

            if (this.FormFUSCZ.FUSForPartner && this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode != null)
            {
                switch (this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner)
                {
                    case "CZQTRULY":
                        ff.MySetField(DynFields.FUS.RadioButtonFields.r177, this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner);
                        this.Part7_FillFormPartner(ff); // plníme jen pokud partner odpovídá
                        break;
                    case "CZQREFUSE":
                        ff.MySetField(DynFields.FUS.RadioButtonFields.r177, this.FormFUSCZ.FinancialInvestment.InvestTowardQuestionnaireCode.Partner);
                        break;
                    case "CZQANSWERED":
                        switch (FormFUSCZ.FinancialInvestment.ExpInvetorCode.Partner)
                        {
                            case "CZINVESTORINEXPERIENCED":
                            case "CZINVESTORLOW":
                            case "CZINVESTORMEDIUM":
                            case "CZINVESTORHIGH":
                                ff.MySetField(DynFields.FUS.RadioButtonFields.r177, this.FormFUSCZ.FinancialInvestment.ExpInvetorCode.Partner);
                                break;
                        }
                        break;
                }
            }
            #endregion

            #region sekce B - investiční pokyny zákazníka

            string investmentNotes = "";

            foreach (var investment in this.FormFUSCZ.SectorInvestments)
            {
                if (!string.IsNullOrEmpty(investment.Note))
                {
                    investmentNotes += investment.Note + ", ";
                }
            }

            if (!string.IsNullOrEmpty(investmentNotes))
                investmentNotes = investmentNotes.Remove(investmentNotes.Length - 2);

            if (!string.IsNullOrEmpty(investmentNotes) && investmentNotes.Length < 65)
                ff.MySetField(DynFields.FUS.TextFields.T222_X, investmentNotes);
            else if (!string.IsNullOrEmpty(investmentNotes) && investmentNotes.Length >= 65)
            {
                ff.MySetField(DynFields.FUS.TextFields.T222_X, investmentNotes.Remove(64));
                ff.MySetField(DynFields.FUS.TextFields.T223_X, investmentNotes.Substring(64, investmentNotes.Length - 64));
            }


            if (this.FormFUSCZ.SectorInvestments.Count > 0)
                this.Part7_FillSector(ff, this.FormFUSCZ.SectorInvestments[0], DynFields.FUS.RadioButtonFields.r190, DynFields.FUS.TextFields.T181, DynFields.FUS.TextFields.T182, DynFields.FUS.TextFields.T183, DynFields.FUS.TextFields.T184, DynFields.FUS.TextFields.T185, DynFields.FUS.TextFields.T186, DynFields.FUS.TextFields.T187);

            if (this.FormFUSCZ.SectorInvestments.Count > 1)
                this.Part7_FillSector(ff, this.FormFUSCZ.SectorInvestments[1], DynFields.FUS.RadioButtonFields.r191, DynFields.FUS.TextFields.T188, DynFields.FUS.TextFields.T189, DynFields.FUS.TextFields.T190, DynFields.FUS.TextFields.T191, DynFields.FUS.TextFields.T192, DynFields.FUS.TextFields.T193, DynFields.FUS.TextFields.T194);

            if (this.FormFUSCZ.SectorInvestments.Count > 2)
                this.Part7_FillSector(ff, this.FormFUSCZ.SectorInvestments[2], DynFields.FUS.RadioButtonFields.r192, DynFields.FUS.TextFields.T195, DynFields.FUS.TextFields.T196, DynFields.FUS.TextFields.T197, DynFields.FUS.TextFields.T198, DynFields.FUS.TextFields.T199, DynFields.FUS.TextFields.T200, DynFields.FUS.TextFields.T201);

            if (this.FormFUSCZ.SectorInvestments.Count > 3)
                this.Part7_FillSector(ff, this.FormFUSCZ.SectorInvestments[3], DynFields.FUS.RadioButtonFields.r193, DynFields.FUS.TextFields.T202, DynFields.FUS.TextFields.T203, DynFields.FUS.TextFields.T204, DynFields.FUS.TextFields.T205, DynFields.FUS.TextFields.T206, DynFields.FUS.TextFields.T207, DynFields.FUS.TextFields.T208);

            if (this.FormFUSCZ.SectorInvestments.Count > 4)
                this.Part7_FillSector(ff, this.FormFUSCZ.SectorInvestments[4], DynFields.FUS.RadioButtonFields.r194, DynFields.FUS.TextFields.T209, DynFields.FUS.TextFields.T210, DynFields.FUS.TextFields.T211, DynFields.FUS.TextFields.T212, DynFields.FUS.TextFields.T213, DynFields.FUS.TextFields.T214, DynFields.FUS.TextFields.T215);


            ff.MySetField(DynFields.FUS.TextFields.T216, this.FormFUSCZ.FinancialInvestment.InvestDate.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
            ff.MySetField(DynFields.FUS.TextFields.T217, this.FormFUSCZ.FinancialInvestment.InvestTime.ValueOrDefault(v => v.Value.ToString("HH:mm")));
            #endregion
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Part7_FillSector
        void Part7_FillSector(AcroFields ff, SectorInvestment data,
            FASelectValue clientOrPartnerF,
            FATextValue institutionF,
            FATextValue toolNameF,
            FATextValue toolTypeF,
            FATextValue buySellF,
            FATextValue oneTimeAmountF,
            FATextValue repeatAmountF,
            FATextValue currencyF
            )
        {
            if (this.FormFUSCZ == null || data == null) return;

            Guid? clientID = this.FormFUSCZ.Client.ValueOrDefault(v => v.ClientTempID);
            ff.MySetField(clientOrPartnerF, data.ClientTempID == clientID);

            ff.MySetField(institutionF, this.GetInstitution(data.InstitutionID));
            ff.MySetField(toolNameF, data.KTFinancialTool);
            ff.MySetField(toolTypeF, data.KTFinancialToolType.Substring(2));
            ff.MySetField(currencyF, FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == data.KTCurrency).Select(p => p.Name).FirstOrDefault());

            if (data.KTInvestAmountTypeCode == "CZONETIME")
                ff.MySetField(oneTimeAmountF, data.Amount);
            else if (data.KTInvestAmountTypeCode == "CZMULTITIME")
            {
                string amount = string.Empty;
                if (!string.IsNullOrWhiteSpace(data.Amount))
                {
                    amount += data.Amount;

                    string period = GetPeriodText(data.AmountPeriod);
                    if (!string.IsNullOrWhiteSpace(period))
                    {
                        amount += " / ";
                        amount += period;
                    }
                }
                ff.MySetField(repeatAmountF, amount);
            }

            ff.MySetField(buySellF, FUShelper.FUSListOptionsResource.Where(p => p.FUSListOptionCode == data.KTBuySell && p.FUSListTypeID == Constants.FUSCZListTypes.BUYSELL).Select(p => p.Name).FirstOrDefault());
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Part7_FillFormClient(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinancialInvestment == null) return;

            if (this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r178, this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.Client);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r180, this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.Client);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r182, this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.Client);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r184, this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.Client);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r186, this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.Client);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r188, this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.Client);

            List<string> codes = new List<string>();
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.ValueOrDefault(v => v.Client));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.ValueOrDefault(v => v.Client));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.ValueOrDefault(v => v.Client));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.ValueOrDefault(v => v.Client));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.ValueOrDefault(v => v.Client));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.ValueOrDefault(v => v.Client));

            int? sum = null;
            if (codes.Any(p => p != null))
                sum = FUShelper.FUSListOptionsResource.Where(p => codes.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

            if (sum.HasValue)
                ff.MySetField(DynFields.FUS.TextFields.T178, sum.ToString());
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Part7_FillFormPartner(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinancialInvestment == null) return;

            if (this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r179, this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.Partner);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r181, this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.Partner);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r183, this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.Partner);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r185, this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.Partner);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r187, this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.Partner);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                ff.MySetField(DynFields.FUS.RadioButtonFields.r189, this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.Partner);

            List<string> codes = new List<string>();
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode.ValueOrDefault(v => v.Partner));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode.ValueOrDefault(v => v.Partner));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode.ValueOrDefault(v => v.Partner));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode.ValueOrDefault(v => v.Partner));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode.ValueOrDefault(v => v.Partner));
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                codes.Add(this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode.ValueOrDefault(v => v.Partner));

            int? sum = null;
            if (codes.Any(p => p != null))
                sum = FUShelper.FUSListOptionsResource.Where(p => codes.Contains(p.FUSListOptionCode) && p.Value != null && p.Value.IsNumber()).Select(p => p.Value).Sum(x => x.HasValue ? x.Value : 0);

            if (sum.HasValue)
                ff.MySetField(DynFields.FUS.TextFields.T179, sum.ToString());
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Write_Part8
        /// <summary>
        /// VIII. ZÁVĚREČNÁ PROHLÁŠENÍ A PODPISY
        /// </summary>
        /// <param name="ff"></param>
        void Write_Part8_STD(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinalStatementSignatures == null) return;

            ff.MySetField(DynFields.FUS.TextFields.T219, this.FormFUSCZ.FinalStatementSignatures.SignedPlace);

            ff.MySetField(DynFields.FUS.TextFields.T220, this.FormFUSCZ.FinalStatementSignatures.DateFUSSigned.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Write_Part8_ZV(AcroFields ff)
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.FinalStatementSignatures == null) return;

            ff.MySetField(DynFields.ZV.TextFields.T70, this.FormFUSCZ.FinalStatementSignatures.SignedPlace);

            ff.MySetField(DynFields.ZV.TextFields.T71, this.FormFUSCZ.FinalStatementSignatures.DateFUSSigned.ValueOrDefault(v => v.Value.ToString("dd'/'MM'/'yyyy")));
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region EAN
        public void FillEAN(PdfReader pdfReader, PdfStamper pdfStamper)
        {
            string prodCode = this.FormFUSCZ.FUSID.ToString();

            var br = new BarcodeLib.Barcode();
            br.IncludeLabel = true;
            br.LabelFont = new System.Drawing.Font("Microsoft Sans Serif", 7, System.Drawing.FontStyle.Bold);
            br.LabelWidth = 58;
            br.PaddingLeft = 62;
            br.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;

            var bar = br.Encode(BarcodeLib.TYPE.CODE128, prodCode, System.Drawing.Color.Black, System.Drawing.Color.White, 180, 23);
            //  bar = resizeImage(bar, new Size(140, 30));            

            var itextImage = iTextSharp.text.Image.GetInstance(bar, System.Drawing.Imaging.ImageFormat.Bmp);

            int count = pdfReader.NumberOfPages;
            for (int i = 1; i <= count; i++)
            {
                var pdfContentByte = pdfStamper.GetOverContent(i);
                itextImage.SetAbsolutePosition(190, 815);
                pdfContentByte.AddImage(itextImage);
            }
        }

        public void FillEANZV(PdfReader pdfReader, PdfStamper pdfStamper)
        {
            string prodCode = this.FormFUSCZ.FUSID.ToString();

            var br = new BarcodeLib.Barcode();
            br.IncludeLabel = true;
            br.LabelFont = new System.Drawing.Font("Microsoft Sans Serif", 7, System.Drawing.FontStyle.Bold);
            br.LabelWidth = 58;
            br.PaddingLeft = 36;
            br.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;

            var bar = br.Encode(BarcodeLib.TYPE.CODE128, prodCode, System.Drawing.Color.Black, System.Drawing.Color.White, 130, 23);
            //  bar = resizeImage(bar, new Size(140, 30));            

            var itextImage = iTextSharp.text.Image.GetInstance(bar, System.Drawing.Imaging.ImageFormat.Bmp);

            int count = pdfReader.NumberOfPages;
            for (int i = 1; i <= count; i++)
            {
                var pdfContentByte = pdfStamper.GetOverContent(i);
                itextImage.SetAbsolutePosition(120, 815);
                pdfContentByte.AddImage(itextImage);
            }
        }

        public void FillEANADK(PdfReader pdfReader, PdfStamper pdfStamper)
        {
            string prodCode = this.FormFUSCZ.FUSID.ToString();

            var br = new BarcodeLib.Barcode();
            br.IncludeLabel = true;
            br.LabelFont = new System.Drawing.Font("Microsoft Sans Serif", 7, System.Drawing.FontStyle.Bold);
            br.LabelWidth = 58;
            br.PaddingLeft = 25;
            br.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;

            var bar = br.Encode(BarcodeLib.TYPE.CODE128, prodCode, System.Drawing.Color.Black, System.Drawing.Color.White, 106, 23);
            //  bar = resizeImage(bar, new Size(140, 30));            

            var itextImage = iTextSharp.text.Image.GetInstance(bar, System.Drawing.Imaging.ImageFormat.Bmp);

            int count = pdfReader.NumberOfPages;
            for (int i = 1; i <= count; i++)
            {
                var pdfContentByte = pdfStamper.GetOverContent(i);
                itextImage.SetAbsolutePosition(122, 815);
                pdfContentByte.AddImage(itextImage);
            }
        }

        public void FillEANZVDraft(PdfReader pdfReader, PdfStamper pdfStamper)
        {
            string prodCode = this.FormFUSCZ.FUSID.ToString();

            var br = new BarcodeLib.Barcode();
            br.IncludeLabel = true;
            br.LabelFont = new System.Drawing.Font("Microsoft Sans Serif", 7, System.Drawing.FontStyle.Bold);
            br.LabelWidth = 58;
            br.PaddingLeft = 36;
            br.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;

            var bar = br.Encode(BarcodeLib.TYPE.CODE128, prodCode, System.Drawing.Color.Black, System.Drawing.Color.White, 130, 23);

            //  bar = resizeImage(bar, new Size(140, 30));            

            var itextImage = iTextSharp.text.Image.GetInstance(bar, System.Drawing.Imaging.ImageFormat.Bmp);

            int count = pdfReader.NumberOfPages;
            for (int i = 1; i <= count; i++)
            {
                var pdfContentByte = pdfStamper.GetOverContent(i);
                itextImage.SetAbsolutePosition(123, 815);
                pdfContentByte.AddImage(itextImage);
            }
        }
        #endregion
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region GetXMLFromPdf
        public void GetXMLFromPdf(Stream stream, string fusTypeCode)
        {
            stream.Position = 0;

            byte[] inputBuffer = new byte[stream.Length];
            stream.Read(inputBuffer, 0, inputBuffer.Length);

            using (MemoryStream fms = new MemoryStream())
            {
                PdfReader reader = new PdfReader(inputBuffer);
                using (PdfStamper stamper = new PdfStamper(reader, fms, '\n', true)) // last TRUE is appent and very important for leaving document editable
                {
                    var fus = new FormFUSCZ();

                    // part1
                    fus.PageFUS = "Agent";

                    if (fusTypeCode == Constants.FUSCZTypes.ZV || fusTypeCode == Constants.FUSCZTypes.SMSZV)
                    {
                        this.Read_Part2_ZV(fus, stamper.AcroFields);
                        this.Read_Part3_ZV(fus, stamper.AcroFields);
                        this.Read_Part4_ZV(fus, stamper.AcroFields);
                        this.Read_Part5_ZV(fus, stamper.AcroFields);
                        this.Read_Part6_ZV(fus, stamper.AcroFields);
                        //--
                        this.Read_Part8_ZV(fus, stamper.AcroFields);
                        ReadFinalChangesZV(fus, stamper.AcroFields);
                    }
                    else
                    {
                        this.Read_Part2_STD(fus, stamper.AcroFields);
                        this.Read_Part3_STD(fus, stamper.AcroFields);
                        this.Read_Part4_STD(fus, stamper.AcroFields);
                        this.Read_Part5_STD(fus, stamper.AcroFields);
                        this.Read_Part6_STD(fus, stamper.AcroFields);
                        this.Read_Part7_STD(fus, stamper.AcroFields);
                        this.Read_Part8_STD(fus, stamper.AcroFields);
                        ReadFinalChangesSTD(fus, stamper.AcroFields);
                    }

                    this.ImportedFUSXML = Serialize(fus);
                    this.IsValid = true;
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part2
        void Read_Part2_STD(FormFUSCZ fus, AcroFields ff)
        {
            fus.Announcement = new Announcement(true);

            fus.Announcement.AnnouncementBuildSaving.Client = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r1, false);
            fus.Announcement.AnnouncementLiveSaving.Client = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r2, false);
            fus.Announcement.AnnouncementLeases.Client = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r3, false);
            fus.Announcement.AnnouncementAct.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r10);

            fus.FUSForClient = true;

            fus.Announcement.AnnouncementBuildSaving.Partner = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r4, false);
            fus.Announcement.AnnouncementLiveSaving.Partner = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r5, false);
            fus.Announcement.AnnouncementLeases.Partner = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r6, false);
            fus.Announcement.AnnouncementAct.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r11);

            fus.Announcement.AnnouncementBuildSaving.Child = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r7, false);
            fus.Announcement.AnnouncementLiveSaving.Child = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r8, false);
            fus.Announcement.AnnouncementLeases.Child = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r9, false);

            fus.Announcement.AnnouncementExpone = ff.ReadBoolValueNullable(DynFields.FUS.RadioButtonFields.r12);

            fus.FSLoanConsumer = fus.FSLoanOther = ExtensionMethods.AtleastOnetrue(fus.Announcement.AnnouncementLeases.Client,
                fus.Announcement.AnnouncementLeases.Partner, fus.Announcement.AnnouncementLeases.Child);

            fus.FSBuildSaving = ExtensionMethods.AtleastOnetrue(fus.Announcement.AnnouncementBuildSaving.Client,
                fus.Announcement.AnnouncementBuildSaving.Partner, fus.Announcement.AnnouncementBuildSaving.Child);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part2_ZV(FormFUSCZ fus, AcroFields ff)
        {
            fus.Announcement = new Announcement(true);

            fus.Announcement.AnnouncementLiveSaving.Client = ff.ReadBoolValue(DynFields.ZV.RadioButtonFields.r6);

            if (fus.Announcement.AnnouncementLiveSaving.Client.HasValue && fus.Announcement.AnnouncementLiveSaving.Client.Value)
            {
                fus.Announcement.AnnouncementAct.Client = ff.MyGetField(DynFields.ZV.RadioButtonFields.r7);

                fus.Announcement.AnnouncementExpone = ff.ReadBoolValueNullable(DynFields.ZV.RadioButtonFields.r8);
            }

            fus.FUSForClient = true;
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part3
        void Read_Part3_STD(FormFUSCZ fus, AcroFields ff)
        {
            fus.OverallInfo = new OverallInfo();

            fus.OverallInfo.StatementClientCode = ff.MyGetField(DynFields.FUS.RadioButtonFields.r14);

            if (fus.OverallInfo.StatementClientCode == Constants.FUSListOptions.WASNTSPECIFIED)
            {
                fus.OverallInfo.LastName = ff.MyGetField(DynFields.FUS.TextFields.T7);
                fus.OverallInfo.FirstName = ff.MyGetField(DynFields.FUS.TextFields.T8);
                fus.OverallInfo.BirthDate = ff.MyGetField(DynFields.FUS.TextFields.T9).ToDateTime();
            }

            // TODO r15, r16, r17, r18

            if (fus.FUSForClient)
            {
                fus.Client = new Client();
                fus.Client.Form_Read(fus, ff,
                    DynFields.FUS.RadioButtonFields.r19,
                    DynFields.FUS.RadioButtonFields.r20,
                    DynFields.FUS.TextFields.T10,
                    DynFields.FUS.TextFields.T11,
                    DynFields.FUS.TextFields.T12,
                    DynFields.FUS.TextFields.T13,
                    DynFields.FUS.TextFields.T14,
                    DynFields.FUS.TextFields.T15,
                    DynFields.FUS.TextFields.T16,
                    DynFields.FUS.TextFields.T17,
                    DynFields.FUS.TextFields.T18,
                    DynFields.FUS.TextFields.T19,
                    DynFields.FUS.TextFields.T20,
                    DynFields.FUS.TextFields.T22,
                    DynFields.FUS.TextFields.T23,
                    DynFields.FUS.TextFields.T24,
                    DynFields.FUS.TextFields.T26,
                    DynFields.FUS.TextFields.T27,
                    DynFields.FUS.TextFields.T28,
                    DynFields.FUS.TextFields.T29,
                    DynFields.FUS.TextFields.T30,
                    DynFields.FUS.TextFields.T31, null, true);

                if (!string.IsNullOrWhiteSpace(fus.Client.PartyCode) && !string.IsNullOrWhiteSpace(fus.Client.TradeLicenceNo)) fus.OverallInfo.ClientType = SealedConstants.PersonType.IndividualBusinessman.Value;
                else if (string.IsNullOrWhiteSpace(fus.Client.PartyCode) && !string.IsNullOrWhiteSpace(fus.Client.TradeLicenceNo)) fus.OverallInfo.ClientType = SealedConstants.PersonType.Corporation.Value;
                else fus.OverallInfo.ClientType = SealedConstants.PersonType.Individual.Value;
            }


            fus.Partner = new Partner();

            fus.Partner.Form_Read(fus, ff,
                DynFields.FUS.RadioButtonFields.r21,
                DynFields.FUS.RadioButtonFields.r22,
                DynFields.FUS.TextFields.T32,
                DynFields.FUS.TextFields.T33,
                DynFields.FUS.TextFields.T34,
                DynFields.FUS.TextFields.T35,
                DynFields.FUS.TextFields.T36,
                DynFields.FUS.TextFields.T37,
                DynFields.FUS.TextFields.T38,
                DynFields.FUS.TextFields.T39,
                DynFields.FUS.TextFields.T40,
                DynFields.FUS.TextFields.T41,
                DynFields.FUS.TextFields.T42,
                DynFields.FUS.TextFields.T44,
                DynFields.FUS.TextFields.T45,
                DynFields.FUS.TextFields.T46,
                DynFields.FUS.TextFields.T48,
                DynFields.FUS.TextFields.T49,
                DynFields.FUS.TextFields.T50,
                DynFields.FUS.TextFields.T51,
                DynFields.FUS.TextFields.T52,
                DynFields.FUS.TextFields.T53, null, false);

            if (!string.IsNullOrWhiteSpace(fus.Partner.TradeLicenceNo))
                fus.OverallInfo.PartnerType = SealedConstants.PersonType.IndividualBusinessman.Value;
            else
                fus.OverallInfo.PartnerType = SealedConstants.PersonType.Individual.Value;


            fus.Childs = new List<Child>();

            Child child = new Child();
            child.Form_Read(fus, ff,
                DynFields.FUS.RadioButtonFields.r23,
                DynFields.FUS.TextFields.T54,
                DynFields.FUS.TextFields.T55,
                DynFields.FUS.TextFields.T56,
                DynFields.FUS.TextFields.T57,
                DynFields.FUS.TextFields.T58,
                DynFields.FUS.TextFields.T59,
                DynFields.FUS.TextFields.T60,
                DynFields.FUS.TextFields.T61,
                DynFields.FUS.TextFields.T62,
                DynFields.FUS.TextFields.T63,
                DynFields.FUS.TextFields.T65);

            if (!string.IsNullOrEmpty(child.LastName) || (!string.IsNullOrEmpty(child.PartyCode) && child.PartyCode != "/"))
                fus.Childs.Add(child);

            Child child2 = new Child();
            child2.Form_Read(fus, ff,
                DynFields.FUS.RadioButtonFields.r24,
                DynFields.FUS.TextFields.T66,
                DynFields.FUS.TextFields.T67,
                DynFields.FUS.TextFields.T68,
                DynFields.FUS.TextFields.T69,
                DynFields.FUS.TextFields.T70,
                DynFields.FUS.TextFields.T71,
                DynFields.FUS.TextFields.T72,
                DynFields.FUS.TextFields.T73,
                DynFields.FUS.TextFields.T74,
                DynFields.FUS.TextFields.T75,
                DynFields.FUS.TextFields.T77);

            if (!string.IsNullOrEmpty(child2.LastName) || (!string.IsNullOrEmpty(child2.PartyCode) && child2.PartyCode != "/"))
                fus.Childs.Add(child2);

            Child child3 = new Child();
            child3.Form_Read(fus, ff,
                DynFields.FUS.RadioButtonFields.r25,
                DynFields.FUS.TextFields.T78,
                DynFields.FUS.TextFields.T79,
                DynFields.FUS.TextFields.T80,
                DynFields.FUS.TextFields.T81,
                DynFields.FUS.TextFields.T82,
                DynFields.FUS.TextFields.T83,
                DynFields.FUS.TextFields.T84,
                DynFields.FUS.TextFields.T85,
                DynFields.FUS.TextFields.T86,
                DynFields.FUS.TextFields.T87,
                DynFields.FUS.TextFields.T89);

            if (!string.IsNullOrEmpty(child3.LastName) || (!string.IsNullOrEmpty(child3.PartyCode) && child3.PartyCode != "/"))
                fus.Childs.Add(child3);

            if (fus.Childs.NullableCount() > 0)
                fus.FUSForChild = true;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part3_ZV(FormFUSCZ fus, AcroFields ff)
        {
            fus.OverallInfo = new OverallInfo();

            // TODO r9

            if (fus.FUSForClient)
            {
                fus.Client = new Client();
                fus.Client.Form_Read(fus, ff,
                    DynFields.ZV.RadioButtonFields.r10,
                    null,
                    DynFields.ZV.TextFields.T7,
                    DynFields.ZV.TextFields.T8,
                    DynFields.ZV.TextFields.T9,
                    DynFields.ZV.TextFields.T10,
                    DynFields.ZV.TextFields.T11,
                    DynFields.ZV.TextFields.T12,
                    DynFields.ZV.TextFields.T13,
                    DynFields.ZV.TextFields.T14,
                    DynFields.ZV.TextFields.T15,
                    DynFields.ZV.TextFields.T16,
                    DynFields.ZV.TextFields.T17,
                    DynFields.ZV.TextFields.T19,
                    DynFields.ZV.TextFields.T20,
                    DynFields.ZV.TextFields.T21,
                    DynFields.ZV.TextFields.T23,
                    DynFields.ZV.TextFields.T24,
                    DynFields.ZV.TextFields.T25,
                    DynFields.ZV.TextFields.T26,
                    DynFields.ZV.TextFields.T27,
                    DynFields.ZV.TextFields.T28,
                    DynFields.ZV.TextFields.T112, true);

                if (!string.IsNullOrWhiteSpace(fus.Client.PartyCode) && !string.IsNullOrWhiteSpace(fus.Client.TradeLicenceNo)) fus.OverallInfo.ClientType = SealedConstants.PersonType.IndividualBusinessman.Value;
                else if (string.IsNullOrWhiteSpace(fus.Client.PartyCode) && !string.IsNullOrWhiteSpace(fus.Client.TradeLicenceNo)) fus.OverallInfo.ClientType = SealedConstants.PersonType.Corporation.Value;
                else fus.OverallInfo.ClientType = SealedConstants.PersonType.Individual.Value;
            }
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part4
        void Read_Part4_STD(FormFUSCZ fus, AcroFields ff)
        {
            fus.FinancialDataVerification = new FinancialDataVerification(true);

            //fus.FinancialDataVerification.RefuseInformation = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb91);
            //if (!fus.FinancialDataVerification.RefuseInformation)
            //{
            this.Read_Part4_Client(fus.FinancialDataVerification, ff,
                DynFields.FUS.RadioButtonFields.r26, DynFields.FUS.RadioButtonFields.r30,
                DynFields.FUS.RadioButtonFields.r28, DynFields.FUS.RadioButtonFields.r32,
                DynFields.FUS.RadioButtonFields.r34);

            this.Read_Part4_Partner(fus.FinancialDataVerification, ff,
                DynFields.FUS.RadioButtonFields.r27, DynFields.FUS.RadioButtonFields.r31,
                DynFields.FUS.RadioButtonFields.r29, DynFields.FUS.RadioButtonFields.r33,
                DynFields.FUS.RadioButtonFields.r35);
            //}

            //if (!FormFUSCZ.FSInsuranceLive && !this.FormFUSCZ.FSSavingDS && !this.FormFUSCZ.FSSavingDPS)
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part4_ZV(FormFUSCZ fus, AcroFields ff)
        {
            fus.FinancialDataVerification = new FinancialDataVerification(true);

            //fus.FinancialDataVerification.RefuseInformation = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb30);

            //if (!fus.FinancialDataVerification.RefuseInformation)
            //{
            this.Read_Part4_Client(fus.FinancialDataVerification, ff,
                DynFields.ZV.RadioButtonFields.r11, DynFields.ZV.RadioButtonFields.r12,
                DynFields.ZV.RadioButtonFields.r13, DynFields.ZV.RadioButtonFields.r14,
                DynFields.ZV.RadioButtonFields.r15);
            //}
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part4_Client(FinancialDataVerification data, AcroFields ff,
            FASelectValue goalF, FASelectValue toolsF, FASelectValue horizontF, FASelectValue experienceF, FASelectValue riskF)
        {
            data.QEInvestGoal.Client = ff.MyGetField(goalF);
            data.QEInvestTools.Client = ff.MyGetField(toolsF);
            data.QEInvestHorizont.Client = ff.MyGetField(horizontF);
            data.QEInvestExperience.Client = ff.MyGetField(experienceF);
            data.QEInvestRisk.Client = ff.MyGetField(riskF);

            if (string.IsNullOrEmpty(data.QEInvestGoal.Client) &&
                string.IsNullOrEmpty(data.QEInvestTools.Client) &&
                string.IsNullOrEmpty(data.QEInvestHorizont.Client) &&
                string.IsNullOrEmpty(data.QEInvestExperience.Client) &&
                string.IsNullOrEmpty(data.QEInvestRisk.Client))
                data.RefuseInformation.Client = true;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part4_Partner(FinancialDataVerification data, AcroFields ff,
            FASelectValue goalF, FASelectValue toolsF, FASelectValue horizontF, FASelectValue experienceF, FASelectValue riskF)
        {
            data.QEInvestGoal.Partner = ff.MyGetField(goalF);
            data.QEInvestTools.Partner = ff.MyGetField(toolsF);
            data.QEInvestHorizont.Partner = ff.MyGetField(horizontF);
            data.QEInvestExperience.Partner = ff.MyGetField(experienceF);
            data.QEInvestRisk.Partner = ff.MyGetField(riskF);

            if (string.IsNullOrEmpty(data.QEInvestGoal.Partner) &&
                string.IsNullOrEmpty(data.QEInvestTools.Partner) &&
                string.IsNullOrEmpty(data.QEInvestHorizont.Partner) &&
                string.IsNullOrEmpty(data.QEInvestExperience.Partner) &&
                string.IsNullOrEmpty(data.QEInvestRisk.Partner))
                data.RefuseInformation.Partner = true;
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part5
        void Read_Part5_STD(FormFUSCZ fus, AcroFields ff)
        {
            bool enabled = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r36);
            if (!enabled) { return; }

            fus.FinancialDsDps = new FinancialDsDps();

            fus.FinancialDsDps.ClientPartnerRefuse = ff.MyGetField(DynFields.FUS.CheckBoxFields.r37_1);

            // DS
            fus.FinancialDsDps.ClientStrategyDS = ff.MyGetField(DynFields.FUS.CheckBoxFields.r37_2);
            fus.FinancialDsDps.ClientStrategyDSOptions = ff.MyGetField(DynFields.FUS.RadioButtonFields.r38);

            fus.FinancialDsDps.PartnerStrategyDS = ff.MyGetField(DynFields.FUS.CheckBoxFields.r37_3);
            fus.FinancialDsDps.PartnerStrategyDSOptions = ff.MyGetField(DynFields.FUS.RadioButtonFields.r39);

            fus.FSSavingDS = fus.FinancialDsDps.ClientStrategyDS || fus.FinancialDsDps.PartnerStrategyDS;
            // --
            if (fus.FSSavingDS)
            {
                fus.FinancialDsDps.ClientDSDate = ff.MyGetField(DynFields.FUS.TextFields.T222).ToDateTime();
                fus.FinancialDsDps.ClientDSTime = ff.MyGetField(DynFields.FUS.TextFields.T223).ToDateTime();
            }

            // DPS client
            string dpsC = ff.MyGetField(DynFields.FUS.RadioButtonFields.r37);
            if (dpsC == "DPS")
            {
                fus.FinancialDsDps.ClientStrategyDPS = true;
                fus.FinancialDsDps.ClientStrategyDPSOptions = ff.MyGetField(DynFields.FUS.RadioButtonFields.r40);
            }
            else if (dpsC == "TF")
            {
                fus.FinancialDsDps.ClientStrategyTF = true;
                fus.FinancialDsDps.ClientStrategyTFOptions = ff.MyGetField(DynFields.FUS.RadioButtonFields.r42);
            }

            // DPS partner
            string dpsP = ff.MyGetField(DynFields.FUS.RadioButtonFields.x37);
            if (dpsP == "DPS")
            {
                fus.FinancialDsDps.PartnerStrategyDPS = true;
                fus.FinancialDsDps.PartnerStrategyDPSOptions = ff.MyGetField(DynFields.FUS.RadioButtonFields.r41);
            }
            else if (dpsP == "TF")
            {
                fus.FinancialDsDps.PartnerStrategyTF = true;
                fus.FinancialDsDps.PartnerStrategyTFOptions = ff.MyGetField(DynFields.FUS.RadioButtonFields.r43);
            }

            fus.FSSavingDPS = fus.FinancialDsDps.PartnerStrategyTF || fus.FinancialDsDps.PartnerStrategyDPS || fus.FinancialDsDps.ClientStrategyTF || fus.FinancialDsDps.ClientStrategyDPS;

            if (fus.FSSavingDPS)
            {
                fus.FinancialDsDps.PartnerDSDate = ff.MyGetField(DynFields.FUS.TextFields.T224).ToDateTime();
                fus.FinancialDsDps.PartnerDSTime = ff.MyGetField(DynFields.FUS.TextFields.T225).ToDateTime();
            }

            // TODO instituce - počet smluv?
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part5_ZV(FormFUSCZ fus, AcroFields ff)
        {
            bool enabled = ff.ReadBoolValue(DynFields.ZV.RadioButtonFields.r16);
            if (!enabled) { return; }

            fus.FinancialDsDps = new FinancialDsDps();

            fus.FinancialDsDps.ClientPartnerRefuse = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb32);

            // DS
            fus.FinancialDsDps.ClientStrategyDS = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb33);
            fus.FinancialDsDps.ClientStrategyDSOptions = ff.MyGetField(DynFields.ZV.RadioButtonFields.r17);

            fus.FSSavingDS = fus.FinancialDsDps.ClientStrategyDS;
            // --
            if (fus.FSSavingDS)
            {
                fus.FinancialDsDps.ClientDSDate = ff.MyGetField(DynFields.ZV.TextFields.T35).ToDateTime();
                fus.FinancialDsDps.ClientDSTime = ff.MyGetField(DynFields.ZV.TextFields.T36).ToDateTime();
            }

            // DPS client
            string dpsC = ff.MyGetField(DynFields.ZV.RadioButtonFields.r18);
            if (dpsC == "DPS")
            {
                fus.FinancialDsDps.ClientStrategyDPS = true;
                fus.FinancialDsDps.ClientStrategyDPSOptions = ff.MyGetField(DynFields.ZV.RadioButtonFields.r19);
            }
            else if (dpsC == "TF")
            {
                fus.FinancialDsDps.ClientStrategyTF = true;
                fus.FinancialDsDps.ClientStrategyTFOptions = ff.MyGetField(DynFields.ZV.RadioButtonFields.r20);
            }

            fus.FSSavingDPS = fus.FinancialDsDps.ClientStrategyTF || fus.FinancialDsDps.ClientStrategyDPS;

            if (fus.FSSavingDPS)
            {
                fus.FinancialDsDps.PartnerDSDate = ff.MyGetField(DynFields.ZV.TextFields.T38).ToDateTime();
                fus.FinancialDsDps.PartnerDSTime = ff.MyGetField(DynFields.ZV.TextFields.T39).ToDateTime();
            }

            // TODO instituce - počet smluv?
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part6
        void Read_Part6_STD(FormFUSCZ fus, AcroFields ff)
        {
            bool enabled = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r44);
            if (!enabled) { return; }

            // #6758
            FinancialInsurance finInsurance = new FinancialInsurance(true);
            bool clientYN, partnerYN, childYN;

            #region 1. pojištění osob
            clientYN = partnerYN = childYN = false;

            clientYN |= finInsurance.IIDeath.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb99);
            partnerYN |= finInsurance.IIDeath.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb100);

            clientYN |= finInsurance.IIDeathInjury.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb101);
            partnerYN |= finInsurance.IIDeathInjury.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb102);

            // jedna kolonka
            finInsurance.IIPermanentInjuryFree = ff.MyGetField(DynFields.FUS.CheckBoxFields.r45);
            finInsurance.IIPermanentInjurySingle = ff.MyGetField(DynFields.FUS.CheckBoxFields.r45_2);
            //--

            clientYN |= finInsurance.IIPermanentInjury.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb105);
            partnerYN |= finInsurance.IIPermanentInjury.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb106);
            childYN |= finInsurance.IIPermanentInjury.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb107);

            // jedna kolonka
            finInsurance.IIInvalidityFree = ff.MyGetField(DynFields.FUS.CheckBoxFields.r46);
            finInsurance.IIInvaliditySingle = ff.MyGetField(DynFields.FUS.CheckBoxFields.r46_2);
            //--

            clientYN |= finInsurance.IIInvalidity.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb108);
            partnerYN |= finInsurance.IIInvalidity.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb109);
            childYN |= finInsurance.IIInvalidity.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb110);

            clientYN |= finInsurance.IILowInjury.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb111);
            partnerYN |= finInsurance.IILowInjury.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb112);
            childYN |= finInsurance.IILowInjury.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb113);

            clientYN |= finInsurance.IISeriousDiseases.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb114);
            partnerYN |= finInsurance.IISeriousDiseases.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb115);
            childYN |= finInsurance.IISeriousDiseases.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb116);

            clientYN |= finInsurance.InsuranceSurvivalGuaranteed.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb117);
            partnerYN |= finInsurance.InsuranceSurvivalGuaranteed.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb118);
            childYN |= finInsurance.InsuranceSurvivalGuaranteed.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb119);

            clientYN |= finInsurance.InsuranceSurvivalInvestFund.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb120);
            partnerYN |= finInsurance.InsuranceSurvivalInvestFund.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb121);
            childYN |= finInsurance.InsuranceSurvivalInvestFund.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb122);

            clientYN |= finInsurance.RequiredInvestProfile.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb123);
            partnerYN |= finInsurance.RequiredInvestProfile.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb124);
            childYN |= finInsurance.RequiredInvestProfile.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb125);

            clientYN |= finInsurance.DynamicProfil.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb126);
            partnerYN |= finInsurance.DynamicProfil.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb127);
            childYN |= finInsurance.DynamicProfil.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb128);

            if (finInsurance.DynamicProfil.Client != true)
                clientYN |= finInsurance.BalancedProfil.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb129);

            if (finInsurance.DynamicProfil.Partner != true)
                partnerYN |= finInsurance.BalancedProfil.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb130);

            if (finInsurance.DynamicProfil.Child != true)
                childYN |= finInsurance.BalancedProfil.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb131);

            if (finInsurance.DynamicProfil.Client != true && finInsurance.BalancedProfil.Client != true)
                clientYN |= finInsurance.KonzervativeProfil.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb132);

            if (finInsurance.DynamicProfil.Partner != true && finInsurance.BalancedProfil.Partner != true)
                partnerYN |= finInsurance.KonzervativeProfil.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb133);

            if (finInsurance.DynamicProfil.Child != true && finInsurance.BalancedProfil.Child != true)
                childYN |= finInsurance.KonzervativeProfil.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb134);

            fus.Announcement.AnnouncementLiveSaving.Client = clientYN;
            fus.Announcement.AnnouncementLiveSaving.Partner = partnerYN;
            fus.Announcement.AnnouncementLiveSaving.Child = childYN;

            bool isInvest = IsInvest(fus);

            if (clientYN || partnerYN || childYN)
            {
                finInsurance.InsuranceIndividual = true;

                if (isInvest)
                {
                    fus.FSInsuranceLive = fus.FSInsurancePersonal = true;
                }
                else
                {
                    fus.FSInsuranceLiveOther = fus.FSInsurancePersonal = true;
                }
            }
            #endregion

            #region 2. pojištění majetku
            clientYN = partnerYN = childYN = false;

            clientYN |= finInsurance.IPHomeReality.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb135);
            partnerYN |= finInsurance.IPHomeReality.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb136);

            clientYN |= finInsurance.IPOtherReality.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb137);
            partnerYN |= finInsurance.IPOtherReality.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb138);

            clientYN |= finInsurance.IPAllRisk.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb139);
            partnerYN |= finInsurance.IPAllRisk.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb140);

            clientYN |= finInsurance.IPTheft.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb141);
            partnerYN |= finInsurance.IPTheft.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb142);

            clientYN |= finInsurance.IPVehicleOthers.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb143);
            partnerYN |= finInsurance.IPVehicleOthers.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb144);

            clientYN |= finInsurance.IPOthers.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb145);
            partnerYN |= finInsurance.IPOthers.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb146);

            fus.FSInsuranceProperty = finInsurance.InsuranceProperty = clientYN || partnerYN;
            #endregion

            #region 3. pojištění odpovědnosti za újmu
            clientYN = partnerYN = childYN = false;

            clientYN |= finInsurance.IDPublic.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb147);
            partnerYN |= finInsurance.IDPublic.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb148);

            clientYN |= finInsurance.IDOwnReality.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb149);
            partnerYN |= finInsurance.IDOwnReality.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb150);

            clientYN |= finInsurance.IDPZP.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb151);
            partnerYN |= finInsurance.IDPZP.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb152);

            clientYN |= finInsurance.IDEmployer.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb153);
            partnerYN |= finInsurance.IDEmployer.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb154);

            clientYN |= finInsurance.IDOthers.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb155);
            partnerYN |= finInsurance.IDOthers.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb156);

            fus.FSInsuranceLiability = finInsurance.InsuranceDamage = clientYN || partnerYN;
            #endregion

            #region 4. cestovní pojištění
            clientYN = partnerYN = childYN = false;

            clientYN |= finInsurance.InsuranceTravelingDetail.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb157);
            partnerYN |= finInsurance.InsuranceTravelingDetail.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb158);
            childYN |= finInsurance.InsuranceTravelingDetail.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb159);

            fus.FSInsuranceTravel = finInsurance.InsuranceTraveling = clientYN || partnerYN || childYN;
            #endregion

            #region 5. Jiné / další požadavky k pojištění
            //clientYN = partnerYN = childYN = false;
            //
            finInsurance.InsuranceOtherDetail.Client = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb160);
            finInsurance.InsuranceOtherDetail.Partner = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb161);
            finInsurance.InsuranceOtherDetail.Child = ff.MyGetField(DynFields.FUS.CheckBoxFields.chb162);
            //
            //fus.FSInsuranceOthers = clientYN || partnerYN || childYN;
            #endregion

            // 6. Výše požadovaná pojištění by měla být daňově zvýhodněná
            finInsurance.InsuranceTaxAdvantage = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r47, false);

            // 7. Podrobnější vyjádření Zákazníka/Partnera k výše uvedeným požadavkům
            finInsurance.InsuranceNote = ff.MyGetField(DynFields.FUS.TextFields.T163);

            // nesrovnalosti
            finInsurance.Discrepancies = ff.MyGetField(DynFields.FUS.TextFields.T167);

            if (!ExtensionMethods.AtleastOnetrue(finInsurance.InsuranceIndividual,
                finInsurance.InsuranceProperty,
                finInsurance.InsuranceDamage,
                finInsurance.InsuranceTraveling)
                && ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r44, false))
                fus.FSInsuranceOthers = true;

            // TODO sectors
        }

        private bool IsInvest(FormFUSCZ fus)
        {
            //fus.FinancialDataVerification.RefuseInformation
            if (fus.FinancialDataVerification != null)
            {
                if (fus.FinancialDataVerification.RefuseInformation.LogicOR)
                    return true;

                if (ExtensionMethods.AtleastOnetrue(fus.FinancialDataVerification.QEInvestGoal,
                    fus.FinancialDataVerification.QEInvestTools,
                    fus.FinancialDataVerification.QEInvestHorizont,
                    fus.FinancialDataVerification.QEInvestExperience,
                    fus.FinancialDataVerification.QEInvestRisk))
                    return true;
            }
            return false;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part6_ZV(FormFUSCZ fus, AcroFields ff)
        {
            bool enabled = ff.ReadBoolValue(DynFields.ZV.RadioButtonFields.r21);
            if (!enabled) { return; }
            // #6758
            FinancialInsurance finInsurance = new FinancialInsurance(true);

            bool clientYN;

            #region 1. pojištění osob
            clientYN = false;

            clientYN |= finInsurance.IIDeath.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb41);

            clientYN |= finInsurance.IIDeathInjury.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb42);

            // jedna kolonka
            finInsurance.IIPermanentInjuryFree = ff.MyGetField(DynFields.ZV.CheckBoxFields.r22_1);
            finInsurance.IIPermanentInjurySingle = ff.MyGetField(DynFields.ZV.CheckBoxFields.r22_2);
            //--

            clientYN |= finInsurance.IIPermanentInjury.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb43);

            // jedna kolonka
            finInsurance.IIInvalidityFree = ff.MyGetField(DynFields.ZV.CheckBoxFields.r23_1);
            finInsurance.IIInvaliditySingle = ff.MyGetField(DynFields.ZV.CheckBoxFields.r23_2);
            //--

            clientYN |= finInsurance.IIInvalidity.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb44);

            clientYN |= finInsurance.IILowInjury.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb45);

            clientYN |= finInsurance.IISeriousDiseases.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb46);

            clientYN |= finInsurance.InsuranceSurvivalGuaranteed.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.r24_1);

            clientYN |= finInsurance.InsuranceSurvivalInvestFund.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.r24_2);

            clientYN |= finInsurance.RequiredInvestProfile.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb47);

            clientYN |= finInsurance.DynamicProfil.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb56);

            if (finInsurance.DynamicProfil.Client != true)
                clientYN |= finInsurance.BalancedProfil.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb57);

            if (finInsurance.DynamicProfil.Client != true && finInsurance.BalancedProfil.Client != true)
                clientYN |= finInsurance.KonzervativeProfil.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb58);

            bool isInvest = IsInvest(fus);

            if (clientYN)
            {
                finInsurance.InsuranceIndividual = true;

                if (isInvest)
                {
                    fus.FSInsuranceLive = fus.FSInsurancePersonal = true;

                    if (finInsurance.InsuranceSurvivalGuaranteed.Client)
                        fus.FSInsuranceLiveOther = fus.FSInsurancePersonal = true;
                }
                else
                {
                    fus.FSInsuranceLiveOther = fus.FSInsurancePersonal = true;
                }
            }
            //fus.FSInsuranceLive = fus.FSInsuranceLiveOther = fus.FSInsurancePersonal = finInsurance.InsuranceIndividual = clientYN;
            #endregion

            #region 2. pojištění odpovědnosti za újmu
            clientYN = false;

            clientYN |= finInsurance.IDPublic.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb48);

            clientYN |= finInsurance.IDOwnReality.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb49);

            clientYN |= finInsurance.IDPZP.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb50);

            clientYN |= finInsurance.IDEmployer.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb51);

            clientYN |= finInsurance.IDOthers.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb52);

            fus.FSInsuranceLiability = finInsurance.InsuranceDamage = clientYN;
            #endregion

            // 3. Jiné / další požadavky k pojištění
            finInsurance.InsuranceOtherDetail.Client = ff.MyGetField(DynFields.ZV.CheckBoxFields.chb53);

            if (!ExtensionMethods.AtleastOnetrue(finInsurance.InsuranceIndividual, finInsurance.InsuranceProperty, finInsurance.InsuranceDamage, finInsurance.InsuranceTraveling) && ff.ReadBoolValue(DynFields.ZV.RadioButtonFields.r21, false))
                fus.FSInsuranceOthers = true;

            // 4. Výše požadovaná pojištění by měla být daňově zvýhodněná
            finInsurance.InsuranceTaxAdvantage = ff.ReadBoolValue(DynFields.ZV.RadioButtonFields.r25, false);

            #region 5. Podrobnější vyjádření Zákazníka/Partnera k výše uvedeným požadavkům
            StringBuilder sb = new StringBuilder();
            sb.Append(ff.MyGetField(DynFields.ZV.TextFields.T54));
            sb.Append(" ");
            sb.Append(ff.MyGetField(DynFields.ZV.TextFields.T55));
            sb.Append(" ");
            sb.Append(ff.MyGetField(DynFields.ZV.TextFields.T56));
            sb.Append(" ");
            sb.Append(ff.MyGetField(DynFields.ZV.TextFields.T57));

            finInsurance.InsuranceNote = sb.ToString();
            #endregion

            #region nesrovnalosti
            sb.Clear();
            sb.Append(ff.MyGetField(DynFields.ZV.TextFields.T62));
            sb.Append(" ");
            sb.Append(ff.MyGetField(DynFields.ZV.TextFields.T63));

            finInsurance.Discrepancies = sb.ToString();
            #endregion
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part7
        void Read_Part7_STD(FormFUSCZ fus, AcroFields ff)
        {
            bool enabled = ff.ReadBoolValue(DynFields.FUS.RadioButtonFields.r175, false);
            if (!enabled) return;

            fus.FSInvestment = enabled;

            fus.FinancialInvestment = new FinancialInvestment(true);

            fus.FinancialInvestment.InvestTowardQuestionnaireCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r176);

            switch (fus.FinancialInvestment.InvestTowardQuestionnaireCode.Client)
            {
                case "CZQTRULY":
                    this.Read_Part7_Client(fus, ff);
                    break;
                case "CZQREFUSE":
                    break;
                case "CZQANSWERED":
                    break;
                case "CZINVESTORINEXPERIENCED":
                case "CZINVESTORLOW":
                case "CZINVESTORMEDIUM":
                case "CZINVESTORHIGH":
                    fus.FinancialInvestment.ExpInvetorCode.Client = fus.FinancialInvestment.InvestTowardQuestionnaireCode.Client;
                    fus.FinancialInvestment.InvestTowardQuestionnaireCode.Client = "CZQANSWERED";
                    break;
            }

            fus.FinancialInvestment.InvestTowardQuestionnaireCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r177);

            switch (fus.FinancialInvestment.InvestTowardQuestionnaireCode.Partner)
            {
                case "CZQTRULY":
                    this.Read_Part7_Partner(fus, ff);
                    break;
                case "CZQREFUSE":
                    break;
                case "CZQANSWERED":
                    break;
                case "CZINVESTORINEXPERIENCED":
                case "CZINVESTORLOW":
                case "CZINVESTORMEDIUM":
                case "CZINVESTORHIGH":
                    fus.FinancialInvestment.ExpInvetorCode.Partner = fus.FinancialInvestment.InvestTowardQuestionnaireCode.Partner;
                    fus.FinancialInvestment.InvestTowardQuestionnaireCode.Partner = "CZQANSWERED";
                    break;
            }

            fus.FinancialInvestment.InvestDate = ff.MyGetField(DynFields.FUS.TextFields.T216).ToDateTime();
            fus.FinancialInvestment.InvestTime = ff.MyGetField(DynFields.FUS.TextFields.T217).ToDateTime();

            //Load invetment contracts into FUS

            //fus.SectorInvestments
            fus.SectorInvestments = new List<SectorInvestment>();

            SectorInvestment sector1 = new SectorInvestment();
            sector1.Form_Read(fus, ff,
                DynFields.FUS.TextFields.T181,
                DynFields.FUS.TextFields.T182,
                DynFields.FUS.TextFields.T183,
                DynFields.FUS.TextFields.T184,
                DynFields.FUS.TextFields.T185,
                DynFields.FUS.TextFields.T186,
                DynFields.FUS.TextFields.T187,
                DynFields.FUS.RadioButtonFields.r190);

            if (!string.IsNullOrWhiteSpace(sector1.KTFinancialTool))
                fus.SectorInvestments.Add(sector1);

            SectorInvestment sector2 = new SectorInvestment();
            sector2.Form_Read(fus, ff,
                DynFields.FUS.TextFields.T188,
                DynFields.FUS.TextFields.T189,
                DynFields.FUS.TextFields.T190,
                DynFields.FUS.TextFields.T191,
                DynFields.FUS.TextFields.T192,
                DynFields.FUS.TextFields.T193,
                DynFields.FUS.TextFields.T194,
                DynFields.FUS.RadioButtonFields.r191);

            if (!string.IsNullOrWhiteSpace(sector2.KTFinancialTool))
                fus.SectorInvestments.Add(sector2);

            SectorInvestment sector3 = new SectorInvestment();
            sector3.Form_Read(fus, ff,
                DynFields.FUS.TextFields.T195,
                DynFields.FUS.TextFields.T196,
                DynFields.FUS.TextFields.T197,
                DynFields.FUS.TextFields.T198,
                DynFields.FUS.TextFields.T199,
                DynFields.FUS.TextFields.T200,
                DynFields.FUS.TextFields.T201,
                DynFields.FUS.RadioButtonFields.r192);

            if (!string.IsNullOrWhiteSpace(sector3.KTFinancialTool))
                fus.SectorInvestments.Add(sector3);

            SectorInvestment sector4 = new SectorInvestment();
            sector4.Form_Read(fus, ff,
                DynFields.FUS.TextFields.T202,
                DynFields.FUS.TextFields.T203,
                DynFields.FUS.TextFields.T204,
                DynFields.FUS.TextFields.T205,
                DynFields.FUS.TextFields.T206,
                DynFields.FUS.TextFields.T207,
                DynFields.FUS.TextFields.T208,
                DynFields.FUS.RadioButtonFields.r193);

            if (!string.IsNullOrWhiteSpace(sector4.KTFinancialTool))
                fus.SectorInvestments.Add(sector4);

            SectorInvestment sector5 = new SectorInvestment();
            sector5.Form_Read(fus, ff,
                DynFields.FUS.TextFields.T209,
                DynFields.FUS.TextFields.T210,
                DynFields.FUS.TextFields.T211,
                DynFields.FUS.TextFields.T212,
                DynFields.FUS.TextFields.T213,
                DynFields.FUS.TextFields.T214,
                DynFields.FUS.TextFields.T215,
                DynFields.FUS.RadioButtonFields.r194);

            if (!string.IsNullOrWhiteSpace(sector5.KTFinancialTool))
                fus.SectorInvestments.Add(sector5);

        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part7_Client(FormFUSCZ fus, AcroFields ff)
        {
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                fus.FinancialInvestment.QEInvestFinancialEducationCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r178);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                fus.FinancialInvestment.QEInvestWorkPositionCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r180);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                fus.FinancialInvestment.QEInvestRiskEducationCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r182);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                fus.FinancialInvestment.QEInvestExpInvestInstrumetnCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r184);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                fus.FinancialInvestment.QEInvestFreqClosingDealCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r186);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                fus.FinancialInvestment.QEInvestExpInvestCode.Client = ff.MyGetField(DynFields.FUS.RadioButtonFields.r188);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part7_Partner(FormFUSCZ fus, AcroFields ff)
        {
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFinancialEducationCode != null)
                fus.FinancialInvestment.QEInvestFinancialEducationCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r179);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestWorkPositionCode != null)
                fus.FinancialInvestment.QEInvestWorkPositionCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r181);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestRiskEducationCode != null)
                fus.FinancialInvestment.QEInvestRiskEducationCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r183);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestInstrumetnCode != null)
                fus.FinancialInvestment.QEInvestExpInvestInstrumetnCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r185);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestFreqClosingDealCode != null)
                fus.FinancialInvestment.QEInvestFreqClosingDealCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r187);
            if (this.FormFUSCZ.FinancialInvestment.QEInvestExpInvestCode != null)
                fus.FinancialInvestment.QEInvestExpInvestCode.Partner = ff.MyGetField(DynFields.FUS.RadioButtonFields.r189);
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Read_Part8
        void Read_Part8_STD(FormFUSCZ fus, AcroFields ff)
        {
            fus.FinalStatementSignatures = new FinalStatementSignatures();

            fus.FinalStatementSignatures.SignedPlace = ff.MyGetField(DynFields.FUS.TextFields.T219);

            fus.FinalStatementSignatures.DateFUSSigned = ff.MyGetField(DynFields.FUS.TextFields.T220).ToDateTime() ?? DateTime.Now.Date;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Read_Part8_ZV(FormFUSCZ fus, AcroFields ff)
        {
            fus.FinalStatementSignatures = new FinalStatementSignatures();

            fus.FinalStatementSignatures.SignedPlace = ff.MyGetField(DynFields.ZV.TextFields.T70);

            fus.FinalStatementSignatures.DateFUSSigned = ff.MyGetField(DynFields.ZV.TextFields.T71).ToDateTime() ?? DateTime.Now.Date;
        }
        #endregion
        #region ReadFinalChangesSTD
        void ReadFinalChangesSTD(FormFUSCZ fus, AcroFields ff)
        {
            if (fus.GetShowSection.AtLeastOneSection == false)
                fus.FSOthers = true;
        }

        void ReadFinalChangesZV(FormFUSCZ fus, AcroFields ff)
        {
            if (fus.GetShowSection.AtLeastOneSection == false)
                fus.FSOthers = true;
        }
        #endregion

        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Helpers
        string GetInstitution(int? id)
        {
            if (id == null || !id.HasValue)
                return string.Empty;

            var institution = DbProvider.Partners().Where(p => p.PartnerID == id).FirstOrDefault();
            if (institution == null)
                return string.Empty;
            return institution.AltName ?? institution.PartyName;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        string GetProduct(int? id, out int categoryID)
        {
            categoryID = -1;

            if (id == null)
                return string.Empty;

            var product = DbProvider.Services().Where(p => p.ServiceID == id).FirstOrDefault();
            if (product == null) return string.Empty;

            categoryID = product.CategoryID;

            if (string.IsNullOrWhiteSpace(product.AltName))
            {
                return string.Empty;
            }

            return product.AltName;
        }

        private string GetPeriodText(string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            switch (code)
            {
                case Constants.FUSListOptions.AMONTHLY:
                    return "M";
                case Constants.FUSListOptions.AQAERTERLY:
                    return "Č";
                case Constants.FUSListOptions.AHALFYEAR:
                    return "P";
                case Constants.FUSListOptions.AYEAR:
                    return "R";
                case Constants.FUSListOptions.AONCE:
                    return "J";
                case Constants.FUSListOptions.AFINAL:
                    return "CČ";
                case Constants.FUSListOptions.AHYPO:
                    return "VU";
                default: return null;

            }

        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 2015-04-08 - DT - generická serializace, pokud předaný typ nelze serializovat, dojde k výjimce
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Serialize<T>(T data)
        {
            string result = string.Empty;
            var serializer = new XmlSerializer(typeof(T));

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, data);
                result = writer.ToString();
            }

            return result;
        }
        #endregion
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region GetAttachment
        public static byte[] GetAttachment(string printType, string fusType)
        {
            string name = string.Empty;

            if (printType == Constants.FUSPDFTypes.Draft)
            {
                if (fusType == Constants.FUSCZTypes.ZV || fusType == Constants.FUSCZTypes.SMSZV)
                    name = Files.ZV.AttachmentDraft;
                else if (fusType == Constants.FUSCZTypes.ADK)
                    name = Files.ADK.AttachmentDraft;
                else
                    name = Files.FUS.AttachmentDraft;
            }
            else
            {
                if (fusType == Constants.FUSCZTypes.ZV || fusType == Constants.FUSCZTypes.SMSZV)
                    name = Files.ZV.Attachment;
                else if (fusType == Constants.FUSCZTypes.ADK)
                    name = Files.ADK.Attachment;
                else
                    name = Files.FUS.Attachment;
            }

            var pathAt = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Content\\Files\\CZFuses", name);

            return File.ReadAllBytes(pathAt);
        }
        #endregion
    }

    public class TextFormater
    {
        const double SPACEWIDTH = 4;

        private static int GetStringWidth(string text)
        {
            System.Drawing.SizeF sizeF = new System.Drawing.SizeF();
            System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new System.Drawing.Bitmap(1, 1));
            sizeF = graphics.MeasureString(text, new System.Drawing.Font("Arial,Verdana,Helvetica,sans-serif", 10, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point));
            return Convert.ToInt32(sizeF.Width);
        }

        /// <summary>
        /// Get formatted columm by width
        /// </summary>
        /// <param name="texts">Text array</param>
        /// <param name="widths">Width in "pixels" (space have width 4)</param>
        /// <returns></returns>
        public static string FormatColumnText(string[] texts, int[] widths)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var textObj in texts.Select((value, i) => new { i, value }))
            {
                int maxWidth = widths.Length > textObj.i ? widths.ElementAt(textObj.i) : 1;
                string text = textObj.value;
                int insertSpaces = 1;

                //Calculate text width
                var insertedTextWidth = GetStringWidth(text);
                var difference = maxWidth - insertedTextWidth;

                if (difference > 0)
                {
                    insertSpaces = Convert.ToInt32(difference / SPACEWIDTH);
                }

                //Add text
                sb.Append(text);

                //Add empty spaces
                sb.Append(' ', insertSpaces);
            }
            return sb.ToString();
        }
    }
}
