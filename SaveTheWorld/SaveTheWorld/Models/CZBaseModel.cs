﻿using SaveTheWorld.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SaveTheWorld.Models
{
    /// <summary>
    /// 2015-05-07 - DT - několik společných properties fusu a amlka
    /// </summary>
    public class CZBaseModel : BaseModel
    {
        protected bool? isAmlForClient, isAmlForPartner;
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual FormFUSCZ FormFUSCZ { get; set; }

        public SealedConstants.PersonType ClientType()
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.OverallInfo == null)
                return null;

            var personType = FormFUSCZ.OverallInfo.ValueOrDefault(p => p.ClientType);

            if (SealedConstants.PersonType.Individual.Value == personType)
                return SealedConstants.PersonType.Individual;
            else if (SealedConstants.PersonType.Corporation.Value == personType)
                return SealedConstants.PersonType.Corporation;
            else if (SealedConstants.PersonType.IndividualBusinessman.Value == personType)
                return SealedConstants.PersonType.IndividualBusinessman;

            return null;
        }

        public SealedConstants.PersonType PartnerType()
        {
            if (this.FormFUSCZ == null || this.FormFUSCZ.OverallInfo == null)
                return null;

            var personType = FormFUSCZ.OverallInfo.ValueOrDefault(p => p.PartnerType);

            if (SealedConstants.PersonType.Individual.Value == personType)
                return SealedConstants.PersonType.Individual;
            else if (SealedConstants.PersonType.Corporation.Value == personType)
                return SealedConstants.PersonType.Corporation;
            else if (SealedConstants.PersonType.IndividualBusinessman.Value == personType)
                return SealedConstants.PersonType.IndividualBusinessman;

            return null;
        }

        /// <summary>
        /// 2015-05-06 - DT 
        /// </summary>
        [XmlIgnore]
        public bool IsAmlForClient
        {
            get
            {
                if (this.FormFUSCZ == null || this.FormFUSCZ.Announcement == null)
                    return false;

                if (!this.isAmlForClient.HasValue)
                    this.isAmlForClient = ExtensionMethods.AtleastOnetrue(this.FormFUSCZ.Announcement.AnnouncementLeases.VoD(p => p.Client), this.FormFUSCZ.Announcement.AnnouncementLiveSaving.VoD(p => p.Client),
                        this.FormFUSCZ.Announcement.AnnouncementBuildSaving.VoD(p => p.Client), this.FormFUSCZ.Announcement.AnnouncementLiveSaving.VoD(p => p.Child),
                        this.FormFUSCZ.Announcement.AnnouncementBuildSaving.VoD(p => p.Child), this.FormFUSCZ.Announcement.AnnouncementLeases.VoD(p => p.Child),
                        this.FormFUSCZ.Announcement.AnnouncementSavingDPS.VoD(p => p.Child), this.FormFUSCZ.Announcement.AnnouncementSavingDPS.VoD(p => p.Child),
                        this.FormFUSCZ.Announcement.AnnouncementInvestment.VoD(p => p.Child), this.FormFUSCZ.Announcement.AnnouncementInvestment.VoD(p => p.Child));

                return isAmlForClient.Value;
            }

            set
            {
                this.isAmlForClient = value;
            }
        }


        /// <summary>
        /// 2015-05-06 - DT 
        /// </summary>
        [XmlIgnore]
        public bool IsAmlForPartner
        {
            get
            {
                if (this.FormFUSCZ == null || this.FormFUSCZ.Announcement == null)
                    return false;

                if (!this.isAmlForPartner.HasValue)
                    this.isAmlForPartner = ExtensionMethods.AtleastOnetrue(this.FormFUSCZ.Announcement.AnnouncementLeases.VoD(p => p.Partner),
                        this.FormFUSCZ.Announcement.AnnouncementLiveSaving.VoD(p => p.Partner),
                        this.FormFUSCZ.Announcement.AnnouncementBuildSaving.VoD(p => p.Partner),
                        this.FormFUSCZ.Announcement.AnnouncementSavingDPS.VoD(p => p.Partner),
                        this.FormFUSCZ.Announcement.AnnouncementInvestment.VoD(p => p.Partner));

                return isAmlForPartner.Value;
            }

            set
            {
                this.isAmlForPartner = value;
            }
        }
    }
}
