﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SaveTheWorld.Models
{
    public class Registration
    {
        public string StatutoryRegistrationCode { get; set; }
        public bool IsRegistered { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
    }

    public class RadioButtonOption
    {
        public string Code { get; set; }
        public int? Value { get; set; }
        public string ResourceCode { get; set; }
        public string Name { get; set; }
    }

    public class CheckBoxOption : RadioButtonOption
    {

    }

    public class ListOption
    {
        public bool? Key { get; set; }
        public string Value { get; set; }
    }

    public class IntListOption
    {
        public int? Key { get; set; }
        public string Value { get; set; }
    }

    public class StringListOption
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class StringListOptionType
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }

    public class Contact
    {
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class YesNoClientPartner
    {
        public bool? Client { get; set; }
        public bool? Partner { get; set; }

    }
    public class YesNoClientPartnerChild
    {
        public bool? Client { get; set; }
        public bool? Partner { get; set; }
        public bool? Child { get; set; }
    }

    public class BoolClientPartner
    {
        public bool Client { get; set; }
        public bool Partner { get; set; }

        [XmlIgnore]
        public bool LogicOR { get { return this.Client || this.Partner; } }

        [XmlIgnore]
        public bool LogicNAND { get { return !this.Client || !this.Partner; } }
    }

    public class DecimalClientPartner
    {
        //[RegularExpressionLocalize(@"^((\d| )*(,\d*[0-9])?)?$", ErrorMessageResourceName = "DecimalClientPartner.ClientDecimal")]
        public string Client { get; set; }
        //[RegularExpressionLocalize(@"^((\d| )*(,\d*[0-9])?)?$", ErrorMessageResourceName = "DecimalClientPartner.PartnerDecimal")]
        public string Partner { get; set; }
    }

    public class StringClientPartner
    {
        public string Client { get; set; }
        public string Partner { get; set; }
    }

    public class BoolClientPartnerChild
    {
        public bool Client { get; set; }
        public bool Partner { get; set; }
        public bool Child { get; set; }
    }
}
