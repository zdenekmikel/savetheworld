﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SaveTheWorld.Models
{
    public class FUSAML
    {
        #region Instance Properties

        public Int32 AMLID { get; set; }

        public Int32 FUSID { get; set; }

        public Int32 AgentID { get; set; }

        public DateTime DateCreated { get; set; }

        public Int32 CreatedBy { get; set; }

        public DateTime DateUpdated { get; set; }

        public Int32 UpdatedBy { get; set; }

        public Boolean Active { get; set; }

        public Int32? AMLStatusID { get; set; }

        public string XmlContent { get; set; }

        #endregion Instance Properties
    }
}
