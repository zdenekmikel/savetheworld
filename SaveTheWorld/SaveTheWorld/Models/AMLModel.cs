﻿using SaveTheWorld.Extensions;
using System;
using System.IO;
using System.Linq;
using static SaveTheWorld.Constants;

namespace SaveTheWorld.Models
{
    public class AMLModel : CZBaseModel
    {
        #region properties
        public string PageActual { get; set; }

        //[Required]
        public int? FUSID { get; set; }
        //public int AgentID { get; set; }
        public int? AMLID { get; set; }

        public DateTime? FUSSignedDate { get; set; }

        public string btnSubmit { get; set; }
        public string btnSubmitClose { get; set; }

        public bool AMLForClient { get; set; }
        public bool AMLForPartner { get; set; }
        public bool IsClosed { get; set; }

        public FormAML FormAML { get; set; }

        public override FormFUSCZ FormFUSCZ
        {
            get
            {
                return dbModel != null ? dbModel.FormFUSCZ : null;
            }
            set
            {

            }
        }

        public FUS FUS { get { return dbModel != null ? dbModel.FUS : null; } }
        public FUSAML FUSAML { get; set; }
        public StandardFUSCZModel dbModel { get; set; }


        #endregion

        #region Work with data
        public void LoadAMLData(int fUSID)
        {
            dbModel = new StandardFUSCZModel();
            int? createdBy = null;
            dbModel.LoadModel(fUSID, out createdBy, true);

            if (dbModel.FormFUSCZ.Announcement != null)
            {
                if (ExtensionMethods.AtleastOnetrue(FormFUSCZ.Announcement.AnnouncementLeases.VoD(p => p.Client), FormFUSCZ.Announcement.AnnouncementLeases.VoD(p => p.Child),
                        FormFUSCZ.Announcement.AnnouncementLiveSaving.VoD(p => p.Client), FormFUSCZ.Announcement.AnnouncementLiveSaving.VoD(p => p.Child),
                        FormFUSCZ.Announcement.AnnouncementBuildSaving.VoD(p => p.Client), FormFUSCZ.Announcement.AnnouncementBuildSaving.VoD(p => p.Child),
                        FormFUSCZ.Announcement.AnnouncementSavingDPS.VoD(p => p.Client), FormFUSCZ.Announcement.AnnouncementSavingDPS.VoD(p => p.Child),
                        FormFUSCZ.Announcement.AnnouncementInvestment.VoD(p => p.Client), FormFUSCZ.Announcement.AnnouncementInvestment.VoD(p => p.Child),
                        FormFUSCZ.Announcement.AnnouncementBanking.VoD(p => p.Client), FormFUSCZ.Announcement.AnnouncementBanking.VoD(p => p.Child)))
                {
                    AMLForClient = true;
                }

                if (dbModel.FormFUSCZ.FUSForPartner && ExtensionMethods.AtleastOnetrue(FormFUSCZ.Announcement.AnnouncementLeases.VoD(p => p.Partner),
                        FormFUSCZ.Announcement.AnnouncementLiveSaving.VoD(p => p.Partner),
                        FormFUSCZ.Announcement.AnnouncementBuildSaving.VoD(p => p.Partner),
                        FormFUSCZ.Announcement.AnnouncementSavingDPS.VoD(p => p.Partner),
                        FormFUSCZ.Announcement.AnnouncementInvestment.VoD(p => p.Partner),
                        FormFUSCZ.Announcement.AnnouncementBanking.VoD(p => p.Partner)))
                {
                    AMLForPartner = true;
                }
            }

            this.FUSID = fUSID;

            FUSAML = DbProvider.CustomerFUSAML(0, 0, fUSID);
            if (FUSAML != null)
                AMLID = FUSAML.AMLID;

        }

        //public void TicketFUSResponse(string content)
        //{
        //    FUShelper.Ticket_WriteOnCloseFUS(FUS, CurrentProfile.UserID, content);
        //}

        //public int? SaveModelToDb(string xml, bool close = false, bool ticketInsert = false)
        //{
        //    var fusAML = new FUSAML()
        //    {
        //        AMLID = this.AMLID.Value,
        //        AMLStatusID = close ? Constants.FUSStatuses.Closed : Constants.FUSStatuses.InProgress,
        //        XmlContent = xml
        //    };
        //    DbProvider.Connection.CustomerFUSAMLUpdate(Auth.Profile.UserID, RequestedAgent.AgentID, fusAML);
        //    if (FUS != null && FUS.XmlContent != null)
        //    {
        //        DbProvider.Connection.CustomerFUSUpdate(Auth.Profile.UserID, RequestedAgent.AgentID, FUS);
        //        if (ticketInsert)
        //        {
        //            TicketFUSResponse((ResourceGlobal("Workflow.TicketStatus.Content." + FUSLists.GetTicketStatusID(FUS.FUSStatusID).ToString(), FUS.FUSID)));
        //        }
        //    }

        //    return FUSID;
        //}

        #endregion
        #region help Methods

        public static int? SaveAMLDocumentDoDb(int FUSID, byte[] pdfFuz, int? docID, int userID)
        {
            pdfFuz = PDFExtensions.LockAcroFieldPdfForms(pdfFuz).ToArray();
            //int? docID = DbProvider.Connection.WebDocumentInsert(DocumentTypes.AML, string.Format("{0}_AML", FUSID.ToString()), "", CurrentProfile.UserID, BL.FinData.Web.Classes.Documents.DocumentManager.ContractsRoot, 1);

            if (docID != null)
            {
                DocumentFileInfo docInfo = new DocumentFileInfo()
                {
                    contentType = "application/pdf",
                    documentFileTypeCode = DocumentFileTypes.FILE,
                    documentID = docID,
                    documentType = ".pdf",
                    index = 1,
                    note = "",
                    size = (long)pdfFuz.Length,
                    userID = userID,
                    FileByteStream = new MemoryStream(pdfFuz)
                };

                Guid? fileID = DbProvider.DocumentFileInsert(docInfo).documentFileID;

                //DbProvider.DocumentRelationInsert(userID, docID.Value, FUSID, DocumentRelationTypes.FUS);

                return docID;
            }

            return null;
        }

        //public string GetErrorResource(string value)
        //{
        //    return ResourceManager.GetResource(string.Format("Web.Model.{0}.Required.Error", value));
        //}

        #endregion
    }
}