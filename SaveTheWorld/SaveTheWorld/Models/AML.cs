﻿using System;
using System.Xml.Serialization;

namespace SaveTheWorld.Models
{
    public class FormAML
    {
        [XmlAttribute("version")]
        public string Version = "0.8";

        public bool FUSForPartner { get; set; }

        public bool HasClientRiskCountry { get; set; }

        public bool HasPartnerRiskCountry { get; set; }

        //[Resource("FUSCZ.AML.ActualFinancialOwner")]
        public string ActualFinancialOwner { get; set; }

        //[Resource("FUSCZ.AML.Details")]
        public string Details { get; set; }

        //[Resource("FUSCZ.AML.Q1TradeOver")]
        public BoolClientPartner Q1TradeOver { get; set; }

        //[Resource("FUSCZ.AML.Q2Exponed")]
        public BoolClientPartner Q2Exponed { get; set; }

        //[Resource("FUSCZ.AML.Q3SelectProduct")]
        public BoolClientPartner Q3SelectProduct { get; set; }

        //[Resource("FUSCZ.AML.Q3DeclarePurpose")]
        public string Q3DeclarePurpose { get; set; }

        //[Resource("FUSCZ.AML.Q3ActualPurpose")]
        public string Q3ActualPurpose { get; set; }

        //[Resource("FUSCZ.AML.Q4LiveRisk")]
        public BoolClientPartner Q4LiveRisk { get; set; }

        //[Resource("FUSCZ.AML.Q5HQRisk")]
        public BoolClientPartner Q5HQRisk { get; set; }

        //[Resource("FUSCZ.AML.Q6LiveTaxFree")]
        public BoolClientPartner Q6LiveTaxFree { get; set; }

        //[Resource("FUSCZ.AML.Q7HQTaxFree")]
        public BoolClientPartner Q7HQTaxFree { get; set; }

        //[Resource("FUSCZ.AML.Q8HQBusinessPartner")]
        public BoolClientPartner Q8HQBusinessPartner { get; set; }

        //[Resource("FUSCZ.AML.Q9OwnerShip")]
        public BoolClientPartner Q9OwnerShip { get; set; }

        //[Resource("FUSCZ.AML.Q10Source")]
        public BoolClientPartner Q10Source { get; set; }

        //[Resource("FUSCZ.AML.Q11ExistFact")]
        public BoolClientPartner Q11ExistFact { get; set; }

        //[Resource("FUSCZ.AML.Q12Unusual")]
        public BoolClientPartner Q12Unusual { get; set; }

        //[Resource("FUSCZ.AML.Q13UnusualFC")]
        public BoolClientPartner Q13UnusualFC { get; set; }

        //[Resource("FUSCZ.AML.Q14Identificiation")]
        public BoolClientPartner Q14Identificiation { get; set; }

        //[Resource("FUSCZ.AML.IncomeLevelRegularCode")]
        public StringClientPartner IncomeLevelRegularCode { get; set; }

        //[Resource("FUSCZ.AML.IncomeLevelIrregularCode")]
        public StringClientPartner IncomeLevelIrregularCode { get; set; }

        public StringClientPartner SourceIncomeLevelRegularCode { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelRegularD")]
        public BoolClientPartner SourceIncomeLevelRegularD { get; set; }

        public StringClientPartner SourceIncomeLevelRegularOthers { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelIrregularA")]
        public BoolClientPartner SourceIncomeLevelIrregularA { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelIrregularB")]
        public BoolClientPartner SourceIncomeLevelIrregularB { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelIrregularC")]
        public BoolClientPartner SourceIncomeLevelIrregularC { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelIrregularD")]
        public BoolClientPartner SourceIncomeLevelIrregularD { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelIrregularE")]
        public BoolClientPartner SourceIncomeLevelIrregularE { get; set; }

        //[Resource("FUSCZ.AML.SourceIncomeLevelIrregularF")]
        public BoolClientPartner SourceIncomeLevelIrregularF { get; set; }

        public StringClientPartner SourceIncomeLevelIrregularOthers { get; set; }

        //[Resource("FUSCZ.AML.OtherRisks")]
        public string OtherRisks { get; set; }

        //[Resource("FUSCZ.AML.RiskUSA")]
        public bool RiskUSA { get; set; }
        public bool HasRiskUSA { get; set; }
        //[Resource("FUSCZ.AML.RiskSouthAmerica")]
        public bool RiskSouthAmerica { get; set; }
        public bool HasRiskSouthAmerica { get; set; }
        //[Resource("FUSCZ.AML.RiskSSSR")]
        public bool RiskSSSR { get; set; }
        public bool HasRiskSSSR { get; set; }
        //[Resource("FUSCZ.AML.RiskPacific")]
        public bool RiskPacific { get; set; }
        public bool HasRiskPacific { get; set; }
        //[Resource("FUSCZ.AML.RiskAsia")]
        public bool RiskAsia { get; set; }
        public bool HasRiskAsia { get; set; }
        //[Resource("FUSCZ.AML.RiskEurope")]
        public bool RiskEurope { get; set; }
        public bool HasRiskEurope { get; set; }

        //[Resource("FUSCZ.AML.VatParadiseA")]
        public bool VatParadiseA { get; set; }
        public bool HasVatParadiseA { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseB")]
        public bool VatParadiseB { get; set; }
        public bool HasVatParadiseB { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseC")]
        public bool VatParadiseC { get; set; }
        public bool HasVatParadiseC { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseF")]
        public bool VatParadiseF { get; set; }
        public bool HasVatParadiseF { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseG")]
        public bool VatParadiseG { get; set; }
        public bool HasVatParadiseG { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseH")]
        public bool VatParadiseH { get; set; }
        public bool HasVatParadiseH { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseK")]
        public bool VatParadiseK { get; set; }
        public bool HasVatParadiseK { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseL")]
        public bool VatParadiseL { get; set; }
        public bool HasVatParadiseL { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseM")]
        public bool VatParadiseM { get; set; }
        public bool HasVatParadiseM { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseN")]
        public bool VatParadiseN { get; set; }
        public bool HasVatParadiseN { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseO")]
        public bool VatParadiseO { get; set; }
        public bool HasVatParadiseO { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseP")]
        public bool VatParadiseP { get; set; }
        public bool HasVatParadiseP { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseS")]
        public bool VatParadiseS { get; set; }
        public bool HasVatParadiseS { get; set; }
        //[Resource("FUSCZ.AML.VatParadiseT")]
        public bool VatParadiseT { get; set; }
        public bool HasVatParadiseT { get; set; }

        //[Resource("FUSCZ.AML.SignedDate")]
        public DateTime? SignedDate { get; set; }

        //[Resource("FUSCZ.AML.Place")]
        public string Place { get; set; }

        public int PlaceID { get; set; }

        public int? FUSID { get; set; }



        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    var aml = (FormAML)validationContext.ObjectInstance;

        //    var errorItems = ValidateModel(aml);

        //    foreach (var item in errorItems.Where(p => p != null).DistinctBy(p => p.ErrorMessage))
        //    {
        //        yield return item;
        //    }

        //    yield return null;
        //}

        //public void ControlLiveRisk(FormFUSCZ FormFUSCZ)
        //{
        //    var amlRisk = new AMLRisks();

        //    HasClientRiskCountry = amlRisk.IsRiskCountry(FormFUSCZ.Client.CountryCode);
        //    HasPartnerRiskCountry = FormFUSCZ.FUSForPartner && amlRisk.IsRiskCountry(FormFUSCZ.Partner.CountryCode);

        //    if (HasClientRiskCountry || HasPartnerRiskCountry)
        //    {
        //        Q4LiveRisk = new BoolClientPartner() { Client = HasClientRiskCountry, Partner = HasPartnerRiskCountry };
        //        var clientRiskCountry = amlRisk.GetRiskCountry(FormFUSCZ.Client.CountryCode);
        //        var partnerRiskCountry = FormFUSCZ.FUSForPartner ? amlRisk.GetRiskCountry(FormFUSCZ.Partner.CountryCode) : null;

        //        if (clientRiskCountry == null)
        //            clientRiskCountry = new BL.FinData.Core.Customer.RiskCountry();
        //        if (partnerRiskCountry == null)
        //            partnerRiskCountry = new BL.FinData.Core.Customer.RiskCountry();

        //        HasRiskEurope = clientRiskCountry.RiskCategory == Constants.AMLRiskCategories.USA || partnerRiskCountry.RiskCategory == Constants.AMLRiskCategories.USA;
        //        HasRiskSouthAmerica = clientRiskCountry.RiskCategory == Constants.AMLRiskCategories.SouthAmerica || partnerRiskCountry.RiskCategory == Constants.AMLRiskCategories.SouthAmerica;
        //        HasRiskSSSR = clientRiskCountry.RiskCategory == Constants.AMLRiskCategories.SSSR || partnerRiskCountry.RiskCategory == Constants.AMLRiskCategories.SSSR;
        //        HasRiskPacific = clientRiskCountry.RiskCategory == Constants.AMLRiskCategories.Pacific || partnerRiskCountry.RiskCategory == Constants.AMLRiskCategories.Pacific;
        //        HasRiskAsia = clientRiskCountry.RiskCategory == Constants.AMLRiskCategories.Asia || partnerRiskCountry.RiskCategory == Constants.AMLRiskCategories.Asia;
        //        HasRiskEurope = clientRiskCountry.RiskCategory == Constants.AMLRiskCategories.Europe || partnerRiskCountry.RiskCategory == Constants.AMLRiskCategories.Europe;

        //        SetUpRisks();

        //        HasVatParadiseA = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseA || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseA;
        //        HasVatParadiseB = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseB || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseB;
        //        HasVatParadiseC = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseC || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseC;
        //        HasVatParadiseF = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseF || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseF;
        //        HasVatParadiseG = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseG || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseG;
        //        HasVatParadiseH = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseH || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseH;
        //        HasVatParadiseK = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseK || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseK;
        //        HasVatParadiseL = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseL || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseL;
        //        HasVatParadiseM = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseM || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseM;
        //        HasVatParadiseN = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseN || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseN;
        //        HasVatParadiseO = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseO || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseO;
        //        HasVatParadiseO = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseO || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseO;
        //        HasVatParadiseS = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseS || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseS;
        //        HasVatParadiseT = clientRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseT || partnerRiskCountry.TaxHavenCategory == Constants.AMLVATHavenCategories.VatParadiseT;

        //        SetUpParadises();
        //    }
        //}

        //public void ControlExponed(FormFUSCZ FormFUSCZ)
        //{
        //    if (FormFUSCZ.Announcement == null) return;

        //    var exponed = FormFUSCZ.Announcement.VoD(x => x.AnnouncementExpone);

        //    if (!exponed.HasValue || !exponed.Value) return;

        //    Q2Exponed = new BoolClientPartner() { Client = true, Partner = FormFUSCZ.FUSForPartner };

        //}

        private void SetUpRisks()
        {
            RiskEurope = HasRiskEurope ? true : RiskEurope;
            RiskSouthAmerica = HasRiskSouthAmerica ? true : RiskSouthAmerica;
            RiskSSSR = HasRiskSSSR ? true : RiskSSSR;
            RiskPacific = HasRiskPacific ? true : RiskPacific;
            RiskAsia = HasRiskAsia ? true : RiskAsia;
            RiskEurope = HasRiskEurope ? true : RiskEurope;
        }

        private void SetUpParadises()
        {
            VatParadiseA = HasVatParadiseA ? true : VatParadiseA;
            VatParadiseB = HasVatParadiseB ? true : VatParadiseB;
            VatParadiseC = HasVatParadiseC ? true : VatParadiseC;
            VatParadiseF = HasVatParadiseF ? true : VatParadiseF;
            VatParadiseG = HasVatParadiseG ? true : VatParadiseG;
            VatParadiseH = HasVatParadiseH ? true : VatParadiseH;
            VatParadiseK = HasVatParadiseK ? true : VatParadiseK;
            VatParadiseL = HasVatParadiseL ? true : VatParadiseL;
            VatParadiseM = HasVatParadiseM ? true : VatParadiseM;
            VatParadiseN = HasVatParadiseN ? true : VatParadiseN;
            VatParadiseO = HasVatParadiseO ? true : VatParadiseO;
            VatParadiseP = HasVatParadiseP ? true : VatParadiseP;
            VatParadiseS = HasVatParadiseS ? true : VatParadiseS;
            VatParadiseT = HasVatParadiseT ? true : VatParadiseT;

        }

        //public static IEnumerable<ValidationResult> ValidateModel(FormAML aml)
        //{
        //    StandardFUSCZModel dbModel = new StandardFUSCZModel();
        //    if (aml.FUSID.HasValue)
        //        dbModel.LoadModel(aml.FUSID);

        //    if (!dbModel.IsSmsType && dbModel.FormFUSCZ.ValueOrDefault(q => q.FinalStatementSignatures.ValueOrDefault(p => p.DateFUSSigned)) > aml.SignedDate)
        //    {
        //        yield return new ValidationResult(GetErrorResource("SignedDateLowerThanFUS"), new[] { "SignedDate" });
        //    }


        //    bool forClient = dbModel.IsAmlForClient;
        //    bool forPartner = dbModel.IsAmlForPartner;

        //    if (forClient && aml.Q3SelectProduct != null && (aml.Q3SelectProduct.Client || aml.Q3SelectProduct.Partner) && string.IsNullOrEmpty(aml.Q3DeclarePurpose))
        //        yield return new ValidationResult(GetErrorResource("Q3DeclarePurpose"), new[] { "Q3DeclarePurpose" });

        //    if (forClient && aml.Q3SelectProduct != null && (aml.Q3SelectProduct.Client || aml.Q3SelectProduct.Partner) && string.IsNullOrEmpty(aml.Q3ActualPurpose))
        //        yield return new ValidationResult(GetErrorResource("Q3ActualPurpose"), new[] { "Q3ActualPurpose" });

        //    // 2015-05-06 - DT - validace sekce A jen pokud je klient nebo partner právnická osoba
        //    if (forClient && dbModel.ClientType() == SealedConstants.PersonType.Corporation ||
        //        forPartner && dbModel.PartnerType() == SealedConstants.PersonType.Corporation)
        //    {
        //        if (string.IsNullOrWhiteSpace(aml.ActualFinancialOwner))
        //            yield return new ValidationResult(GetErrorResource("ActualFinancialOwner"), new[] { "ActualFinancialOwner" });

        //        else if (aml.ActualFinancialOwner.Length > 200)
        //            yield return new ValidationResult(GetErrorResource("ActualFinancialOwner.MaxLength"), new[] { "ActualFinancialOwner" });
        //    }

        //    if (string.IsNullOrWhiteSpace(aml.Details))
        //        yield return new ValidationResult(GetErrorResource("Details"), new[] { "Details" });
        //    else if (aml.Details.Length > 400)
        //        yield return new ValidationResult(GetErrorResource("Details.MaxLength"), new[] { "Details" });

        //    bool eRiskValid = true;
        //    bool eVatValid = true;

        //    bool eRisk = aml.Q4LiveRisk.LogicOR || aml.Q5HQRisk.LogicOR || aml.Q8HQBusinessPartner.LogicOR;

        //    if (eRisk && !aml.RiskUSA && !aml.RiskSouthAmerica && !aml.RiskSSSR && !aml.RiskPacific && !aml.RiskAsia && !aml.RiskEurope)
        //        eRiskValid = false;
        //    //yield return new ValidationResult(GetErrorResource("Risk.ChooseLeastOne"), new[] { "RiskUSA" });

        //    bool eVat = aml.Q4LiveRisk.LogicOR || aml.Q5HQRisk.LogicOR || aml.Q8HQBusinessPartner.LogicOR;

        //    if (eVat &&
        //        !aml.VatParadiseA &&
        //        !aml.VatParadiseB &&
        //        !aml.VatParadiseC &&
        //        !aml.VatParadiseF &&
        //        !aml.VatParadiseG &&
        //        !aml.VatParadiseH &&
        //        !aml.VatParadiseK &&
        //        !aml.VatParadiseL &&
        //        !aml.VatParadiseM &&
        //        !aml.VatParadiseN &&
        //        !aml.VatParadiseO &&
        //        !aml.VatParadiseP &&
        //        !aml.VatParadiseS &&
        //        !aml.VatParadiseT)
        //        eVatValid = false;
        //    //yield return new ValidationResult(GetErrorResource("Vat.ChooseLeastOne"), new[] { "VatParadiseA" });

        //    if (!eVatValid && !eRiskValid)
        //        yield return new ValidationResult(GetErrorResource("Vat.Risk.ChooseLeastOne"), new[] { "RiskUSA", "VatParadiseA" });

        //    //---------------------------------------------------------
        //    bool eIncomeClient = aml.Q1TradeOver.Client || aml.Q10Source.Client || aml.Q12Unusual.Client || aml.Q13UnusualFC.Client;

        //    bool clientRegularIncome = !(aml.IncomeLevelRegularCode == null || string.IsNullOrEmpty(aml.IncomeLevelRegularCode.Client));
        //    bool clientIrregularIncome = !(aml.IncomeLevelIrregularCode == null || string.IsNullOrEmpty(aml.IncomeLevelIrregularCode.Client));

        //    if (eIncomeClient && (!clientRegularIncome && !clientIrregularIncome))
        //        yield return new ValidationResult(GetErrorResource("Income.Client"), new[] { "IncomeLevelCode" });


        //    var clientSourceIncomeLevelRegularOthers = aml.SourceIncomeLevelRegularD.VoD(x => x.Client) && string.IsNullOrEmpty(aml.SourceIncomeLevelRegularOthers.VoD(x => x.Client));

        //    if (eIncomeClient
        //        && clientRegularIncome
        //        && string.IsNullOrEmpty(aml.SourceIncomeLevelRegularCode.VoD(x => x.Client))
        //        && (!aml.SourceIncomeLevelRegularD.VoD(x => x.Client)
        //        || clientSourceIncomeLevelRegularOthers))
        //        //            (
        //        //((aml.SourceIncomeLevelRegularD != null && aml.SourceIncomeLevelRegularD.Client) && string.IsNullOrEmpty(aml.SourceIncomeLevelRegularOthers.Client)))
        //        //)
        //        yield return new ValidationResult(GetErrorResource("Income.Client.Regular.Source"), new[] { "IncomeRegularSource" });

        //    if (eIncomeClient && clientRegularIncome && clientSourceIncomeLevelRegularOthers)
        //        yield return new ValidationResult(GetErrorResource("Income.Client.Regular.Source.Other"), new[] { "IncomeRegularSource" });

        //    bool cA = aml.SourceIncomeLevelIrregularA == null ? false : aml.SourceIncomeLevelIrregularA.Client;
        //    bool cB = aml.SourceIncomeLevelIrregularB == null ? false : aml.SourceIncomeLevelIrregularB.Client;
        //    bool cC = aml.SourceIncomeLevelIrregularC == null ? false : aml.SourceIncomeLevelIrregularC.Client;
        //    bool cD = aml.SourceIncomeLevelIrregularD == null ? false : aml.SourceIncomeLevelIrregularD.Client;
        //    bool cE = aml.SourceIncomeLevelIrregularE == null ? false : aml.SourceIncomeLevelIrregularE.Client;
        //    bool cF = aml.SourceIncomeLevelIrregularF == null ? false : aml.SourceIncomeLevelIrregularF.Client;

        //    bool irregularLevelClient = cA || cB || cC || cD || cE || cF;

        //    if (eIncomeClient && clientIrregularIncome && !irregularLevelClient)
        //        yield return new ValidationResult(GetErrorResource("Income.Client.Irregular"), new[] { "IncomeIrregularSource" });

        //    if (aml.SourceIncomeLevelIrregularF != null && (aml.SourceIncomeLevelIrregularF.Client && string.IsNullOrEmpty(aml.SourceIncomeLevelIrregularOthers.Client)))
        //        yield return new ValidationResult(GetErrorResource("Income.Client.IrregularOther"), new[] { "IncomeIrregularSource" });

        //    //---------------------------------------------------------
        //    bool eIncomePartner = aml.Q1TradeOver.Partner || aml.Q10Source.Partner || aml.Q12Unusual.Partner || aml.Q13UnusualFC.Partner;

        //    bool partnerRegularIncome = !(aml.IncomeLevelRegularCode == null || string.IsNullOrEmpty(aml.IncomeLevelRegularCode.Partner));
        //    bool partnerIrregularIncome = !(aml.IncomeLevelIrregularCode == null || string.IsNullOrEmpty(aml.IncomeLevelIrregularCode.Partner));

        //    if (eIncomePartner && (!partnerRegularIncome && !partnerIrregularIncome))
        //        yield return new ValidationResult(GetErrorResource("Income.Client"), new[] { "IncomeLevelCode" });

        //    var partnerSourceIncomeLevelRegularOthers = aml.SourceIncomeLevelRegularD.VoD(x => x.Partner) && string.IsNullOrEmpty(aml.SourceIncomeLevelRegularOthers.VoD(x => x.Partner));

        //    if (eIncomePartner
        //        && partnerRegularIncome
        //        && string.IsNullOrEmpty(aml.SourceIncomeLevelRegularCode.VoD(x => x.Partner))
        //        && (!aml.SourceIncomeLevelRegularD.VoD(x => x.Partner)
        //        || partnerSourceIncomeLevelRegularOthers))
        //        //            if (eIncomePartner && partnerRegularIncome &&
        //        //((aml.SourceIncomeLevelRegularCode == null || string.IsNullOrEmpty(aml.SourceIncomeLevelRegularCode.Partner)) &&
        //        //((aml.SourceIncomeLevelRegularD != null && aml.SourceIncomeLevelRegularD.Partner) && string.IsNullOrEmpty(aml.SourceIncomeLevelRegularOthers.Partner)))
        //        //)
        //        yield return new ValidationResult(GetErrorResource("Income.Partner.Regular.Source"), new[] { "IncomeRegularSource" });

        //    if (eIncomePartner && partnerRegularIncome && partnerSourceIncomeLevelRegularOthers)
        //        yield return new ValidationResult(GetErrorResource("Income.Partner.Regular.Source.Other"), new[] { "IncomeRegularSource" });

        //    bool pA = aml.SourceIncomeLevelIrregularA == null ? false : aml.SourceIncomeLevelIrregularA.Partner;
        //    bool pB = aml.SourceIncomeLevelIrregularB == null ? false : aml.SourceIncomeLevelIrregularB.Partner;
        //    bool pC = aml.SourceIncomeLevelIrregularC == null ? false : aml.SourceIncomeLevelIrregularC.Partner;
        //    bool pD = aml.SourceIncomeLevelIrregularD == null ? false : aml.SourceIncomeLevelIrregularD.Partner;
        //    bool pE = aml.SourceIncomeLevelIrregularE == null ? false : aml.SourceIncomeLevelIrregularE.Partner;
        //    bool pF = aml.SourceIncomeLevelIrregularF == null ? false : aml.SourceIncomeLevelIrregularF.Partner;

        //    bool irregularLevelPartner = pA || pB || pC || pD || pE || pF;

        //    if (eIncomePartner && partnerIrregularIncome && !irregularLevelPartner)
        //        yield return new ValidationResult(GetErrorResource("Income.Partner.Irregular"), new[] { "IncomeIrregularSource" });

        //    if (aml.SourceIncomeLevelIrregularF != null && (aml.SourceIncomeLevelIrregularF.Partner && string.IsNullOrEmpty(aml.SourceIncomeLevelIrregularOthers.Partner)))
        //        yield return new ValidationResult(GetErrorResource("Income.Partner.IrregularOther"), new[] { "IncomeIrregularSource" });

        //    if (aml.SignedDate == null)
        //    {
        //        yield return new ValidationResult(GetErrorResource("SignedDate"), new[] { "SignedDate" });
        //    }
        //    else if (dbModel.IsSmsType && aml.SignedDate.Value.Date < dbModel.FormFUSCZ.Client.FirstMeetingDate.Value.Date)
        //    {
        //        yield return new ValidationResult(GetErrorResource("SignedDate.FirstMeetingDate"), new[] { "SignedDate" });
        //    }

        //    if (string.IsNullOrWhiteSpace(aml.Place))
        //        yield return new ValidationResult(GetErrorResource("Place"), new[] { "Place" });
        //}
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //public static string GetErrorResource(string value, string validType = null)
        //{
        //    return ResourceManager.GetResourceRaw(string.Format("Web.Model.FUSCZ.AML.{0}.Required.{1}", value, (validType ?? Constants.ValidateType.Error)));
        //}
    }
}