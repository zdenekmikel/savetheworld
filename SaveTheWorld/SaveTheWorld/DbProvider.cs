﻿using Dapper;
using SaveTheWorld.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SaveTheWorld
{
    public static class DbProvider
    {
        public static FUS CustomerFUS(int userID, int agentID, int fusID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<FUS>("Customer.ProcFUS", new { userID, fusID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }
        public static List<Service> Services(string searchString = null)
        {
            using (var db = CreateConnection())
            {
                return db.Query<Service>("[Biz].[ProcServiceListAll]", new {}, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<Models.Attribute> Attributes(string searchString = null)
        {
            using (var db = CreateConnection())
            {
                return db.Query<Models.Attribute>("[Com].[ProcAttributes]", new { }, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public static List<Category> Categories(int userID = 0, string categoryTypeCode = null, string name = null, bool? active = null, int? parentCategoryID = null)
        {
            using (var db = CreateConnection())
            {
                return db.Query<Category>("[Com].[ProcCategories]", new {
                    CategoryTypeCode = categoryTypeCode,
                    Name = name,
                    ParentCategoryID = parentCategoryID,
                    Active = active,
                    UserID = userID
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public static FUSAML CustomerFUSAML(int userID, int agentID, int fusID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<FUSAML>("Customer.ProcFUSAML", new { userID, fusID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }
        public static List<PartnerSearch> Partners(int userID = 0, string name = null, int? partnerTypeID = null, int? valid = 0)
        {
            using (var db = CreateConnection())
            {
                return db.Query<PartnerSearch>("[Biz].[ProcPartners]", new
                {
                    UserID = userID,
                    Name = name,
                    PartnerTypeID = partnerTypeID,
                    Valid = valid
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static ClientDetail ClientDetail(int userID, int clientID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<ClientDetail>("[Customer].[ProcClientDetail]", new
                {
                    UserID = userID,
                    ClientID = clientID
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public static List<PartyAddress> PartyAddresses(int partyID, int userID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<PartyAddress>("[Com].[ProcPartyAddresses]", new
                {
                    UserID = userID,
                    PartyID = partyID
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<ClientAttribute> ClientAttributes(int clientID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<ClientAttribute>("[Customer].[ProcClientAttributes]", new
                {
                    ClientID = clientID
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<PartyContact> PartyContacts(int partyID, int userID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<PartyContact>("[Com].[ProcPartyContacts]", new
                {
                    PartyID = partyID,
                    UserID = userID
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }
        public static int? ClientIDByPartyID(int userId, int partyID)
        {
            using (var db = CreateConnection())
            {
                return db.Query<int?>("[Customer].[ProcClientIDByPartyID]", new
                {
                    PartyID = partyID,
                    UserID = userId
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public static List<PartyRelationshipDetail> PartyRelationships(int partyId, int userId, string relatedPartyTypeCode)
        {
            using (var db = CreateConnection())
            {
                return db.Query<PartyRelationshipDetail>("[Com].[ProcPartyRelationships]", new
                {
                    PartyID = partyId,
                    UserID = userId,
                    RelatedPartyTypeCode = relatedPartyTypeCode
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<Resource> GetResources(string cultureCode = "cs", int? applicationID = null)
        {
            using (var db = CreateConnection())
            {
                return db.Query<Resource>("[App].[ProcResources]", new
                {
                    CultureCode = cultureCode,
                    ApplicationID = applicationID
                }, commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static List<ProcCIClientsAutocomplete_Result> FUSClientsSearchWeb(int userID, int agentID, string partyTypeCode, string partyCode, string partyName, bool orderByPartyCode)
        {
            using (var db = CreateConnection())
            {
                return db.Query<ProcCIClientsAutocomplete_Result>("Customer.ProcClientsFUSAutocomplete", new { userID, agentID, partyTypeCode, partyName, partyCode, orderByPartyCode }, commandType: CommandType.StoredProcedure, commandTimeout: 100).ToList();
            }
        }
        

        public static DocumentFileID DocumentFileInsert(DocumentFileInfo file)
        {
            if (string.IsNullOrWhiteSpace(file.documentFileTypeCode)) throw new ArgumentException("DocumentFileTypeCode cant be empty");

            int chunkSize = 2048;
            byte[] buffer = new byte[chunkSize];
            byte[] fil;
            using (System.IO.MemoryStream writeStream = new System.IO.MemoryStream())
            {
                do
                {
                    // read bytes from input stream
                    int bytesRead = file.FileByteStream.Read(buffer, 0, chunkSize);
                    //int bytesRead = file.FileByteStream.Read(
                    if (bytesRead == 0) break;
                    //fil += buffer;
                    writeStream.Write(buffer, 0, bytesRead);
                } while (true);
                fil = writeStream.ToArray();
            }
            DocumentFileID did = new DocumentFileID();
            using (var con = CreateDAMConnection())
            {
                did.documentFileID = con.Query<Guid>("dbo.ProcDocumentFileInsert",
                    new
                    {
                        DocumentID = file.documentID,
                        Index = file.index,
                        Size = file.size,
                        ContentType = file.contentType,
                        DocumentType = file.documentType,
                        Note = file.note,
                        File = fil,
                        UserID = file.userID,
                        DocumentFileTypeCode = file.documentFileTypeCode
                    }, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            return did;              
        }
        
        public static List<FUSListOptions> FUSListOptions()
        {
            using (var db = CreateConnection())
            {
                return db.Query<FUSListOptions>("Customer.ProcFUSListOptions", commandType: CommandType.StoredProcedure).ToList();
            }
        }

        public static void DocumentRelationInsert(int userID, int documentID, int objectID, string documentRelationTypeCode)
        {
            using (var con = CreateDAMConnection())
            {
                con.Execute("dbo.ProcDocumentRelationsInsert",
                    new
                    {
                        UserID = userID,
                        DocumentID = documentID,
                        ObjectID = objectID,
                        DocumentRelationTypeCode = documentRelationTypeCode
                    }, commandType: CommandType.StoredProcedure);
            }
        }

        public static SqlConnection CreateConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
        }

        public static SqlConnection CreateDAMConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["DAM"].ConnectionString);
        }
    }

    public class Service
    {
        public int CategoryID { get; set; }
        public int ServiceID { get; set; }
        public string AltName { get; set; }
    }

    public class Category
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }
    }

    public class PartnerSearch
    {
        public int PartnerID { get; set; }
        public string AltName { get; set; }
        public string PartyName { get; set; }
    }

    public class Resource
    {
        public string ResourceCode { get; set; }
        public string Value { get; set; }
    }
    public class ProcServiceSectors_Result
    {
        public int SectorID { get; set; }
        public bool Value { get; set; }
    }

    public class ProcCategorySectors_Result
    {
        public int SectorID { get; set; }
    }

    public class FUSListOptions
    {
        public String FUSListOptionCode { get; set; }

        public Boolean Active { get; set; }

        public String Name { get; set; }

        public Int32? Rank { get; set; }

        public String ResourceCode { get; set; }

        public Int32 FUSListTypeID { get; set; }

        public int? Value { get; set; }

        public string FUSListTypeCode { get; set; }
    }

    public class DocumentFileID
    {
        public int? userID;
        
        public Guid? documentFileID;
    }
}
