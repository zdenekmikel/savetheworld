﻿using SaveTheWorld.Models;
using SaveTheWorld.Models.PDF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static SaveTheWorld.Constants;

namespace SaveTheWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            List<FUSInfo> values = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "Content\\FUSES.csv")
                                           .Skip(1)
                                           .Select(v => FUSInfo.FromCsv(v))
                                           .ToList();
            
            for (int i = 0; i < values.Count; i++)//values.Count;
            {
                var selectedFus = values[i];
                //selectedFus = new FUSInfo()
                //{
                //    FUSID = 220013491,
                //    DocumentID = 12913065,
                //    DocumentTypeID = 2,
                //    Version = 160301
                //};
                //selectedFus = new FUSInfo()
                //{
                //    FUSID = 220013491,
                //    DocumentID = 12913065,
                //    DocumentTypeID = 2,
                //    Version = 160301
                //};
                if (selectedFus.DocumentTypeID == DocumentTypes.FUS)
                {
                    Console.WriteLine($"Generate eFUS {selectedFus.FUSID}");
                    StandardFUSCZModel dbModel = new StandardFUSCZModel();
                    try
                    {
                        int? createdBy = null;
                        dbModel.LoadModel(selectedFus.FUSID, out createdBy, true);
                        byte[] pdfFuz = dbModel.GetFusPDF(selectedFus.Version);
                        //File.WriteAllBytes($"FUS_{selectedFus.FUSID}.pdf", pdfFuz);
                        StandardFUSCZModel.SaveFUZDocumentToDb(dbModel.FUSID.Value, pdfFuz, selectedFus.DocumentID, createdBy.Value);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Failed to generate eFUS {selectedFus.FUSID}. Message: {e.Message}");
                        Log($"Failed to generate eFUS {selectedFus.FUSID}. Message: {e.Message}");
                    }
                }
                else
                {
                    try
                    {
                        Console.WriteLine($"Generate AML {selectedFus.FUSID}");
                        int? createdBy = null;
                        var pdfAML = new PDFAMLDynamic(selectedFus.FUSID, out createdBy);
                        pdfAML.GeneratePdf(selectedFus.Version.ToString());
                        //File.WriteAllBytes($"FUS_AML_{selectedFus.FUSID}.pdf", pdfAML.GetPdf());

                        AMLModel.SaveAMLDocumentDoDb(selectedFus.FUSID, pdfAML.GetPdf(), selectedFus.DocumentID, createdBy.Value);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Failed to generate AML {selectedFus.FUSID}. Message: {e.Message}");
                        Log($"Failed to generate AML {selectedFus.FUSID}. Message: {e.Message}");
                    }

                }
                
            }
        }

        private static void Log(string message)
        {
            File.AppendAllText("eFUS.txt", message + Environment.NewLine);
        }

        public class FUSInfo
        {
            public int DocumentID { get; set; }
            public DateTime? DateCreated { get; set; }
            public int? DocumentTypeID { get; set; }
            public string Name { get; set; }
            public int FUSID { get; set; }
            public string FUSTypeCode { get; set; }
            public int FUSStatusID { get; set; }
            public int Version { get; set; }
            public static FUSInfo FromCsv(string csvLine)
            {
                string[] values = csvLine.Split(';');
                FUSInfo fus = new FUSInfo();
                fus.DocumentID = Convert.ToInt32(values[0]);
                fus.DateCreated = Convert.ToDateTime(values[1]);
                fus.DocumentTypeID = Convert.ToInt32(values[2]);
                fus.Name = Convert.ToString(values[3]);
                fus.FUSID = Convert.ToInt32(values[4]);
                fus.FUSTypeCode = Convert.ToString(values[5]);
                fus.FUSStatusID = Convert.ToInt32(values[6]);
                fus.Version = Convert.ToInt32(values[7]);
                return fus;
            }
        }
    }
}
